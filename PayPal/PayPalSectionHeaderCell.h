//
//  PayPalSectionHeaderCell.h
//  
//
//  Created by Rahul Sharma on 02/01/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PayPalSectionHeaderCell : UITableViewCell
@property (weak, nonatomic) IBOutlet AlignmentOfLabel *exampleLabel;

@end
