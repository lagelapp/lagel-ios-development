//
//  CurrencySelectVC.m
//  ADCurrencyPicker
//
//  Created by Adnan on 12/10/15.
//  Copyright © 2015 TheGoal. All rights reserved.
//

#import "CurrencySelectVC.h"

@interface CurrencySelectVC ()<UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate>
{
      NSMutableArray *searchobjectArray;
      BOOL isFiltered;
}
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (retain, nonatomic) NSMutableArray *countryList;
@property (nonatomic, assign) NSInteger selectedIndex ;
@property (nonatomic)NSIndexPath *previosIndexPath ;

@end

@implementation CurrencySelectVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
//If you did not specify delegate and data source in story board,uncomment below lines
//    [self.tableView setDelegate:self];
//    [self.tableView setDataSource:self];
    isFiltered = NO;
    self.selectedIndex = -1;
    [self setUpNavigationBar];
    [self loadCountries];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Custum methods

-(void) setUpNavigationBar {
    
    UIButton *navLeft = [CommonMethods createNavButtonsWithselectedState:mNavigationBackButtonImageName     normalState:mNavigationBackButtonImageName];
    UIBarButtonItem *navLeftButton = [[UIBarButtonItem alloc]initWithCustomView:navLeft];
    [navLeft setFrame:CGRectMake(0,0,20,20)];
    self.navigationItem.leftBarButtonItem = navLeftButton;
    [navLeft addTarget:self action:@selector(navLeftButton) forControlEvents:UIControlEventTouchUpInside];
    [self.navigationItem setLeftBarButtonItems:@[[CommonMethods getNegativeSpacer],navLeftButton]];
    
    UIButton *rightBtn= [UIButton buttonWithType:UIButtonTypeCustom];
    [rightBtn addTarget:self action:@selector(doneBtnAction:) forControlEvents:UIControlEventTouchDown];
    rightBtn.frame=CGRectMake(0, 3.0, 60.0, 30.0);
    [rightBtn setTitle:@"Done" forState:UIControlStateNormal];
    [rightBtn setTitleColor:mBaseColor forState:UIControlStateNormal];
    
    UIBarButtonItem *rightbarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:rightBtn];
    self.navigationItem.rightBarButtonItem = rightbarButtonItem;
}

-(void)navLeftButton
{
    [self.navigationController popViewControllerAnimated:YES];
}


-(void) loadCountries{
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"currency" ofType:@"plist"];
    self.countryList =[NSMutableArray arrayWithContentsOfFile:filePath];
    [self.tableView reloadData];
}

-(void) doneBtnAction:(UIButton *) sender{
    
    if (self.selectedIndex !=-1) {
        
        if(self.delegate && [self.delegate respondsToSelector:@selector(country:didChangeValue:)]){
            if(isFiltered )
                [self.delegate country:self didChangeValue:[searchobjectArray objectAtIndex:self.selectedIndex]];
            else
                [self.delegate country:self didChangeValue:[self.countryList objectAtIndex:self.selectedIndex]];
        }
        
    }
    [self.navigationController popViewControllerAnimated:YES];
}

-(void) backbuttonAction:(id) sender{
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark - searchView Delegate

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{

    NSString *nameString = searchText;
    searchobjectArray = [NSMutableArray array];
    for(NSDictionary *wine in self.countryList)
    {
        
        NSString *countryName = [wine objectForKey:COUNTRY_NAME];
       
        
       
        
        
            if ([countryName rangeOfString:nameString options:NSCaseInsensitiveSearch].location != NSNotFound ) {
                
                [searchobjectArray addObject:wine];
                
            }
        
    }
    
    
    
    if([nameString length] == 0)
    {
        isFiltered = NO;
    }
    else{
        isFiltered = YES;
    }
     self.selectedIndex =-1;
    [_tableViewOutlet reloadData];
    
    
}

#pragma mark - TableView DataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 40;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(isFiltered)
        return searchobjectArray.count;
    else
        return self.countryList.count;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
  UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 40)];
    view.backgroundColor =[UIColor whiteColor];
    /* Create custom view to display section header... */
    //creating user name label
    UILabel *UserNamelabel = [[UILabel alloc] init];
    UserNamelabel.text =@"Country Name";
    [UserNamelabel setFont:[UIFont boldSystemFontOfSize:14]];
    UserNamelabel.textColor =[UIColor lightGrayColor];
    UserNamelabel.frame=CGRectMake(15, 0,150,view.frame.size.height);
    UserNamelabel.textAlignment = NSTextAlignmentLeft;
    [view addSubview:UserNamelabel];
    UILabel *currencyNamelabel = [[UILabel alloc] init];
    
    
    currencyNamelabel.text =@"Currency Name";
    [currencyNamelabel setFont:[UIFont boldSystemFontOfSize:13]];
    currencyNamelabel.textColor =[UIColor lightGrayColor];
    currencyNamelabel.frame=CGRectMake(self.view.frame.size.width - 110,0,115,view.frame.size.height);
    currencyNamelabel.textAlignment = NSTextAlignmentLeft;
    [view addSubview:currencyNamelabel];
    
    return view;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"countyCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    if(isFiltered)
    {
        NSDictionary *country = [searchobjectArray objectAtIndex:indexPath.row] ;
        
        UILabel *countryNameLabel = (UILabel *)[cell viewWithTag:100];
        countryNameLabel.text = [country objectForKey:COUNTRY_NAME];
        
        UIImageView *flagImageView = (UIImageView *)[cell viewWithTag:101];
        flagImageView.image = [UIImage imageNamed:[country objectForKey:COUNTRY_CODE]];
        flagImageView.contentMode = UIViewContentModeScaleAspectFit;
        flagImageView.clipsToBounds =YES;
        
        UILabel *currencysymbolLabel = (UILabel *)[cell viewWithTag:102];
        currencysymbolLabel.text = [NSString stringWithFormat:@"%@ %@",[country objectForKey:CURRENCY_CODE],[country objectForKey:CURRENCY_SYMBOL]];
        
        cell.accessoryType = UITableViewCellAccessoryNone;
        
        if (self.selectedIndex == indexPath.row)
            cell.accessoryType =UITableViewCellAccessoryCheckmark;
        
    }
    else{
    NSDictionary *country = [self.countryList objectAtIndex:indexPath.row] ;
        NSString *countryName = [country objectForKey:COUNTRY_NAME];
    
    UILabel *countryNameLabel = (UILabel *)[cell viewWithTag:100];
    countryNameLabel.text = [country objectForKey:COUNTRY_NAME];
    
    UIImageView *flagImageView = (UIImageView *)[cell viewWithTag:101];
    flagImageView.image = [UIImage imageNamed:[country objectForKey:COUNTRY_CODE]];
    flagImageView.contentMode = UIViewContentModeScaleAspectFit;
    flagImageView.clipsToBounds =YES;
    
    UILabel *currencysymbolLabel = (UILabel *)[cell viewWithTag:102];
    currencysymbolLabel.text = [NSString stringWithFormat:@"%@ %@",[country objectForKey:CURRENCY_CODE],[country objectForKey:CURRENCY_SYMBOL]];
        NSArray *currencyArray = [currencysymbolLabel.text componentsSeparatedByString:@" "];
        NSString *currencySbustring = currencyArray[0];
    if([self.previousSelection containsString:currencySbustring] || [countryName isEqualToString:@"All"])
    {
       cell.accessoryType = UITableViewCellAccessoryCheckmark;
        _previosIndexPath = indexPath   ;
    }
    else
    {
       cell.accessoryType = UITableViewCellAccessoryNone;
    }
   
    }
    return cell;
}

#pragma mark - TableView Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    self.selectedIndex =indexPath.row;
    [self.searchBarOutlet resignFirstResponder];
    
    [tableView cellForRowAtIndexPath:_previosIndexPath].accessoryType = UITableViewCellAccessoryNone;
    [tableView cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryCheckmark;
}
- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{
    self.selectedIndex =-1;
    [tableView cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryNone;
}


@end
