//
//  AskPermissionViewController.m

//
//  Created by Rahul Sharma on 03/07/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "AskPermissionViewController.h"

//#define TermsKey @"TermsAndCondition"

@interface AskPermissionViewController ()<CLLocationManagerDelegate,GetCurrentLocationDelegate >

@end

@implementation AskPermissionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    if(self.locationPermission)
    {
        self.titleOfPermission.text =  Localized(allowLocationTitle) ;
        
        self.messageBelowTitleLabel.text = Localized(allowNotificationMessage);
        
        [self.allowButtonOutlet setTitle:Localized(allowLocationButtonTitle) forState:UIControlStateNormal];
        
    }
    else
    {
        self.titleOfPermission.text = Localized(allowNotificationTitle) ;
        self.messageBelowTitleLabel.text = Localized(allowNotificationMessage);
        
        [self.allowButtonOutlet setTitle:Localized(allowNotificationButtonTitle) forState:UIControlStateNormal];
    }
    
    
    
}

//- (void)viewDidAppear:(BOOL)animated {
//    [super viewDidAppear:animated];
//    
//    [self checkTermsAndCondition];
//}
//
//- (void)checkTermsAndCondition {
//    BOOL isTermsAccepted = [[NSUserDefaults standardUserDefaults] boolForKey:TermsKey];
//    if (!isTermsAccepted) {
//        [self presentTermsAlert];
//    }
//}
//
//- (void)presentTermsAlert {
//    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:NSLocalizedString(@"To continue in the application", nil) preferredStyle:UIAlertControllerStyleAlert];
//    UIAlertAction *terms = [UIAlertAction actionWithTitle:NSLocalizedString(@"Terms & Conditions", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//        
//    }];
//    UIAlertAction *privacyPolicy = [UIAlertAction actionWithTitle:NSLocalizedString(@"Privacy Policy", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//        
//    }];
//    UIAlertAction *continueAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Continue", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//        
//    }];
//    [alertController addAction:terms];
//    [alertController addAction:privacyPolicy];
//    [alertController addAction:continueAction];
//    [self presentViewController:alertController animated:YES completion:nil];
//}

#pragma mark -
#pragma mark - Status Bar

-(BOOL)prefersStatusBarHidden
{
    return YES ;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)yesNotifyMeButtonAction:(id)sender {
    
    if(self.LocationPrivacy)
    {
        [self actionWhenLocationDisabled];
        
        return ;
    }
    
    
    if(!self.locationEnable)
    {
        UIAlertController *alertForLocation = [UIAlertController alertControllerWithTitle:Localized(allowLocationAlertTitle) message:Localized(allowLocationAlertMessage) preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *actionForOk = [UIAlertAction actionWithTitle:Localized(alertOk) style:UIAlertActionStyleDefault handler:^(UIAlertAction *Handle){
            [self.permissionDelegate allowPermission:NO];
            
        }];
        
        UIAlertAction *actionForsettings = [UIAlertAction actionWithTitle:Localized(allowLocationSettingsActionTitle) style:UIAlertActionStyleDefault handler:^(UIAlertAction *Handle){
            self.locationManager = [[CLLocationManager alloc] init];
            self.locationManager.delegate = self;
            self.locationManager.distanceFilter = kCLDistanceFilterNone;
            self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
            
        }];
        
        [alertForLocation addAction:actionForsettings];
        [alertForLocation addAction:actionForOk];
        [self presentViewController:alertForLocation animated:YES completion:nil];
    }
    
  else
  {
    
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    self.locationManager.distanceFilter = kCLDistanceFilterNone;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    if  ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)])
    {
        [self.locationManager requestWhenInUseAuthorization];
    }
  }

}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    if (status == kCLAuthorizationStatusDenied) {
        [self.permissionDelegate allowPermission:NO];
        [self dismissViewControllerAnimated:NO completion:nil];
    }
  if (status == kCLAuthorizationStatusAuthorizedWhenInUse) {
         [self.permissionDelegate allowPermission:YES];
      [self dismissViewControllerAnimated:NO completion:nil];
    }
}



- (IBAction)okayButtonAction:(id)sender {
    
    if(self.LocationPrivacy)
    {
        [self actionWhenLocationDisabled];
        
        return ;
    }
    
    if(!self.locationEnable)
    {
        UIAlertController *alertForLocation = [UIAlertController alertControllerWithTitle:Localized(allowLocationAlertTitle) message:Localized(allowLocationAlertMessage) preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *actionForOk = [UIAlertAction actionWithTitle:Localized(alertOk) style:UIAlertActionStyleDefault handler:^(UIAlertAction *Handle){
            [self.permissionDelegate allowPermission:NO];
            
        }];
        
        UIAlertAction *actionForsettings = [UIAlertAction actionWithTitle:Localized(allowLocationSettingsActionTitle) style:UIAlertActionStyleDefault handler:^(UIAlertAction *Handle){
            self.locationManager = [[CLLocationManager alloc] init];
            self.locationManager.delegate = self;
            self.locationManager.distanceFilter = kCLDistanceFilterNone;
            self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
            
        }];
        
        [alertForLocation addAction:actionForsettings];
        [alertForLocation addAction:actionForOk];
        [self presentViewController:alertForLocation animated:YES completion:nil];
    }
    
    else
    {
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    self.locationManager.distanceFilter = kCLDistanceFilterNone;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    if  ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)])
    {
        [self.locationManager requestWhenInUseAuthorization];
    }
  }
}

- (IBAction)cancelButtonAction:(id)sender {
    
 [self dismissViewControllerAnimated:NO completion:nil];
}

-(void)actionWhenLocationDisabled {
    UIAlertController *alertForLocation = [UIAlertController alertControllerWithTitle:Localized(allowLocationAlertTitle) message:NSLocalizedString(allowLocationAlertMessage, allowLocationAlertMessage) preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *actionForOk = [UIAlertAction actionWithTitle:NSLocalizedString(alertOk, alertOk) style:UIAlertActionStyleDefault handler:^(UIAlertAction *Handle){
        
        [self.permissionDelegate allowPermission:NO];
        [self dismissViewControllerAnimated:NO completion:nil];
        
    }];
    
    UIAlertAction *actionForsettings = [UIAlertAction actionWithTitle:NSLocalizedString(allowLocationSettingsActionTitle, allowLocationSettingsActionTitle) style:UIAlertActionStyleDefault handler:^(UIAlertAction *Handle){
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
    }];
    
    [alertForLocation addAction:actionForsettings];
    [alertForLocation addAction:actionForOk];
    [self presentViewController:alertForLocation animated:YES completion:nil];
}



@end
