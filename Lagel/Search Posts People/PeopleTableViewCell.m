//
//  PeopleTableViewCell.m

//
//  Created by Rahul Sharma on 7/25/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import "PeopleTableViewCell.h"

@implementation PeopleTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    [self layoutIfNeeded];
    self.profileImageViewOutlet.layer.cornerRadius = self.profileImageViewOutlet.frame.size.height/2;
    self.profileImageViewOutlet.clipsToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setValuesInCellWithUsername:(NSString *)username fullName :(NSString *)fullname andProfileImage:(NSString *)profileImageUrl
{
    self.userNameLabel.text= username;
    self.fullnameLabel.text = @"";
    [self.profileImageViewOutlet sd_setImageWithURL:[NSURL URLWithString:profileImageUrl] placeholderImage:[UIImage imageNamed:@"defaultpp"]];
    
}
@end
