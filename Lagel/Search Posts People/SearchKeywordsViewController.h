//
//  SearchKeywordsViewController.h
//  Lagel
//
//  Created by Rahul Sharma on 20/01/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchKeywordsViewController : UIViewController

@property (strong, nonatomic) IBOutlet UISearchBar *searchBarOutlet;
@property (weak, nonatomic) IBOutlet UICollectionView *postCollectionViewOutlet;
@property (strong,nonatomic) NSString *searchKeyword ;
@property (strong, nonatomic) IBOutlet UIView *noResultView;
@property (weak, nonatomic) IBOutlet UILabel *noResultsLabel;

@end
