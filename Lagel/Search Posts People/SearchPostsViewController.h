//
//  SearchPostsViewController.h

//
//  Created by Rahul Sharma on 17/05/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchPostsViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIButton *postButtonOutlet;
@property (strong, nonatomic) IBOutlet UIButton *peopleButtonOutlet;
@property (strong, nonatomic) IBOutlet UIView *baseViewOutlet;
@property (strong, nonatomic) IBOutlet UIScrollView *mainScrollViewOutlet;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *contentViewHeight;
- (IBAction)postsButtonAction:(id)sender;
- (IBAction)peopleButtonAction:(id)sender;

@property (strong, nonatomic) IBOutlet UIView *movingDividerOutlet;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *movingDividerLeadingConstraint;
@property (weak, nonatomic) IBOutlet UICollectionView *postCollectionViewOutlet;

@property (strong, nonatomic) IBOutlet UITableView *postTableView;
@property (strong, nonatomic) IBOutlet UITableView *peopleTableView;
@property (strong, nonatomic) IBOutlet UISearchBar *searchBarOutlet;
@property (strong, nonatomic) IBOutlet UIView *noPeopleResultsView;
@property (strong, nonatomic) IBOutlet UIView *noPostsResultsView;
@property (weak, nonatomic) IBOutlet UILabel *noPeopleResultsLabel;
@property (weak, nonatomic) IBOutlet UILabel *noPostResultsLabel;

@end
