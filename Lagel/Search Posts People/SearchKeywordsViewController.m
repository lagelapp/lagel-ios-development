//
//  SearchKeywordsViewController.m
//  Lagel
//
//  Created by Rahul Sharma on 20/01/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import "SearchKeywordsViewController.h"
#import "RFQuiltLayout.h"
#import "ProductDetailsViewController.h"
#import "RFQuiltLayout.h"
#import "ListingCollectionViewCell.h"

@interface SearchKeywordsViewController () <RFQuiltLayoutDelegate, UICollectionViewDataSource, UICollectionViewDelegate,UISearchBarDelegate,WebServiceHandlerDelegate>
{
    NSMutableArray *postsData ;
}
@end

@implementation SearchKeywordsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
  
    
    
    
    [self requestForPosts:self.searchKeyword];
    self.searchBarOutlet.text = self.searchKeyword ;
        self.navigationItem.title = NSLocalizedString(navTitleForSearch, navTitleForSearch) ;
        self.searchBarOutlet.placeholder = NSLocalizedString(searchPosts, searchPosts);
        self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
        UIButton *navLeft = [CommonMethods createNavButtonsWithselectedState:mNavigationBackButtonImageName normalState:mNavigationBackButtonImageName];
        UIBarButtonItem *navLeftButton = [[UIBarButtonItem alloc]initWithCustomView:navLeft];
        [navLeft setFrame:CGRectMake(0,0,25,40)];
        self.navigationItem.leftBarButtonItem = navLeftButton;
        [navLeft addTarget:self action:@selector(navBack) forControlEvents:UIControlEventTouchUpInside];
        [self.navigationItem setLeftBarButtonItems:@[[CommonMethods getNegativeSpacer],navLeftButton]];
        [self RFQuiltLayoutIntialization];
        postsData = [NSMutableArray new];
    [self requestForPosts:self.searchKeyword];
}


-(void)navBack
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark - RFQuiltLayout Intialization -

/**
 This method will create an object for RFQuiltLayout.
 */
//-(void)RFQuiltLayoutIntialization
//{
//    RFQuiltLayout* layout = [[RFQuiltLayout alloc]init];
//    layout.direction = UICollectionViewScrollDirectionVertical;
//    if(self.view.frame.size.width == 375)
//    {
//        layout.blockPixels = CGSizeMake( 37,31);
//    }
//
//    else if(self.view.frame.size.width == 414)
//    {
//        layout.blockPixels = CGSizeMake( 102,31);
//    }
//    else
//    {
//        layout.blockPixels = CGSizeMake( 79,31);
//    }
//
//    self.postCollectionViewOutlet.collectionViewLayout = layout;
//     layout.delegate=self;
//
//}

-(void)RFQuiltLayoutIntialization
{
    RFQuiltLayout* layout = [[RFQuiltLayout alloc]init];
    layout.direction = UICollectionViewScrollDirectionVertical;
    if(self.view.frame.size.width == 375)
    {
        layout.blockPixels = CGSizeMake( 24.7,31);
    }
    
    else if(self.view.frame.size.width == 414)
    {
        layout.blockPixels = CGSizeMake( 68,31);
    }
    else
    {
        layout.blockPixels = CGSizeMake(52.5, 31);
    }
    
    self.postCollectionViewOutlet.collectionViewLayout = layout;
    layout.delegate=self;
    
}
/*---------------------------------------------------------*/
#pragma mark - searchbar delegates
/*---------------------------------------------------------*/

#pragma UIsearchbardelegate


- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
    NSString *encodedString =  [searchBar.text stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
        if(searchBar.text.length > 0) {
            [self requestForPosts:encodedString];
        }
}

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar{

    self.searchBarOutlet.placeholder = NSLocalizedString(searchPosts, searchPosts);
    return YES;
}



- (BOOL)searchBar:(UISearchBar *)searchBar shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    return YES;
}
- (void)searchBarBookmarkButtonClicked:(UISearchBar *)searchBar {

    [self performSegueWithIdentifier:@"addContactToDiscoverPeopleSegue" sender:nil];
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {

}

-(void)requestForPosts:(NSString *)searchText {
    
    ProgressIndicator *HomePI = [ProgressIndicator sharedInstance];  [HomePI showPIOnView:self.view withMessage:@"Loading..."];
    NSString *encodedString =  [searchText stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
    NSDictionary *requestDict = @{
                                  mauthToken :flStrForObj([Helper userToken]),
                                  mlatitude:[NSString stringWithFormat:@"%lf", [[GetCurrentLocation sharedInstance] lastLatLong].latitude],
                                  mlongitude: [NSString stringWithFormat:@"%lf", [[GetCurrentLocation sharedInstance] lastLatLong].longitude],
                                  mProductName:encodedString,
                                  moffset:@"0",
                                  mlimit:@"20"

                                  };
    [WebServiceHandler getSearchForPosts:requestDict andDelegate:self];
}


/*------------------------------------------------------*/
#pragma mark -
#pragma mark - WebServiceDelegate
/*--------------------------------------------------------*/
- (void) didFinishLoadingRequest:(RequestType)requestType withResponse:(id)response error:(NSError*)error {

    [[ProgressIndicator sharedInstance] hideProgressIndicator];
    //handling response.
    if (error) {
      return;
    }

    NSDictionary *responseDict = (NSDictionary*)response;
    //response for hashtagsuggestion api.
        if (requestType == RequestTypeGetSearchForPosts ) {
        switch ([responseDict[@"code"] integerValue]) {
            case 200: {
                [postsData removeAllObjects];
                [self handlingPostsResponseData:responseDict[@"data"]];
            }
                break;
            case 204 :
            {
             [postsData removeAllObjects];
             [self noResultsForProductsWithSearchText:@""];
              [self.postCollectionViewOutlet reloadData];
            }
                break;
        }
    }

    if (requestType == RequestTypegetUserSearchHistory) {
        switch ([responseDict[@"code"] integerValue]) {
            case 200: {
               // NSLog(@"searched history is :%@",responseDict);
            }
                break;
        }
    }

    if (requestType == RequestTypeAddToSearchHistory) {
        switch ([responseDict[@"code"] integerValue]) {
            case 200: {
               // NSLog(@"added key to searched history is :%@",responseDict);
            }
                break;
        }
    }

}

- (void)errorAlert:(NSString *)message {
    //alert for failure response.
    UIAlertController *controller = [CommonMethods showAlertWithTitle:@"Message" message:message actionTitle:@"Ok"];
    [self presentViewController:controller animated:YES completion:nil];
}


-(void)handlingPostsResponseData:(NSArray *)postsResponse {
    if (postsResponse.count) {
        self.postCollectionViewOutlet.backgroundView = nil ;
        [postsData addObjectsFromArray:postsResponse];
        [self.postCollectionViewOutlet reloadData];
    }
    else {
       // [postsData removeAllObjects];
//        [self noResultsForProductsWithSearchText:@""];
//        [self.postCollectionViewOutlet reloadData];
    }
}

#pragma mark -
#pragma mark - collectionview delegates -


- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {

    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{

    return  postsData.count;

}

-(void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {

//    if (indexPath.row +12 == postsDataa.count && self.paging!= self.currentIndex && self.cellDisplayedIndex != indexPath.row) {
//        self.cellDisplayedIndex = indexPath.row ;
//        if(temp.count)
//        {
//            [self applyFiltersOnProductsWithIndex:self.currentIndex];
//        }
//        else
//        {
//            [self requestForExplorePosts:self.currentIndex];
//        }
//    }


}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    ListingCollectionViewCell  *collectionViewCell = [collectionView dequeueReusableCellWithReuseIdentifier:mSearchCollectioncellIdentifier forIndexPath:indexPath];
    if (collectionViewCell == nil) {
        collectionViewCell.postedImageOutlet = nil ;
    }
    else {
        dispatch_async(dispatch_get_main_queue(), ^{
            [collectionViewCell setImageForCell:self->postsData forIndexPath:indexPath];
            collectionViewCell.productNameLabel.text = flStrForObj(self->postsData[indexPath.item][@"productName"]);
            collectionViewCell.productPriceLabel.text = [NSString stringWithFormat:@"%@%@",flStrForObj(self->postsData[indexPath.item][@"currency"]),flStrForObj(self->postsData[indexPath.item][@"price"])] ;
        });
    }
    return collectionViewCell;
}


-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    ListingCollectionViewCell *cell = (ListingCollectionViewCell *)[self.postCollectionViewOutlet cellForItemAtIndexPath:indexPath];
        ProductDetailsViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:mInstaTableVcStoryBoardId];
        newView.indexPath = indexPath ;
        newView.hidesBottomBarWhenPushed = YES;
        newView.postId = flStrForObj(postsData[indexPath.row][@"postId"]);
        newView.dataFromHomeScreen = YES;
//        newView.currentCity = getLocation.currentCity;
//        newView.countryShortName = getLocation.countryShortCode ;
//        newView.currentLattitude = [NSString stringWithFormat:@"%lf",self.currentLat];
//        newView.currentLongitude = [NSString stringWithFormat:@"%lf",self.currentLong];
        newView.movetoRowNumber = indexPath.item;
        newView.imageFromHome = cell.postedImageOutlet.image;
        newView.product = [[ProductDetails alloc]initWithDictionary:postsData[indexPath.row]];
        [self.navigationController pushViewController:newView animated:YES];
}
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(5,5,5,5);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section;
{
    return 0;
}


#pragma mark -
#pragma mark – RFQuiltLayoutDelegate -

-(CGSize) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout blockSizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    NSInteger width,width2;
    NSInteger height,height2;
    if(self.view.frame.size.width == 375){
        width = 5;
        height2 = [flStrForObj(postsData[indexPath.row][@"containerHeight"]) integerValue]/30;
        width2 =  [flStrForObj(postsData[indexPath.row][@"containerWidth"]) integerValue]/37;
        height = (width * height2)/width2;
    }
    else{
        width = 2;
        height2 = [flStrForObj(postsData[indexPath.row][@"containerHeight"]) integerValue]/30;
        if(self.view.frame.size.width == 414)
        {
            width2 =  [flStrForObj(postsData[indexPath.row][@"containerWidth"]) integerValue]/102;
        }
        else
        {
            width2 =  [flStrForObj(postsData[indexPath.row][@"containerWidth"]) integerValue]/79;
        }

        height = (width * height2)/width2;
    }
    return CGSizeMake(width,height);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetsForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return UIEdgeInsetsMake (5,5,0,0);
}


#pragma mark -
#pragma mark - No Posts View

-(void)noResultsForProductsWithSearchText :(NSString *)searchText
{
    self.noResultsLabel.text = @"No results for products with this search" ;
    
    UIView *backGroundViewForNoPost = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.postCollectionViewOutlet.frame.size.width,self.postCollectionViewOutlet.frame.size.height)];
    self.noResultView.frame = backGroundViewForNoPost.frame ;
    [backGroundViewForNoPost addSubview:self.noResultView];
    self.postCollectionViewOutlet.backgroundView = backGroundViewForNoPost;
    [self.postCollectionViewOutlet reloadData];
}



@end
