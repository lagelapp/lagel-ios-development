//
//  TopTableViewCell.h

//
//  Created by Rahul Sharma on 7/25/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TopTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet AlignmentOfLabel *productSuggestionName;
@property (weak, nonatomic) IBOutlet UIButton *btnDeleteSearch;


-(void)setSuggestionsName:(NSString *)productName ;
@property (strong, nonatomic) IBOutlet UIView *bottomline;
@end
