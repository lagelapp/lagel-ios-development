//
//  TopTableViewCell.m

//
//  Created by Rahul Sharma on 7/25/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import "TopTableViewCell.h"

@implementation TopTableViewCell
@synthesize bottomline;
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self layoutIfNeeded];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setSuggestionsName:(NSString *)productName
{
    self.productSuggestionName.text = productName;
}

@end
