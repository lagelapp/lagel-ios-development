//
//  ReportPostViewController.m

//
//  Created by Rahul_Sharma on 02/05/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "ReportPostViewController.h"
#import "ReportPostTableViewCell.h"



@interface ReportPostViewController ()<UITableViewDelegate,UITableViewDataSource,UITextViewDelegate,WebServiceHandlerDelegate>
{
    NSMutableArray *arrayOfReportReasons;
    NSString * reasonId, *message200,*message409;
}
@end

@implementation ReportPostViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    arrayOfReportReasons = [NSMutableArray new];
    [self createCloseButton];
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    self.navigationController.navigationBar.barTintColor = [UIColor whiteColor];
    [self addPostDetails];
    self.postImageView.layer.cornerRadius = 5;
    self.postImageView.clipsToBounds = YES;
    self.submitButtonOutlet.enabled = NO;

    if(self.user)
    {
        
        arrayOfReportReasons[0] = NSLocalizedString(userReport1, userReport1);
        arrayOfReportReasons[1] = NSLocalizedString(userReport2, userReport2);
        arrayOfReportReasons[2] = NSLocalizedString(userReport3, userReport3);
        arrayOfReportReasons[3] = NSLocalizedString(userReport4, userReport4);
        arrayOfReportReasons[4] = NSLocalizedString(userReport5, userReport5);
        arrayOfReportReasons[5] = NSLocalizedString(userReport6, userReport6);
        arrayOfReportReasons[6] = NSLocalizedString(userReport7, userReport7);
        arrayOfReportReasons[7] = NSLocalizedString(userReport8, userReport8);
        
//        [self.activityIndicatorOutlet startAnimating];
//
//
//        ProgressIndicator *pro = [ProgressIndicator sharedInstance];
//        [pro showPIOnView:self.view withMessage:NSLocalizedString(LoadingIndicatorTitle, LoadingIndicatorTitle)];
//
      self.navigationItem.title =  NSLocalizedString(navTitleForReportUser, navTitleForReportUser);
//    [WebServiceHandler getReportReasonForUser:reqDic andDelegate:self];
    }
    else
    {
        arrayOfReportReasons[0] = NSLocalizedString(report1, report1);
        arrayOfReportReasons[1] = NSLocalizedString(report2, report2);
        arrayOfReportReasons[2] = NSLocalizedString(report3, report3);
        arrayOfReportReasons[3] = NSLocalizedString(report4, report4);
        arrayOfReportReasons[4] = NSLocalizedString(report5, report5);
        arrayOfReportReasons[5] = NSLocalizedString(report6, report6);
        arrayOfReportReasons[6] = NSLocalizedString(report7, report7);
        arrayOfReportReasons[7] = NSLocalizedString(report8, report8);
 
 //   [WebServiceHandler getReportReasonForPost:reqDic andDelegate:self];
    self.navigationItem.title =  NSLocalizedString(navTitleForReportItem, navTitleForReportItem);
    }
     self.selectedRow = -1;
    
    
    if(self.user)
    {
        message200 = NSLocalizedString(userHasBeenReprted, userHasBeenReprted);
        message409 = NSLocalizedString(userIsAlreadyReported, userIsAlreadyReported) ;
    }
    else
    {
        message200 = NSLocalizedString(productIsReported, productIsReported) ;
        message409 = NSLocalizedString(productIsAlreadyReported, productIsAlreadyReported) ;
        
    }
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.shadowImage = nil;
}


-(void)addPostDetails {
    
    if(self.user)
    {
        self.titleForDescription.text = NSLocalizedString(whyToReportUser, whyToReportUser) ;
        self.productNameLabel.text = self.userName ;
//        self.soldByUserNameLabel.text = self.fullName;
        self.soldByUserNameLabel.text = @"";
        NSString *imageURL = self.profilePicUrl;
        
        [self.postImageView setImageWithURL:[NSURL URLWithString:imageURL] placeholderImage:[UIImage imageNamed:@"defaultpp" ]usingActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];

    }
    else
    {
    self.titleForDescription.text = NSLocalizedString(whyToReportProduct, whyToReportProduct) ;
        self.postID = self.postdetails.postId ;
        self.productNameLabel.text = self.postdetails.productName ;
        self.soldByUserNameLabel.text = self.postdetails.membername ;
        NSURL *imageURL = [NSURL URLWithString:self.postdetails.thumbnailImageUrl];

    [self.postImageView setImageWithURL:imageURL placeholderImage:[UIImage imageNamed:@"defaultpp"] options:SDWebImageProgressiveDownload completed:^(UIImage *image,NSError *error, SDImageCacheType cacheType,NSURL *imageURL){
            dispatch_async(dispatch_get_main_queue(),^{
                self.postImageView.image = image;});
        } usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray] ;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




/*-----------------------------------------------------*/
#pragma mark -
#pragma mark - WebServiceDelegate
/*----------------------------------------------------*/

- (void) didFinishLoadingRequest:(RequestType)requestType withResponse:(id)response error:(NSError*)error {
    [[ProgressIndicator sharedInstance] hideProgressIndicator];
    
    [self.activityIndicatorOutlet stopAnimating];
       if (error) {
           return ;
       }
    
    NSDictionary *responseDict = (NSDictionary*)response;
    
    
    // Request for Profile Basics.
    
    switch (requestType) {
        case RequestTypeGetReportReasonForPost:
        {
            switch ([responseDict[@"code"] integerValue]) {
                case 200: {
                    [arrayOfReportReasons addObjectsFromArray:responseDict[@"data"]] ;
                    [self.tableView reloadData];
                }
                    break;
                    
                default:
                    break;
            }
        }
            break;
         case RequestTypereportPost:
        {
            switch ([responseDict[@"code"] integerValue]) {
                case 200: {
                   
                    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil
                                                                                   message:message200
                                                                            preferredStyle:UIAlertControllerStyleAlert]; // 1
                    
                    [self presentViewController:alert animated:YES completion:nil];
                     [self performSelector:@selector(removeAlert:) withObject:alert afterDelay:2.0f];

                 }
                    break;
                case 409:{
                    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil
                                                                                   message:message409
                                                                            preferredStyle:UIAlertControllerStyleAlert]; // 1
                    
                    [self presentViewController:alert animated:YES completion:nil];
                    [self performSelector:@selector(removeAlert:) withObject:alert afterDelay:2.0f];
                }
                default:
                    break;
            }
               }
        default:
            break;
    }

}


-(void)removeAlert:(UIAlertController *) alert
{
    [self dismissViewControllerAnimated:YES completion:nil];
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

/*-----------------------------------------------*/
#pragma mark -
#pragma mark - creatingNavBarButtons.
/*-----------------------------------------------*/

- (void)createCloseButton {
 
    UIButton *navLeft = [CommonMethods createNavButtonsWithselectedState:mCloseButton normalState:mCloseButton];
    UIBarButtonItem *navLeftButton = [[UIBarButtonItem alloc]initWithCustomView:navLeft];
    [navLeft setFrame:CGRectMake(0,0,25,20)];
    self.navigationItem.leftBarButtonItem = navLeftButton;
    [navLeft addTarget:self action:@selector(closeButtonAction) forControlEvents:UIControlEventTouchUpInside];
    [self.navigationItem setLeftBarButtonItems:@[[CommonMethods getNegativeSpacer],navLeftButton]];

}


-(void)closeButtonAction {
    [self.view endEditing:YES];
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}



#pragma mark-
#pragma mark - Tableview Delegate Methods.


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    self.reasonForPost = arrayOfReportReasons[indexPath.row][@"reportReason"];
    
    if(self.selectedRow != -1 && indexPath.row!= self.selectedRow)
    {
        NSIndexPath *ind =[NSIndexPath indexPathForRow:self.selectedRow inSection:0 ];
        ReportPostTableViewCell *cell=[tableView cellForRowAtIndexPath:ind];
        cell.markImageView.hidden = cell.viewForAdditionalNote.hidden = YES ;
        cell.reportTypeLabel.textColor = [UIColor colorWithRed:40/255.0f green:40/255.0f blue:40/255.0f alpha:1.0f];
        self.selectedRow = -1;
    }
    
    for(int i=0;i<arrayOfReportReasons.count;i++)
    {
        NSIndexPath *ind =[NSIndexPath indexPathForRow:i inSection:0 ];
        ReportPostTableViewCell *cell=[tableView cellForRowAtIndexPath:ind];
        if(i==indexPath.row && indexPath.row != self.selectedRow)
        {
            cell.reportTypeLabel.textColor = mBaseColor ;
            cell.markImageView.hidden = NO;
            cell.textview.hidden = NO ;
            self.selectedRow = indexPath.row;
            [self.tableView beginUpdates];
            NSIndexPath* rowToReload = [NSIndexPath indexPathForRow:indexPath.row inSection:0];
            NSArray* rowsToReload = [NSArray arrayWithObjects:rowToReload, nil];
            [self.tableView reloadRowsAtIndexPaths:rowsToReload withRowAnimation:UITableViewRowAnimationFade];
            [self.tableView endUpdates];
            reasonId = arrayOfReportReasons[indexPath.row][@"_id"];
            break;
        }
        else if (indexPath.row != self.selectedRow)
        {
            cell.markImageView.hidden = cell.viewForAdditionalNote.hidden = YES ;
            cell.reportTypeLabel.textColor = [UIColor colorWithRed:40/255.0f green:40/255.0f blue:40/255.0f alpha:1.0f];
             self.selectedRow = -1;

        }
    }
    
    self.submitButtonOutlet.enabled = YES ;
  }

#pragma mark-
#pragma mark - Tableview DataSource Methods.

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return arrayOfReportReasons.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ReportPostTableViewCell *reportCell = (ReportPostTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"reporttableviewcell"];
    
    
    [reportCell setPropertiesForObjectForIndexPath:indexPath forSelected:self.selectedRow];
    reportCell.reportTypeLabel.text = arrayOfReportReasons[indexPath.row];
    

    return reportCell;
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == self.selectedRow) {
        return 200;
    }
    return 50 ;
}

#pragma mark-
#pragma mark - TextView Delegate Methods.


- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if([text isEqualToString:@"\n"]) {
        
        [textView resignFirstResponder];
        return NO;
    }
    return YES;
}




#pragma mark-
#pragma mark - Submit Button Action

- (IBAction)submitButtonAction:(id)sender {
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:_selectedRow inSection:0];
    ReportPostTableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
    NSString *description ;
    if(cell.textview.text.length > 0)
    {
     description = cell.textview.text;
    }
    else
    {
        description = @"";
    }
    
  if(self.user)
  {
      NSDictionary *reqDic = @{mauthToken:[Helper userToken],
                               mReasonId : flStrForObj(reasonId),
                               mDescription : description,
                               mmemberName :[Helper userName],
                               mReportedUser : self.userName
                               
                               };
      [WebServiceHandler sendreportUser:reqDic andDelegate:self];

      
  }
    else
    {
  
    NSDictionary *reqDic = @{mauthToken:[Helper userToken],
                             mpostid : self.postID,
                             mReasonId :flStrForObj(reasonId),
                             mDescription : description,
                             mmemberName :[Helper userName]
                             };
    [WebServiceHandler sendreportPost:reqDic andDelegate:self];
    }
}


#pragma mark -
#pragma mark- Tap Gesture

- (IBAction)tapToDismissKeyboard:(id)sender {
    
    [self.view endEditing:YES];
}
@end
