//
//  TWPhotoPickerController.h
//
//
//  Created by Emar on 12/4/14.
//  Copyright (c) 2014 wenzhaot. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TWPhotoPickerController : UIViewController

@property (nonatomic, copy) void(^cropBlock)(UIImage *image);
@property (strong,nonatomic)  NSString *viewFromProfileSelector;
@property  BOOL profilePhotoForSignUP;

@property (strong, nonatomic) IBOutlet UIView *enableLibraryAccessPermissionView;
@end


