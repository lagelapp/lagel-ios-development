//
//  PostingScreenModel.m

//
//  Created by Rahul Sharma on 08/03/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "PostingScreenModel.h"
#import "UploadToCloudinary.h"


#define mCloudinaryPublicId          @"cloudinaryPublicId"
#define mCloudinaryPublicId1          @"cloudinaryPublicId1"
#define mCloudinaryPublicId2          @"cloudinaryPublicId2"
#define mCloudinaryPublicId3          @"cloudinaryPublicId3"
#define mCloudinaryPublicId4          @"cloudinaryPublicId4"


@implementation PostingScreenModel
static PostingScreenModel *sharedInstance = nil;

+(instancetype)sharedInstance
{
    if(sharedInstance == nil)
        sharedInstance = [PostingScreenModel new];
    return sharedInstance;
}


/**
 This metod will intialize the properties by values.
 @return SELF OBJECT.
 */
-(id)initWithTitle:(NSString *)titleOfPost
       description:(NSString *)descriptionForPost
          category:(NSString *)category
       subcategory:(NSString *)subcategory
          currency:(NSString *)currency
         condition:(NSString *)condition
             price:(NSString *)price negotiable:(NSNumber *)negotiable
             place:(NSString *)place latitude:(double)latitude longitude:(double)longitude
   postedImagePath:(NSString *)postedImagePath postedThumbnailPath:(NSString *)postedThumbnailPath
facebookSwitchState:(BOOL)facebookSwitchState
twitterSwitchState:(BOOL)twitterSwitchState instgramSwitchState:(BOOL)instgramSwitchState
 arrayOfimagePaths:(NSArray *)arrayOfImagePaths
tagPoductCoordinates:(NSArray *)tagPoductCoordinates
taggedFriendsString:(NSArray *)taggedFriendsString
 countryShortName :(NSString *) countryShortName
         andPostId:(NSString *)postID andCity:(NSString *)cityName
{
    
    self = [super init];
    if(!self)return nil;
        _titleOfPost = titleOfPost;
        _descriptionForPost = descriptionForPost;
        _category    = category;
        _subcategory = subcategory;
        _currency = currency;
        _condition = condition;
        _price = price;
        _negotiable = [NSString stringWithFormat:@"%@",negotiable];
        _place = place;
        _latitude =  [NSString stringWithFormat:@"%lf",latitude];
        _longitude = [NSString stringWithFormat:@"%lf",longitude];
        _postedImagePath = postedImagePath;
        _postedThumbnailPath = postedThumbnailPath;
    _facebookSwitchState = facebookSwitchState;
    _twitterSwitchState = twitterSwitchState;
    _instgramSwitchState =instgramSwitchState;
        _arrayOfImagePaths = arrayOfImagePaths;
        _tagPoductCoordinates = [[tagPoductCoordinates valueForKey:@"description"] componentsJoinedByString:@",,"];
        _taggedFriendsString =  [[taggedFriendsString valueForKey:@"description"] componentsJoinedByString:@","];
        _postID = flStrForObj(postID);
        self.countryShortName  = countryShortName ;
        self.cityName = cityName;
    return self;
}

/**
 Get heights and widths from arrayOf image paths.
 */
-(void)getHeightsAndWidthsForContainer :(NSArray *)arrayOfImagePaths andCheckType:(BOOL)editPostType
{   arrayOfWidths = [NSMutableArray new];
    arrayOfHeights = [NSMutableArray new];
    
    if(editPostType){
    if([arrayOfImagePaths[0][@"imageType"] isEqualToString:@"cloudinaryUrl"])
    {
        NSURL *url = [NSURL URLWithString:arrayOfImagePaths[0][@"imageValue"]];
        NSData *data = [NSData dataWithContentsOfURL:url];
        UIImage *img = [[UIImage alloc] initWithData:data];
        NSNumber *width = [NSNumber numberWithFloat:img.size.width];
        NSNumber *height= [NSNumber numberWithFloat:img.size.height];
        [arrayOfWidths addObject:width];
        [arrayOfHeights addObject:height];
    }
    else
    {
        NSString *filePath = flStrForObj(arrayOfImagePaths[0][@"imageValue"]);
        NSData *imgData = [NSData dataWithContentsOfFile:filePath];
        UIImage *fullImage=[[UIImage alloc]initWithData:imgData];
        NSNumber *width = [NSNumber numberWithFloat:fullImage.size.width];
        NSNumber *height= [NSNumber numberWithFloat:fullImage.size.height];
        [arrayOfWidths addObject:width];
        [arrayOfHeights addObject:height];

    
    }
    }
    else{
    for(int i=0;i<arrayOfImagePaths.count;i++)
    {   NSString *filePath = flStrForObj(arrayOfImagePaths[i][@"imageValue"]);
        NSData *imgData = [NSData dataWithContentsOfFile:filePath];
        UIImage *img    = [[UIImage alloc] initWithData:imgData];
        NSNumber *width = [NSNumber numberWithFloat:img.size.width];
        NSNumber *height= [NSNumber numberWithFloat:img.size.height];
        [arrayOfWidths addObject:width];
        [arrayOfHeights addObject:height];
    }
    }
}
/**
 To get the Heights.
 */
-(void)getHeightForParticularImage
{
    for(int i=0;i<arrayOfHeights.count;i++)
    {
        switch (i) {
            case 0:mainImgHeight=[arrayOfHeights objectAtIndex:i];
                break;
            case 1: height1=[arrayOfHeights objectAtIndex:i];
                break;
            case 2: height2=[arrayOfHeights objectAtIndex:i];
                break;
            case 3: height3=[arrayOfHeights objectAtIndex:i];
                break;
            case 4: height4=[arrayOfHeights objectAtIndex:i];
                break;
            default:
                break;
        }
        
    }
}


/**
 To get the widths.
 */
-(void)getWidthForParticularImage
{
    for(int i=0;i<arrayOfHeights.count;i++)
    {
        switch (i) {
            case 0:mainImgWidth=[arrayOfWidths objectAtIndex:i];
                break;
            case 1: width1=[arrayOfWidths objectAtIndex:i];
                break;
            case 2: width2=[arrayOfWidths objectAtIndex:i];
                break;
            case 3: width3=[arrayOfWidths objectAtIndex:i];
                break;
            case 4: width4=[arrayOfWidths objectAtIndex:i];
                break;
            default:
                break;
        }
        
    }
    
}

/**
 This method will give the main and thumbnail Urls in string format.

 @param mainImgUrl       mainImage url.
 @param imgUrl1          Url of image1.
 @param imgUrl2          Url of image2.
 @param thumbUrl4        thumbUrl of image4.
 */
-(void)getImagesMainUrlsInStringFormat:(NSString *)mainImgUrl imgUrl1:(NSString *)imgUrl1 imgUrl2:(NSString *)imgUrl2 imgUrl3:(NSString *)imgUrl3 imgUrl4:(NSString *)imgUrl4 andThumnailUrls :(NSString *)mainthumbNailUrl thumbUrl1:(NSString *)thumbUrl1 thumbUrl2:(NSString *)thumbUrl2 thumbUrl3:(NSString *)thumbUrl3 thumbUrl4:(NSString *)thumbUrl4
{
    _mainImgUrl = mainImgUrl;
    _imgUrl1 = imgUrl1;
    _imgUrl2 = imgUrl2;
    _imgUrl3 = imgUrl3;
    _imgUrl4 = imgUrl4;
    _mainthumbNailUrl = mainthumbNailUrl;
    _thumbUrl1 = thumbUrl1;
    _thumbUrl2 = thumbUrl2;
    _thumbUrl3 = thumbUrl3;
    _thumbUrl4 = thumbUrl4;
}

-(void)getUrlsOfImagePaths :(UploadToCloudinary *)cloudinaryObj
{
    _mainImgUrl = cloudinaryObj.cloudObj.mainImageUrl;
    _imgUrl1 = cloudinaryObj.cloudObj.ImageUrl1;
    _imgUrl2 = cloudinaryObj.cloudObj.ImageUrl2;
    _imgUrl3 = cloudinaryObj.cloudObj.ImageUrl3;
    _imgUrl4 = cloudinaryObj.cloudObj.ImageUrl4;
    _cloudinaryPublicId = cloudinaryObj.cloudObj.cloudinaryPublicId ;
    _cloudinaryPublicId1 = cloudinaryObj.cloudObj.cloudinaryPublicId1 ;
    _cloudinaryPublicId2 = cloudinaryObj.cloudObj.cloudinaryPublicId2 ;
    _cloudinaryPublicId3 = cloudinaryObj.cloudObj.cloudinaryPublicId3 ;
    _cloudinaryPublicId4 = cloudinaryObj.cloudObj.cloudinaryPublicId4 ;
    _mainthumbNailUrl = cloudinaryObj.cloudObj.mainThumbnailUrl;
    _thumbUrl1 = @"";
    _thumbUrl2 = @"";
    _thumbUrl3 = @"";
    _thumbUrl4 = @"";
}


/**
 Create dictionary with service params for posting.

 @param postingScreenObject of model class.

 @return dictionary with params.
 */
-(NSDictionary *)createParamDictionaryForAPICall : (PostingScreenModel *)postingScreenObject{
    [self getHeightsAndWidthsForContainer:postingScreenObject.arrayOfImagePaths andCheckType:postingScreenObject.editPost];
    [self getWidthForParticularImage];
    [self getHeightForParticularImage];
    NSDictionary *dict = @{mtype : @"0",
                           mauthToken :flStrForObj([Helper userToken]),
                           mProductName :flStrForObj(postingScreenObject.titleOfPost),
                           mDescription :flStrForObj(postingScreenObject.descriptionForPost),
                           mCategory :flStrForObj(postingScreenObject.category),
                           mSubCategory : flStrForObj(postingScreenObject.subcategory),
                           mCondition : flStrForObj(postingScreenObject.condition),
                           mCurrency   : flStrForObj(postingScreenObject.currency),
                           mPrice       :flStrForObj(postingScreenObject.price),
                           mFirmOnPrice : flStrForObj(postingScreenObject.negotiable),
                           mlocation :flStrForObj(postingScreenObject.place),
                           mlatitude:flStrForObj(postingScreenObject.latitude),
                           mlongitude:flStrForObj(postingScreenObject.longitude),
                           mMainImgeUrl :flStrForObj(self.mainImgUrl),
                           mMainImgeThumb:flStrForObj(self.mainthumbNailUrl),
                           mMainImgeHeight:flStrForObj(mainImgHeight),
                           mMainImgeWidth:flStrForObj(mainImgWidth),
                           mImgHeight1 :flStrForObj(height1),
                           mImgHeight2 :flStrForObj(height2),
                           mImgHeight3 :flStrForObj(height3),
                           mImgHeight4 :flStrForObj(height4),
                           mImgWidth1  :flStrForObj(width1),
                           mImgWidth2  :flStrForObj(width2),
                           mImgWidth3  :flStrForObj(width3),
                           mImgWidth4  :flStrForObj(width4),
                           mImgUrl1    :flStrForObj(self.imgUrl1),
                           mImgUrl2    :flStrForObj(self.imgUrl2),
                           mImgUrl3    :flStrForObj(self.imgUrl3),
                           mImgUrl4    :flStrForObj(self.imgUrl4),
                           mThumbUrl1  :flStrForObj(self.thumbUrl1),
                           mThumbUrl2  :flStrForObj(self.thumbUrl2),
                           mThumbUrl3  :flStrForObj(self.thumbUrl3),
                           mThumbUrl4  :flStrForObj(self.thumbUrl4),
                           mCloudinaryPublicId : flStrForObj(self.cloudinaryPublicId),
                           mCloudinaryPublicId1 : flStrForObj(self.cloudinaryPublicId1),
                           mCloudinaryPublicId2 : flStrForObj(self.cloudinaryPublicId2),
                           mCloudinaryPublicId3 : flStrForObj(self.cloudinaryPublicId3),
                           mCloudinaryPublicId4 : flStrForObj(self.cloudinaryPublicId4),
                           mImageCount :[NSString stringWithFormat:@"%lu",(unsigned long)postingScreenObject.arrayOfImagePaths.count],
                           mTaggedProductCoordinates :flStrForObj(postingScreenObject.tagPoductCoordinates),
                           mTagProductStrings :flStrForObj(postingScreenObject.taggedFriendsString),
                           mpostid : flStrForObj(postingScreenObject.postID),
                           mCountryShortName : flStrForObj(postingScreenObject.countryShortName),
                           mCity   : flStrForObj(postingScreenObject.cityName)
                           
                           
                           };

    return dict;
}

@end
