//
//  SignupDetail.m
//  Lagel
//
//  Created by admin on 2018/10/22.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import "SignupDetail.h"

@implementation SignupDetail

- (id)init {
    if (self = [super init]) {
        [self resetValue];
    }
    
    return self;
}

- (void)resetValue {
    _type = mUserNormalSignup;
    _fullName = @"";
    _email = @"";
    _phone = @"";
    _password = @"";
    _deviceID = @"";
    _city = @"";
    
    _googlePlusID = @"";
    _googlePlusToken = @"";
    _facebookToken = @"";
    _facebookID = @"";
    
    _phoneFlag = NO;
    
    _username = [self randomStringWithLength:6];
    [self generateDeviceID];
}

- (NSDictionary *)getDictionary {
    
    NSDictionary *requestDict = [[NSMutableDictionary alloc] init];
    
    requestDict = @{
                    mSignUpType  : _type,
                    mUserName    : _username,
                    mDeviceType  : @"1",
                    mpushToken   : flStrForObj([Helper deviceToken]),
                    mDeviceId    : _deviceID,
                    mfullName    : _fullName,
                    mPswd        : _password,
                    mGooglePlusId : _googlePlusID,
                    mGooglePlusAccessToken : _googlePlusToken,
                    mFacebookAccessToken: _facebookToken,
                    mEmail       : _email,
                    mphoneNumber : _phone,
                    mlocation    : @"",
                    mCity : _city,
                    mlatitude : [NSString stringWithFormat:@"%lf",_latitue],
                    mlongitude : [NSString stringWithFormat:@"%lf",_longitude],
                    mProfileUrl  : @"",
                    };
    
    NSMutableDictionary *muRequestDic = [requestDict mutableCopy];
    
    if ([_type isEqualToString:mUserNormalSignup]) {
        if (_phoneFlag) {
            [muRequestDic setValue:_phone forKey:mphoneNumber];
            NSString *email = [[_phone stringByReplacingOccurrencesOfString:@"+" withString:@""] stringByAppendingString:@"@email.com"];
            [muRequestDic setValue:email forKey:mEmail];
        }else{
            [muRequestDic setValue:@"" forKey:mphoneNumber];
            [muRequestDic setValue:_email forKey:mEmail];
        }
    }
    
    /* It's for temporarily fixing fb/gp issue. Needs to remove after fix web api */
    
    else if ([_type isEqualToString:mUserFacebookLogin]) {
        [muRequestDic setValue:_facebookID forKey:mPswd];
        [muRequestDic setValue:mUserNormalSignup forKey:mSignUpType];
    } else if ([_type isEqualToString:mUserGooglePlusLogin]) {
        [muRequestDic setValue:_googlePlusID forKey:mPswd];
        [muRequestDic setValue:mUserNormalSignup forKey:mSignUpType];
    }
    
    /* --------------- */
    
    return muRequestDic;
}

-(NSString *) randomStringWithLength: (int) len {
    
    NSMutableString *randomString = [NSMutableString stringWithCapacity: len];
    
    for (int i=0; i<len; i++) {
        [randomString appendFormat:@"%C", (unichar)('a' + arc4random_uniform(26))];
    }
    
    return randomString;
}

-(void)generateDeviceID {
    NSUUID *oNSUUID = [[UIDevice currentDevice] identifierForVendor];
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1)
        _deviceID = [oNSUUID UUIDString];
    else
        _deviceID = [oNSUUID UUIDString];
}

@end
