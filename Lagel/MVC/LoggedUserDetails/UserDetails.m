
//  LoggedUser.m
//  Created by Ajay Thakur on 19/09/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "UserDetails.h"

@implementation UserDetails

-(instancetype)initWithDictionary:(NSDictionary *)response
{
    self = [super init];
    if (!self) { return nil; }
    
    self.fullName = flStrForObj(response[mfullName]);
    self.userId = flStrForObj(response[@"userId"]);
    self.username = flStrForObj(response[@"username"]);
    self.facebookId = flStrForObj(response[@"facebookId"]);
    self.googleId = flStrForObj(response[@"googleId"]);
    self.profilePicUrl = flStrForObj(response[mProfileUrl]);
    self.website = flStrForObj(response[@"website"]);
    self.pushToken = flStrForObj(response[@"pushToken"]);
    self.token = flStrForObj(response[@"token"]);
    self.mqttId = flStrForObj(response[@"mqttId"]);
    self.code = [flStrForObj(response[@"code"]) integerValue];
    self.message = flStrForObj(response[@"message"]) ;
    self.countrySname = flStrForObj(response[@"countrySname"]) ;
    self.phoneNumber = flStrForObj(response[@"phoneNumber"]) ;
    self.email = flStrForObj(response[mEmail]);
    self.bio = flStrForObj(response[mbio]);
    return self;
}


@end
