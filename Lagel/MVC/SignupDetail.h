//
//  SignupDetail.h
//  Lagel
//
//  Created by admin on 2018/10/22.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SignupDetail : NSObject

@property (nonatomic, strong) NSString *type;
@property (nonatomic, strong) NSString *username;
@property (nonatomic, strong) NSString *fullName;
@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *phone;
@property (nonatomic, strong) NSString *password;
@property (nonatomic, strong, readonly) NSString *deviceID;
@property (nonatomic, strong) NSString *city;
@property (nonatomic, assign) double latitue;
@property (nonatomic, assign) double longitude;

@property(nonatomic, strong) NSString *googlePlusID;
@property(nonatomic, strong) NSString *googlePlusToken;
@property(nonatomic, strong) NSString *facebookToken;
@property(nonatomic, strong) NSString *facebookID;

@property(nonatomic, assign) BOOL phoneFlag;

- (NSDictionary *)getDictionary;

@end
