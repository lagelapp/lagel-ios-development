//
//  InsightsViewController.m
//  
//
//  Created by Rahul Sharma on 02/08/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "InsightsViewController.h"
#import "InsightsTableviewCell.h"
#import "DropDownTableViewCell.h"
#import "CityClicksViewController.h"
@interface InsightsViewController ()<SimpleBarChartDelegate,SimpleBarChartDataSource,UITableViewDataSource,UITableViewDelegate,WebServiceHandlerDelegate>

@end

@implementation InsightsViewController


- (void)loadView
{
    [super loadView];
    
    self.heightConstraintForChartView.constant = self.view.frame.size.width - 100 ;
    _values = self.timeInsight;
      xLabels 		= @[@"Sun", @"Mon", @"Tue", @"Wed", @"Thu", @"Fri",@"Sat"];
    
    _barColors		    = @[mBaseColor2, [UIColor redColor], [UIColor blackColor], [UIColor orangeColor], [UIColor purpleColor], [UIColor greenColor]];
    _currentBarColor	= 0;

    UIView *customView = [[UIView alloc]initWithFrame:CGRectMake(30, 50, self.view.frame.size.width-50,self.heightConstraintForChartView.constant - 70 )];

    _chart							= [[SimpleBarChart alloc] initWithFrame:customView.frame];
    _chart.center					= CGPointMake(customView.frame.size.width / 2.0, customView.frame.size.height / 2.0);
    _chart.delegate					= self;
    _chart.dataSource				= self;
    _chart.barShadowOffset			= CGSizeMake(2.0, 1.0);
    _chart.animationDuration		= 1.0;
    _chart.barShadowColor			= [UIColor grayColor];
    _chart.barShadowAlpha			= 0.5;
    _chart.barShadowRadius			= 1.0;
    _chart.barWidth					= 18.0;
    _chart.xLabelType				= SimpleBarChartXLabelTypeHorizontal;
    _chart.incrementValue			= 20;
    _chart.barTextType				= SimpleBarChartBarTextTypeRoof;
    _chart.barTextColor				= mTextBaseColor;
    _chart.hasGrids = NO ;
    _chart.barTextFont              = [UIFont fontWithName:RobotoLight size:10.0];
    [customView addSubview:_chart];
    self.dropDownTabbleView.hidden = NO;
    [self.chartView insertSubview:customView belowSubview:self.dropDownFullView];
    [self.contentView insertSubview:_dropDownFullView aboveSubview:self.chartView];    
   }

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self getInsightsForOtherTimeFrames];
    self.navigationItem.title = NSLocalizedString(navTitleForInsights, navTitleForInsights);
    
    UIButton *navLeft = [CommonMethods createNavButtonsWithselectedState:mNavigationBackButtonImageName normalState:mNavigationBackButtonImageName];
    UIBarButtonItem *navLeftButton = [[UIBarButtonItem alloc]initWithCustomView:navLeft];
    [navLeft setFrame:CGRectMake(0,0,20,20)];
    self.navigationItem.leftBarButtonItem = navLeftButton;
    [navLeft addTarget:self action:@selector(navLeftButtonInsights) forControlEvents:UIControlEventTouchUpInside];
    [self.navigationItem setLeftBarButtonItems:@[[CommonMethods getNegativeSpacer],navLeftButton]];
    
    
    dropDownArray = [[NSArray alloc] initWithArray:[NSArray arrayWithObjects:@"This Week",@"This Month",@"This Year",nil]];
    self.labelForTimeFrame.text = [dropDownArray objectAtIndex:0];
    [self setObjectsWithValues];
    [_chart reloadData];

}

-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    
    NSString *text = NSLocalizedString(clicks, clicks) ;
    
    //set background color to see if the frame is rotated
    CGSize labelSize	=  [text sizeWithAttributes:@{NSFontAttributeName:[UIFont fontWithName:RobotoMedium size:13.0]}];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0,self.view.frame.size.width - 150 ,labelSize.width,labelSize.height)];
    [label setText:text];
    label.clipsToBounds = YES ;
    label.font = [UIFont fontWithName:RobotoMedium size:13.0];
    label.textColor = mBaseColor;
    label.transform = CGAffineTransformMakeRotation( -M_PI / 2);
    [self.contentView addSubview:label];
    self.countryTableViewHeightConstraint.constant = self.countryTableView.contentSize.height;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        //This code will run in the main thread:
        
        CGRect frame = self.dropDownTabbleView.frame;
        frame.size.height = self.dropDownTabbleView.contentSize.height;
        self.dropDownTabbleView.frame = frame;
        self.dropDownFullView.frame = CGRectMake(self.dropDownFullView.frame.origin.x,self.dropDownFullView.frame.origin.y , 100, frame.size.height);
    });
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}


-(void)setObjectsWithValues
{
    self.uniqueClicksCount.text = [self.basicInsight[0][@"distinctViews"]stringValue] ;
    self.reviewsCount.text = [self.basicInsight[0][@"commented"]stringValue];
    self.likesCount.text = [self.basicInsight[0][@"likes"]stringValue];
    self.totalClicksCount.text = [self.basicInsight[0][@"totalViews"]stringValue];
    
    NSTimeInterval seconds = [self.basicInsight[0][@"postedOn"] doubleValue];
    NSDate *epochNSDate = [[NSDate alloc] initWithTimeIntervalSince1970:(seconds/1000)];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd MMM yyyy"];
     NSString *postedDate =  [dateFormatter stringFromDate:epochNSDate];
    self.postedTimeLabel.text = [[NSString stringWithFormat :@"%@",@"Posted on "]stringByAppendingString:postedDate];
    
}


#pragma mark SimpleBarChartDataSource

- (NSUInteger)numberOfBarsInBarChart:(SimpleBarChart *)barChart
{
    return _values.count;
}

- (CGFloat)barChart:(SimpleBarChart *)barChart valueForBarAtIndex:(NSUInteger)index
{
    return [[_values objectAtIndex:index] floatValue];
}

- (NSString *)barChart:(SimpleBarChart *)barChart textForBarAtIndex:(NSUInteger)index
{
    return [[_values objectAtIndex:index] stringValue];
}

- (NSString *)barChart:(SimpleBarChart *)barChart xLabelForBarAtIndex:(NSUInteger)index
{
    return [xLabels objectAtIndex:index];
}

- (UIColor *)barChart:(SimpleBarChart *)barChart colorForBarAtIndex:(NSUInteger)index
{
    return [_barColors objectAtIndex:_currentBarColor];
}



- (void) navLeftButtonInsights{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark-
#pragma mark - TableView DataSource Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView == self.dropDownTabbleView)
    {
        return dropDownArray.count ;
    }
    return self.locationInsight.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView == self.dropDownTabbleView)
    {
        DropDownTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"DropDownCell"];
        cell.dropDOwnOptions.text = dropDownArray[indexPath.row];
        return cell ;
    }
    else
    {
    InsightsTableviewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"InsightsCell"];
        
        NSString *identifier = [NSLocale localeIdentifierFromComponents:[NSDictionary dictionaryWithObject:flStrForObj(self.locationInsight[indexPath.row][@"countrySname"]) forKey:NSLocaleCountryCode]];
        NSString *country = [[[NSLocale alloc] initWithLocaleIdentifier:@"en_UK"] displayNameForKey: NSLocaleIdentifier value: identifier];
        
    cell.countryNameLabel.text = country;
    cell.clicksLabel.text = flStrForObj(self.locationInsight[indexPath.row][@"totalViews"]);
    return cell ;
    }

}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView == self.dropDownTabbleView)
    {
        switch (indexPath.row) {
            case 0:
            {
                self.labelForTimeFrame.text = @"This Week" ;
                _values	= self.timeInsight ;
                xLabels		= @[@"Sun", @"Mon", @"Tue", @"Wed", @"Thu", @"Fri",@"Sat"];
                _chart.barWidth	= 20.0;
                _chart.xLabelFont	= [UIFont fontWithName:RobotoLight size:12.0];
            }
                break;
            case 1:
            {
                self.labelForTimeFrame.text = @"This Month" ;
                _values	= monthInsightsArray;
            xLabels = @[@"Week 1" , @"Week 2" , @"Week 3" , @"Week 4" , @"Week 5"];
                
               if(self.view.frame.size.width == 320)
                {
                    _chart.barWidth	= 21.0;
                    _chart.xLabelFont			= [UIFont fontWithName:RobotoLight size:10.0];
   
                }
               else
                {
                    _chart.barWidth	= 21.0;
                    _chart.xLabelFont			= [UIFont fontWithName:RobotoLight size:12.0];

                }
                
            }
                break;
            case 2:
            {
                self.labelForTimeFrame.text = @"This Year" ;
                _values = yearInsightsArray ;
                xLabels = @[@"Jan", @"Feb", @"Mar", @"Apr", @"May", @"Jun",@"Jul",@"Aug",@"Sep",@"Oct",@"Nov",@"Dec"];
                if(self.view.frame.size.width == 320)
                {
                    _chart.barWidth	= 15.0;
                    _chart.xLabelFont	= [UIFont fontWithName:RobotoLight size:10.0];
                    
                }
                else
                {
                    _chart.barWidth	= 18.0;
                    _chart.xLabelFont	= [UIFont fontWithName:RobotoLight size:12.0];

                    
                }

            }

            default:
                break;
        }
        
        self.dropDownFullView.hidden = YES ;
         [_chart reloadData];
    }
    
    else
    {
        InsightsTableviewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
      CityClicksViewController *cityVC = [self.storyboard instantiateViewControllerWithIdentifier:@"CityClicksStoryboardId"];
        cityVC.countrySName = flStrForObj(self.locationInsight[indexPath.row][@"countrySname"]) ;
        cityVC.postId = self.postId ;
        cityVC.countryFullName = cell.countryNameLabel.text;
        [self.navigationController pushViewController:cityVC animated:YES];
        
        
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}


-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(tableView == self.dropDownTabbleView)
    {
        return 0 ;
    }
    return 60;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if(tableView == self.dropDownTabbleView)
    {
    return nil;
    }
    self.sectionHeaderViewForTable.frame = CGRectMake(0, 0, self.view.frame.size.width,30 );
    
    return self.sectionHeaderViewForTable;
}



-(void)getInsightsForOtherTimeFrames
{
    NSDictionary *requestDictMonth = @{mauthToken : [Helper userToken],
                                  mpostid : self.postId,
                                  mDurationType : @"year"
                                  };
    
    [WebServiceHandler getInsightsOfProduct:requestDictMonth andDelegate:self];
    
    NSDictionary *requestDictWeek = @{mauthToken : [Helper userToken],
                                  mpostid : self.postId,
                                  mDurationType : @"month"
                                  };
    
    [WebServiceHandler getMonthInsightsOfProduct:requestDictWeek andDelegate:self];
    
    
}


#pragma mark -
#pragma mark - Webservice Delegate

-(void)didFinishLoadingRequest:(RequestType)requestType withResponse:(id)response error:(NSError *)error
{
    if (error) {
        return ;
    }
    
    NSDictionary *responseDict = (NSDictionary*)response;
    
    if(requestType == RequestToGetInsights)
    {
        switch ([response[@"code"] integerValue])
        {
            case 200:
            {
                
                NSMutableArray *yearInsights = [NSMutableArray alloc];
                yearInsights   = responseDict[@"data"];
                yearInsightsArray = yearInsights[1][@"timeInsight"][@"data"][@"count"];
            }
                break;
            default:
                break;
        }
        
    }
    if(requestType == RequestToGetMonthInsights)
    {
        switch ([response[@"code"] integerValue])
        {
            case 200:
            {
                
                NSMutableArray *monthInsights = [NSMutableArray alloc];
                monthInsights   = responseDict[@"data"];
                monthInsightsArray = monthInsights[1][@"timeInsight"][@"data"][@"count"];
            }
                break;
            default:
                break;
        }
        
    }



}




- (IBAction)chooseTimeFrameButtonAction:(id)sender {
    
    if(self.dropDownFullView.hidden)
    {
        self.dropDownFullView.hidden = NO;
    }
    else
    {
        self.dropDownFullView.hidden = YES ;
    }
    
}
@end
