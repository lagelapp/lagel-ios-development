//
//  CameraViewController.h

//
//  Created by Rahul Sharma on 4/21/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PGShareViewController.h"
#import <AVFoundation/AVFoundation.h>
#import <AssetsLibrary/AssetsLibrary.h>

#import "SCRecorder.h"


#define CAPTURE_FRAMES_PER_SECOND		20


/**
 Delegate In Camera Screen for maintaining data of images.
 */
@protocol cameraScreenDelegate <NSObject>

-(void)getMaintainedDataOfImages :(NSMutableArray *)arrayOfmaintainedImages;

@end


@interface CameraViewController : UIViewController<UIScrollViewDelegate>

{
    BOOL WeAreRecording;
    
}

@property (strong,nonatomic)id<cameraScreenDelegate>cameraDelegate;

@property (retain) AVCaptureVideoPreviewLayer *PreviewLayer;

@property (strong, nonatomic) IBOutlet UIImageView *photoView;
@property (strong,nonatomic) UIImagePickerController *imgpicker;
@property (strong, nonatomic) NSMutableArray *arrayOfChoosenImages,*assets;
@property (strong, nonatomic) ALAssetsLibrary *assetsLibrary;

//screcorder
@property(nonatomic,strong)SCRecorder *recorder;
@property(nonatomic,strong)SCRecorderToolsView *focusView;

@property (nonatomic, strong) NSTimer *timer;

@property  int counter;
@property BOOL cameraAgain ;
@property (nonatomic)BOOL dismiss;
@property  BOOL sellProduct;


@property (weak, nonatomic) IBOutlet UIButton *libraryButtonOutlet;
@property (weak, nonatomic) IBOutlet UIButton *photoButtonOutlet;
@property (strong, nonatomic) IBOutlet UIButton *flashButtonOutlet;

@property (strong, nonatomic) IBOutlet UIButton *cameraButtonOutlet;


- (IBAction)libraryButtonAction:(id)sender;
- (IBAction)photoButtonAction:(id)sender;
- (IBAction)takePhoto:(id)sender;

@property (weak, nonatomic) IBOutlet UIView *frameForCaptureImage;

@property(weak,nonatomic) UIImage *selectedImage;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@property UIImageView *previewView;
@property (weak, nonatomic) IBOutlet UIView *viewWhenCameraPermissonDenied;
- (IBAction)enableCameraAccessButtonAction:(id)sender;
@property (strong, nonatomic) IBOutlet UICollectionView *collectionViewForCapturedImages;
- (IBAction)doneButtonAction:(id)sender;
- (IBAction)flashButtonAction:(id)sender;
- (IBAction)cancelButtonAction:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *labelToShowAlert;

@end
