//
//  UIViewController+topViewController.h
//  Lagel
//
//  Created by Imran Ali on 14/02/2019.
//  Copyright © 2019 Gaurav Gudaliya. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIViewController(topViewController)

+ (UIViewController*)topViewController;

@end

NS_ASSUME_NONNULL_END
