//
//  GoogleSignInHandler.h

//  Created by Rahul Sharma on 23/05/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <GoogleSignIn/GoogleSignIn.h>
#import <Foundation/Foundation.h>

@interface GoogleSignInHandler : NSObject < GIDSignInDelegate>

- (void)signIn:(GIDSignIn *)signIn didSignInForUser:(GIDGoogleUser *)user withError:(NSError *)error ;

- (void)signIn:(GIDSignIn *)signIn didDisconnectWithUser:(GIDGoogleUser *)user  withError:(NSError *)error;


@end
