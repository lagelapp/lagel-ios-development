//
//  RegisterNameViewController.h
//  Lagel
//
//  Created by Stars on 5/30/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SignupDetail.h"

@interface RegisterNameViewController : UIViewController
{
   
}

@property (weak, nonatomic) IBOutlet UITextField *firstNameField;
@property (weak, nonatomic) IBOutlet UITextField *lastNameField;
@property (weak, nonatomic) IBOutlet UIButton *signinButton;

@property (nonatomic, strong) SignupDetail *signupInfo;

@end
