//
//  CreatPasswordViewController.m
//  Lagel
//
//  Created by Stars on 5/30/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import "CreatPasswordViewController.h"
#import "Lagel-Swift.h"
#import "LoginViewController.h"
#import "WebServiceHandler.h"

@interface CreatPasswordViewController () <WebServiceHandlerDelegate, GetCurrentLocationDelegate>
{
    NSString *password;
    NSString *confirmPassword;
    NSString *deviceId;
    NSString *phoneCode;
    NSString *countryCode;
    GetCurrentLocation *getLocation;
}

@property (nonatomic, strong) NSString *currentCity, *currentCountryShortName, *currentAddress;
@property (nonatomic, assign) double  currentLat, currentLong;
@property (nonatomic, strong) AFWrapper *afWrapper;

@end

@implementation CreatPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _signinButton.layer.borderColor = _signinButton.currentTitleColor.CGColor;
    _signinButton.layer.borderWidth = 1;
    
    getLocation = [GetCurrentLocation sharedInstance];
    [getLocation getLocation];
    getLocation.delegate = self;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)onNextButton:(id)sender {
    if (![self.passwordField hasText]){
        [self.passwordField becomeFirstResponder];
        [self showAlert:Localized(@"Warning") message:Localized(@"Please enter password")];
        return;
    }
    if(![self.passwordField.text isEqualToString:self.confirmPasswordField.text]){
        [self.confirmPasswordField becomeFirstResponder];
        [self showAlert:Localized(@"Warning") message:Localized(@"Password is incorrect, Password does not match")];
        return;
    }
    
    confirmPassword = self.confirmPasswordField.text;
    
    _signupInfo.password = self.passwordField.text;
    _signupInfo.city = flStrForObj(getLocation.currentCity);
    _signupInfo.latitue = self.currentLat;
    _signupInfo.longitude = self.currentLong;

    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [WebServiceHandler newRegistration:[_signupInfo getDictionary] andDelegate:self];
}

- (IBAction)onBackButton:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)onToLoginPageButton:(id)sender {
    LoginViewController *passwordVC = [self.storyboard instantiateViewControllerWithIdentifier:@"loginVC"];
    [self.navigationController pushViewController:passwordVC animated:YES];
}

#pragma mark - WebServerHandler Delegate

- (void) didFinishLoadingRequest:(RequestType)requestType withResponse:(id)response error:(NSError*)error {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    if (requestType == RequestTypenewRegister) {
        UserDetails *user = response ;
        NSLog(@"%@", response);
        switch (user.code) {
            case 200: {
                //save username
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:mSignupFirstTime];
                [[NSUserDefaults standardUserDefaults] setObject:user.username forKey:@"newRegisterUsername"];
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"recent_login"];
                [[NSUserDefaults standardUserDefaults] setObject:_signupInfo.deviceID forKey:mDeviceId];
                [[NSUserDefaults standardUserDefaults] synchronize];
                NSLog(@"%@",user);
                [Helper storeUserLoginDetails:user];
                NSDictionary *param = [CommonMethods updateDeviceDetailsForAdmin];
                [WebServiceHandler logUserDevice:param andDelegate:self];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.parentViewController dismissViewControllerAnimated:YES completion:nil];
                });
                
                // creating connection
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                    MQTT *mqttModel = [MQTT sharedInstance];
                    [mqttModel createConnection];
                });
            }
                break;
            default:
                [self showAlert:Localized(@"Error") message:Localized(@"Request Failed")];
                break ;
        }
    }
}

#pragma mark - GeoLocation Delegate

- (void)updatedLocation:(double)latitude and:(double)longitude
{
    self.currentLat = latitude;
    self.currentLong = longitude;
}

- (void)updatedAddress:(NSString *)currentAddress
{
    self.currentAddress = currentAddress ;
}

@end
