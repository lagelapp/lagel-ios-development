
//  LoginSignUpViewController.h
//  Created by Ajay Thakur on 16/07/17.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "FBLoginHandler.h"
#import <GoogleSignIn/GoogleSignIn.h>

@class GIDSignInButton;

@interface LoginSignUpViewController : UIViewController <UITextFieldDelegate,FBLoginHandlerDelegate,CLUploaderDelegate,UIImagePickerControllerDelegate,WebServiceHandlerDelegate,UIActionSheetDelegate, GetCurrentLocationDelegate,GIDSignInUIDelegate> {
    BOOL userNameLength,signUpByPhoneNumber,updateFbImageOnlyFirstTime, keyboardShown, SignUpFinshed;
    CLCloudinary *cloudinary;
    NSString *profilePicUrl, *deviceId, *ThumbnailimagePath ,*previousSelection;
    NSDictionary *cloundinaryCreditinals;
    UILabel *labelForErrorMessg;
    UITextField *activeField;
    GetCurrentLocation *getLocation;
    
}

#pragma mark -
#pragma mark - Non IB Properties -
@property BOOL googleSignin ;
@property NSDictionary *fbLoginDetails ;
@property NSString *faceBookUniqueIdOfUser, *googlePlusId , *googlePlusName;
@property NSString *faceBookUserEmailId, *googlePlusEmailId;
@property NSString *profilepicurlFb,*googlePlusProfileUrl;
@property NSString *fullNameForFb ,*fbloggedUserAccessToken, *googlePlusUserAccessToken;



// Current Lat , Long values holder.
@property (nonatomic)double  currentLat, currentLong;

// Strings value to hold the google/Facebook properties.
@property NSString *faceBookUniqueIdOfUserToRegister, *currentCity , *currentCountryShortName ,*currentAddress;

@property NSDictionary *googlePlusLoginDetails, *fbResponseDetails;
@property BOOL signUpWithFacebook,signupFlag, signUpWithGooglePlus,isSocialLogin;
@property(strong,nonatomic) NSString *googlePlusEmail, *googlePlusAccessToken;
@property(strong, nonatomic)NSString *faceBookEmailIdOfUserToRegister, *SignUpType, *codeForSignUpType, *userEnteredEmail, *emailForRegistration;

#pragma mark -


#pragma mark - UIView Outlets -

@property (strong, nonatomic) IBOutlet UIView *loginTopView;
@property (strong, nonatomic) IBOutlet UIView *signupTopView;
@property (strong, nonatomic) IBOutlet UIView *viewForLoginSignup;
@property (strong, nonatomic) IBOutlet UIView *loginViewOutlet;
@property (strong, nonatomic) IBOutlet UIView *signupViewOutlet;
@property (strong, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityViewOutlet;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *createActivityIndicatorOutlet;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;

#pragma mark -
#pragma mark - UILabel Outlets -

#pragma mark -
#pragma mark - UITextField Outlets -
@property (weak, nonatomic) IBOutlet UITextField *usernameTextField;
@property (strong, nonatomic) IBOutlet UITextField *loginPasswordTextfield;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (strong, nonatomic) IBOutlet UITextField *userNameTextField;
@property (strong, nonatomic) IBOutlet UITextField *nameTextField;
@property (strong, nonatomic) IBOutlet UITextField *signupPasswordTextfield;


#pragma mark -
#pragma mark - UILabes Outlets -

@property (strong, nonatomic) IBOutlet UILabel *lblgotosignintext;
@property (strong, nonatomic) IBOutlet UILabel *lbltitle1;
@property (strong, nonatomic) IBOutlet UILabel *lbldescription;
@property (strong, nonatomic) IBOutlet UILabel *lblor;


#pragma mark -
#pragma mark - UIButton Outlets -

@property (strong, nonatomic) IBOutlet UIButton *loginSelectOutlet;
@property (strong, nonatomic) IBOutlet UIButton *signupSelectOutlet;
@property (weak, nonatomic) IBOutlet UIButton *createButton;
@property (strong, nonatomic) IBOutlet UIButton *agreeButtonOutlet;
@property (weak, nonatomic) IBOutlet UIButton *loginbutton;
@property (strong, nonatomic) IBOutlet UIButton *btngoforsignup;
@property (strong, nonatomic) IBOutlet UIButton *btnFBSignIn;
@property (strong, nonatomic) IBOutlet UIButton *btnGPSignIn;
@property (weak, nonatomic) IBOutlet UIButton *backButtonOutLet;

#pragma mark -
#pragma mark - UIImageView Outlets -
@property (strong, nonatomic) IBOutlet UIImageView *editImageOnProfileView;
@property (strong, nonatomic) IBOutlet UIImageView *profileImageView;
@property (strong, nonatomic) IBOutlet UIImageView *userNameCheckImage;
@property (strong, nonatomic) IBOutlet UIImageView *imageForEmailCheck;



#pragma mark -
#pragma mark - UIButton Actions -

- (IBAction)logInButtonAction:(id)sender;
- (IBAction)getHelpSignInButtonAction:(id)sender;
- (IBAction)textFieldDidChange:(id)sender;
- (IBAction)loginOptionButtonAction:(id)sender;
- (IBAction)signupOptionButtonAction:(id)sender;
- (IBAction)backButtonAction:(id)sender;
- (IBAction)privacyPolicyButtonAction:(id)sender;
- (IBAction)termsConditionsButtonAction:(id)sender;
- (IBAction)createButtonAction:(id)sender;
- (IBAction)editProfileImageButton:(id)sender;
- (IBAction)agreeButtonAction:(id)sender;
- (IBAction)textFieldValueIsChanged:(id)sender;
- (IBAction)btnFBSignInAction:(id)sender;
- (IBAction)btnGPSignInAction:(id)sender;

@end

