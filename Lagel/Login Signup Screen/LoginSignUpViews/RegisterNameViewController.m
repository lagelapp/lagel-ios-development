//
//  RegisterNameViewController.m
//  Lagel
//
//  Created by Stars on 5/30/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import "RegisterNameViewController.h"
#import "CreatPasswordViewController.h"
#import "Lagel-Swift.h"

@interface RegisterNameViewController ()
{
}

@end

@implementation RegisterNameViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _signinButton.layer.borderColor = _signinButton.currentTitleColor.CGColor;
    _signinButton.layer.borderWidth = 1;

    // Do any additional setup after loading the view.
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapToDismissKeyboard:)];
    [self.view addGestureRecognizer:tapGesture];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)onNextButton:(id)sender {
    if (![self.firstNameField hasText]){
        [self.firstNameField becomeFirstResponder];
        [self showAlert:Localized(@"Warning") message:Localized(@"Please input the First Name")];
        return;
    }
    if (![self.lastNameField hasText]){
        [self.lastNameField becomeFirstResponder];
        [self showAlert:Localized(@"Warning") message:Localized(@"Please input the Last Name")];
        return;
    }
    
    NSString *fullName =  [self.firstNameField.text stringByAppendingString:@" "];
    fullName = [fullName stringByAppendingString:self.lastNameField.text];
    _signupInfo.fullName = fullName;
    
    CreatPasswordViewController *passwordVC = [self.storyboard instantiateViewControllerWithIdentifier:@"creatPasswordVC"];
    passwordVC.signupInfo = self.signupInfo;
    [self.navigationController pushViewController:passwordVC animated:YES];

}

- (IBAction)onBackButton:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)onToLoginPageButton:(id)sender {
}

- (IBAction)tapToDismissKeyboard:(id)sender {
    [self.view endEditing:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
