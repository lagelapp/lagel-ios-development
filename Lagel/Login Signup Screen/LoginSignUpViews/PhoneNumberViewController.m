//
//  PhoneNumberViewController.m
//  Lagel
//
//  Created by Stars on 6/4/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import "PhoneNumberViewController.h"
#import "CountryListDataSource.h"
#import "PhoneNumberCell.h"
#import "RegisterViewController.h"
#import "AppDelegate.h"

@interface PhoneNumberViewController ()<UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate>
@property (strong, nonatomic) NSArray *dataRows;
@property (strong, nonatomic) NSMutableArray *searchDataRows;
@end

@implementation PhoneNumberViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    CountryListDataSource *dataSource = [[CountryListDataSource alloc] init];
    _dataRows = [dataSource countries];
    _searchDataRows = [NSMutableArray arrayWithArray:_dataRows];
    _phoneTableView.dataSource = self;
    _phoneTableView.delegate = self;
    _phoneTableView.rowHeight = 60;
    _searchBar.delegate = self;
    
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated{
    [self.navigationController setNavigationBarHidden:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableView Datasource


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_searchDataRows count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    PhoneNumberCell *cell = (PhoneNumberCell *)[self.phoneTableView dequeueReusableCellWithIdentifier:@"phoneNumberCell"];
    
    cell.countryLabel.text = [[_searchDataRows objectAtIndex:indexPath.row] valueForKey:kCountryName];
    cell.codeLabel.text = [[_searchDataRows objectAtIndex:indexPath.row] valueForKey:kCountryCallingCode];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *code = [[_searchDataRows objectAtIndex:indexPath.row] valueForKey:kCountryCallingCode];
    NSString *countryCode = [[_searchDataRows objectAtIndex:indexPath.row] valueForKey:kCountryCode];
    AppDelegate *delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    delegate.userPhoneCode = code;
    delegate.counryCode = countryCode;
    //[(AppDelegate*)[[UIApplication sharedApplication] delegate] setUserPhoneCode:code countrycode:countryCode];
    
    if (self.navigationController != nil) {
        [[self navigationController] popViewControllerAnimated:YES];
    } else {
        [self dismissViewControllerAnimated:YES completion:NULL];
    }
}

#pragma mark - SearchBar Delegate

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    NSString *lowercaseSearchText = [searchText lowercaseString];
    
    if (searchText.length == 0) {
        _searchDataRows = [NSMutableArray arrayWithArray:_dataRows];
    } else {
        NSMutableArray *searchedCountry = [NSMutableArray array];
        for (NSDictionary *countryInfo in _dataRows) {
            NSString *countryName = [[countryInfo valueForKey:kCountryName] lowercaseString];
            if ([countryName containsString:lowercaseSearchText]) {
                [searchedCountry addObject:countryInfo];
            }
        }
        
        _searchDataRows = searchedCountry;
    }
    
    [_phoneTableView reloadData];
}

@end
