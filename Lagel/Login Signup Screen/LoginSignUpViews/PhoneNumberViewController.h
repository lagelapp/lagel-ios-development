//
//  PhoneNumberViewController.h
//  Lagel
//
//  Created by Stars on 6/4/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhoneNumberViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *phoneTableView;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;

@end
