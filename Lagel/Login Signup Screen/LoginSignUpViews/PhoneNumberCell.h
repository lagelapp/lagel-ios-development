//
//  PhoneNumberCell.h
//  Lagel
//
//  Created by Stars on 6/4/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhoneNumberCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *countryLabel;
@property (weak, nonatomic) IBOutlet UILabel *codeLabel;

@end
