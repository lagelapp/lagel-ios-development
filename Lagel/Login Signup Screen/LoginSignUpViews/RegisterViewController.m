//
//  RegisterViewController.m
//  Lagel
//
//  Created by Stars on 5/30/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import "RegisterViewController.h"
#import "RegisterNameViewController.h"
#import "PhoneNumberViewController.h"
#import "Lagel-Swift.h"
#import "Helper.h"
#import "WebServiceHandler.h"
#import "AppDelegate.h"
#import "LoginViewController.h"

@interface RegisterViewController () <WebServiceHandlerDelegate>
{
    UIView *bottomAnimationView;
    BOOL phoneFlag;
    NSString *deviceId;
    NSString *phoneCode;
    NSString *countryCode;
    GetCurrentLocation *getLocation;
}
@property NSString *faceBookUniqueIdOfUser, *googlePlusId , *googlePlusName, *googlePlusUserAccessToken;
@property NSString *faceBookUniqueIdOfUserToRegister, *currentCity , *currentCountryShortName ,*currentAddress;
@property (nonatomic)double  currentLat, currentLong;
@property (nonatomic, strong) AFWrapper *afWrapper;

@end

@implementation RegisterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    phoneCode = @"+1";
    countryCode = @"US";
    phoneFlag = YES;
    [_phoneField setPlaceholder:Localized(@"Phone")];
    
    [self resetCountryCodeButton];
    
    bottomAnimationView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.bttomView.frame.size.width/2, 1.0)];
    bottomAnimationView.backgroundColor = [UIColor blackColor];
    [self.bttomView addSubview:bottomAnimationView];
    
    _emailView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    _emailView.layer.borderWidth = 1;
    _phoneView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    _phoneView.layer.borderWidth = 1;
    _signinButton.layer.borderColor = _signinButton.currentTitleColor.CGColor;
    _signinButton.layer.borderWidth = 1;
    
    // Do any additional setup after loading the view.
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapToDismissKeyboard:)];
    [self.view addGestureRecognizer:tapGesture];
}

- (void)viewDidLayoutSubviews{
    
    /*bottomAnimationView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.bttomView.frame.size.width/2, 1.0)];
    NSLog(@"%@", NSStringFromCGRect(bottomAnimationView.frame));
    bottomAnimationView.backgroundColor = [UIColor blackColor];*/
}

-(void)viewWillAppear:(BOOL)animated{
    
    [self.navigationController setNavigationBarHidden:YES];
}

-(void)viewDidAppear:(BOOL)animated{
    if (phoneFlag){
        [bottomAnimationView setFrame:CGRectMake(0, 0, self.bttomView.frame.size.width/2, 1.0)];
        self.phoneView.hidden = NO;
        self.emailView.hidden = YES;
        
    }else{
        [bottomAnimationView setFrame:CGRectMake(self.bttomView.frame.size.width/2, 0, self.bttomView.frame.size.width/2, 1.0)];
        self.phoneView.hidden = YES;
        self.emailView.hidden = NO;
    }
    
    AppDelegate *delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    NSLog(@"%@", delegate.userPhoneCode);
    if (delegate.userPhoneCode != nil && ![delegate.userPhoneCode  isEqual: @""]) {
        phoneCode = delegate.userPhoneCode;
        delegate.userPhoneCode = @"";
        countryCode = delegate.counryCode;
        delegate.counryCode = @"";
        
        [self resetCountryCodeButton];
    }
}

- (void)resetCountryCodeButton {
    NSString *codeString = [NSString stringWithFormat:@"%@ %@", countryCode,phoneCode];
    [_codeButton setTitle:codeString forState:UIControlStateNormal];
}

- (IBAction)onPhoneButton:(id)sender {
    phoneFlag = YES;
    [UIView animateWithDuration:0.5 animations:^{
        [self->bottomAnimationView setFrame:CGRectMake(0, 0, self.bttomView.frame.size.width/2, 1.0)];
        [self.phoneButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [self.emailButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    } completion:^(BOOL finished) {
        NSLog(@"%@", NSStringFromCGRect(self->bottomAnimationView.frame));
    }];
    self.phoneView.hidden = NO;
    self.emailView.hidden = YES;
}

- (IBAction)onEmailButton:(id)sender {
    phoneFlag = NO;
    [UIView animateWithDuration:0.5 animations:^{
        [self->bottomAnimationView setFrame:CGRectMake(self.bttomView.frame.size.width/2, 0, self.bttomView.frame.size.width/2, 1.0)];
        [self.phoneButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        [self.emailButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    } completion:^(BOOL finished) {
        NSLog(@"%@", NSStringFromCGRect(self->bottomAnimationView.frame));
    }];
    self.phoneView.hidden = YES;
    self.emailView.hidden = NO;
}

- (IBAction)onPhoneNumberButton:(id)sender {
    PhoneNumberViewController *passwordVC = [self.storyboard instantiateViewControllerWithIdentifier:@"phoneNumberVC"];
    [self.navigationController pushViewController:passwordVC animated:YES];
}

- (IBAction)onNextButton:(id)sender {
    SignupDetail *signupData = [[SignupDetail alloc] init];
    
    if (phoneFlag) {
        if (phoneCode.length == 0) {
            [self showAlert:Localized(@"Warning") message:Localized(phoneNumberIsInvalid)];
            return;
        }
        
        if (![self.phoneField hasText]) {
            [self.phoneField becomeFirstResponder];
            [self showAlert:Localized(@"Warning") message:Localized(phoneNumberIsInvalid)];
            return;
        }
        
        signupData.phone = [NSString stringWithFormat:@"%@%@", phoneCode, self.phoneField.text];
        NSDictionary *param = @{@"phoneNumber": signupData.phone};
        [WebServiceHandler phoneNumberCheck:param andDelegate:self];
    }else{
        if (![self.emailField hasText]) {
            [self.emailField becomeFirstResponder];
            [self showAlert:Localized(@"Warning") message:Localized(@"Please input the Email")];
            return;
        }
        signupData.email = self.emailField.text;
    }
    
    signupData.phoneFlag = phoneFlag;
    signupData.type = mUserNormalSignup;
    
    RegisterNameViewController *registerNameVC = [self.storyboard instantiateViewControllerWithIdentifier:@"registerNameVC"];
    registerNameVC.signupInfo = signupData;
    [self.navigationController pushViewController:registerNameVC animated:YES];
}

- (IBAction)onToLoginPageButton:(id)sender {
    LoginViewController *passwordVC = [self.storyboard instantiateViewControllerWithIdentifier:@"loginVC"];
    [self.navigationController pushViewController:passwordVC animated:YES];
}

- (IBAction)onBackButton:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)tapToDismissKeyboard:(id)sender {
    [self.view endEditing:YES];
}

#pragma mark - WebServiceHandler Delegate

- (void)didFinishLoadingRequest:(RequestType)requestType withResponse:(id)response error:(NSError *)error {
    if (error) {
        [Helper showAlertWithTitle:Localized(alertError) Message:[error localizedDescription] viewController:self];
        return;
    }
    //storing the response from server to dictonary.
    NSDictionary *responseDict = (NSDictionary*)response;
    //checking the request type and handling respective response code.
    if (requestType ==  RequestTypePhoneNumberCheck ) {
        switch ([responseDict[@"code"] integerValue]) {
            case 200:
                break;
            default:
                [Helper showAlertWithTitle:Localized(alertMessage) Message:responseDict[@"message"] viewController:self];
                break;
        }
    }
}

@end
