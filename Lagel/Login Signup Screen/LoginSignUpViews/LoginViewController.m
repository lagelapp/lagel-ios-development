//
//  LoginViewController.m
//  Lagel
//
//  Created by Stars on 6/5/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import "LoginViewController.h"
#import "AppDelegate.h"
#import "PhoneNumberViewController.h"
#import "Lagel-Swift.h"
#import "ForgotPasswordViewController.h"
#import "CreatPasswordViewController.h"
#import "WebServiceHandler.h"
#import "FBLoginHandler.h"
#import <GoogleSignIn/GoogleSignIn.h>

typedef enum {
    kFacebook,
    kGoogle
} SocialType;

@interface LoginViewController () <WebServiceHandlerDelegate, FBLoginHandlerDelegate, GIDSignInUIDelegate, GIDSignInDelegate, GetCurrentLocationDelegate>
{
    UIView *bottomAnimationView;
    BOOL phoneFlag;
    NSString *phoneCode;
    NSString *countryCode;
    GetCurrentLocation *getLocation;
}

@property (nonatomic)double  currentLat, currentLong;
@property NSString *faceBookUniqueIdOfUserToRegister, *currentCity , *currentCountryShortName ,*currentAddress;

#pragma mark -
#pragma mark - Non IB Properties -
@property SocialType socialType ;
@property NSDictionary *fbLoginDetails ;
@property NSString *faceBookUniqueIdOfUser, *googlePlusId , *googlePlusName;
@property NSString *faceBookUserEmail;
@property NSString *profilepicurlFb,*googlePlusProfileUrl;
@property NSString *fullNameForFb ,*fbloggedUserAccessToken, *googlePlusUserAccessToken;
@property NSDictionary *googlePlusLoginDetails, *fbResponseDetails;
@property BOOL signUpWithFacebook,signupFlag,isSocialLogin;
@property(strong, nonatomic) NSString *googlePlusEmail;
@property(strong, nonatomic) NSString *faceBookEmailIdOfUserToRegister, *SignUpType, *codeForSignUpType, *userEnteredEmail, *emailForRegistration;

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    phoneCode = @"+1";
    countryCode = @"US";
    phoneFlag = YES;
    [_phoneField setPlaceholder:Localized(@"Phone")];
    
    [self resetCountryCodeButton];
    
    bottomAnimationView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.bttomView.frame.size.width/2, 1.0)];
    bottomAnimationView.backgroundColor = [UIColor blackColor];
    [self.bttomView addSubview:bottomAnimationView];
    
    _emailView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    _emailView.layer.borderWidth = 1;
    _phoneView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    _phoneView.layer.borderWidth = 1;
    _passwordView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    _passwordView.layer.borderWidth = 1;
    _createAccountButton.layer.borderColor = _createAccountButton.currentTitleColor.CGColor;
    _createAccountButton.layer.borderWidth = 1;
    
    getLocation = [GetCurrentLocation sharedInstance];
    [getLocation getLocation];
    getLocation.delegate = self;
    
    // Do any additional setup after loading the view.
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapToDismissKeyboard:)];
    [self.view addGestureRecognizer:tapGesture];
}

-(void)viewWillAppear:(BOOL)animated{
    
    [self.navigationController setNavigationBarHidden:YES];
}

-(void)viewDidAppear:(BOOL)animated{
    if (phoneFlag){
        [bottomAnimationView setFrame:CGRectMake(0, 0, self.bttomView.frame.size.width/2, 1.0)];
        self.phoneView.hidden = NO;
        self.emailView.hidden = YES;
        
    }else{
        [bottomAnimationView setFrame:CGRectMake(self.bttomView.frame.size.width/2, 0, self.bttomView.frame.size.width/2, 1.0)];
        self.phoneView.hidden = YES;
        self.emailView.hidden = NO;
    }
    
    AppDelegate *delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    NSLog(@"%@", delegate.userPhoneCode);
    if (delegate.userPhoneCode != nil && ![delegate.userPhoneCode  isEqual: @""]) {
        phoneCode = delegate.userPhoneCode;
        delegate.userPhoneCode = @"";
        countryCode = delegate.counryCode;
        delegate.counryCode = @"";
        
        [self resetCountryCodeButton];
    }
}

- (IBAction)onPhoneButton:(id)sender {
    phoneFlag = YES;
    [UIView animateWithDuration:0.5 animations:^{
        [self->bottomAnimationView setFrame:CGRectMake(0, 0, self.bttomView.frame.size.width/2, 1.0)];
        [self.phoneButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [self.emailButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    } completion:^(BOOL finished) {
        NSLog(@"%@", NSStringFromCGRect(self->bottomAnimationView.frame));
    }];
    self.phoneView.hidden = NO;
    self.emailView.hidden = YES;
}

- (IBAction)onEmailButton:(id)sender {
    phoneFlag = NO;
    [UIView animateWithDuration:0.5 animations:^{
        [self->bottomAnimationView setFrame:CGRectMake(self.bttomView.frame.size.width/2, 0, self.bttomView.frame.size.width/2, 1.0)];
        [self.phoneButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        [self.emailButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    } completion:^(BOOL finished) {
        NSLog(@"%@", NSStringFromCGRect(self->bottomAnimationView.frame));
    }];
    self.phoneView.hidden = YES;
    self.emailView.hidden = NO;
}

- (IBAction)onCoutryCodeButton:(id)sender {
    PhoneNumberViewController *passwordVC = [self.storyboard instantiateViewControllerWithIdentifier:@"phoneNumberVC"];
    [self.navigationController pushViewController:passwordVC animated:YES];
}

- (IBAction)onLoginButton:(id)sender {
    NSDictionary *requestDict = @{
                                  mLoginType   : mUserNormalLogin,
                                  mPswd :self.passwordField.text ,
                                  mlatitude : [NSString stringWithFormat:@"%lf",self.currentLat],
                                  mlongitude : [NSString stringWithFormat:@"%lf",self.currentLong],
                                  mCity : flStrForObj(getLocation.currentCity),
                                  mCountryShortName : flStrForObj(getLocation.countryShortCode),
                                  mpushToken   :flStrForObj([Helper deviceToken]),
                                  mplace : @"",
                                  };
    NSLog(@"%@",requestDict);
    NSMutableDictionary *muRequestDic = [requestDict mutableCopy];
    if (phoneFlag) {
        if (phoneCode.length == 0) {
            [self showAlert:Localized(@"Warning") message:Localized(phoneNumberIsInvalid)];
            return;
        }
        
        if (_phoneField.text.length == 0) {
            [self showAlert:Localized(@"Warning") message:Localized(phoneNumberIsInvalid)];
            return;
        }
        
        [muRequestDic setValue:[self getFullPhoneNumber] forKey:mDevicePhoneNo];
    }else{
        [muRequestDic setValue:self.emailField.text forKey:mEmail];
        [muRequestDic setValue:self.emailField.text forKey:mUserName];
    }
    
    self.isSocialLogin = NO;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [WebServiceHandler logId:muRequestDic andDelegate:self];
}

- (IBAction)getHelpSignInButtonAction:(id)sender {
    ForgotPasswordViewController *forgotVC = [self.storyboard instantiateViewControllerWithIdentifier:mForgotPasswordStoryboardID];
    [self.navigationController pushViewController:forgotVC animated:YES];
}

- (IBAction)onBackButton:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)tapFacebookSignIn:(id)sender {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    self.isSocialLogin = YES;
    self.socialType = kFacebook ;
    FBLoginHandler *handler = [FBLoginHandler sharedInstance];
    [handler loginWithFacebook:self];
    [handler setDelegate:self];
}

- (IBAction)tapGoogleSignIn:(id)sender {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    self.isSocialLogin = YES;
    self.socialType = kGoogle;
    GIDSignIn *signin = [GIDSignIn sharedInstance];
    [signin signOut];
    signin.shouldFetchBasicProfile = true;
    signin.delegate = self;
    signin.uiDelegate = self;
    [signin signIn];
}

- (IBAction)tapCreateAccount:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)tapToDismissKeyboard:(id)sender {
    [self.view endEditing:YES];
}

- (NSString *)getFullPhoneNumber {
    NSString *text = [NSString stringWithFormat:@"%@%@", phoneCode, self.phoneField.text];
    return text;
}

- (void)resetCountryCodeButton {
    NSString *codeString = [NSString stringWithFormat:@"%@ %@", countryCode,phoneCode];
    [_codeButton setTitle:codeString forState:UIControlStateNormal];
}

- (void)signupWithSocial {
    SignupDetail *signupInfo = [[SignupDetail alloc] init];
    signupInfo.phoneFlag = NO;
    
    if (_socialType == kGoogle) {
        signupInfo.type = mUserGooglePlusSignup;
        signupInfo.fullName = self.googlePlusName;
        signupInfo.email = self.googlePlusEmail;
        signupInfo.googlePlusID = self.googlePlusId;
        signupInfo.googlePlusToken = self.googlePlusUserAccessToken;
    } else if (_socialType == kFacebook) {
        signupInfo.type = mUserFacebookSignup;
        signupInfo.fullName = self.fullNameForFb;
        signupInfo.email = self.faceBookUserEmail;
        signupInfo.facebookToken = self.faceBookUniqueIdOfUser;
        signupInfo.facebookID = self.faceBookUniqueIdOfUser;
    }
    
    /* It's for temporarily fixing fb/gp issue. Needs to remove after fix web api */
    
    
    signupInfo.city = flStrForObj(getLocation.currentCity);
    signupInfo.latitue = self.currentLat;
    signupInfo.longitude = self.currentLong;
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [WebServiceHandler newRegistration:[signupInfo getDictionary] andDelegate:self];
    
    
    /* ------------------  */
    
    
    /* Need to remove mark after fb/gp web api is fixed
     
    CreatPasswordViewController *passwordVC = [self.storyboard instantiateViewControllerWithIdentifier:@"creatPasswordVC"];
    passwordVC.signupInfo = signupInfo;
    [self.navigationController pushViewController:passwordVC animated:YES];
     
     */
}

- (void)didSignupWithUser:(UserDetails *)user {
    [Helper storeUserLoginDetails:user];
    [[NSUserDefaults standardUserDefaults]setObject:[NSString stringWithFormat:@"%@",user.userId] forKey:@"userId"];
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"recent_login"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    // Log User Device Details
    NSDictionary *param = [CommonMethods updateDeviceDetailsForAdmin];
    [WebServiceHandler logUserDevice:param andDelegate:self];
    //Check Campaign
    NSDictionary *requestDic = @{mauthToken : [Helper userToken] };
    [WebServiceHandler runCampaign:requestDic andDelegate:self];
    
    [self.parentViewController dismissViewControllerAnimated:YES completion:nil];
    
    // MQTT Connection Created
    MQTT *mqttModel = [MQTT sharedInstance];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [mqttModel createConnection];
    });
}

#pragma mark - WebServiceHandler Delegate

- (void)didFinishLoadingRequest:(RequestType)requestType withResponse:(id)response error:(NSError *)error {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    if (requestType == RequestTypeLogin) {
        UserDetails *user = (UserDetails *)response;
        switch (user.code) {
            case 200:
                [self didSignupWithUser:user];
                break;
            case 204: {
                if (self.isSocialLogin) {
                    self.isSocialLogin = NO;
                    self.signupFlag =  YES;
                    
                    if(self.socialType == kGoogle)
                    {
                        self.SignUpType = mUserGooglePlusSignup;
                    }
                    else
                    {
                        self.signUpWithFacebook = YES;
                        self.fbResponseDetails =  self.fbLoginDetails;
                        self.fbloggedUserAccessToken = self.fbloggedUserAccessToken;
                        self.SignUpType = mUserFacebookSignup;
                    }
                    
                    [self signupWithSocial];
                } else {
                    [Helper showAlertWithTitle:NSLocalizedString(muserNotFoundAlertTitle, muserNotFoundAlertTitle) Message:NSLocalizedString(userNotFound, userNotFound) viewController:self];
                }
            }
                break;
            default:
                [Helper showAlertWithTitle:NSLocalizedString(mincorrectPasswordAlertTitle, mincorrectPasswordAlertTitle) Message:NSLocalizedString(incorrectPasswordMessage, incorrectPasswordMessage) viewController:self];
                break ;
        }
    } else if (requestType == RequestTypenewRegister) {
        UserDetails *user = response;
        
        switch (user.code) {
            case 200:
                [self didSignupWithUser:user];
                break;
            default:
                break;
        }
    }
}

#pragma mark - Google Login  handler -
/*--------------------------------------------*/

/**
 Google Login Handler delegate methods.
 
 
 @param signIn Google SignedIn object.
 @param user   signed User Details.
 @param error  error if signIn was not successful.
 */
- (void)signIn:(GIDSignIn *)signIn didSignInForUser:(GIDGoogleUser *)user withError:(NSError *)error {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    if (error == nil) {
        
        self.googlePlusUserAccessToken = user.authentication.accessToken;
        self.googlePlusId = user.userID ;
        self.googlePlusEmail  = user.profile.email ;
        self.googlePlusName = user.profile.name;
        self.googlePlusProfileUrl = [user.profile imageURLWithDimension:300].absoluteString;
        
        NSDictionary *requestDict = @{mLoginType  :mUserGooglePlusLogin ,
                                      mGooglePlusId :flStrForObj(user.userID),
                                      mEmail :flStrForObj(user.profile.email),
                                      mpushToken   :flStrForObj([Helper deviceToken]),
                                      mlatitude : [NSString stringWithFormat:@"%lf",self.currentLat],
                                      mlongitude : [NSString stringWithFormat:@"%lf",self.currentLong],
                                      mCity : flStrForObj(getLocation.currentCity),
                                      mCountryShortName : flStrForObj(getLocation.countryShortCode),
                                      
                                      };
        
        /* It's for temporarily fixing fb/gp issue. Needs to remove after fix web api */
        
        requestDict = @{
                        mLoginType   : mUserNormalLogin,
                        mPswd : flStrForObj(user.userID),
                        mEmail :flStrForObj(user.profile.email),
                        mUserName :flStrForObj(user.profile.email),
                        mlatitude : [NSString stringWithFormat:@"%lf",self.currentLat],
                        mlongitude : [NSString stringWithFormat:@"%lf",self.currentLong],
                        mCity : flStrForObj(getLocation.currentCity),
                        mCountryShortName : flStrForObj(getLocation.countryShortCode),
                        mpushToken   :flStrForObj([Helper deviceToken]),
                        mplace : @"",
                        };
        
        /* ---------------- */
        
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [WebServiceHandler logId:requestDict andDelegate:self];
        
        
    } else {
        NSLog(@"%@", error.localizedDescription);
    }
}

/**
 SignIn cancelled by user.
 
 @param signIn Google SignedIn object.
 @param user   user Details fetched before cancelling Sign In.
 @param error  error if any.
 */
- (void)signIn:(GIDSignIn *)signIn didDisconnectWithUser:(GIDGoogleUser *)user withError:(NSError *)error {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    // Perform any operations on signed in user here.
    if (error == nil) {
        
    } else {
        NSLog(@"%@", error.localizedDescription);
    }
}

- (void)signInWillDispatch:(GIDSignIn *)signIn error:(NSError *)error {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    if(error){
        UIAlertController *alertController = [CommonMethods showAlertWithTitle:NSLocalizedString(alertError, alertError) message:NSLocalizedString(mGoogleLoginErrorMessage, mGoogleLoginErrorMessage) actionTitle:NSLocalizedString(alertOk, alertOk)];
        [self presentViewController:alertController animated:YES completion:nil];
    }
}


/**
 This method will Present the google LogIn Controller over present ViewController.
 
 @param viewController viewController refrence object.
 */
- (void)presentSignInViewController:(UIViewController *)viewController {
    [[self navigationController] presentViewController:viewController animated:YES completion:nil];
}


/**
 Dismiss ViewController on Successfull fetching userDetails or on cancelling.
 
 @param signIn         signInDescription Object.
 @param viewController refrence object of viewController.
 */
- (void)signIn:(GIDSignIn *)signIn dismissViewController:(UIViewController *)viewController {
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - facebook handler

/**
 Facebook handler get call on success of facebook service.
 
 @param userInfo user information in dictionary.
 */
- (void)didFacebookUserLoginWithDetails:(NSDictionary*)userInfo {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    self.fbloggedUserAccessToken = flStrForObj([FBSDKAccessToken currentAccessToken].tokenString);
    
    
    NSDictionary *requestDict = @{mLoginType :mUserFacebookLogin,
                                  mfaceBookId :flStrForObj(userInfo[@"id"]),
                                  mEmail :flStrForObj(userInfo[@"email"]),
                                  mpushToken   :flStrForObj([Helper deviceToken]),
                                  mlatitude : [NSString stringWithFormat:@"%lf",self.currentLat],
                                  mlongitude : [NSString stringWithFormat:@"%lf",self.currentLong],
                                  mCity : flStrForObj(getLocation.currentCity),
                                  mCountryShortName : flStrForObj(getLocation.countryShortCode),
                                  }
    ;
    
    /* It's for temporarily fixing fb/gp issue. Needs to remove after fix web api */
    
    requestDict = @{
                    mLoginType   : mUserNormalLogin,
                    mPswd : flStrForObj(userInfo[@"id"]),
                    mEmail :flStrForObj(userInfo[@"email"]),
                    mUserName :flStrForObj(userInfo[@"email"]),
                    mlatitude : [NSString stringWithFormat:@"%lf",self.currentLat],
                    mlongitude : [NSString stringWithFormat:@"%lf",self.currentLong],
                    mCity : flStrForObj(getLocation.currentCity),
                    mCountryShortName : flStrForObj(getLocation.countryShortCode),
                    mpushToken   :flStrForObj([Helper deviceToken]),
                    mplace : @"",
                    };
    
    /* ---------------- */
    
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [WebServiceHandler logId:requestDict andDelegate:self];
    
    self.fbLoginDetails = userInfo;
    self.faceBookUniqueIdOfUser = userInfo[@"id"];
    self.faceBookUserEmail  =  flStrForObj( userInfo[@"email"]);
    self.fullNameForFb = flStrForObj( userInfo[@"name"]);
    if (!(self.faceBookUserEmail.length >1)) {
        NSString *UniqueMailId = [self.faceBookUniqueIdOfUser stringByAppendingString:@"@facebook.com"];
        self.faceBookUserEmail = UniqueMailId;
    }
    NSURL *pictureURL = [NSURL URLWithString:[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large&return_ssl_resources=1", self.faceBookUniqueIdOfUser]];
    self.profilepicurlFb = flStrForObj(pictureURL.absoluteString);
    
}

- (void)didFailWithError:(NSError *)error {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

- (void)didUserCancelLogin {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

#pragma mark - GetCurrentLocation Delegate

- (void)updatedLocation:(double)latitude and:(double)longitude
{
    self.currentLat = latitude;
    self.currentLong = longitude;
}

- (void)updatedAddress:(NSString *)currentAddress
{
    self.currentAddress = currentAddress ;
    
}

@end
