//
//  LanguagesOptionTableCell.h
//  MobiVenta
//
//  Created by Rahul Sharma on 11/01/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LanguagesOptionTableCell : UITableViewCell
- (void)setLanguageName:(NSString *)name andIsSelected:(BOOL)selected;
@property (nonatomic, strong)NSString *previousSelection;
@property (weak, nonatomic) IBOutlet UIImageView *selectionImageView;
@property (weak, nonatomic) IBOutlet UILabel *languageLabel;
    
@end
