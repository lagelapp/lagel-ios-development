//
//  OtpScreenViewController.m

//
//  Created by Rahul Sharma on 19/04/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "OtpScreenViewController.h"
#import "FontDetailsClass.h"
#import "UserDetails.h"
#import "Lagel-Swift.h"


#define MAXLENGTH   1

#define mEnter4DigitCode        @"Enter the code that we sent to "
#define mSentence               @"Resend Code."
#define mNavigationTitle        @"Mobile Confirmation"


@interface OtpScreenViewController ()<UIGestureRecognizerDelegate>
{
    UILabel *errorMessageLabelOutlet;
    bool flag;
}
@end


@implementation OtpScreenViewController

#pragma mark
#pragma mark - view controller

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.backButtonOutLet rotateButton];
    _phnNumbLabel.text = [_numb stringByAppendingString:@"."];
    self.navigationController.navigationBar.hidden = YES ;
    self.activityViewOutlet.hidden = YES;
    self.continueButtonOutlet.backgroundColor = mSignupButtonDisableBackgroundColor;
    [self customeError];

}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    if(!self.updateNumber)
    {
    [self genrateOtp];
    }
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(veiwMoveAccKeyboardInOTP:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(veiwMoveToOriginalInOTP) name:UIKeyboardWillHideNotification object:nil];
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    [self.otpTextfield1 becomeFirstResponder];
    self.otpTextfield1.text = @"";
}


-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:YES];
    [self.view endEditing:YES];
    [[ProgressIndicator sharedInstance] hideProgressIndicator];
    [[NSNotificationCenter defaultCenter] removeObserver:UIKeyboardWillShowNotification];
    [[NSNotificationCenter defaultCenter] removeObserver:UIKeyboardWillHideNotification ];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
}

-(BOOL)prefersStatusBarHidden
{
    return YES;
}


-(void)genrateOtp
{
    
    NSDictionary *requestDict = @{mphoneNumber    : self.numb,
                                  mDeviceId: flStrForObj([[[UIDevice currentDevice] identifierForVendor] UUIDString])
                                  };
    [WebServiceHandler generateOtp:requestDict andDelegate:self];
}


#pragma mark - Move View

/**
 This method will move view by evaluating keyboard height.
 
 @param notification post by Notificationcentre.
 */
-(void)veiwMoveAccKeyboardInOTP :(NSNotification *)notification {
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    //Given size may not account for screen rotation
    NSInteger HeightOfkeyboard = MIN(keyboardSize.height,keyboardSize.width);
    float maxY = CGRectGetMaxY(self.continueButtonOutlet.frame) + HeightOfkeyboard +3;
    float reminder = CGRectGetHeight(self.view.frame) - maxY;
    if (reminder < 0) {
        [UIView animateWithDuration:0.4 animations: ^ {
            CGRect frameOfView = self.view.frame;
            frameOfView.origin.y = reminder;
            self.view.frame = frameOfView;}];
    }
}

/**
 This method will move view back to original frame.
 */
-(void)veiwMoveToOriginalInOTP
{
    [UIView animateWithDuration:0.4 animations: ^ {
        CGRect frameOfView = self.view.frame;
        frameOfView.origin.y = 0;
        self.view.frame = frameOfView;}];
}


#pragma mark
#pragma mark - TextField Delegate


-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    switch (textField.tag) {
        case 1:
            [self.otpTextfield2 becomeFirstResponder];
            break;
        case 2:
            [self.otpTextfield3 becomeFirstResponder];
            break;
        case 3:
            [self.otpTextfield4 becomeFirstResponder];
            break;

        case 4:
            [self continueButtonAction:self];
            self.continueButtonOutlet.enabled = YES;
            break;

        default:
            break;
    }
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    // Prevent crashing undo bug – see note below.
    if(range.length + range.location > textField.text.length)
    {
        return NO;
    }
    
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    return newLength <= 1;
}



#pragma mark - WebServiceDelegate

- (void) didFinishLoadingRequest:(RequestType)requestType withResponse:(id)response error:(NSError*)error {
    //handling response.
    
    [self.activityViewOutlet stopAnimating];
    self.activityViewOutlet.hidden = YES;
    
    
    if (error) {
        UIAlertController *controller = [CommonMethods showAlertWithTitle:@"Error" message:[error localizedDescription] actionTitle:@"Ok"];
        mPresentAlertController;
        return;
    }
    //storing the response from server to dictonary.
    NSDictionary *responseDict = (NSDictionary*)response;
    if (requestType == RequestTypeotpGeneration) {
        
        switch ([responseDict[@"code"] integerValue]) {
            case 200: {
                receivedOTP =response[@"data"];
//                [_otpTextField setPlaceholder:response[@"data"]];
            }
                break;
            case 429: {
                [self errAlert:responseDict[@"message"]];
                }
                break;
            default:
            {
                //[self errAlert:responseDict[@"message"]];
            }
                break;
        }
    }
    
    //if user registering with phone number then we will get this response.
    if (requestType == RequestTypenewRegister ) {
        UserDetails *user = response ;
        switch (user.code) {
            case 200: {
                //save username
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:mSignupFirstTime];
                [[NSUserDefaults standardUserDefaults] synchronize];
                [[NSUserDefaults standardUserDefaults]setObject:user.username forKey:@"newRegisterUsername"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"recent_login"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                [Helper storeUserLoginDetails:user];
                NSDictionary *param = [CommonMethods updateDeviceDetailsForAdmin];
                [WebServiceHandler logUserDevice:param andDelegate:self];
                [self.parentViewController dismissViewControllerAnimated:YES completion:nil];
                [self.view endEditing:YES];
                
                // creating connection
                MQTT *mqttModel = [MQTT sharedInstance];
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                    [mqttModel createConnection];
                });
            }
                break;
            default:
                flag = NO;
                [self  showingErrorAlertfromTop:user.message];
                break;
        }
    }
    
    if(requestType == RequestTypeupdatePhoneNumber)
    {
        switch ([responseDict[@"code"] integerValue]) {
            case 200: {
                NSString *phoneNumber = responseDict[@"result"][0][@"phoneNumber"];
                [[NSNotificationCenter defaultCenter]postNotificationName:mUpdateNumberNotification object:[NSDictionary dictionaryWithObject:phoneNumber forKey:@"updatedPhoneNumber"]];
                [self.navigationController popToRootViewControllerAnimated:YES];
            }
                break;
            default:
                flag = NO ;
                [self errAlert:responseDict[@"message"]];
                break;
        }
    }

}

- (void)errAlert:(NSString *)message {
    //creating alert for error message
    UIAlertController *controller = [CommonMethods showAlertWithTitle:@"Message" message:message actionTitle:@"Ok"];
    mPresentAlertController;
}


- (IBAction)textFieldValueChangedAction:(id)sender {
    UITextField *textfield = (UITextField *)sender;
    
    switch (textfield.tag) {
        case 1:{
            if(self.otpTextfield1.text.length!=0){
                [self.otpTextfield2 becomeFirstResponder];
            }
             self.otpTextfield2.text = @"";
             }
            break;
        case 2:{
            if(self.otpTextfield2.text.length!=0)
            [self.otpTextfield3 becomeFirstResponder];
            self.otpTextfield3.text = @"";
        }
            break;
        case 3:{
            if(self.otpTextfield3.text.length!=0)
            [self.otpTextfield4 becomeFirstResponder];
            self.otpTextfield4.text = @"";
        }
            break;
        case 4:{
            if(self.otpTextfield4.text.length!=0)
                [self resignFirstResponder];
             [self continueButtonAction:self];
        }
            break;
            
        default:
            break;
    }
    if (_otpTextfield1.text.length == 1 && _otpTextfield2.text.length == 1 && _otpTextfield3.text.length == 1 && _otpTextfield4.text.length == 1) {
   
        self.continueButtonOutlet.backgroundColor = mBaseColor;
    }
    else {
        
        self.continueButtonOutlet.backgroundColor = mLoginButtonDisableBackgroundColor;
    }
}

- (IBAction)tapToDismissKeyboard:(id)sender {
    
    [self.view endEditing:YES];
}



-(void)customeError
{
    errorMessageLabelOutlet = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 50)];
    errorMessageLabelOutlet.backgroundColor = mBaseColor;
    errorMessageLabelOutlet.textColor = [UIColor whiteColor];
    errorMessageLabelOutlet.textAlignment = NSTextAlignmentCenter;
    [errorMessageLabelOutlet setHidden:YES];
    [self.view addSubview:errorMessageLabelOutlet];
}
-(void)showingErrorAlertfromTop:(NSString *)message {
    
[errorMessageLabelOutlet setHidden:NO];
[errorMessageLabelOutlet setFrame:CGRectMake(0,50, [UIScreen mainScreen].bounds.size.width, 30)];
[self.view layoutIfNeeded];
errorMessageLabelOutlet.text = message;
[UIView animateWithDuration:0.4 animations:
 ^ {
     
     [self.view layoutIfNeeded];
 }];

int duration = 2; // duration in seconds
dispatch_after(dispatch_time(DISPATCH_TIME_NOW, duration * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
    [UIView animateWithDuration:0.4 animations:
     ^ {
         [self->errorMessageLabelOutlet setFrame:CGRectMake(0,0, [UIScreen mainScreen].bounds.size.width, 100)];
         [self->errorMessageLabelOutlet setHidden:YES];
         [self.view layoutIfNeeded];
     }];
});

}

- (IBAction)continueButtonAction:(id)sender {
    
    if(!flag)
    {
      flag = YES;
     self.activityViewOutlet.hidden = NO;
    [self.activityViewOutlet startAnimating];
    getCodeFromTextfields = [[[[NSString stringWithFormat:@"%@",self.otpTextfield1.text] stringByAppendingString:self.otpTextfield2.text]stringByAppendingString:self.otpTextfield3.text]stringByAppendingString:self.otpTextfield4.text];
    
    
    if(_updateNumber)
    {
        NSDictionary *reqDic = @{mauthToken : [Helper userToken],
                                 motp : getCodeFromTextfields,
                                 mphoneNumber : self.numb};
        
        [WebServiceHandler RequestTypeupdatePhoneNumber:reqDic andDelegate:self];
    }
    else if ([getCodeFromTextfields isEqualToString:receivedOTP]||[getCodeFromTextfields isEqualToString:@"1111" ]) {
        [WebServiceHandler newRegistration:self.paramsDict andDelegate:self];
    }
    
    else {
        flag = NO;
        [self.activityViewOutlet stopAnimating];
        self.activityViewOutlet.hidden = YES;
        [self showingErrorAlertfromTop:@"Entered OTP code is Wrong"];
    }
    }

}
- (IBAction)backButtonAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)resendButtonAction:(id)sender {
    
    self.otpTextfield1.text = self.otpTextfield2.text = self.otpTextfield3.text = self.otpTextfield4.text = @"";
    [self genrateOtp];
    NSString *alertMessage = @"OTP resent successfully.";
    [self showingErrorAlertfromTop:alertMessage];
}
@end
