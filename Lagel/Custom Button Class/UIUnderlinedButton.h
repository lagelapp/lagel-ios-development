//
//  UIUnderlinedButton.h
//  Lagel
//
//  Created by admin on 2018/10/23.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIUnderlinedButton : UIButton {
    
}


+ (UIUnderlinedButton*) underlinedButton;
@end
