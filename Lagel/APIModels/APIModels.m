//
//  APIModels.m
// 
//
//  Created by Rahul Sharma on 18/09/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "APIModels.h"
#import "AFNetworking.h"
#import "Reachability.h"

#define Token_Expired               @"unauthorized (401)"
#define request_Timeout             @"The request timed out"

@implementation APIModels

#pragma mark - HTTPS Security Methods

+ (NSString *)httpsSecurityHeaderForManager :(AFHTTPSessionManager *)manager{
    
    AFSecurityPolicy *securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
    securityPolicy.allowInvalidCertificates = YES;
    [securityPolicy setValidatesDomainName:NO];
    manager.securityPolicy = securityPolicy;
     NSString *authHeader = [NSString stringWithFormat:@"%@:%@",authUsername,authPassword];
    NSData *plainData = [authHeader dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64String = [plainData base64EncodedStringWithOptions:kNilOptions];  // iOS 7+
    
    return base64String;
}


+(BOOL)networkAvailable{
    Reachability* reach = [Reachability sharedInstance];
    reach = [Reachability reachabilityWithHostname:@"www.google.com"];
    if ([reach isReachable]){
        return YES;
    }
    else {
        return NO;
    }
}


#pragma mark -
#pragma mark - POST Manager -

/*
 @method makePostRequestFor
 @abstract Private method to make request and forward the response to calling class
 @param requestType - Request Type for the request
 @param request - NSURLRequest for the request
 @param delegate - id<WebServiceHandlerDelegate> calling class object
 @result void
 */
+ (void)makePostRequest:(RequestType)requestType
                   path:(NSString *)path
                 params:(NSDictionary *)params
               onComplition:(void (^)(NSDictionary  *response ,NSError *error, BOOL isSuccess ))completionBlock
{
    
    if ([self networkAvailable]){
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:iPhoneBaseURL]];
        manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments|NSJSONReadingMutableContainers];
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json",@"text/json", nil];
        
        NSString *base64String =  [self httpsSecurityHeaderForManager:manager];
        
        [manager.requestSerializer setValue:[NSString stringWithFormat:@"Basic %@",base64String] forHTTPHeaderField:@"Authorization"];
        NSLog(@"%@", path);
        [manager POST:path parameters:params progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
            //here is place for code executed in success case
            completionBlock(responseObject,nil,YES);
            
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            NSLog(@"%@", error);
            completionBlock(nil,error,NO);
            if([[error localizedDescription] containsString :@"bad gateway (502)"])
            {
                error = nil ;
            }
            if([[error localizedDescription] containsString :request_Timeout])
            {
                error = nil ;
                
            }
            
            
            if([[error localizedDescription] containsString :Token_Expired] )
            {
                UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                
                UITabBarController *tabBarViewCont = [story instantiateViewControllerWithIdentifier:@"TabBarStoryboardID"];              [CommonMethods authTokenExpired_UserLoggedInOtherDeviceForview:nil  andTaBbar:tabBarViewCont];
            }
            
        }];
        
    } else {
      
    }
}


+ (void) makePostRequest:(RequestType)requestType path:(NSString*)path params:(NSDictionary*)params bsaeUrl : (NSString *)baseUrl delegate:(id<WebServiceHandlerDelegate>)delegate  {
    
    if ([self networkAvailable]){
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:baseUrl]];
        manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments|NSJSONReadingMutableContainers];
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json",@"text/json", nil];
        
        NSString *base64String =  [self httpsSecurityHeaderForManager:manager];
        
        [manager.requestSerializer setValue:[NSString stringWithFormat:@"Basic %@",base64String] forHTTPHeaderField:@"authorization"];
        [manager POST:path parameters:params progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
            //here is place for code executed in success case
            
            if (delegate && [delegate respondsToSelector:@selector(didFinishLoadingRequest:withResponse:error:)]) {
                [delegate didFinishLoadingRequest:requestType withResponse:responseObject error:nil];
                NSLog(@"%@", responseObject);
            }
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            
            //here is place for code executed in success case
            if (delegate && [delegate respondsToSelector:@selector(didFinishLoadingRequest:withResponse:error:)]) {
                
                if([[error localizedDescription] containsString :@"bad gateway (502)"])
                {
                    error = nil ;
                }
                if([[error localizedDescription] containsString :request_Timeout])
                {
                    error = nil ;
                    
                }
                
                
                if([[error localizedDescription] containsString :Token_Expired] )
                {
                    UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                    
                    UITabBarController *tabBarViewCont = [story instantiateViewControllerWithIdentifier:@"TabBarStoryboardID"];              [CommonMethods authTokenExpired_UserLoggedInOtherDeviceForview:nil  andTaBbar:tabBarViewCont];
                }
                else
                {
                    
                    [delegate didFinishLoadingRequest:requestType withResponse:nil error:error];
                }
                
            }
        }];
        
    } else {
        if (delegate && [delegate respondsToSelector:@selector(internetIsNotAvailable:)]) {
            [delegate internetIsNotAvailable:requestType];
        }
    }
}


#pragma mark-

#pragma mark - PUT Manager-

+ (void) makePutRequest:(RequestType)requestType
                   path:(NSString*)path
                 params:(NSDictionary*)params
               bsaeUrl : (NSString *)baseUrl
               delegate:(id<WebServiceHandlerDelegate>)delegate
{
    if ([self networkAvailable]){
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:baseUrl]];
        manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments|NSJSONReadingMutableContainers];
        
        //    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json",@"text/json", nil];
        
        NSString *base64String =  [self httpsSecurityHeaderForManager:manager];
        
        [manager.requestSerializer setValue:[NSString stringWithFormat:@"Basic %@",base64String] forHTTPHeaderField:@"authorization"];
        
        [manager PUT:path parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
            
            //here is place for code executed in success case
            
            if (delegate && [delegate respondsToSelector:@selector(didFinishLoadingRequest:withResponse:error:)]) {
                NSLog(@"%@", responseObject);
                [delegate didFinishLoadingRequest:requestType withResponse:responseObject error:nil];
                
            }
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            
            //here is place for code executed in success case
            if (delegate && [delegate respondsToSelector:@selector(didFinishLoadingRequest:withResponse:error:)]) {
                if([[error localizedDescription] containsString :@"bad gateway (502)"])
                {
                    error = nil ;
                }
                else if([[error localizedDescription] containsString :Token_Expired] )
                {
                    UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                    
                    UITabBarController *tabBarViewCont = [story instantiateViewControllerWithIdentifier:@"TabBarStoryboardID"];              [CommonMethods authTokenExpired_UserLoggedInOtherDeviceForview:nil  andTaBbar:tabBarViewCont];
                }
                else
                {
                    [delegate didFinishLoadingRequest:requestType withResponse:nil error:error];
                }
            }
            
        }];
    }
    else {
        if (delegate && [delegate respondsToSelector:@selector(internetIsNotAvailable:)]) {
            [delegate internetIsNotAvailable:requestType];
        }
    }
}


#pragma mark -
#pragma mark - POST For Manager -

+ (void) makePostRequestFor:(RequestType)requestType
                       path:(NSString*)path
                     params:(NSDictionary*)params
                   bsaeUrl : (NSString *)baseUrl
                   delegate:(id<WebServiceHandlerDelegate>)delegate
{
    
    
    if ([self networkAvailable]){
        [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status){
            if (status == AFNetworkReachabilityStatusNotReachable) {
                if (delegate && [delegate respondsToSelector:@selector(internetIsNotAvailable:)]) {
                    [delegate internetIsNotAvailable:requestType];
                }
            }
            else {
                AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:baseUrl]];
                manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments|NSJSONReadingMutableContainers];
                
                manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json",@"text/json", nil];
                [manager POST:path parameters:params progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
                    //here is place for code executed in success case
                    
                    if (delegate && [delegate respondsToSelector:@selector(didFinishLoadingRequest:withResponse:error:)]) {
                        
                        [delegate didFinishLoadingRequest:requestType withResponse:responseObject error:nil];
                    }
                } failure:^(NSURLSessionDataTask *task, NSError *error) {
                    
                    //here is place for code executed in success case
                    if (delegate && [delegate respondsToSelector:@selector(didFinishLoadingRequest:withResponse:error:)]) {
                        
                        if([[error localizedDescription] containsString :@"bad gateway (502)"])
                        {
                            error = nil ;
                        }
                        
                        else if([[error localizedDescription] containsString :Token_Expired] )
                        {
                            UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                            
                            UITabBarController *tabBarViewCont = [story instantiateViewControllerWithIdentifier:@"TabBarStoryboardID"];              [CommonMethods authTokenExpired_UserLoggedInOtherDeviceForview:nil  andTaBbar:tabBarViewCont];
                        }
                        else
                        {
                            [delegate didFinishLoadingRequest:requestType withResponse:nil error:error];
                        }
                    }
                    
                }];
            }
        }];
    }
    
    else {
        if (delegate && [delegate respondsToSelector:@selector(internetIsNotAvailable:)]) {
            [delegate internetIsNotAvailable:requestType];
        }
    }
    
}


#pragma mark -

#pragma mark - GET Manager -

/*
 @method makeGetRequestFor
 @abstract Private method to make request and forward the response to calling class
 @param requestType - Request Type for the request
 @param request - NSURLRequest for the request
 @param delegate - id<WebServiceHandlerDelegate> calling class object
 @result void
 */
+ (void) makeGetRequest:(RequestType)requestType
                   path:(NSString*)path
                 params:(NSDictionary*)params
               bsaeUrl : (NSString *)baseUrl
               delegate:(id<WebServiceHandlerDelegate>)delegate
{
    
    if ([self networkAvailable]){
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:baseUrl]];
        
        manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments|NSJSONReadingMutableContainers];
        
        NSString *base64String =  [self httpsSecurityHeaderForManager:manager];
        
        [manager.requestSerializer setValue:[NSString stringWithFormat:@"Basic %@",base64String] forHTTPHeaderField:@"authorization"];
        
        
        
        [manager GET:path parameters:params progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            if (delegate && [delegate respondsToSelector:@selector(didFinishLoadingRequest:withResponse:error:)]) {
                
                [delegate didFinishLoadingRequest:requestType withResponse:responseObject error:nil];
            }
            
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            
            if (delegate && [delegate respondsToSelector:@selector(didFinishLoadingRequest:withResponse:error:)])
            {
                if([[error localizedDescription] containsString :@"bad gateway (502)"])
                {
                    error = nil ;
                    [[ProgressIndicator sharedInstance] hideProgressIndicator];
//                    [delegate didFinishLoadingRequest:requestType withResponse:nil error:error];
                }
                else if([[error localizedDescription] containsString :Token_Expired] )
                {
                    UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                    
                    UITabBarController *tabBarViewCont = [story instantiateViewControllerWithIdentifier:@"TabBarStoryboardID"];              [CommonMethods authTokenExpired_UserLoggedInOtherDeviceForview:nil  andTaBbar:tabBarViewCont];
                }
                else
                {
                    [delegate didFinishLoadingRequest:requestType withResponse:nil error:error];
                }
            }}];
        
    }
    else {
        if (delegate && [delegate respondsToSelector:@selector(internetIsNotAvailable:)]) {
            [delegate internetIsNotAvailable:requestType];
        }
    }
    
}


#pragma mark -
#pragma mark - DELETE Manager -

+ (void) makeDeleteRequest:(RequestType)requestType
                      path:(NSString*)path
                    params:(NSDictionary*)params
                  bsaeUrl : (NSString *)baseUrl
                  delegate:(id<WebServiceHandlerDelegate>)delegate
{
    if ([self networkAvailable]){
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:baseUrl]];
        manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments|NSJSONReadingMutableContainers];
        
        NSString *base64String =  [self httpsSecurityHeaderForManager:manager];
        
        [manager.requestSerializer setValue:[NSString stringWithFormat:@"Basic %@",base64String] forHTTPHeaderField:@"authorization"];
        
        [manager DELETE:path parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
            
            //here is place for code executed in success case
            
            if (delegate && [delegate respondsToSelector:@selector(didFinishLoadingRequest:withResponse:error:)]) {
                
                [delegate didFinishLoadingRequest:requestType withResponse:responseObject error:nil];
            }
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            
            //here is place for code executed in success case
            if (delegate && [delegate respondsToSelector:@selector(didFinishLoadingRequest:withResponse:error:)]) {
                if([[error localizedDescription] containsString :@"bad gateway (502)"])
                {
                    error = nil ;
                }
                else if([[error localizedDescription] containsString :Token_Expired] )
                {
                    UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                    
                    UITabBarController *tabBarViewCont = [story instantiateViewControllerWithIdentifier:@"TabBarStoryboardID"];              [CommonMethods authTokenExpired_UserLoggedInOtherDeviceForview:nil  andTaBbar:tabBarViewCont];
                }
                else
                {
                    
                    [delegate didFinishLoadingRequest:requestType withResponse:nil error:error];
                }
            }
            
        }];
    }
    else
    {
        if (delegate && [delegate respondsToSelector:@selector(internetIsNotAvailable:)]) {
            [delegate internetIsNotAvailable:requestType];
        }
    }
    
}

@end
