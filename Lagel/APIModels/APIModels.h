//
//  APIModels.h
//  Created by Ajay Thakur on 18/09/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"

@interface APIModels : NSObject

+(BOOL)networkAvailable ;
+(NSString *)httpsSecurityHeaderForManager :(AFHTTPSessionManager *)manager;



+ (void)makePostRequest:(RequestType)requestType
                             path:(NSString *)path
                               params:(NSDictionary *)params
           onComplition:(void (^)(NSDictionary  *response ,NSError *error, BOOL isSuccess))completionBlock;


+ (void) makePostRequest:(RequestType)requestType path:(NSString*)path params:(NSDictionary*)params bsaeUrl : (NSString *)baseUrl delegate:(id<WebServiceHandlerDelegate>)delegate ;


+ (void) makePutRequest:(RequestType)requestType   path:(NSString*)path params:(NSDictionary*)params bsaeUrl : (NSString *)baseUrl delegate:(id<WebServiceHandlerDelegate>)delegate ;


+ (void) makeGetRequest:(RequestType)requestType path:(NSString*)path params:(NSDictionary*)params bsaeUrl : (NSString *)baseUrl delegate:(id<WebServiceHandlerDelegate>)delegate ;

+ (void) makeDeleteRequest:(RequestType)requestType path:(NSString*)path params:(NSDictionary*)params bsaeUrl : (NSString *)baseUrl delegate:(id<WebServiceHandlerDelegate>)delegate ;

+ (void) makePostRequestFor:(RequestType)requestType path:(NSString*)path params:(NSDictionary*)params bsaeUrl : (NSString *)baseUrl delegate:(id<WebServiceHandlerDelegate>)delegate ;
@end
