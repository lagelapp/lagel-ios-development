//
//  LandingPageController.m
//  Created by Rahul Sharma on 12/05/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "LandingPageViewController.h"
#import "LoginSignUpViewController.h"
#import "FBLoginHandler.h"
#import "WebViewForDetailsVc.h"
#import "UserDetails.h"
#import "Lagel-Swift.h"

@class MQTT;
@interface LandingPageViewController ()<FBLoginHandlerDelegate, WebServiceHandlerDelegate,GIDSignInDelegate,GetCurrentLocationDelegate >

@end

@implementation LandingPageViewController

/*-----------------------------------*/
#pragma mark
#pragma mark -  ViewController LifeCycle -
/*-----------------------------------*/

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.loginButtonOutlet.layer.borderColor = [UIColor colorWithRed:253/255.0f green:64/255.0f blue:84/255.0f alpha:1.0f].CGColor ;
    self.signupButtonOutlet.layer.borderColor = [UIColor colorWithRed:253/255.0f green:64/255.0f blue:84/255.0f alpha:1.0f].CGColor ;
    
    getLocation = [GetCurrentLocation sharedInstance];
    [getLocation getLocation];
    getLocation.delegate = self;
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    self.navigationController.navigationBar.hidden = YES;
    
}


-(BOOL)prefersStatusBarHidden
{
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



/*-----------------------------------*/
#pragma mark
#pragma mark - Location Delegate -
/*-----------------------------------*/


/**
 Location Handler Model Delegate Methods.

 @param latitude  current lattitude fetched bu location handler.
 @param longitude  current longitude fetched bu location handler.
 */
- (void)updatedLocation:(double)latitude and:(double)longitude
{
    self.currentLat = latitude;
    self.currentLong = longitude;
}


/**
 This method will give the updated address from location handler Model.

 @param currentAddress location address.
 */
- (void)updatedAddress:(NSString *)currentAddress
{
    
    
}


/*-----------------------------------*/
#pragma mark
#pragma mark - Button Actions -
/*-----------------------------------*/

#pragma mark - LogIn Button Action -

- (IBAction)loginButtonAction:(id)sender {
    
    LoginSignUpViewController *loginVC = [self.storyboard instantiateViewControllerWithIdentifier:mLoginScreenID];
    loginVC.signupFlag = NO;
    
    [self.navigationController pushViewController:loginVC animated:YES];
    
}

#pragma mark - SignUp Button Action -

- (IBAction)signupButtonAction:(id)sender {
    
    LoginSignUpViewController *loginVC = [self.storyboard instantiateViewControllerWithIdentifier:mLoginScreenID];
    loginVC.signupFlag = YES;
    loginVC.SignUpType = mUserNormalLogin;
    [self.navigationController pushViewController:loginVC animated:YES];
}

#pragma mark - Terms&Condition Button Action -


/**
 This method will redirect user to terms & conditions Page.

 @param sender terms&conditions onject.
 */
- (IBAction)termsButtonACtion:(id)sender {
    
    WebViewForDetailsVc *newView = [self.storyboard instantiateViewControllerWithIdentifier:mDetailWebViewStoryBoardId];
    newView.showTermsAndPolicy = YES;
    [self.navigationController pushViewController:newView animated:YES];
}

#pragma mark - Privacy Button Action -


/**
 This method will redirect user to privacy page.

 @param sender privacy button object.
 */
- (IBAction)privacyButtonAction:(id)sender {
    WebViewForDetailsVc *newView = [self.storyboard instantiateViewControllerWithIdentifier:mDetailWebViewStoryBoardId];
    newView.showTermsAndPolicy = NO;
    [self.navigationController pushViewController:newView animated:YES];
    
}

#pragma mark - Close Button Action -


/**
 Dismiss Landing (Login/Signup) permission screen.

 @param sender close button object.
 */
- (IBAction)closeButtonAction:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - Facebook Login Button -


/**
 Continue with Facebook.
 This method will redirect to facebook login screen and invoke facebook Login handler.
 If user is already registered with facebook account, login directly else bring for signup.

 @param sender continue With Facebook button.
 */
- (IBAction)continueFacbookButtonAction:(id)sender {
    self.googleSignin = NO ;
    FBLoginHandler *handler = [FBLoginHandler sharedInstance];
    [handler loginWithFacebook:self];
    [handler setDelegate:self];
    
}

#pragma mark - Google Login Button -

/**
 Continue with Google.
 This method will redirect to Google login screen and invoke Google Login handler.
 If user is already registered with Google account, login directly else bring for signup.
 
 @param sender continue With Google button.
 */
- (IBAction)googleLoginAction:(id)sender {
    self.googleSignin = YES;
    GIDSignIn *signin = [GIDSignIn sharedInstance];
    [signin signOut];
    signin.shouldFetchBasicProfile = true;
    signin.delegate = self;
    signin.uiDelegate = self;
    [signin signIn];
}
/*--------------------------------------------*/
#pragma mark
#pragma mark - facebook handler
/*--------------------------------------------*/

/**
 Facebook handler get call on success of facebook service.
 
 @param userInfo user information in dictionary.
 */
- (void)didFacebookUserLoginWithDetails:(NSDictionary*)userInfo {
    
    self.fbloggedUserAccessToken = flStrForObj([FBSDKAccessToken currentAccessToken].tokenString);
    
    
    NSDictionary *requestDict = @{mLoginType :mUserFacebookLogin,
                                  mfaceBookId :flStrForObj(userInfo[@"id"]),
                                  mEmail :flStrForObj(userInfo[@"email"]),
                                  mpushToken   :flStrForObj([Helper deviceToken]),
                                  mlatitude : [NSString stringWithFormat:@"%lf",self.currentLat],
                                  mlongitude : [NSString stringWithFormat:@"%lf",self.currentLong],
                                  mCity : flStrForObj(getLocation.currentCity),
                                  mCountryShortName : flStrForObj(getLocation.countryShortCode),
                                  }
    ;
    [WebServiceHandler logId:requestDict andDelegate:self];
    
    self.fbLoginDetails = userInfo;
    self.faceBookUniqueIdOfUser = userInfo[@"id"];
    self.faceBookUserEmailId  =  flStrForObj( userInfo[@"email"]);
    self.fullNameForFb = flStrForObj( userInfo[@"name"]);
    if (!(self.faceBookUserEmailId.length >1)) {
        NSString *UniqueMailId = [self.faceBookUniqueIdOfUser stringByAppendingString:@"@facebook.com"];
        self.faceBookUserEmailId = UniqueMailId;
    }
    NSURL *pictureURL = [NSURL URLWithString:[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large&return_ssl_resources=1", self.faceBookUniqueIdOfUser]];
    self.profilepicurlFb = flStrForObj(pictureURL.absoluteString);
    
}

/**
 If facebook handler get unsuccessful rsponse.
 */
- (void)didFailWithError:(NSError *)error {
    UIAlertController *alertController = [CommonMethods showAlertWithTitle:NSLocalizedString(alertError, alertError) message:NSLocalizedString(mFacebookLoginErrorMessage, mFacebookLoginErrorMessage) actionTitle:NSLocalizedString(alertOk, alertOk)];
    [self presentViewController:alertController animated:YES completion:nil];
}


/*-----------------------------*/
#pragma mark -
#pragma mark - WebServiceDelegate
/*-----------------------------*/

/**
 Webservice delegate method will get call on success of service call.
 
 @param requestType requestType sent.
 @param response    response comes from webservice.
 @param error       error if any.
 */
- (void) didFinishLoadingRequest:(RequestType)requestType withResponse:(id)response error:(NSError*)error {
    if (error) {
        [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveLinear  animations:^{
            CGRect frameOfView = self.view.frame;
            frameOfView.origin.y = 0;
            frameOfView.origin.x=0;
            self.view.frame = frameOfView;
        } completion:^(BOOL finished) {
            //code for completion
            UIAlertController *alertController = [CommonMethods showAlertWithTitle:NSLocalizedString(alertError, alertError) message:NSLocalizedString(mLoginFailedMessage, mLoginFailedMessage) actionTitle:NSLocalizedString(alertOk, alertOk)];
            [self presentViewController:alertController animated:YES completion:nil];
        }];
        return;
    }
    
    if (requestType == RequestTypeLogin ) {
        UserDetails *user = response ;
        switch (user.code) {
            case 200: {
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"recent_login"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                [Helper storeUserLoginDetails:user];
                NSDictionary *param = [CommonMethods updateDeviceDetailsForAdmin];
                [WebServiceHandler logUserDevice:param andDelegate:self];
                [self.parentViewController dismissViewControllerAnimated:YES completion:nil];
                
                // MQTT Connection Created
                MQTT *mqttModel = [MQTT sharedInstance];
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                    [mqttModel createConnection];
                });
                
            }
                break;
            case 307: {
                LoginSignUpViewController *signupVC=[self.storyboard instantiateViewControllerWithIdentifier:mLoginScreenID];
                signupVC.signupFlag =  YES;
                signupVC.signUpWithFacebook = YES;
                signupVC.SignUpType = mUserGooglePlusSignup;
                [self.navigationController pushViewController:signupVC animated:YES];
            }
                break;
            case 204: {
                LoginSignUpViewController *signupVC=[self.storyboard instantiateViewControllerWithIdentifier:mLoginScreenID];
                signupVC.signupFlag =  YES;
                if(self.googleSignin)
                {
                signupVC.signUpWithGooglePlus = YES;
                signupVC.googlePlusName = self.googlePlusName;
                signupVC.googlePlusProfileUrl = self.googlePlusProfileUrl;
                signupVC.googlePlusId = self.googlePlusId;
                signupVC.googlePlusAccessToken = self.googlePlusUserAccessToken;
                signupVC.googlePlusEmail = self.googlePlusEmailId ;
                signupVC.SignUpType = mUserGooglePlusSignup;
                }
                else
                {
                signupVC.signUpWithFacebook = YES;
                signupVC.fbResponseDetails =  self.fbLoginDetails;
                signupVC.fbloggedUserAccessToken = self.fbloggedUserAccessToken;
                signupVC.SignUpType = mUserFacebookSignup;
                }
                [self.navigationController pushViewController:signupVC animated:YES];
            }
                break;
            default:
                break;
        }
     }
    
    }


/*--------------------------------------------*/
#pragma mark
#pragma mark - Google Login  handler -
/*--------------------------------------------*/

/**
 Google Login Handler delegate methods.
 

 @param signIn Google SignedIn object.
 @param user   signed User Details.
 @param error  error if signIn was not successful.
 */
- (void)signIn:(GIDSignIn *)signIn didSignInForUser:(GIDGoogleUser *)user withError:(NSError *)error {
    if (error == nil) {
        
        self.googlePlusUserAccessToken = user.authentication.accessToken;
        self.googlePlusId = user.userID ;
        self.googlePlusEmailId  = user.profile.email ;
        self.googlePlusName = user.profile.name;
        self.googlePlusProfileUrl = [user.profile imageURLWithDimension:300].absoluteString;
        
        NSDictionary *requestDict = @{mLoginType  :mUserGooglePlusLogin ,
                                      mGooglePlusId :flStrForObj(user.userID),
                                      mEmail :flStrForObj(user.profile.email),
                                      mpushToken   :flStrForObj([Helper deviceToken]),
                                      mlatitude : [NSString stringWithFormat:@"%lf",self.currentLat],
                                      mlongitude : [NSString stringWithFormat:@"%lf",self.currentLong],
                                      mCity : flStrForObj(getLocation.currentCity),
                                      mCountryShortName : flStrForObj(getLocation.countryShortCode),
                                      
                                      };
        [WebServiceHandler logId:requestDict andDelegate:self];
        
        
    } else {
        NSLog(@"%@", error.localizedDescription);
    }
}


/**
 SignIn cancelled by user.

 @param signIn Google SignedIn object.
 @param user   user Details fetched before cancelling Sign In.
 @param error  error if any.
 */
- (void)signIn:(GIDSignIn *)signIn didDisconnectWithUser:(GIDGoogleUser *)user withError:(NSError *)error {

    // Perform any operations on signed in user here.
    if (error == nil) {
        
    } else {
        NSLog(@"%@", error.localizedDescription);
    }
}

- (void)signInWillDispatch:(GIDSignIn *)signIn error:(NSError *)error {
    
    if(error){
    UIAlertController *alertController = [CommonMethods showAlertWithTitle:NSLocalizedString(alertError, alertError) message:NSLocalizedString(mGoogleLoginErrorMessage, mGoogleLoginErrorMessage) actionTitle:NSLocalizedString(alertOk, alertOk)];
    [self presentViewController:alertController animated:YES completion:nil];
    }
}


/**
 This method will Present the google LogIn Controller over present ViewController.

 @param viewController viewController refrence object.
 */
- (void)presentSignInViewController:(UIViewController *)viewController {
    [[self navigationController] presentViewController:viewController animated:YES completion:nil];
}


/**
 Dismiss ViewController on Successfull fetching userDetails or on cancelling.

 @param signIn         signInDescription Object.
 @param viewController refrence object of viewController.
 */
- (void)signIn:(GIDSignIn *)signIn dismissViewController:(UIViewController *)viewController {
    [self dismissViewControllerAnimated:YES completion:nil];
}



@end
