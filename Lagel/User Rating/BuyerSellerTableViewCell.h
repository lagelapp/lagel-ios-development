//
//  BuyerSellerTableViewCell.h

//
//  Created by Rahul Sharma on 26/04/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BuyerSellerTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *memberPic;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *widthImage;

@property (strong, nonatomic) IBOutlet UILabel *nameLabel;
@property (strong, nonatomic) IBOutlet UILabel *addressLabel;

@property (strong, nonatomic) IBOutlet UILabel *timeStampLabel;

@property (strong, nonatomic) IBOutlet UIButton *selectButtonOutlet;



@end
