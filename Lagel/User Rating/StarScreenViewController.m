//
//  StarScreenViewController.m
//  Picogram
//
//  Created by Rahul Sharma on 27/04/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "StarScreenViewController.h"
#import "StarTableViewCell.h"
#import "UIImageView+AFNetworking.h"
#import "WebServiceHandler.h"
#import "ProgressIndicator.h"

@interface StarScreenViewController ()<UITableViewDataSource, UITableViewDelegate, WebServiceHandlerDelegate>

@end

@implementation StarScreenViewController
{
    StarTableViewCell *cell;
}

- (void)viewDidLoad {
    self.navigationItem.title = @"Rate user";
    [super viewDidLoad];
    [self createNavLeftButton];
    // Do any additional setup after loading the view.
}
-(void)createNavLeftButton
{
    UIButton *navLeft = [CommonMethods createNavButtonsWithselectedState:mNavigationBackButtonImageName normalState:mNavigationBackButtonImageName];
    [navLeft rotateButton];
    UIBarButtonItem *navLeftButton = [[UIBarButtonItem alloc]initWithCustomView:navLeft];
    [navLeft setFrame:CGRectMake(0,0,25,40)];
    self.navigationItem.leftBarButtonItem = navLeftButton;
    [navLeft addTarget:self action:@selector(navLeftButtonAction) forControlEvents:UIControlEventTouchUpInside];
    [self.navigationItem setLeftBarButtonItems:@[[CommonMethods getNegativeSpacer],navLeftButton]];
}

-(void)navLeftButtonAction
{
    [self.navigationController popViewControllerAnimated:YES];;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    cell = [tableView dequeueReusableCellWithIdentifier:@"StarCell"
                                           forIndexPath:indexPath];
    NSString *imageURL1;
    if(self.activityResponse.count > 0)
    {
        if (self.ratiingForSeller) {
            cell.starLabel.text = [NSString stringWithFormat:@"Would you like to rate %@ ?",self.activityResponse[@"membername"]];
            imageURL1 =[NSString stringWithFormat:@"%@",_activityResponse[@"memberProfilePicUrl"]];
        }
        else {
            cell.starLabel.text = [NSString stringWithFormat:@"Would you like to sell %@ again ?",self.activityResponse[@"buyername"]];
            imageURL1 =[NSString stringWithFormat:@"%@",_activityResponse[@"buyerProfilePicUrl"]];
        }
    }
    else
    {
        if (self.ratiingForSeller) {
            cell.starLabel.text = [NSString stringWithFormat:@"Would you like to rate %@ ?",self.activityResponse[@"membername"]];
            imageURL1 =[NSString stringWithFormat:@"%@",_activityResponse[@"memberProfilePicUrl"]];
        }
        else {
            cell.starLabel.text = [NSString stringWithFormat:@"Would you like to sell %@ again ?",_responseDict[@"buyername"]];
            imageURL1 =[NSString stringWithFormat:@"%@",_activityResponse[@"buyerProfilePicUrl"]];
        }
    }
    
    if(imageURL1==nil || imageURL1==(id)[NSNull null] || [imageURL1 isEqualToString:@"(null)"] || [imageURL1 isEqualToString:@"defaultUrl"] )
    {
        cell.sellerPic.image = [UIImage imageNamed:@"contacts_profile_default_image_frame"];
    }
    else{
        NSURL *imageUrl =[NSURL URLWithString:imageURL1];
        NSURLRequest *request = [NSURLRequest requestWithURL:imageUrl];
        UIImage *placeholderImage = [UIImage imageNamed:@"contacts_profile_default_image_frame"];
        cell.sellerPic.image = placeholderImage;
        cell.sellerPic.layer.cornerRadius = cell.sellerPic.frame.size.width / 2;
        cell.sellerPic.clipsToBounds = YES;
        [cell.sellerPic layoutIfNeeded];
        [cell setNeedsLayout];
        
        
        [cell.sellerPic setImageWithURLRequest:request
                              placeholderImage:placeholderImage
                                       success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                                           
                                           self->cell.sellerPic.image = image;
                                           self->cell.sellerPic.layer.cornerRadius = self->cell.sellerPic.frame.size.width / 2;
                                           self->cell.sellerPic.clipsToBounds = YES;
                                            [self->cell.sellerPic layoutIfNeeded];
                                           [self->cell setNeedsLayout];
                                       } failure:nil];
        
        
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)SubmitButton:(id)sender {
    int rate = cell.ratingValue;
    if (self.ratiingForSeller ) {
        if (self.activityResponse) {
            NSDictionary* markSold = @{ @"token":[Helper userToken],
                                        @"rating":[NSString stringWithFormat:@"%d",rate],
                                        @"seller" : [NSString stringWithFormat:@"%@",self.activityResponse[@"membername"] ],
                                        mpostid : self.activityResponse[@"postId"]
                                        };
            [WebServiceHandler rateForSeller:markSold andDelegate:self];
            
            ProgressIndicator *HomePI = [ProgressIndicator sharedInstance];
            [HomePI showPIOnView:self.view withMessage:@"Rating..."];
        }
        
    } else{
        if(self.activityResponse.count >0) {
            NSDictionary* markSold = @{ @"token":[Helper userToken],
                                        @"postId":[NSString stringWithFormat:@"%@",self.activityResponse[@"postId"] ],
                                        @"type":@"0",
                                        @"ratings":[NSString stringWithFormat:@"%d",rate],
                                        @"membername" : [NSString stringWithFormat:@"%@",self.activityResponse[@"buyername"] ]
                                        };
            [WebServiceHandler RequestTypeMarkAsSold:markSold andDelegate:self];
            
            ProgressIndicator *HomePI = [ProgressIndicator sharedInstance];
            [HomePI showPIOnView:self.view withMessage:@"Rating..."];
        }
    }
}

-(void)didFinishLoadingRequest:(RequestType)requestType withResponse:(id)response error:(NSError *)error{
    NSDictionary *responseDict = (NSDictionary*)response;
    [[ProgressIndicator sharedInstance] hideProgressIndicator];
    
    if (requestType == RequestTypemarkSold) {
        switch ([responseDict[@"code"] integerValue]) {
            case 200:
                //// notification*///
                [[NSNotificationCenter defaultCenter]postNotificationName:mSellingToSoldNotification object:[NSDictionary dictionaryWithObject:response[@"data"] forKey:@"data"]];
                [self.navigationController popToRootViewControllerAnimated:YES];
                break;
            case 409: {
                UIAlertController *controller = [CommonMethods showAlertWithTitle:@"" message:@"Product is already sold out" actionTitle:@"Ok"];
                mPresentAlertController;
            }
                break;
            default:
                break;
        }
    }
    
    
    if (requestType == RequestTyperateForSeller) {
        switch ([responseDict[@"code"] integerValue]) {
            case 200: {
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:@"Thank you for your rating." preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction* alertFortHANKyou = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                                   {
                                                    if(self.callBackForRateUser)
                                                       {
                                                           self.callBackForRateUser(YES);
                                                       }
                                                       [self.navigationController popViewControllerAnimated:YES];
                                                   }];
                [alertController addAction:alertFortHANKyou];
                [self presentViewController:alertController animated:YES completion:nil];
            }
                break;
            default:{
                UIAlertController *controller = [CommonMethods showAlertWithTitle:@"" message:responseDict[@"message"] actionTitle:@"Ok"];
                mPresentAlertController;
            }
                break;
        }
    }
}
@end
