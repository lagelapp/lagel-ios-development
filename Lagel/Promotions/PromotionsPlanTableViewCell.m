//
//  PromotionsPlanTableViewCell.m
//  
//
//  Created by Rahul Sharma on 19/08/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "PromotionsPlanTableViewCell.h"

@implementation PromotionsPlanTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.viewForPlans.layer.borderColor = mDividerColor.CGColor ;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setViewsWithData:(NSArray *)arrayOfPlans forIndex:(NSInteger)index
{
    if(index == 0)
    {
        [self updateViewForActiveState:YES];
    }
    self.priceOfPlan.text = [NSString stringWithFormat:@"$%@",flStrForObj(arrayOfPlans[index][@"price"])];
    self.numberOfLikes.text = flStrForObj(arrayOfPlans[index][@"name"]);
}

-(void)updateViewForActiveState :(BOOL)active
{
    if(active)
    {
        self.viewForPlans.layer.borderColor = mBaseColor2.CGColor ;
        self.viewForPlans.backgroundColor = mBaseColor2;
        self.priceOfPlan.textColor = self.numberOfLikes.textColor = [UIColor whiteColor];
    }
    else
    {
        self.viewForPlans.layer.borderColor = mDividerColor.CGColor ;
        self.viewForPlans.backgroundColor = [UIColor colorWithRed:219/255.0f green:219/255.0f blue:219/255.0f alpha:0.5f];
        self.priceOfPlan.textColor = self.numberOfLikes.textColor = mTextBaseColor;
    }
    
}

@end
