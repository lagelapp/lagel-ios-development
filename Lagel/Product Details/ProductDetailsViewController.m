
//  ProductDetailsViewController.m
//  Created by Ajay Thakur on 16/09/17.
//  Copyright © 2016 Rahul Sharma. All rights reserved.

#import "ProductDetailsViewController.h"
#import "LikeCommentTableViewCell.h"
#import "SVPullToRefresh.h"
#import "CellForProfile.h"
#import "CellForDescNCond.h"
#import "CellForProdSummry.h"
#import "CellForLocationCheck.h"
#import "PinAddressController.h"
#import "LikersCollectionViewCell.h"
#import "UserProfileViewController.h"
#import "CommentsViewController.h"
#import "LikeViewController.h"
#import "MakeOfferViewController.h"
#import "ShowFullImageViewController.h"
#import "ReportPostViewController.h"
#import "ZoomTransitionProtocol.h"
#import "EditProductViewController.h"
#import "PageControl.h"
#import "ProductDetails.h"


#define mShowFullImageStoryboardID          @"showFullImageStoryboard"

@interface ProductDetailsViewController ()<WebServiceHandlerDelegate,UIGestureRecognizerDelegate,GMSMapViewDelegate,UITableViewDelegate,MakeOfferDelegate,FBSDKSharingDelegate,ZoomTransitionProtocol,PageControlDelegate>

{   UIView *navBackView, *dividerView, *headerView ;
    UIButton *navLeft, *navRight;
    UILabel *errorMessageLabelOutlet;
    NSMutableArray  *arrayOfProfilePics, *arrayOfImageUrls;
    UIImageView *headerImageView;
    CGFloat xOrigin;
    PageControl *pageControl;
    UIScrollView *scrollView;
    float viewWidth;
    int sold;
    BOOL classIsAppearing ,addBannerSubview;
    NSInteger imageCount , imageTag;
    NSString *currency,*userName;
}
@end

@implementation ProductDetailsViewController


#pragma mark -
#pragma mark - ViewController LifeCycle -

- (UIColor *)color {
    return [UIColor colorWithRed:28.0/255.0 green:180.0/255.0 blue:172.0/255.0 alpha:1.0];
}

-(void)viewDidLoad {
    [super viewDidLoad];
     [[self navigationController] setNavigationBarHidden:NO animated:YES];
    imageCount = 0;
    self.floatingButtonTopConstraint.constant = self.view.frame.size.width - 94 ;
    arrayOfProfilePics = [NSMutableArray new];
    viewWidth = self.view.frame.size.width;
    [self showLoading:self.noAnimation];
    [self setValuesFromHomeScreenData];
    [self createNavLeftButton];
    [self addPanGestureToChatButton];
    [self setNotificationObserver];
    userName = [Helper userName];

    [self webServiceForGettingPostsByID];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    
    if([[NSUserDefaults standardUserDefaults] valueForKey:@"recent_login"])
    {
       self.dataFromHomeScreen = YES ;
       self.makeOfferOutlet.enabled = NO ;
       self.floatingButtonOutlet.enabled = NO;
        userName = [Helper userName];
       [self webServiceForGettingPostsByID];
    }
    
    [self.navigationController.navigationBar setBackgroundColor:[UIColor clearColor]];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    classIsAppearing = YES;
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:YES];
    
    classIsAppearing = NO;
}

/*---------------------------------------------------*/
#pragma
#pragma mark - Status Bar.
/*---------------------------------------------------*/

-(BOOL)prefersStatusBarHidden
{
    return YES  ;
}


#pragma mark -
#pragma mark - SetValues For Data -

-(void)setValuesFromHomeScreenData
{
     CGFloat navHeight = [UIApplication sharedApplication].statusBarFrame.size.height +  self.navigationController.navigationBar.frame.size.height ;
    self.navigationController.view.backgroundColor = [UIColor clearColor];
    navBackView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width,navHeight)];
    navBackView.backgroundColor=[UIColor clearColor];
    dividerView = [[UIView alloc]initWithFrame:CGRectMake(0, navHeight -0.5, self.view.frame.size.width,0.5)];
    dividerView.backgroundColor = [UIColor clearColor] ;
    [navBackView addSubview:dividerView];
    [self.view addSubview:navBackView];
    
    if([[Helper userToken] isEqualToString:mGuestToken])
    {
        [self.makeOfferOutlet setTitle:NSLocalizedString(MakeofferButtonTitle, MakeofferButtonTitle) forState:UIControlStateNormal];
        self.makeOfferOutlet.enabled = YES;
        self.floatingButtonOutlet.enabled = YES;
    }
    else
    {
        self.makeOfferOutlet.enabled = NO;
        self.floatingButtonOutlet.enabled = NO;
        
    }
    
    
    [self headerForTableView];
    [self updatePricedetails];
    NSString *curncy = [self returnCurrency:[NSString stringWithFormat:@"%@",self.product.currency]];
    _offercurrencyLabel.text = curncy ;
    
}

-(void)showOutletsOnTheBasisOfData{
    
    if([ self.product.postedByUserName isEqualToString:userName] ||
       [ flStrForObj(self.product.membername) isEqualToString:userName])
    {
        sold = 1 ;
        self.floatingButtonView.hidden = YES;
        [_makeOfferOutlet setTitle:NSLocalizedString(MarkAsSoldButtonTitle, MarkAsSoldButtonTitle) forState:UIControlStateNormal];
    }
    else
    {   self.floatingButtonView.hidden = NO;
        
        [self createNavRightButton];
        if(self.product.sold || sold == 2)
        {   sold = 2;
            self.makeOfferOutlet.enabled = NO;
            [_makeOfferOutlet setTitle:NSLocalizedString(productSoldOut, productSoldOut) forState:UIControlStateNormal];
        }
        else
        {
            sold  = 0 ;
            [_makeOfferOutlet setTitle:NSLocalizedString(MakeofferButtonTitle, MakeofferButtonTitle) forState:UIControlStateNormal];
        }
        
    }
}



#pragma mark -
#pragma  mark - Notification Observer

-(void)setNotificationObserver
{
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(changeSellingToSold:) name:mSellingToSoldNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateFollwoStatus:) name:@"updatedFollowStatus" object:nil];
}


#pragma mark -
#pragma mark - Notification Methods

#pragma mark -
#pragma mark - Follow Status Notification

-(void)updateFollwoStatus:(NSNotification *)noti {
    
    if (!classIsAppearing) {
        
        NSIndexPath *index = [NSIndexPath indexPathForRow:2 inSection:0];
        CellForProfile *cell=[_tableView cellForRowAtIndexPath:index];
        
        //check the postId and Its Index In array.
        NSInteger followStatusRespectToUser = [noti.object[@"newUpdatedFollowData"][@"newFollowStatus"] integerValue];
        
        switch (followStatusRespectToUser) {
            case 0:{
                
            }
                break;
            case 1:{
                [cell.buttonFollowOutlet makeButtonAsFollowing];
            }
                break;
            case 2:
            {
                [cell.buttonFollowOutlet makeButtonAsFollow];
            }
                break;
                
        }
    }
}

#pragma mark -
#pragma mark - MarkAsSold Notification

-(void)changeSellingToSold:(NSNotification *)noti
{
    self.makeOfferOutlet.enabled = NO;
    [_makeOfferOutlet setTitle:NSLocalizedString(productSoldOut, productSoldOut) forState:UIControlStateNormal];
    
}

#pragma mark -
#pragma mark - Navigation Bar Buttons

//   navigation bar back button

- (void)createNavLeftButton {
    navLeft = [CommonMethods createNavButtonsWithselectedState:@"item backButton" normalState:@"item backButton"];
    [navLeft rotateButton];
    navLeft.layer.masksToBounds = NO;
    navLeft.layer.shadowOffset = CGSizeMake(0, -2);
    navLeft.layer.shadowRadius = 4;
    navLeft.layer.shadowOpacity = 0.4;
    [navLeft setTitleColor:[self color] forState:UIControlStateNormal];
    [navLeft setImage:[UIImage imageNamed:mNavigationBackButtonImageName] forState:UIControlStateNormal];
    UIBarButtonItem *navLeftButton = [[UIBarButtonItem alloc]initWithCustomView:navLeft];
    [navLeft setFrame:CGRectMake(0,0,30,30)];
    self.navigationItem.leftBarButtonItem = navLeftButton;
    [navLeft addTarget:self action:@selector(backButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    [self.navigationItem setLeftBarButtonItems:@[[CommonMethods getNegativeSpacer],navLeftButton]];
}
// navigation Report Button
- (void)createNavRightButton {
    navRight = [UIButton buttonWithType:UIButtonTypeCustom];
    [navRight setTitle:NSLocalizedString(@"Report", @"Report")  forState:UIControlStateNormal];
    navRight.titleLabel.font = [UIFont fontWithName:RobotoMedium size:15];
    [navRight setTitleColor:[self color] forState:UIControlStateNormal];
    navRight.layer.masksToBounds = NO;
    navRight.layer.shadowOffset = CGSizeMake(0, -2);
    navRight.layer.shadowRadius = 2;
    navRight.layer.shadowOpacity = 0.2;
    UIBarButtonItem *navRightButton = [[UIBarButtonItem alloc]initWithCustomView:navRight];
    [navRight setFrame:CGRectMake(0,0,30,30)];
    self.navigationItem.rightBarButtonItem = navRightButton;
    [navRight addTarget:self action:@selector(reportItem) forControlEvents:UIControlEventTouchUpInside];
    [self.navigationItem setRightBarButtonItems:@[[CommonMethods getNegativeSpacer],navRightButton]];
}

-(void)backButtonClicked {
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"TabBarSelectMake"];
    
    headerImageView = [[UIImageView alloc] initWithFrame:CGRectMake( self.view.frame.size.width
                                                                    , self.view.frame.size.width, self.view.frame.size.width,  self.view.frame.size.width)];
    headerImageView.layer.cornerRadius = 5 ;
    headerImageView.contentMode = UIViewContentModeScaleAspectFit;
    [self.navigationController popViewControllerAnimated:YES];
    
}

-(void)reportItem {
    
    if(![[Helper userToken] isEqualToString:mGuestToken])
    {
        ReportPostViewController *reportVc = [self.storyboard instantiateViewControllerWithIdentifier:@"reprtPostVc"];
        reportVc.postdetails = self.product;
        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:reportVc];
        [self presentViewController:nav animated:YES completion:nil];
    }
    else
    {
        if([[Helper userToken] isEqualToString:mGuestToken])
        {
            UINavigationController *navigationController = [CommonMethods presentLoginScreenController];
            [self presentViewController:navigationController animated:YES completion:nil];
        }
    }
}

#pragma mark -
#pragma mark - No Animations -

-(void)showLoading :(BOOL)noAnimation
{
    if(self.noAnimation)
    {
        ProgressIndicator *pi = [ProgressIndicator sharedInstance];
        [pi showPIOnView:self.view withMessage:NSLocalizedString(LoadingIndicatorTitle, LoadingIndicatorTitle)];
        self.viewForMakeOffer.hidden = YES;
        self.tableView.separatorColor = [UIColor clearColor];
    }
    else
    {
        self.viewForMakeOffer.hidden = NO;
        self.tableView.separatorColor = mTableViewCellSepratorColor;
        
    }
}


#pragma
#pragma mark -  TableView data source and delegates

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if(! self.product)
        return 0;
    else
        return mNumberOfCellsRequired;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.row) {
        case 0:
        {
            CellForProdSummry *cell=[tableView dequeueReusableCellWithIdentifier:@"cellForProdSummry" forIndexPath:indexPath];
            [cell updateFieldsForProductWithDataArray:self.product];
            return cell;
        }
            break;
        case 1:
        {
            LikeCommentTableViewCell *cell = (LikeCommentTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"LikeCommentTableViewCell"];
            
            cell.arrayOfProfilePics = arrayOfProfilePics;
            cell.product = self.product;
            cell.index = indexPath;
            cell.collectionViewCellID = mCollectionViewCellIDInProductDetails;
            cell.navController = self.navigationController;
            [cell setPropertiesOfOutletscheckFlag:self.dataFromHomeScreen];
            
            if(self.product.likeStatus){
                [cell.likeButtonOutlet setTitle:self.product.likes forState:UIControlStateSelected];
                cell.likeButtonOutlet.layer.borderColor = mBaseColor2.CGColor;
                cell.likeButtonOutlet.selected = YES;
            }
            else{
                [cell.likeButtonOutlet setTitle:self.product.likes forState:UIControlStateNormal];
                cell.likeButtonOutlet.selected = NO;
                cell.likeButtonOutlet.layer.borderColor = [UIColor colorWithRed:145/255.0 green:145/255.0 blue:145/255.0 alpha:.7].CGColor;
            }
            return cell;
        }
            break;
        case 2:
        {   CellForProfile *cell=[tableView dequeueReusableCellWithIdentifier:@"cellForProfile" forIndexPath:indexPath];
            [cell setObjectForData:self.product andDataFrom:self.dataFromHomeScreen];
            return cell;
        }
            break;
        case 3:
        {
            NSString *strTitle = @"";
            int strcount = 0;
            NSString *strNumValue = @"";
            
            if (![self.product.productDescription isEqualToString:@""]) {
                strTitle = self.product.productDescription;
                NSCharacterSet* notDigits = [[NSCharacterSet letterCharacterSet] invertedSet];
                for (int i = 0; i< [self.product.productDescription length]; i++) {
                    //            NSString *ichar  = [NSString stringWithFormat:@"%c", [self.textFieldForTitle.text characterAtIndex:i]];
                    char ch = [self.product.productDescription characterAtIndex:i];
                    NSString *ichar  = [NSString stringWithFormat:@"%c", ch];
                    if ([ichar rangeOfCharacterFromSet:notDigits].location == NSNotFound) {
                        NSLog(@"Called");
                        if (strcount > 7 && strcount < 13) {
                            strTitle = [strTitle stringByReplacingOccurrencesOfString:strNumValue withString:@" "];
                        }
                        strcount = 0;
                        strNumValue = @"";
                    } else {
                        NSLog(@"Called else");
                        NSCharacterSet* noDigits = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
                        if ([ichar rangeOfCharacterFromSet:noDigits].location == NSNotFound) {
                            NSLog(@"Called else if");
                            strcount += 1;
                        } else {
                            NSLog(@"Called else else");
                            
                        }
                        strNumValue = [strNumValue stringByAppendingString:ichar];
                    }
                }
                
                if (strcount > 7 && strcount < 13) {
                    strTitle = [strTitle stringByReplacingOccurrencesOfString:strNumValue withString:@" "];
                }
                strcount = 0;
                strNumValue = @"";
            }
            
            CellForDescNCond *cell=[tableView dequeueReusableCellWithIdentifier:@"cellForDescNCond" forIndexPath:indexPath];
            cell.labelTitle.text = NSLocalizedString(productDescriptionTitle, productDescriptionTitle);

            cell.labelForText.text = strTitle;
            return cell;
        }
            break;
        case 4:
        {
            CellForDescNCond *cell=[tableView dequeueReusableCellWithIdentifier:@"cellForDescNCond" forIndexPath:indexPath];
            cell.labelTitle.text = NSLocalizedString(productConditionTitle, productConditionTitle);
            cell.labelForText.text =  NSLocalizedString(self.product.condition, self.product.condition);
            return cell;
        }
            
            break;
        case 5:
        {
            CellForLocationCheck *cell=[tableView dequeueReusableCellWithIdentifier:@"cellForLocationCheck" forIndexPath:indexPath];
            self.lat = self.product.latitude;
            self.log = self.product.longitude;
            
            [cell setUpMapView:self.lat andLongitude:self.log andUserID:self.postedByUserID];
            
            [cell displayLocationName:self.product];
            return cell;
        }
        
            
        default:
        {
            UITableViewCell *cell = [[UITableViewCell alloc]init];
            return cell;
        }
            
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    switch (indexPath.row) {
        case 0:
            return 60;
            break;
            
        case 1:  return 97;
            break;
        case 2:
            return 60;
            break;
        case 5:
            return 250;
            break;
        default:
        {       CellForDescNCond *cell=[tableView dequeueReusableCellWithIdentifier:@"cellForDescNCond"];
            UILabel *lbl=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width,cell.labelForText.frame.size.height)];
            lbl.font=cell.labelForText.font;
            if(indexPath.row ==3){
                lbl.text = self.product.productDescription ;
            }
            else{
                lbl.text = self.product.condition ;
            }
            CGFloat labelHieght = [Helper measureHieightLabel:lbl];
            CGFloat cellHieght=labelHieght + 50;
            if([lbl.text isEqualToString:@""])
                return 0;
            return (cellHieght);
        }
            
            break;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
{
    if(indexPath.row==5)
    {
//        int distRadians = 5000;
//
//
//        NSString *lat1 = [NSString stringWithFormat:@"%f", self.lat];
//        NSString *lon1 = [NSString stringWithFormat:@"%f", self.log];
//        NSArray *lat1arr = [lat1 componentsSeparatedByString:@"."];
//        NSArray *lon1arr = [lon1 componentsSeparatedByString:@"."];
//
//        NSNumber *latnum = lat1arr.lastObject;
//        NSNumber *lonnum = lon1arr.lastObject;
//
//        int finallat = [latnum intValue] + distRadians;
//        int finallon = [lonnum intValue] + distRadians;
//
//        double latnew = [[NSString stringWithFormat:@"%@.%d",[lat1arr firstObject],finallat] doubleValue];
//        double lonnew = [[NSString stringWithFormat:@"%@.%d",[lon1arr firstObject],finallon] doubleValue];
//        NSString *userID = [[NSUserDefaults standardUserDefaults] valueForKey:@"userId"];
//        if ([self.postedByUserID isEqualToString: userID]) {
//            latnew = self.lat;
//            lonnew = self.log;
//        }
        
        PinAddressController *pinAddrVC = [self.storyboard instantiateViewControllerWithIdentifier:mPinAddressStoryboardID];
        pinAddrVC.lat = self.lat;
        pinAddrVC.longittude = self.log;
        pinAddrVC.isProductLocation = YES;
        pinAddrVC.navigationItem.title = NSLocalizedString(mNavTitleForProductLocation, mNavTitleForProductLocation) ;
        [self.navigationController pushViewController:pinAddrVC animated:YES];
    }
}

#pragma mark -
#pragma mark - Table HeaderView -

-(void) headerForTableView
{
    headerView.backgroundColor = [UIColor blackColor];
    scrollView.backgroundColor  = [UIColor blackColor];
    headerView = [[UIView alloc] initWithFrame:CGRectMake(0,-self.view.frame.size.width,self.view.frame.size.width,self.view.frame.size.width)];
    scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, headerView.frame.size.width, headerView.frame.size.height)];
    scrollView.showsVerticalScrollIndicator=NO;
    scrollView.showsHorizontalScrollIndicator=NO;
    [scrollView setPagingEnabled:YES];
    [scrollView setAlwaysBounceVertical:NO];
    [scrollView setAlwaysBounceHorizontal:NO];
    scrollView.delegate=self;
    headerImageView = [[UIImageView alloc] initWithFrame:CGRectMake(xOrigin, 0, self.view.frame.size.width, self.view.frame.size.width)];
    headerImageView.contentMode = UIViewContentModeScaleAspectFit;
    [scrollView addSubview:headerImageView];
    
    for (int i = 0; i < imageCount; i++)
    {
        xOrigin = i * scrollView.frame.size.width;
        headerImageView = [[UIImageView alloc] initWithFrame:CGRectMake(xOrigin, 0, self.view.frame.size.width, self.view.frame.size.width)];
        headerImageView.contentMode = UIViewContentModeScaleAspectFit;
        
        if(i == 0)
        {
        [headerImageView setImageWithURL:[NSURL URLWithString:[arrayOfImageUrls objectAtIndex:i]] placeholderImage:self.imageFromHome usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        }
        else
        {
        [headerImageView setImageWithURL:[NSURL URLWithString:[arrayOfImageUrls objectAtIndex:i]] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        }
        headerImageView.clipsToBounds = YES ;
        UITapGestureRecognizer *tapForImage = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageTapped:)];
        tapForImage.delegate =self;
        headerImageView.tag = i;
        headerImageView.userInteractionEnabled = YES;
        tapForImage.numberOfTapsRequired = 1 ;
        [headerImageView addGestureRecognizer:tapForImage];
        [scrollView addSubview:headerImageView];
    }
    [scrollView setContentSize:CGSizeMake(scrollView.frame.size.width * imageCount, scrollView.frame.size.height)];
    [headerView addSubview:scrollView];
    
    pageControl = [[PageControl alloc]init];
    pageControl.center = CGPointMake((viewWidth - 75)/2 ,viewWidth - 25);
    CGRect aFrame = pageControl.frame;
    aFrame.size.width = 75;
    aFrame.size.height = 20;
    pageControl.frame = aFrame;
    pageControl.numberOfPages=imageCount;
    pageControl.currentPage=0;
    pageControl.delegate = self ;
    pageControl.layer.cornerRadius=2;
    pageControl.backgroundColor=[UIColor clearColor];
    if(imageCount>1)
        [headerView addSubview:pageControl];
    _tableView.tableHeaderView = headerView;
}


#pragma mark -
#pragma mark - ScrollView Delegate -

- (void)scrollViewDidScroll:(UIScrollView *)sender {
    
    if([sender isEqual:scrollView])
    {
        CGFloat pageWidth = sender.frame.size.width;
        float fractionalPage = sender.contentOffset.x / pageWidth;
        NSInteger page = lround(fractionalPage);
        pageControl.currentPage = page;
    }
    else{
        CGFloat movedOffset=-sender.contentOffset.y;
        
        if (movedOffset<self.view.frame.size.width) {
            CGFloat ratio= -((movedOffset)/self.view.frame.size.width) ;
            if (ratio>0.8) {
                navBackView.backgroundColor = [UIColor colorWithRed:250/255.0f green:250/255.0f blue:250/255.0f alpha:1.0f];
                dividerView.backgroundColor = [UIColor colorWithRed:225/255.0f green:225/255.0f blue:225/255.0f alpha:1.0f];
                [[navLeft layer] setShadowOpacity:0.0];
                [[navLeft layer] setShadowRadius:0.0];
                [[navLeft layer] setShadowColor:[UIColor clearColor].CGColor];
                [[navRight layer] setShadowOpacity:0.0];
                [[navRight layer] setShadowRadius:0.0];
                [[navRight layer] setShadowColor:[UIColor clearColor].CGColor];
                self.title= self.product.productName;
                [navLeft setImage:[UIImage imageNamed:mNavigationBackButtonImageName] forState:UIControlStateNormal];
                [navRight setTitle:NSLocalizedString(@"Report", @"Report")  forState:UIControlStateNormal];
//                [navRight setTitleColor:mBaseColor forState:UIControlStateNormal];
                [navRight setTitleColor:[self color] forState:UIControlStateNormal];
                
            }else{
                navBackView.backgroundColor = dividerView.backgroundColor = [UIColor clearColor];
                [[navLeft layer] setShadowColor:[UIColor blackColor].CGColor];
                navLeft.layer.shadowOffset = CGSizeMake(0, -2);
                navLeft.layer.shadowRadius = 5;
                navLeft.layer.shadowOpacity = 0.5;
                [[navRight layer] setShadowColor:[UIColor blackColor].CGColor];
                navRight.layer.shadowOffset = CGSizeMake(0, -2);
                navRight.layer.shadowRadius = 5;
                navRight.layer.shadowOpacity = 0.5;
                [navLeft setImage:[UIImage imageNamed:@"item backButton"] forState:UIControlStateNormal];
                [navRight setTitle:NSLocalizedString(@"Report", @"Report")  forState:UIControlStateNormal];
//                [navRight setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                [navRight setTitleColor:[self color] forState:UIControlStateNormal];
                self.title=@"";
            }
        }
        
        else{
            CGRect frame=headerView.frame;
            frame.size.height=movedOffset-64;
            frame.origin.y=-movedOffset+64;
            headerView.frame=frame;
            
        }
        float f=_tableView.contentSize.height-_tableView.frame.size.height;
        if (_tableView.contentOffset.y >= f+30)
        {
            CGPoint offset = _tableView.contentOffset;
            offset.y = f+30;
            _tableView.contentOffset = offset;
            
        }
        
    }
}



#pragma mark -
#pragma mark - Reload Row Of TableView -

-(void)animateButton:(UIButton *)likeButton {
    CABasicAnimation *ani = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
    [ani setDuration:0.2];
    [ani setRepeatCount:1];
    [ani setFromValue:[NSNumber numberWithFloat:1.0]];
    [ani setToValue:[NSNumber numberWithFloat:0.5]];
    [ani setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
    [[likeButton layer] addAnimation:ani forKey:@"zoom"];
}

-(void)reloadRowToShowNewNumberOfLikes:(NSInteger )reloadRowAtSection {
    NSIndexPath* rowToReload = [NSIndexPath indexPathForRow:1 inSection:reloadRowAtSection];
    NSArray* rowsToReload = [NSArray arrayWithObjects:rowToReload, nil];
    [self.tableView reloadRowsAtIndexPaths:rowsToReload withRowAnimation:UITableViewRowAnimationNone];
}

/*---------------------------------------*/
#pragma
#pragma mark - Button Actions
/*---------------------------------------*/

#pragma mark - LikeButton Action -

- (IBAction)likeButtonAction:(id)sender {
    UIButton *likeButton = (UIButton *)sender;
    // adding animation for selected button
    
    if([[Helper userToken] isEqualToString:@"guestUser"]){
        UINavigationController *navigation = [CommonMethods presentLoginScreenController];
        [self.navigationController presentViewController:navigation animated:YES completion:nil];
    }
    else{
        
        [self animateButton:likeButton];
        if(likeButton.selected) {
            likeButton.selected = NO;
            self.product.likeStatus = NO ;
            NSInteger newNumberOfLikes = [self.product.likes integerValue];
            newNumberOfLikes --;
            [likeButton setTitle:[NSString stringWithFormat:@"%ld",(long)newNumberOfLikes] forState:UIControlStateNormal];
            self.product.likes  = [@(newNumberOfLikes)stringValue];
            for(int j=0;j<arrayOfProfilePics.count;j++)
            {
                if([arrayOfProfilePics[j][@"likedByUsers"] isEqualToString:userName])    // remove ProfilePic if userName has liked the post.
                {   [arrayOfProfilePics removeObjectAtIndex:j];
                    [self.product.likedByUsers removeObjectAtIndex:j];
                }
            }
            [self reloadRowToShowNewNumberOfLikes:likeButton.tag];
            [self unlikeAPost:self.product.postId postType:0];
            
            NSIndexPath *indexpathToreload = [NSIndexPath indexPathForRow:1 inSection:0];
            LikeCommentTableViewCell *cell = (LikeCommentTableViewCell *)[self.tableView cellForRowAtIndexPath:indexpathToreload];
            NSDictionary *dicForUnlike = @{
                                           @"notificationForLike":@"0",
                                           @"postdetails":self.product.responseArray
                                           };
            [[NSNotificationCenter defaultCenter] postNotificationName:mfavoritePostNotifiName object:dicForUnlike];
            [cell.likersCollectionViewOutlet reloadData];
        }
        else  {
            likeButton.selected = YES;
            
            self.product.likeStatus = YES ;
            NSInteger newNumberOfLikes = [self.product.likes integerValue];
            newNumberOfLikes ++;
            [likeButton setTitle:[NSString stringWithFormat:@"%ld",(long)newNumberOfLikes] forState:UIControlStateSelected];
            NSDictionary *dic = [NSMutableDictionary new];
            [dic setValue:self.product.profilePicUrl forKey:@"profilePicUrl"]; // add Profile pic of user If liked he post.
            [dic setValue:userName forKey:@"likedByUsers"];
            [arrayOfProfilePics insertObject:dic atIndex:0];
            [self.product.likedByUsers insertObject:dic atIndex:0];
            self.product.likes  = [@(newNumberOfLikes)stringValue];
            [self reloadRowToShowNewNumberOfLikes:likeButton.tag];
            
            [self likeAPost:self.product.postId postType:0];
            
            NSIndexPath *indexpathToreload = [NSIndexPath indexPathForRow:1 inSection:0];
            LikeCommentTableViewCell *cell = (LikeCommentTableViewCell *)[self.tableView cellForRowAtIndexPath:indexpathToreload];
            NSDictionary *dicForUnlike = @{
                                           @"notificationForLike":@"1",
                                           @"postdetails":self.product.responseArray
                                           };
            [[NSNotificationCenter defaultCenter] postNotificationName:mfavoritePostNotifiName object:dicForUnlike];
            [cell.likersCollectionViewOutlet reloadData];
        }
        
        
        if (self.isNewsFeedSocial)
        {
         NSDictionary *likeEventData = @{
                                       @"productDetails": self.product,
                                       @"indexPath": self.indexPath,
                                       @"arrayOfProfilePics" : arrayOfProfilePics
                                       };
        
         [[NSNotificationCenter defaultCenter] postNotificationName:@"likesEvent" object:likeEventData];
        }
        
    }
}

#pragma mark - All LikesButton Action -

- (IBAction)numberOfLikesButtonAction:(id)sender {
    if([[Helper userToken] isEqualToString:mGuestToken]){
        UINavigationController *navigation = [CommonMethods presentLoginScreenController];
        [self.navigationController presentViewController:navigation animated:YES completion:nil];
    }
    else{
        LikeViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"likeStoryBoardId"];
        newView.navigationTitle = NSLocalizedString(navTitleForLikes, navTitleForLikes);
        newView.postId =  self.product.postId ;
        newView.postType = 0 ;
        [self.navigationController pushViewController:newView animated:YES];
    }
}

#pragma mark - Share Button Action -

- (IBAction)shareButtonAction:(id)sender {
    
    if([[Helper userToken] isEqualToString:mGuestToken]){
        
        UINavigationController *navigation = [CommonMethods presentLoginScreenController];
        [self.navigationController presentViewController:navigation animated:YES completion:nil];
    }
    else{
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil
                                                                       message:nil
                                                                preferredStyle:UIAlertControllerStyleActionSheet]; // 1
        UIAlertAction *firstAction = [UIAlertAction actionWithTitle:NSLocalizedString(actionSheetShareOnFacebook, actionSheetShareOnFacebook)
                                                              style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                  [self shareToFacebook];
                                                              }]; // 2
        
        UIAlertAction *secondAction = [UIAlertAction actionWithTitle:@"Share to Instagram"                                                               style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
            NSString *mainUrl = self.product.mainUrl ;
            NSURL *instagramURL = [NSURL URLWithString:@"instagram:app"];
            if ([[UIApplication sharedApplication] canOpenURL:instagramURL]) {
                [Helper shareOnInstagram:mainUrl viewController:self];
            }
            else
            {
                [Helper showAlertWithTitle:NSLocalizedString(alertMessage,alertMessage) Message:NSLocalizedString(instagramIsNotInstalled,instagramIsNotInstalled) viewController:self];
                
            } }]; //
        
        UIAlertAction *thirdAction = [UIAlertAction actionWithTitle:@"Share to Twitter"
                                                  style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                {
                [Helper shareOnTwiter:self.product.postId viewControllerRef:self];
                 }
                }];
        
        UIAlertAction *fourthAction = [UIAlertAction actionWithTitle:NSLocalizedString(actionSheetCopyUrl, actionSheetCopyUrl)
                                                               style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                   [self copyShareURL];
                                                               }]; // 3
        UIAlertAction *fifthAction = [UIAlertAction actionWithTitle:NSLocalizedString(alertCancel, alertCancel) style:UIAlertActionStyleCancel handler:nil];
        
        [alert addAction:firstAction]; // 4
        [alert addAction:secondAction]; // 5
        [alert addAction:thirdAction];
        [alert addAction:fourthAction];
        [alert addAction:fifthAction];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
}

#pragma mark - CommentButton Action -

- (IBAction)viewAllCommentsButtonAction:(id)sender {
    
    if([[Helper userToken] isEqualToString:@"guestUser"])
    {
        UINavigationController *navigationController = [CommonMethods presentLoginScreenController];
        [self presentViewController:navigationController animated:YES completion:nil];
    }
    else{
        UIButton *allCommentButton = (UIButton *)sender;
        CommentsViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"commentsStoryBoardId"];
        newView.postId =  self.product.postId;
        newView.postCaption =  self.product.productDescription ;
        newView.postType = 0 ;
        newView.imageUrlOfPostedUser = (NSString *)self.product.profilePicUrl ;
        newView.selectedCellIs = allCommentButton.tag;
        newView.userNameOfPostedUser = self.product.postedByUserName;
        newView.commentingOnPostFrom = @"ToHomeScreen";
        [self.navigationController pushViewController:newView animated:YES];
    }
}

#pragma mark - Show UserProfile Action -

- (IBAction)showProfileButton:(id)sender {
    UserProfileViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"userProfileStoryBoardId"];
    newView.checkProfileOfUserNmae = self.product.membername ;
    newView.checkingFriendsProfile = YES;
    newView.ProductDetails = YES;
    newView.hidesBottomBarWhenPushed = YES ;
    [self.navigationController pushViewController:newView animated:YES];
}

#pragma mark - Follow Button Action -
- (IBAction)followButtonAction:(id)sender {
    
    if([[Helper userToken] isEqualToString:mGuestToken])
    {
        UINavigationController *nav = [CommonMethods presentLoginScreenController];
        [self.navigationController presentViewController:nav animated:YES completion:nil];
    }
    else
        [self followButtonClicked:sender];
}

-(void)followButtonClicked:(id)sender {
    UIButton *selectedButton = (UIButton *)sender;
    //actions for when the account is public.
    if ([selectedButton.titleLabel.text containsString:NSLocalizedString(followingButtonTitle, followingButtonTitle)]) {
        UIImageView *userImageView =[[UIImageView alloc] init];
        [userImageView sd_setImageWithURL:[NSURL URLWithString: self.product.memberProfilePicUrl] placeholderImage:[UIImage imageNamed: @"defaultpp.png"]];
        [Helper showUnFollowAlert:userImageView.image and:self.product.membername viewControllerReference:self onComplition:^(BOOL isUnfollow)
         {
             if(isUnfollow){
                 [self unfollowAction:sender];
             }
         }];
    }
    else {
        [selectedButton makeButtonAsFollowing];
        [self sendNewFollowStatusThroughNotification:self.product.username andNewStatus:@"1"];
        //passing parameters.
        NSDictionary *requestDict = @{muserNameTofollow     :self.product.membername,
                                      mauthToken            :flStrForObj([Helper userToken]),
                                      };
        //requesting the service and passing parametrs.
        [WebServiceHandler follow:requestDict andDelegate:self];
    }
}

#pragma mark - DirectChat Button Action -

- (IBAction)FloatingButtonAction:(id)sender {
    if ([[Helper userToken] isEqualToString:@"guestUser"]) {
        UINavigationController *navigationController = [CommonMethods presentLoginScreenController];
        [self presentViewController:navigationController animated:YES completion:nil];
    } else {
        MakeOfferViewController *controller = [[MakeOfferViewController alloc] init];
        [self.navigationController dismissViewControllerAnimated:true completion:nil];
        [controller createObjectForMakeOfferwithData:self.product withPrice:0 andType:@"0" withAPICall:NO];
    }
}

#pragma mark - Make Offer Action -

- (IBAction)makeOfferButton:(id)sender {
    
    if ([[Helper userToken] isEqualToString:mGuestToken]) {
        UINavigationController *navigationController = [CommonMethods presentLoginScreenController];
        [self presentViewController:navigationController animated:YES completion:nil];
    }
    else if(sold == 0){
         MakeOfferViewController *makeOfferVC  = [self.storyboard instantiateViewControllerWithIdentifier:@"MakeOfferViewController"];
        makeOfferVC.product = self.product;
        makeOfferVC.delegate = self;
        [self.navigationController pushViewController:makeOfferVC animated:YES];
    }
    else
    {
        EditProductViewController *editVC = [self.storyboard instantiateViewControllerWithIdentifier:mEditItemStoryBoardID];
        editVC.hidesBottomBarWhenPushed = YES;
        editVC.product = self.product ;
        editVC.isProductScreen = YES ;
        [self.navigationController pushViewController:editVC animated:YES];
    }
}


#pragma mark -
#pragma mark - Show Full Image -
/**
 This Method will show full images of products on tapping.
 
 @param tap object to get the tap details like tags.
 */
-(void)imageTapped:(UITapGestureRecognizer *)tap
{
    imageTag = tap.view.tag ;
    ShowFullImageViewController *showFullVC = [self.storyboard instantiateViewControllerWithIdentifier:mShowFullImageStoryboardID];
    showFullVC.arrayOfImagesURL  = arrayOfImageUrls;
    showFullVC.imageCount = imageCount;
    showFullVC.imageTag = imageTag ;
    UINavigationController *navigation = [[UINavigationController alloc] initWithRootViewController:showFullVC];
        [self.navigationController presentViewController:navigation animated:YES completion:nil];
}

/*-----------------------------------------------------*/
#pragma mark -
#pragma mark - Unfollow Action
/*----------------------------------------------------*/


/**
 This method will get invoked on unfollow button action to ask the permission to get
 user unfollowed with one action sheet.
 
 @param sender      unFollow Button .
 */

-(void)unfollowAction:(id)sender {
    UIButton *selectedButton = (UIButton *)sender;
    [selectedButton makeButtonAsFollow];
    [self sendNewFollowStatusThroughNotification:self.product.postedByUserName andNewStatus:@"2"];
    //passing parameters.
    NSDictionary *requestDict = @{muserNameToUnFollow:self.product.membername,
                                  mauthToken            :flStrForObj([Helper userToken]),
                                  };
    //requesting the service and passing parametrs.
    [WebServiceHandler unFollow:requestDict andDelegate:self];
}

-(void)sendNewFollowStatusThroughNotification:(NSString *)username andNewStatus:(NSString *)newFollowStatus {
    NSDictionary *newFollowDict = @{@"newFollowStatus"     :flStrForObj(newFollowStatus),
                                    mUserName            :flStrForObj(username),
                                    };
    [[NSNotificationCenter defaultCenter] postNotificationName:@"updatedFollowStatus" object:[NSDictionary dictionaryWithObject:newFollowDict forKey:@"newUpdatedFollowData"]];
}


#pragma mark -
#pragma mark - Server Requests. -

#pragma mark -
#pragma mark - RequestForPostDetails

/**
 Server call for getting post details by post ID.
 */
-(void)webServiceForGettingPostsByID
{
    
    
    if([[Helper userToken] isEqualToString:mGuestToken])
    {
        NSDictionary *requestDict = @{
                                      mpostid:flStrForObj(self.postId),
                                      mCity : flStrForObj(self.currentCity),
                                      mCountryShortName : flStrForObj(self.countryShortName),
                                      mlatitude : flStrForObj(self.currentLattitude),
                                      mlongitude : flStrForObj(self.currentLongitude)
                                      };
        
        [WebServiceHandler getPostsForGuests:requestDict andDelegate:self];
        
        
    }
    else{
        
        NSDictionary *requestDict = @{
                                      mauthToken :flStrForObj([Helper userToken]),
                                      mpostid:flStrForObj(self.postId),
                                      mCity : flStrForObj(self.currentCity),
                                      mCountryShortName : flStrForObj(self.countryShortName),
                                      mlatitude : flStrForObj(self.currentLattitude),
                                      mlongitude : flStrForObj(self.currentLongitude)
                                      };
        
        [WebServiceHandler getPostsByusers:requestDict andDelegate:self];
        
    }
}


#pragma mark -
#pragma mark - Like Post Server Request

-(void)likeAPost:(NSString *)postId postType:(NSString *)postType {
    NSDictionary *requestDict;
    requestDict = @{
                    mauthToken:flStrForObj([Helper userToken]),
                    mpostid:postId,
                    mLabel:@"Photo"
                    };
    [WebServiceHandler likeAPost:requestDict andDelegate:self];
}

#pragma mark -
#pragma mark - Unlike Post Server Request

-(void)unlikeAPost:(NSString *)postId postType:(NSString *)postType {
    NSDictionary *requestDict;
    requestDict = @{
                    mauthToken:flStrForObj([Helper userToken]),
                    mpostid:postId,
                    mLabel:@"Photo",
                    mUserName:userName
                    };
    [WebServiceHandler unlikeAPost:requestDict andDelegate:self];
}

#pragma mark
#pragma mark - WebServiceDelegate

- (void) didFinishLoadingRequest:(RequestType)requestType withResponse:(id)response error:(NSError*)error {
    if (error) {
        [[ProgressIndicator sharedInstance] hideProgressIndicator];
        [Helper showAlertWithTitle:NSLocalizedString(alertError, alertError) Message:NSLocalizedString(mCommonServerErrorMessage, mCommonServerErrorMessage) viewController:self];
        return;
    }
    
    NSDictionary *responseDict = (NSDictionary*)response;
    
    switch (requestType) {
        case RequestTypeGetPostsByUsers:
        {
            self.product = response ;
            switch (self.product.code) {
                case 200: {
                    if(self.isNewsFeedSocial)
                    {
                     NSDictionary *clicksCount = @{ @"productDetails": self.product,
                                                            @"indexPath": self.indexPath,};
                     [[NSNotificationCenter defaultCenter] postNotificationName:@"clickCount" object:clicksCount];
                    }
                    
                    [[ProgressIndicator sharedInstance] hideProgressIndicator];
                    self.makeOfferOutlet.enabled =  YES;
                    self.floatingButtonOutlet.enabled = YES;
                    [self showOutletsOnTheBasisOfData];
                    [self handlingResponseOfExplorePosts];
                    [self updatePricedetails];
                }
                    break;
                case 204:
                {
                    [self productIsNotFound];
                    
                }
                    break;
                    
                default:
                    break;
            }
        }
            break;
            
        case RequestTypeGetPostsForGuests:
        {  self.product = response ;
            switch (self.product.code) {
                case 200: {
                    [self updatePricedetails];
                    [self handlingResponseOfExplorePosts];
                    [[ProgressIndicator sharedInstance] hideProgressIndicator];
                }
                    break;
                case 204:
                {
                    [self productIsNotFound];
                }
                    break;
                    
                default:
                {   [[ProgressIndicator sharedInstance] hideProgressIndicator];
                    [Helper showAlertWithTitle:NSLocalizedString(alertError, alertError) Message:NSLocalizedString(mCommonServerErrorMessage, mCommonServerErrorMessage) viewController:self];
                }
                    break;
            }
        }
            break ;
        case RequestTypeLikeAPost:
        {
            
            switch ([responseDict[@"code"] integerValue]) {
                case 200: {
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"updatePostDetails" object:[NSDictionary dictionaryWithObject:responseDict forKey:@"profilePicUrl"]];
                }
                    break;
                default:
                    break;
            }
        }
            break ;
        case RequestTypeUnlikeAPost:
        {
            switch ([responseDict[@"code"] integerValue]) {
                case 200: {
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"updatePostDetails" object:[NSDictionary dictionaryWithObject:responseDict forKey:@"profilePicUrl"]];
                }
                    break;
                default:
                    break;
            }
            
        }
        default:
            break;
    }
    
    
}


#pragma mark -
#pragma mark - Copy ProductUrl -

-(void)copyShareURL {
    [errorMessageLabelOutlet setHidden:NO];
    [self showingErrorAlertfromTop:@"Link copied to clipboard."];
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    NSString *url =  [SHARE_LINK stringByAppendingString:self.product.postId];
    NSString *copymainurl = url ;
    pasteboard.string = copymainurl;
}

- (void)paste {
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    NSString *string = pasteboard.string;
    NSLog(@"%@",string);
}


#pragma mark -
#pragma mark - Share On Facebook -

-(void)shareToFacebook {
    
    
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi showMessage:@"sharing.." On:self.view];
    
    NSString *productName = self.product.productName ;
    NSString *productDescription = self.product.condition ;
    NSString *productCondition   = self.product.productDescription;
    NSString *imageUrl = (NSString *)self.product.mainUrl ;
    NSString *postId = self.product.postId;
    NSMutableDictionary *params1 = [NSMutableDictionary dictionaryWithCapacity:5L];
    [params1 setObject:imageUrl forKey:@"link"];
    [params1 setObject:productName forKey:@"title"];
    [params1 setObject:productDescription forKey:@"description"];
    [params1 setObject:productCondition forKey:@"condition"];
    [params1 setObject:postId forKey:@"postId"];
    [self makeFBPostWithParams:params1];
    
}


/**
 Method if sharing did cancel.
 */
-(void)sharerDidCancel:(id<FBSDKSharing>)sharer
{
    
}
- (void) makeFBPostWithParams:(NSDictionary*)params
{
    NSString *url =  [SHARE_LINK stringByAppendingString:[NSString stringWithFormat:@"%@",params[@"postId"]]];
    
    if ([[FBSDKAccessToken currentAccessToken] hasGranted:@"publish_actions"]) {
        [self ShowFacebookSharingDialog:url];
    }
    else{
        
        FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
        [login logInWithPublishPermissions:@[@"publish_actions"] fromViewController:self
                                   handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
                                       if (error) {
                                       } else if (result.isCancelled)
                                       {
                                           [self ShowFacebookSharingDialog:url];
                                       }
                                       else
                                       {
                                           
                                       }
                                   }];
        
    }
}

-(void)ShowFacebookSharingDialog :(NSString *)url
{
    dispatch_async(dispatch_get_main_queue(), ^{
        FBSDKShareLinkContent  *content = [[FBSDKShareLinkContent  alloc]init];
        content.contentURL = [NSURL URLWithString:url];
        FBSDKShareDialog* dialog = [[FBSDKShareDialog alloc] init];
        dialog.mode = FBSDKShareDialogModeAutomatic;
        dialog.shareContent = content;
        [dialog setDelegate:self];
        dialog.fromViewController = self;
        [dialog show];
        
    });
}


-(void)sharer:(id<FBSDKSharing>)sharer didFailWithError:(NSError *)error {
    
}

-(void)sharer:(id<FBSDKSharing>)sharer didCompleteWithResults:(NSDictionary *)results {
    
}

#pragma mark -
#pragma mark - Handle PostDetails Response -
/**
 Handling the response comes from WebService to extract the required data.
 */
-(void)handlingResponseOfExplorePosts
{
    self.dataFromHomeScreen = NO;
    
    [arrayOfProfilePics removeAllObjects];
    arrayOfImageUrls = [[NSMutableArray alloc] initWithObjects:self.product.mainUrl,[self.product.imageUrl1 stringByReplacingOccurrencesOfString:@"upload/" withString:@"upload/c_fit/q_auto:best/"],[self.product.imageUrl2 stringByReplacingOccurrencesOfString:@"upload/" withString:@"upload/c_fit/q_auto:best/"],[self.product.imageUrl3 stringByReplacingOccurrencesOfString:@"upload/" withString:@"upload/c_fit/q_auto:best/"],[self.product.imageUrl4 stringByReplacingOccurrencesOfString:@"upload/" withString:@"upload/c_fit/q_auto:best/"], nil ];
    
    NSInteger strForCount = self.product.imageCount;
    
    if (strForCount >0) {
        imageCount = strForCount;
    }
    else {
        imageCount = 1;
    }
    [self headerForTableView];
    //get array of Likers profile
    NSInteger likeCount = [self.product.likes integerValue];
    if(likeCount>6)
        likeCount = 6;
    for(int j=0;j<likeCount;j++){
        NSString *user = flStrForObj(self.product.likedByUsers[j][@"likedByUsers"]);
        NSString *profilePicOfUser = flStrForObj(self.product.likedByUsers[j][@"profilePicUrl"]);
        NSMutableDictionary  *tempDict = [[NSMutableDictionary alloc] init];
        [tempDict setValue:user forKey:@"likedByUsers"];
        [tempDict setValue:profilePicOfUser forKey:@"profilePicUrl"];
        [arrayOfProfilePics addObject:tempDict];
    }
    self.tableView.separatorColor = mTableViewCellSepratorColor;
    self.viewForMakeOffer.hidden = NO ;
    [self.tableView reloadData];
    
    
    
}


#pragma mark -
#pragma mark - ZoomTransitionProtocol -

-(UIView *)viewForZoomTransition:(BOOL)isSource
{
    
    headerImageView.image = _imageFromHome;
    return headerImageView;
}


-(void)updatePricedetails {
    _offercurrencyLabel.text =  [self returnCurrency:self.product.currency];
    _priceOutlet.text = self.product.price;
    
    if(!self.product.negotiable)
    {
        _offerNegotLabel.text = NSLocalizedString(productNotNegotiable, productNotNegotiable);
    }else
    {
        _offerNegotLabel.text = NSLocalizedString(productNegotiable, productNegotiable);
        
    }
}


#pragma mark -
#pragma mark - Product Removed -

-(void)productIsNotFound
{
    if(self.productDelegate)
    {
        [self.productDelegate productIsRemovedForIndex:self.indexPath];
    }
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:NSLocalizedString(productRemovedAlertMessage, productRemovedAlertMessage) preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *action = [UIAlertAction actionWithTitle:NSLocalizedString(alertOk, alertOk) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                             {
                                 [self.navigationController popViewControllerAnimated:YES];
                             }];
    [alertController addAction:action];
    [self presentViewController:alertController animated:YES completion:nil];
    
}

#pragma mark -
#pragma mark - Pan Gesture -

-(void)addPanGestureToChatButton
{
    UIPanGestureRecognizer *panRecognizer;
    panRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(wasDragged:)];
    panRecognizer.cancelsTouchesInView = YES;
    [[self floatingButtonView] addGestureRecognizer:panRecognizer];
}

- (void)wasDragged:(UIPanGestureRecognizer *)recognizer {
    UIButton *button = (UIButton *)recognizer.view;
    CGPoint translation = [recognizer translationInView:button];
    
    button.center = CGPointMake(button.center.x + translation.x, button.center.y + translation.y);
    [recognizer setTranslation:CGPointZero inView:button];
}

#pragma mark -
#pragma mark - Return CurrencySym -


/**
 This method will return currency symbol for respective currency.
 
 @param curr currency.
 
 @return currency symbol for respective currency.
 */
-(NSString*)returnCurrency:(NSString *)curr
{
    NSLocale *local = [[NSLocale alloc] initWithLocaleIdentifier:curr];
    NSString *currencySym = [NSString stringWithFormat:@"%@",[local displayNameForKey:NSLocaleCurrencySymbol value:curr]];
    return currencySym;
}

#pragma mark -
#pragma mark - Alert From Top -


/**
 This method will Invoked to show message from top.
 
 @param message message to show.
 */
-(void)showingErrorAlertfromTop:(NSString *)message {
    [errorMessageLabelOutlet setHidden:NO];
    
    [errorMessageLabelOutlet setFrame:CGRectMake(0,50, [UIScreen mainScreen].bounds.size.width, 30)];
    
    [self.view layoutIfNeeded];
    errorMessageLabelOutlet.text = message;
    [UIView animateWithDuration:0.4 animations:
     ^ {
         [self.view layoutIfNeeded];
     }];
    int duration = 2;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, duration * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [UIView animateWithDuration:0.4 animations:
         ^ {
             [self->errorMessageLabelOutlet setFrame:CGRectMake(0, -100, [UIScreen mainScreen].bounds.size.width, 100)];
             [self->errorMessageLabelOutlet setHidden:YES];
             [self.view layoutIfNeeded];
         }];
    });
    
}

@end

