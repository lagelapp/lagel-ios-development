//
//  SuccessFullyPostedViewController.h

//
//  Created by Rahul Sharma on 10/07/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol successfullyPostedDelegate <NSObject>

-(void)postingListedSuccessfully ;

@end


@interface SuccessFullyPostedViewController : UIViewController

@property BOOL isTwitterSharing ;
@property (strong, nonatomic) NSString *postId ;
@property (strong,nonatomic)id<successfullyPostedDelegate> postingDelegate;
@property (strong, nonatomic) IBOutlet UIButton *okButtonOutlet;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityLoaderOutlet;


- (IBAction)okaButtonAction:(id)sender;


@end

