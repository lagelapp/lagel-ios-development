//
//  SuccessFullyPostedViewController.m

//
//  Created by Rahul Sharma on 10/07/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "SuccessFullyPostedViewController.h"

@interface SuccessFullyPostedViewController ()

@end

@implementation SuccessFullyPostedViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    if(_isTwitterSharing)
    {
        [self.activityLoaderOutlet startAnimating];
        
        [Helper shareOnTwiter:self.postId viewControllerRef:self];
        [self.activityLoaderOutlet stopAnimating];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL)prefersStatusBarHidden
{
    return YES ;
}




- (IBAction)okaButtonAction:(id)sender {
    
    if(self.postingDelegate)
    {
        [_postingDelegate postingListedSuccessfully];
    }
    
}

- (IBAction)closeButtonAction:(id)sender {
    
    if(self.postingDelegate)
    {
        [_postingDelegate postingListedSuccessfully];
    }
    
    
}
@end

