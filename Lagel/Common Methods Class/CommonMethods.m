//
//  CommonMethods.m

//
//  Created by Rahul Sharma on 04/02/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "AppDelegate.h"
#import "CommonMethods.h"
#import <AddressBookUI/AddressBookUI.h>
#import <FirebaseMessaging/FirebaseMessaging.h>
#import "APIModels.h"
#import "Lagel-Swift.h"
#import "RegisterViewController.h"
@implementation CommonMethods


+ (UIButton *) createNavButtonsWithselectedState:(NSString *)selectedStateImage normalState :(NSString *)normalStateImage;
{
    UIButton *leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [leftButton setImage:[UIImage imageNamed: normalStateImage] forState:UIControlStateNormal];
    [leftButton setImage:[UIImage imageNamed: selectedStateImage] forState:UIControlStateSelected];
    return leftButton;
    
}

+(UIAlertController *) showAlertWithTitle:(NSString *)title message:(NSString *)message actionTitle:(NSString *)actionTitle
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *action = [UIAlertAction actionWithTitle:actionTitle style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:action];
    return alertController;
    
 }
+ (UINavigationController *) presentLoginScreenController
{
   UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    
//    LandingPageViewController *splashVC = [storyboard instantiateViewControllerWithIdentifier:mSplashScreenID];
        RegisterViewController *splashVC = [storyboard instantiateViewControllerWithIdentifier:mLoginScreenID];
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:splashVC];
    
    navigationController.modalPresentationStyle = UIModalPresentationFormSheet;
    
    return navigationController;

}
+(UIBarButtonItem *)getNegativeSpacer
{
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    negativeSpacer.width = -10 ; // it was -6 in iOS 6
    return negativeSpacer;
}

+ (CGFloat)measureWidthLabel: (UILabel *)label
{
    CGSize constrainedSize = CGSizeMake(9999 , label.frame.size.height);
    CGRect requiredWidth=CGRectMake(0, 0, 0, 0);
    NSDictionary *attributesDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                          [UIFont fontWithName:label.font.fontName size:label.font.pointSize], NSFontAttributeName,
                                          nil];
    if (label.text) {
        NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:label.text attributes:attributesDictionary];
        requiredWidth=[string boundingRectWithSize:constrainedSize options:NSStringDrawingUsesLineFragmentOrigin context:nil];
        if (requiredWidth.size.width < label.frame.size.width) {
            requiredWidth = CGRectMake(0,0, requiredWidth.size.width, label.frame.size.height);
        }
    }
    
    CGRect newFrame = label.frame;
    newFrame.size.width = requiredWidth.size.width;
    return  newFrame.size.width;
}


/*-----------------------------*/
#pragma mark -
#pragma mark - Update Model OS Version
/*-----------------------------*/

+(NSDictionary *)updateDeviceDetailsForAdmin {
    NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"]; // used for getting the app version
    
    NSDictionary *requestDict = @{@"deviceName"    :flStrForObj([UIDevice currentDevice].name), //to get deviceName
                                  @"deviceId" :flStrForObj([[[UIDevice currentDevice] identifierForVendor] UUIDString]), //to get deviceId
                                  @"modelNumber" :flStrForObj([UIDevice currentDevice].model), //to get modelNumber
                                  @"deviceOs" :flStrForObj([[UIDevice currentDevice] systemVersion]), // to get deviceOs
                                  @"appVersion" :flStrForObj(version), //to get appVersion
                                  @"token":flStrForObj([Helper userToken]), //logged user token
                                  @"deviceType":@"1"
                                  };
    return requestDict;
}


+ (void)setNegativeSpacingforNavigationItem :(UINavigationItem *)navItem andExtraBarItem:(UIBarButtonItem *)extraItem;
{
    UIBarButtonItem *containingcancelButton = navItem.leftBarButtonItem;
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    negativeSpacer.width = -14;// it was -6 in iOS 6
    [navItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];
    UIBarButtonItem *containingRightButton = navItem.rightBarButtonItem;
    UIBarButtonItem *containingExtraRightButton = extraItem ;
    [navItem setRightBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingRightButton,negativeSpacer,containingExtraRightButton,nil] animated:NO];
}


#pragma mark -
#pragma mark - Session Expired

+(void)authTokenExpired_UserLoggedInOtherDeviceForview :(UIView *)view andTaBbar :(UITabBarController *)tabBarController
{
        [[NSUserDefaults standardUserDefaults] setValue:@"recntlyLoggedOut" forKey:@"showLoginScreen"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        dispatch_async(dispatch_get_main_queue(), ^{
            AppDelegate *appDelegateTemp = (AppDelegate *)[[UIApplication sharedApplication]delegate];
            UIViewController* rootController = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"TabBarStoryboardID"];
            appDelegateTemp.window.rootViewController = rootController;
        });

    //User for logging out the user
    
    [[MQTTChatManager sharedInstance] sendOnlineStatusWithOfflineStatus:YES];
    [[[MQTT sharedInstance] manager] disconnect];
    [[MQTTChatManager sharedInstance] unsubscribeAllTopics];
    [[MQTT sharedInstance] setManager:nil];
    [[FIRMessaging messaging] unsubscribeFromTopic:[Helper getMQTTID]];

        ABAuthorizationStatus status = ABAddressBookGetAuthorizationStatus();
        status =  kABAuthorizationStatusDenied;
        
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"userDetailWhileRegistration"];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"preferenceName"];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"userFbDetails"];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"phoneContacts"];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"userToken"];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"userFullName"];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:mMqttId];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:numberOfFbFriendFoundInPicogram];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:numberOfContactsFoundInPicogram];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        // deletCouchDataBase
    
        NSUserDefaults * defs = [NSUserDefaults standardUserDefaults];
        NSDictionary * dict = [defs dictionaryRepresentation];
        for (id key in dict) {
            if (![key isEqualToString:cloudinartyDetails] && ![key isEqualToString:mdeviceToken] && ![key isEqualToString:mpushToken] && ![key isEqualToString:TermsKey]) {
                [defs removeObjectForKey:key];
            }
        }
        
        //    _totalRows = [NSMutableArray new];
        CBLManager *manager = [CBLManager sharedInstance];
        CBLManager* bgMgr = [manager copy];
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
            CBLQueryEnumerator *result;
            NSError *error;
            CBLDatabase* bgDB = [bgMgr databaseNamed:@"couchbasenew" error: &error];
            CBLQuery *query = [bgDB createAllDocumentsQuery];
            query.allDocsMode = kCBLAllDocs;
            query.descending = YES;
            

            result = [query run:&error];
            for (NSInteger count = 0; count < result.count; count++) {
                
                CBLDocument* document = [bgDB documentWithID:[result rowAtIndex:count].documentID];
                NSString *groupId = [NSString stringWithFormat:@"%@",[document.properties objectForKey:@"groupID"]];
                [[FIRMessaging messaging] unsubscribeFromTopic:groupId];
                NSError* error; 
                [document deleteDocument:&error];
            }
        });
    
    
        dispatch_async(dispatch_get_main_queue(), ^{
            [tabBarController setSelectedIndex:0];
            [view removeFromSuperview];
        });
}



@end
