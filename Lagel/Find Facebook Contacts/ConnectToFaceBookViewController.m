//
//  ConnectToFaceBookViewController.m

//
//  Created by Rahul Sharma on 5/10/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import "ConnectToFaceBookViewController.h"
#import "WebServiceConstants.h"
#import "WebServiceHandler.h"
#import "FBLoginHandler.h"

#import "UIImageView+WebCache.h"
#import "TinderGenericUtility.h"
#import "UserProfileViewController.h"
#import "FontDetailsClass.h"
#import <AddressBookUI/AddressBookUI.h>

#import "ProgressIndicator.h"
#import "Helper.h"


@interface ConnectToFaceBookViewController ()<UITableViewDataSource,UITableViewDelegate,WebServiceHandlerDelegate,FBLoginHandlerDelegate> {
  
    FacebookNumberOfContactsTableViewCell *numberOfcontactcell;
    NSArray *names;
    NSInteger rowCountForTitleSection;
    NSMutableArray *arrayOfReceivedContactDetails;

    
    UIActivityIndicatorView *avForBackGround;
    UIButton *navSetiingButton;
    ProgressIndicator *indicator;
}
@property (strong ,nonatomic) UIWindow *mainWindow;

@property bool classIsAppearing;
@end

@implementation ConnectToFaceBookViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter]removeObserver:@"getResponseFromCallChannel"];
    [self navTitileSelection];
    [self createNavLeftButton];
    [self createNavSettingButton];
    rowCountForTitleSection = 0;
    [self addActiVityIndicator];
    
    self.automaticallyAdjustsScrollViewInsets =  NO;
   arrayOfReceivedContactDetails = [[NSMutableArray alloc] init];
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:YES];
    _classIsAppearing = NO;
    self.navigationController.navigationBar.shadowImage = nil;

}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    _classIsAppearing = YES;
}


-(void)updateFollowStatus {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateFollwoStatus:) name:@"updatedFollowStatus" object:nil];
}

-(void)updateFollowSttusForPhoneContacts {
      [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateFollwoStatusForPhoneContacts:) name:@"updatedFollowStatus" object:nil];
}
-(void)updateFollwoStatusForPhoneContacts:(NSNotification *)noti {
    if (!_classIsAppearing) {
        //check the postId and Its Index In array.
        NSString *userNamer = flStrForObj(noti.object[@"newUpdatedFollowData"][@"userName"]);
        NSString *foolowStatusRespectToUser = noti.object[@"newUpdatedFollowData"][@"newFollowStatus"];
        for (int i=0; i <arrayOfReceivedContactDetails.count;i++) {
            if ([flStrForObj(arrayOfReceivedContactDetails[i][@"membername"]) isEqualToString:userNamer]) {
                
                [self updateDataLocallyForFollowStatus:foolowStatusRespectToUser andAtIndex:i];
                NSIndexPath* rowToReload = [NSIndexPath indexPathForRow:i inSection:1];
                NSArray* rowsToReload = [NSArray arrayWithObjects:rowToReload, nil];
                [self.contatctsTableView reloadRowsAtIndexPaths:rowsToReload withRowAnimation:UITableViewRowAnimationNone];
                break;
            }
        }
    }
}



-(void)updateFollwoStatus:(NSNotification *)noti {
    
    if (!_classIsAppearing) {
        //check the postId and Its Index In array.
        NSString *userNamer = flStrForObj(noti.object[@"newUpdatedFollowData"][@"userName"]);
        NSString *foolowStatusRespectToUser = noti.object[@"newUpdatedFollowData"][@"newFollowStatus"];
        
        for (int i=0; i <arrayOfReceivedContactDetails.count;i++) {
            if ([flStrForObj(arrayOfReceivedContactDetails[i][@"membername"]) isEqualToString:userNamer]) {
                arrayOfReceivedContactDetails[i][@"followRequestStatus"] = foolowStatusRespectToUser;
                NSIndexPath* rowToReload = [NSIndexPath indexPathForRow:i inSection:1];
                NSArray* rowsToReload = [NSArray arrayWithObjects:rowToReload, nil];
                [self.contatctsTableView reloadRowsAtIndexPaths:rowsToReload withRowAnimation:UITableViewRowAnimationNone];
                break;
            }
        }

        
    }
}

-(void)sendNewFollowStatusThroughNotification:(NSString *)userNamer andNewStatus:(NSString *)newFollowStatus {
    
    
    
    NSDictionary *newFollowDict = @{@"newFollowStatus"     :flStrForObj(newFollowStatus),
                                    @"userName"            :flStrForObj(userNamer),
                                    };
    
    
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"updatedFollowStatus" object:[NSDictionary dictionaryWithObject:newFollowDict forKey:@"newUpdatedFollowData"]];
}

-(void)addActiVityIndicator {
    
    if ([self.syncingContactsOf isEqualToString:@"phoneBook"]) {
        //showing progress indicator and requesting for posts.
        ProgressIndicator *HomePI = [ProgressIndicator sharedInstance];  [HomePI showPIOnView:self.view withMessage:@"Syncing Contacts .."];
        
        [self updateFollowSttusForPhoneContacts];
    }
    else {
        
        [self updateFollowStatus];
        
        avForBackGround = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        [avForBackGround setFrame:CGRectMake(0,0,50,30)];
        [self.view addSubview:avForBackGround];
        avForBackGround.tag  = 1;
        [avForBackGround startAnimating];
        self.contatctsTableView.backgroundView = avForBackGround;
    }
}

- (void)createNavSettingButton {
    if ([self.syncingContactsOf isEqualToString:@"phoneBook"]) {
        
        if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"accessDenied"] isEqualToString:@"No"]) {
            
            [self showAlert];
        }
        else
        {
            [self requestForFbOrPhoneContactsSync];
        }
        
        navSetiingButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [navSetiingButton setImage:[UIImage imageNamed:mSettingsON]
                          forState:UIControlStateNormal];
        [navSetiingButton setImage:[UIImage imageNamed:mSettingsON]
                          forState:UIControlStateSelected];
        [navSetiingButton setTitleColor:[UIColor grayColor]
                               forState:UIControlStateHighlighted];
        [navSetiingButton setFrame:CGRectMake(-10,17,45,45)];
        [navSetiingButton addTarget:self action:@selector(SettingButtonAction:)
                   forControlEvents:UIControlEventTouchUpInside];
        // Create a container bar button
        UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc] initWithCustomView:navSetiingButton];
        UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                           initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                           target:nil action:nil];
        negativeSpacer.width = -14;// it was -6 in iOS 6
        [self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];
    }
    else {
         [self requestForFbOrPhoneContactsSync];
    }
}

-(void)SettingButtonAction:(id)sender {
    
    UIAlertController *controller = [UIAlertController alertControllerWithTitle:nil message:NSLocalizedString(alertToRemoveAllContacts, alertToRemoveAllContacts) preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *disconnectAction = [UIAlertAction actionWithTitle:NSLocalizedString(disconnectAlertActionTitle, disconnectAlertActionTitle) style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action){
        [self updateTitlesForContactSync:[NSString stringWithFormat:@"%d",0]];
        
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:numberOfContactsFoundInPicogram];
        
        [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"phoneContacts"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        NSLog(@"contact List:%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"phoneContacts"]);
        self->arrayOfReceivedContactDetails = nil;
        ABAuthorizationStatus status = ABAddressBookGetAuthorizationStatus();
        status = kABAuthorizationStatusDenied;
        [[NSUserDefaults standardUserDefaults]setObject:@"No" forKey:@"accessDenied"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        [self.navigationController popViewControllerAnimated:YES];
        
    }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(alertCancel, alertCancel) style:UIAlertActionStyleCancel handler:nil];
    
    [controller addAction:disconnectAction];
    [controller addAction:cancelAction];
    [self presentViewController:controller animated:YES completion:nil];
}

-(void)navTitileSelection {
    if ([self.syncingContactsOf isEqualToString:@"faceBook"]) {
        self.navigationItem.title = NSLocalizedString(mFacebookTitle, mFacebookTitle);
    }
    if ([self.syncingContactsOf isEqualToString:@"phoneBook"]) {
        self.navigationItem.title = NSLocalizedString(mContactsTitle, mContactsTitle);
    }
}

-(void)requestForFbOrPhoneContactsSync {
    if ([self.navigationItem.title isEqualToString:mFacebookTitle]) {
        //if user already login then no need to request for fb login othewise need to show fb login page.
        NSString *listOfFaceBookIds = [[NSUserDefaults standardUserDefaults]
                                       stringForKey:@"preferenceName"];
        
        if (listOfFaceBookIds) {
            // request for facebook contact syncing.
            //getting list of faceBook friends id.
            NSString *listOfFaceBookIds = [[NSUserDefaults standardUserDefaults]
                                           stringForKey:@"preferenceName"];
            //passing parameters(faceBookids list and token)
            listOfFaceBookIds = nil;
            
            FBLoginHandler *handler = [FBLoginHandler sharedInstance];
            listOfFaceBookIds =  [handler getDetailsFromFacebookUpdate];
            [handler setDelegate:self];
        }
        else {
            FBLoginHandler *handler = [FBLoginHandler sharedInstance];
            [handler loginWithFacebook:self];
            [handler setDelegate:self];
            
        }
    }
    else if ([self.navigationItem.title isEqualToString:mContactsTitle]) {
        //connect channel.
        NSString *phoneContacts = [[NSUserDefaults standardUserDefaults]objectForKey:@"phoneContacts"];
        if (phoneContacts.length) {
            [self requsetForcontactsSync:phoneContacts];
        }
        else
        {
            [self loadPhoneContacts];
        }
    }
}

- (void)createNavLeftButton {
    self.navigationController.navigationItem.hidesBackButton =  YES;

    UIButton  *navCancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [navCancelButton rotateButton];
    [navCancelButton setImage:[UIImage imageNamed:mNavigationBackButtonImageName]
                     forState:UIControlStateNormal];
    [navCancelButton setImage:[UIImage imageNamed:mNavigationBackButtonImageName]
                     forState:UIControlStateSelected];
    [navCancelButton addTarget:self
                        action:@selector(backButtonClicked)
              forControlEvents:UIControlEventTouchUpInside];
    [navCancelButton setFrame:CGRectMake(0,0,25,40)];
    UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc] initWithCustomView:navCancelButton];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -10;// it was -6 in iOS 6
    [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];
}

-(void)backButtonClicked {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)updateDataLocallyForFollowStatus:(NSString *)newFollowStatus andAtIndex:(NSInteger )atRow {
    NSMutableDictionary *tempDict = [arrayOfReceivedContactDetails[atRow] mutableCopy];
    [tempDict setValue:newFollowStatus forKey:@"followRequestStatus"];
    [arrayOfReceivedContactDetails setObject:tempDict atIndexedSubscript:atRow];
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if(section==0) {
        return rowCountForTitleSection;
    }
    else  {
        return arrayOfReceivedContactDetails.count;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.section == 0) {
        numberOfcontactcell = [tableView dequeueReusableCellWithIdentifier:@"numberOfContactTableViewCell" forIndexPath:indexPath];
        //the numberOfContacsSynced is used to print no of friends .
        
        NSString *numberOfContacsSynced =[NSString stringWithFormat:@"%lu", (unsigned long)arrayOfReceivedContactDetails.count];
        //tp print on view controller changing the label text.
        if (arrayOfReceivedContactDetails.count >1) {
            numberOfcontactcell.numberOfFriendsLabelOutlet.text  = [numberOfContacsSynced stringByAppendingString:[NSString stringWithFormat:@"%@ %@",@" Friends On" ,APP_NAME]];
        }
        else {
            numberOfcontactcell.numberOfFriendsLabelOutlet.text  = [numberOfContacsSynced stringByAppendingString:[NSString stringWithFormat:@"%@ %@",@" Friend On" ,APP_NAME]];
        }
        
        numberOfcontactcell.followAllButtonOutlet.hidden = YES;
        
        [numberOfcontactcell.followAllButtonOutlet addTarget:self
                                    action:@selector(cellFollowAllButtonAction:)
                          forControlEvents:UIControlEventTouchUpInside];
        
        numberOfcontactcell.followAllButtonOutlet.layer.cornerRadius = 5;
        numberOfcontactcell.followAllButtonOutlet.layer.borderWidth = 1;
        numberOfcontactcell.followAllButtonOutlet.layer.borderColor = [UIColor colorWithRed:0.2392 green:0.3216 blue:0.5922 alpha:1.0].CGColor;
        
        return numberOfcontactcell;
    }
    else {
        
       ConnectToFaceBookContactsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ContactDetailsCell"
                                               forIndexPath:indexPath];
         cell.userNameLabelOutlet.text = flStrForObj(arrayOfReceivedContactDetails[indexPath.row][@"membername"]);
        [cell.contactUserImageViewOutlet sd_setImageWithURL:[NSURL URLWithString:flStrForObj(arrayOfReceivedContactDetails[indexPath.row][@"profilePicUrl"]]) placeholderImage:[UIImage imageNamed:@"defaultpp.png"]];
         
         [cell.followButtonOutlet addTarget:self
                                     action:@selector(cellFollowButtonAction:)
                           forControlEvents:UIControlEventTouchUpInside];
         
         //updating FollowButtonTitle
         [cell updateFollowButtonTitle:flStrForObj(arrayOfReceivedContactDetails[indexPath.row][@"followRequestStatus"]) andIndexPath:indexPath.row];
         
         [cell layoutIfNeeded];
         cell.contactUserImageViewOutlet.layer.cornerRadius = cell.contactUserImageViewOutlet.frame.size.height/2;
         cell.contactUserImageViewOutlet.clipsToBounds = YES;
         cell.followButtonOutlet.tag = 1000 + indexPath.row;
        
        if ([self.syncingContactsOf isEqualToString:@"phoneBook"]) {
            //plot contacts data.
            cell.fullNameLabelOutlet.text = flStrForObj(arrayOfReceivedContactDetails[indexPath.row][@"fullName"]);
             [cell showImagesForContacts:arrayOfReceivedContactDetails forIndex:indexPath.row];
        }
        else {
            //plot fb data.
            cell.fullNameLabelOutlet.text = flStrForObj(arrayOfReceivedContactDetails[indexPath.row][@"fullname"]);
             [cell showImagesForFb:arrayOfReceivedContactDetails forIndex:indexPath.row];
        }
        return cell;
     }
}
             
-(void)cellFollowAllButtonAction:(id)sender {
 // [cell.followButtonOutlet sendActionsForControlEvents:UIControlEventTouchUpInside];
}

-(void)cellFollowButtonAction:(id)sender {
                 
    UIButton *selectedButton = (UIButton *)sender;
    NSIndexPath *selectedCellForLike = [self.contatctsTableView indexPathForCell:(UITableViewCell *)[[[sender superview] superview] superview]];
    
    //actions for when the account is public.
        if ([selectedButton.titleLabel.text containsString:NSLocalizedString(followingButtonTitle, followingButtonTitle)]) {
            UIImageView *userImageView =[[UIImageView alloc] init];
            //UserImageView.image = profieImage;
            [userImageView sd_setImageWithURL:[NSURL URLWithString:flStrForObj(arrayOfReceivedContactDetails[selectedButton.tag%1000][@"profilePicUrl"])] placeholderImage:[UIImage imageNamed: @"defaultpp.png"]];
            [Helper showUnFollowAlert:userImageView.image and:flStrForObj(arrayOfReceivedContactDetails[selectedButton.tag%1000][@"membername"]) viewControllerReference:self onComplition:^(BOOL isUnfollow)
             {
                 if(isUnfollow){
                [self unfollowAction:sender];
                 }
             }];
        }
        else {
            
            [self updateDataLocallyForFollowStatus:@"1" andAtIndex:selectedCellForLike.row];
            
             [self sendNewFollowStatusThroughNotification:flStrForObj(arrayOfReceivedContactDetails[selectedButton.tag%1000][@"membername"]) andNewStatus:@"1"];
            
            [selectedButton makeButtonAsFollowing];
            
            //passing parameters.
            NSDictionary *requestDict = @{muserNameTofollow     :flStrForObj(arrayOfReceivedContactDetails[selectedCellForLike.row][@"membername"]),
                                          mauthToken            :flStrForObj([Helper userToken]),
                                          };
            //requesting the service and passing parametrs.
            [WebServiceHandler follow:requestDict andDelegate:self];
        }
}
             

             
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(indexPath.section==0) {
        return 50;
    }
    else  {
        NSMutableArray *numberOfuserPosts;
        
        if ([self.syncingContactsOf isEqualToString:@"phoneBook"]) {
            numberOfuserPosts = arrayOfReceivedContactDetails[indexPath.row][@"postData"];
            
            if ([flStrForObj(numberOfuserPosts[0][@"thumbnailImageUrl"]) isEqualToString:@""]) {
                numberOfuserPosts = nil;
            }
            //numberOfuserPosts =arrayOfReceivedContactDetails[indexPath.row][@"postData"];
        }
        else {
            numberOfuserPosts =arrayOfReceivedContactDetails[indexPath.row][@"userPosts"];
            if ([flStrForObj(numberOfuserPosts[0][@"thumbnailImageUrl"]) isEqualToString:@""]) {
                numberOfuserPosts = nil;
            }
         
        }
        
        NSString *memberPrivateStatus = flStrForObj(arrayOfReceivedContactDetails[indexPath.row][@"userPrivate"]);
        NSString *followStatus = flStrForObj(arrayOfReceivedContactDetails[indexPath.row][@"followRequestStatus"]);
        
        if ([followStatus isEqualToString:@"1"]) {
            if(numberOfuserPosts.count ==0) {
                return 100;
            }
            else {
                return 60 + self.view.frame.size.width/4
                ;
            }
        }
        else {
            
            
            
            
            
            if([memberPrivateStatus isEqualToString:@"1"]){
               return 100;
            }
            else {
                if(numberOfuserPosts.count ==0) {
                    return 100;
                }
                else {
                   return 60 + self.view.frame.size.width/4;
                }
                
            }
        }
    }
}

            
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    UserProfileViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"userProfileStoryBoardId"];
    newView.checkProfileOfUserNmae =flStrForObj(arrayOfReceivedContactDetails[indexPath.row][@"membername"]);
    newView.checkingFriendsProfile = YES;
    newView.ProductDetails = YES;
    [self.navigationController pushViewController:newView animated:YES];
}
         
#pragma mark
#pragma mark - facebook

- (void)didFacebookUserLoginWithDetails:(NSDictionary*)userInfo {
    NSLog(@"FB Data =  %@", userInfo);
    // request for facebook contact syncing.
    //getting list of faceBook friends id.
    NSString *listOfFaceBookIds = [[NSUserDefaults standardUserDefaults]
                                   stringForKey:@"preferenceName"];
    //passing parameters(faceBookids list and token)
    NSDictionary *requestDict = @{mfaceBookId     :listOfFaceBookIds,
                                  mauthToken      :flStrForObj([Helper userToken]),
                                  };
    //requesting the service and passing parametrs.
    [WebServiceHandler faceBookContactSync:requestDict andDelegate:self];
}
                 
- (void)didFailWithError:(NSError *)error {
    [Helper showAlertWithTitle:nil Message:NSLocalizedString(mFacebookLoginErrorMessage, mFacebookLoginErrorMessage) viewController:self];
}
                 
- (void)didUserCancelLogin {
    NSLog(@"USER CANCELED THE LOGIN");
    [self backButtonClicked];
}
            
/*---------------------------------------------------*/
  #pragma mark - Webservice Handler
  #pragma mark - WebServiceDelegate
/*---------------------------------------------------*/
            
- (void) didFinishLoadingRequest:(RequestType )requestType withResponse:(id)response error:(NSError*)error {
         // handling response.
         self.contatctsTableView.backgroundView = nil;
         
         
         if (error) {
             
             [avForBackGround stopAnimating];
             [[ProgressIndicator sharedInstance] hideProgressIndicator];
             return;
         }
         
         //storing response data  in dictonary.
         NSDictionary *responseDict = (NSDictionary*)response;
         
         //checking the request type and handling response.
         if (requestType == RequestTypeLoginfaceBookContactSync ) {
             //checking the response  code and handling error message or success message by depending on code.
             switch ([responseDict[@"code"] integerValue]) {
                 case 200: {
                     //successs response.
                     arrayOfReceivedContactDetails =responseDict[@"facebookUsers"];
                     
                     [self updateTitlesForFbSync:[NSString stringWithFormat:@"%lu",(unsigned long)arrayOfReceivedContactDetails.count]];
                     
                     //it will reload the data.
                     if(arrayOfReceivedContactDetails.count) {
                         rowCountForTitleSection = 1;
                         NSString *numberOfContacsSynced =[NSString stringWithFormat:@"%lu", (unsigned long)arrayOfReceivedContactDetails.count];
                         
                         [[NSUserDefaults standardUserDefaults] setObject:numberOfContacsSynced forKey:numberOfFbFriendFoundInPicogram];
                         [[NSUserDefaults standardUserDefaults] synchronize];
                         
                         [self.contatctsTableView reloadData];
                     }
                     else
                     {
                         UIAlertController *controller = [CommonMethods showAlertWithTitle:NSLocalizedString(alertMessage, alertMessage) message:[NSString stringWithFormat:NSLocalizedString(noneOfFriendUsingApp, noneOfFriendUsingApp),APP_NAME] actionTitle:NSLocalizedString(alertOk, alertOk)];
                         mPresentAlertController;
                     }
                 }
                     break;
                 case 2022:
                 case 2023: {
                     [avForBackGround stopAnimating];
                     [[ProgressIndicator sharedInstance] hideProgressIndicator];
                     [self backGrounViewForNoSuggestions:NSLocalizedString(noSuggestionsAvailable, noSuggestionsAvailable)];
                 }
                     break;
             }
         }
         else if (requestType == RequestTypePhoneContactSync) {
             [self responseFromChannels:(NSMutableDictionary *)responseDict];
         }
     }
         
             
-(void)unfollowAction:(id)sender {
    NSLog(@"unfollow clicked");
    UIButton *selectedButton = (UIButton *)sender;
    [self updateDataLocallyForFollowStatus:@"2" andAtIndex:selectedButton.tag%1000];
    [self sendNewFollowStatusThroughNotification:flStrForObj(arrayOfReceivedContactDetails[selectedButton.tag%1000][@"membername"]) andNewStatus:@"2"];
    [selectedButton makeButtonAsFollow];
    NSDictionary *requestDict = @{muserNameToUnFollow: flStrForObj(arrayOfReceivedContactDetails[selectedButton.tag %1000][@"membername"]),
                                  mauthToken            :flStrForObj([Helper userToken]),
                                  };
    //requesting the service and passing parametrs.
    [WebServiceHandler unFollow:requestDict andDelegate:self];
}

#pragma mark - DeviceContacts
                 
-(void)loadPhoneContacts{
    ABAuthorizationStatus status = ABAddressBookGetAuthorizationStatus();
    if (status == kABAuthorizationStatusDenied) {
        
        // if you got here, user had previously denied/revoked permission for your
        
        // app to access the contacts, and all you can do is handle this gracefully,
        
        // perhaps telling the user that they have to go to settings to grant access
        
        // to contacts
        [[[UIAlertView alloc] initWithTitle:nil message:NSLocalizedString(allowAccessToContactsMessage, allowAccessToContactsMessage) delegate:nil cancelButtonTitle:NSLocalizedString(alertOk, alertOk) otherButtonTitles:nil] show];
        return;
    }
    CFErrorRef error = NULL;
    ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, &error);
    if (error) {
        
        NSLog(@"ABAddressBookCreateWithOptions error: %@", CFBridgingRelease(error));
        
        if (addressBook) CFRelease(addressBook);
        
        return;
        
    }
    if (status == kABAuthorizationStatusNotDetermined) {
        // present the user the UI that requests permission to contacts ...
        ABAddressBookRequestAccessWithCompletion(addressBook, ^(bool granted, CFErrorRef error) {
            if (error) {
                NSLog(@"ABAddressBookRequestAccessWithCompletion error: %@", CFBridgingRelease(error));
            }
            if (granted) {
                // if they gave you permission, then just carry on
                [self listPeopleInAddressBook:addressBook];
            } else {
                
                // however, if they didn't give you permission, handle it gracefully, for example...
                
               
                
                    dispatch_async(dispatch_get_main_queue(), ^{
                    
                    // BTW, this is not on the main thread, so dispatch UI updates back to the main queue
                        [Helper showAlertWithTitle:nil Message:NSLocalizedString(allowAccessToContactsMessage, allowAccessToContactsMessage) viewController:self];
                });
            }
            if (addressBook) CFRelease(addressBook);
        });
    } else if (status == kABAuthorizationStatusAuthorized) {
        [self listPeopleInAddressBook:addressBook];
        if (addressBook) CFRelease(addressBook);
    }
}
                 
- (void)listPeopleInAddressBook:(ABAddressBookRef)addressBook {
    NSString *phoneNumber;
    NSInteger numberOfPeople = ABAddressBookGetPersonCount(addressBook);
    NSArray *allPeople = CFBridgingRelease(ABAddressBookCopyArrayOfAllPeople(addressBook));
    NSMutableArray  *onlyPhoneNumbers = [[NSMutableArray alloc] init];;
    for (NSInteger i = 0; i < numberOfPeople; i++) {
        ABRecordRef person = (__bridge ABRecordRef)allPeople[i];
              ABMultiValueRef phoneNumbers = ABRecordCopyValue(person, kABPersonPhoneProperty);
        CFIndex numberOfPhoneNumbers = ABMultiValueGetCount(phoneNumbers);
        for (CFIndex i = 0; i < numberOfPhoneNumbers; i++) {
            phoneNumber = CFBridgingRelease(ABMultiValueCopyValueAtIndex(phoneNumbers, i));
        
        }
        CFRelease(phoneNumbers);
        if(phoneNumber){
            [onlyPhoneNumbers addObject:phoneNumber];
        }
    }
  
    
    self.greeting = [onlyPhoneNumbers componentsJoinedByString:@","];
    
    NSCharacterSet *notAllowedChars = [[NSCharacterSet characterSetWithCharactersInString:@"0123456789,+"] invertedSet];
    NSString *resultString = [[self.greeting componentsSeparatedByCharactersInSet:notAllowedChars] componentsJoinedByString:@""];
    
    [self requsetForcontactsSync:resultString];
    [[NSUserDefaults standardUserDefaults] setObject:resultString forKey:@"phoneContacts"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}
         
-(void)requsetForcontactsSync:(NSString *)phoneContscts {
    NSDictionary *requestDict = @{
                                  mauthToken      :flStrForObj([Helper userToken]),
                                  mcontacts:flStrForObj(phoneContscts)
                                  };
    //requesting the service and passing parametrs.
    [WebServiceHandler phoneContactSync:requestDict andDelegate:self];
}
         
   
-(void)updateTitlesForContactSync:(NSString *)numberOfContcts {
    NSDictionary *newFollowDict = @{@"numberOfContacts" :flStrForObj(numberOfContcts),
                                    };
    
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"updateContactSectionTitle" object:[NSDictionary dictionaryWithObject:newFollowDict forKey:@"numberOfContactsSynced"]];
}
             
-(void)updateTitlesForFbSync:(NSString *)numberOfContcts {
    NSDictionary *newFollowDict = @{@"numberOfContacts" :flStrForObj(numberOfContcts),
                                    };
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"updateFaceBookSectionTitle" object:[NSDictionary dictionaryWithObject:newFollowDict forKey:@"numberOfContactsSynced"]];
 }
             
#pragma marks-socket Delegate

-(void)responseFromChannels:(NSMutableDictionary *)responseDictionary {
        [[ProgressIndicator sharedInstance] hideProgressIndicator];
    dispatch_async(dispatch_get_main_queue(), ^{
        switch ([responseDictionary[@"code"] integerValue]) {
            case 200: {
                [self->avForBackGround stopAnimating];
                
                [[ProgressIndicator sharedInstance] hideProgressIndicator];
                
                //successs response.
                self->arrayOfReceivedContactDetails = [responseDictionary[@"data"] mutableCopy];
                
                [self updateTitlesForContactSync:[NSString stringWithFormat:@"%lu",(unsigned long)self->arrayOfReceivedContactDetails.count]];
                
                //it will reload the data.
                if(self->arrayOfReceivedContactDetails.count) {
                    self->rowCountForTitleSection = 1;
                    NSString *numberOfContacsSynced =[NSString stringWithFormat:@"%lu", (unsigned long)self->arrayOfReceivedContactDetails.count];
                    
                    [[NSUserDefaults standardUserDefaults] setObject:numberOfContacsSynced forKey:numberOfContactsFoundInPicogram];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    
                    
                    [self.contatctsTableView reloadData];
                    
                }
                else
                {
                    UIAlertController *controller = [CommonMethods showAlertWithTitle:NSLocalizedString(alertMessage, alertMessage) message:[NSString stringWithFormat:NSLocalizedString(noneOfFriendUsingApp, noneOfFriendUsingApp),@" %@",APP_NAME] actionTitle:NSLocalizedString(alertOk, alertOk)];
                    mPresentAlertController;
                    
                }
            }
                break;
                //failure responses.
            case 2021: {
                [self->avForBackGround stopAnimating];
                [[ProgressIndicator sharedInstance] hideProgressIndicator];
                
                [self backGrounViewForNoSuggestions:NSLocalizedString(errorFetchingContactList, errorFetchingContactList)];
            }
                break;
            case 2022:
            case 204: {
                [self->avForBackGround stopAnimating];
                [[ProgressIndicator sharedInstance] hideProgressIndicator];
                [self backGrounViewForNoSuggestions:NSLocalizedString(noSuggestionsAvailable, noSuggestionsAvailable)];
            }
                break;
        }
        
    });
}
         
#pragma mark-
#pragma mark - Show No Suggestions
         
-(UIView *)backGroundViewForEmptyTable:(NSString *)message {
    UILabel *labelForNoPostsMessage = [[UILabel alloc] init];
    labelForNoPostsMessage.text = message;
    labelForNoPostsMessage.numberOfLines =0;
    labelForNoPostsMessage.frame = CGRectMake(0, self.view.frame.size.height/2 - 20, self.view.frame.size.width, 40);
    [labelForNoPostsMessage setFont:[UIFont fontWithName:RobotoMedium size:15]];
    labelForNoPostsMessage.textAlignment = NSTextAlignmentCenter;
    
    return labelForNoPostsMessage;
}

 -(void)backGrounViewForNoSuggestions:(NSString *)message{
    
    self.viewForNoSuggestions.frame = CGRectMake(0, 0, self.contatctsTableView.bounds.size.width, self.contatctsTableView.bounds.size.height);
     self.labelForMessage.text = message ;
    
    self.contatctsTableView.backgroundView = self.viewForNoSuggestions;
}
                 
                 
#pragma mark - Custome Alert
-(void)showAlert
{
    _mainWindow = [[[UIApplication sharedApplication] delegate] window];
    UIView *iView = [[UIView alloc] initWithFrame:CGRectMake(0, 0,self.view.frame.size.width , self.view.frame.size.height)];
    [self.view addSubview:iView];
    
    iView.alpha = 0;
    [UIView animateWithDuration:0.6
                     animations:^{
                         iView.center = self.view.center;
                         iView.alpha = 15;
                     }
                     completion:^(BOOL finished){
                     }];
    
    iView.tag =10;
    [_mainWindow addSubview:iView];
    
    
    //[iView addGestureRecognizer:oneTap];
    iView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:.7];
    
    UIView *infoDetailsView = [[UIView alloc] initWithFrame:CGRectMake(25 ,self.view.frame.size.height/2 - 200/2 , 250+15, 203)];
    
    infoDetailsView.center = iView.center;
    
    infoDetailsView.backgroundColor = [UIColor whiteColor];
    [iView addSubview:infoDetailsView];
    
    infoDetailsView.layer.cornerRadius = 10.0f;
    infoDetailsView.layer.masksToBounds = YES;
    
    
    UILabel *infoTitle = [[UILabel alloc] initWithFrame:CGRectMake(10,8,250 , 25)];
    infoTitle.textAlignment = NSTextAlignmentCenter;
    [Helper setToLabel:infoTitle Text:NSLocalizedString(findPeopleToFollow, findPeopleToFollow) WithFont:RobotoMedium FSize:16 Color:[UIColor blackColor]];//[UIColor colorWithRed:47/255.0 green:183/255.0 blue:107/255.0 alpha:1.0]];
    [infoDetailsView addSubview:infoTitle];
    
    UILabel *infoDetail =[[UILabel alloc]initWithFrame:CGRectMake(15,0,247-10,158-15)];//10,32-10,247-5,158-20
    infoDetail.backgroundColor = [UIColor clearColor];
    [Helper setToLabel:infoDetail Text:NSLocalizedString(helpPeopleToFindMessage, helpPeopleToFindMessage) WithFont:RobotoRegular FSize:12 Color:[UIColor blackColor]];
    infoDetail.numberOfLines = 0;
    infoDetail.textAlignment = NSTextAlignmentCenter;
    [infoDetailsView addSubview:infoDetail];
    
    UIView *bottomView = [[UIView alloc] initWithFrame:CGRectMake(0, 104+20, 265, 80)];
    bottomView.backgroundColor = [UIColor colorWithRed:247.0f/255.0 green:247.0f/255.0 blue:247.0f/255.0 alpha:1.0];
    [infoDetailsView addSubview:bottomView];
    
    
    
    
    UIButton *allowAccessBtn = [[UIButton alloc] initWithFrame:CGRectMake(0,45, 265, 30)];//120, 0, 40, 40
    allowAccessBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
    [Helper setButton:allowAccessBtn  Text:NSLocalizedString(allowAccess, allowAccess) WithFont:RobotoMedium FSize:16 TitleColor:[UIColor colorWithRed:59/255.0f green:132/255.0f blue:239/255.0f alpha:1.0f] ShadowColor:nil];
    [allowAccessBtn addTarget:self action:@selector(allowAccess:) forControlEvents:UIControlEventTouchUpInside];
    [bottomView addSubview:allowAccessBtn];
    
    UIView *bottomLineView = [[UIView alloc] initWithFrame:CGRectMake(0, 40, 265, 0.5)];
    bottomLineView.backgroundColor = [UIColor colorWithRed:216.0f/255.0 green:216.0f/255.0 blue:216.0f/255.0 alpha:1.0];
    [bottomView addSubview:bottomLineView];
    
    UIView *topLineView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 265, 0.5)];
    topLineView.backgroundColor = [UIColor colorWithRed:216.0f/255.0 green:216.0f/255.0 blue:216.0f/255.0 alpha:1.0];
    [bottomView addSubview:topLineView];
    
    UIButton *cancelBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 5,265, 30)];
    
    [cancelBtn addTarget:self action:@selector(dissmissBtn:) forControlEvents:UIControlEventTouchUpInside];
    [Helper setButton:cancelBtn Text:NSLocalizedString(alertCancel, alertCancel) WithFont:RobotoMedium FSize:16 TitleColor:[UIColor colorWithRed:59/255.0f green:132/255.0f blue:239/255.0f alpha:1.0f] ShadowColor:nil];
    //[iView addSubview:cancelBtn];
    [bottomView addSubview:cancelBtn];
}
                 
-(IBAction)allowAccess:(id)sender{
    [UIView animateWithDuration:0 animations:^ {
        [self dissmissBtn:self];
    }completion:^(BOOL finished) {
        [self requestForFbOrPhoneContactsSync];
    }];
    
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"accessDenied"];
    [[NSUserDefaults standardUserDefaults]synchronize];
}
                 
- (IBAction)dissmissBtn:(id)sender {
    
    UIView *view = [_mainWindow   viewWithTag:10];
    [UIView animateWithDuration:0.6
                     animations:^{
                        
                         view.alpha = 0.0;
                     }
                     completion:^(BOOL finished){
                         
                         [view removeFromSuperview];
                         if([[NSUserDefaults standardUserDefaults]objectForKey:@"accessDenied"]){
                             [self.navigationController popViewControllerAnimated:YES];
                         }
                     }];
}
                 
@end
