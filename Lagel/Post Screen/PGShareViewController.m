//
//  ShareViewController.m
//
//  Created by Rahul Sharma on 3/16/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import "PGShareViewController.h"
#import "CameraViewController.h"
#import "FullImageViewXib.h"
#import "FontDetailsClass.h"
#import "MultipleImagesCell.h"
#import "CameraViewController.h"
#import "PinAddressController.h"
#import "UITextView+Placeholder.h"
#import "CategoriesListTableViewController.h"
#import "PostingScreenModel.h"
#import "HomeScreenController.h"
#import "UploadToCloudinary.h"
#import "CurrencySelectVC.h"
#define  ACCEPTABLE_CHARACTERSFORPRICE @"1234567890$₹"
#import "UIImageView+AFNetworking.h"
#import "JTSImageViewController.h"
#import "JTSImageInfo.h"
#import "SuccessFullyPostedViewController.h"

@interface PGShareViewController ()<CLLocationManagerDelegate,MKMapViewDelegate,UIGestureRecognizerDelegate,UICollectionViewDataSource,UICollectionViewDelegate,UITextFieldDelegate ,UITextViewDelegate,WebServiceHandlerDelegate,CloudinaryModelDelegate,CurrencyDelegate,cameraScreenDelegate,GetCurrentLocationDelegate,successfullyPostedDelegate> {
    CLGeocoder *geocoder;
    NSMutableArray *arrayOfSubList,*arrayOfSubcategory;
    int count ;
    BOOL _isSearching,textViewKeyboard, isValidForHashTags, showTableViewHeader;
    UIImage *containerImage;
    NSMutableArray *taggedProductsArray,*taggedProductPositions, *arrayOfContainerWidths, *arrayOfContainerHeights,*hashTagresponseData,*arrayOfHashTags, *arrayOfHashtagCount;
    NSNumber *negotiable;
    double latInDouble;
    double longInDouble;
    NSString *lastString, *searchResultsFor,*taggedFriendsString ,*postCityName ,*postCountryShortName;
    BOOL keyboardIsShown;
     GetCurrentLocation *getLocation;
    UITextField *activeField;
    UITapGestureRecognizer *tapToDismissKeyboard ;
    NSString *GoogleImagebyteArray ;
    NSDictionary *hashTagsData;
    PostingScreenModel *postModel;
    NSString *strTitle;
    NSString *strDescription;
    NSString *strNumValue;
    
    int strcount;
    int otherCount;
}

@end

@implementation PGShareViewController

/*--------------------------------------*/
#pragma mark
#pragma mark - viewcontroller LifeCycle
/*--------------------------------------*/

- (void)viewDidLoad {
    [super viewDidLoad];
    count=0;
    
    if(!self.arrayOfImagePaths)
    self.arrayOfImagePaths = [NSMutableArray new];
    arrayOfSubList = [NSMutableArray new];
    arrayOfSubcategory = [NSMutableArray new];
    arrayOfContainerWidths=[[NSMutableArray alloc]init];
    arrayOfContainerHeights=[[NSMutableArray alloc]init];
    taggedProductPositions = [[NSMutableArray alloc]init];
    hashTagresponseData = [[NSMutableArray alloc]init];
    arrayOfHashTags = [[NSMutableArray alloc]init];
    arrayOfHashtagCount = [[NSMutableArray alloc]init];
    [self setNavigationBar];
    [self GetTheUserCurrentLocation];
    [self addLongTapGestureRecognizer]; //Add LongTapGesture Recogniser
   
    self.labelForCurrency.text = @"HTG";
    self.currency = @"HTG";
    getLocation = [GetCurrentLocation sharedInstance];
    [getLocation getLocation];
    getLocation.delegate = self;
    
    tapToDismissKeyboard = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapToDismissKeyboard:)]; //Tap gesture to dismiss keyboard.
    tapToDismissKeyboard.enabled = NO;
    [self.contentView addGestureRecognizer:tapToDismissKeyboard];
    if(self.editingPost)
    {
        self.heightOfSocialMedia.constant = 0;
        self.socialMediaView.hidden = YES;
        self.contentViewBottomConstraint.constant = -125;
        [self updateFieldsForPostedProduct];
    }
    else
    {
        self.heightOfSocialMedia.constant = 120;
        self.socialMediaView.hidden = NO;
        self.contentViewBottomConstraint.constant = -5;
    }
}


-(void)setNavigationBar
{
    self.navigationController.navigationBar.translucent = NO;
    [self createNavLeftButton];
    if(self.editingPost){
        self.navigationItem.title = NSLocalizedString(navTitleForEditPost, navTitleForEditPost);
        [self.postButtonOutlet setTitle:NSLocalizedString(updateButtonTitle, updateButtonTitle) forState:UIControlStateNormal];;
        
    }
    else
        self.navigationItem.title = NSLocalizedString(navTitleForPostProduct, navTitleForPostProduct);

}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    self.navigationController.navigationBar.hidden = NO;
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.shadowImage = nil;
    self.labelForAddImagesCount.text = [NSString stringWithFormat:@"Add upto %u more photos (optional)",(unsigned)(5 - self.arrayOfImagePaths.count)];

    self.heightOfAddImagesCountView.constant = 12;
    self.navigationController.navigationBarHidden = NO;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(fbNotification:)name:@"facebookCancel" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(twitterNotification:)name:@"TwitterLoginFailed" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getMaintainedDataOfImagesFromLibrary:)name:mLibraryNotification object:nil];

}


-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardDidShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:@"facebookCancel"];
    [[NSNotificationCenter defaultCenter] removeObserver:@"TwitterLoginFailed" ];
    [[NSNotificationCenter defaultCenter] removeObserver:mLibraryNotification];
    [self.view endEditing:YES];
}


-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
}


#pragma mark
#pragma mark - Location Delegate -

- (void)updatedLocation:(double)latitude and:(double)longitude
{
    if(!self.editingPost)
    {
    latInDouble = latitude;
    longInDouble = longitude;
    }
    
}

- (void)updatedAddress:(NSString *)currentAddress
{
     if(!self.editingPost)
     {
         self.labelForAddLocation.text = currentAddress;
         postCityName = getLocation.currentCity;
         postCountryShortName = getLocation.countryShortCode;
     }
}


-(void)tapToDismissKeyboard:(id)sender
{
    [self.view endEditing:YES];
}

#pragma mark
#pragma mark - Move View

/**
 This method will move view by evaluating keyboard height.
 
 @param n pass notification data.
 */

- (void)keyboardWillShow:(NSNotification *)n
{
    
    tapToDismissKeyboard.enabled = YES;
    
    if(!textViewKeyboard){
    NSDictionary* info = [n userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0,0 /*kbSize.height*/, 0.0);
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;

    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your application might not need or want this behavior.
    CGRect aRect = self.view.frame;
    aRect.size.height -= kbSize.height;
    if (!CGRectContainsPoint(aRect, activeField.frame.origin) ) {
        CGPoint scrollPoint = CGPointMake(0.0,-(activeField.frame.origin.y - kbSize.height));
        [self.scrollView setContentOffset:scrollPoint animated:YES];
    }
    }
    
}

- (void)keyboardWillHide:(NSNotification *)n
{
    tapToDismissKeyboard.enabled = NO;
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;
}

/*--------------------------------------*/
#pragma mark
#pragma mark- LongTapGestureRecognizer
/*--------------------------------------*/

/**
 Long TapGestureRecogniser.
 */
-(void)addLongTapGestureRecognizer
{
    UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(longPressGestureRecognized:)];
    [_addImagesCollectionView addGestureRecognizer:longPress];
}


/**
 LongPress Gesture is Recognied Method.
 */
-(void)longPressGestureRecognized :(id)sender
{
    UILongPressGestureRecognizer *longPress = (UILongPressGestureRecognizer *)sender;
    UIGestureRecognizerState state=longPress.state;
    CGPoint location = [longPress locationInView:_addImagesCollectionView];
    NSIndexPath *indexPath = [_addImagesCollectionView indexPathForItemAtPoint:location];
    static UIView       *snapshot = nil;        ///< A snapshot of the row user is moving.
    static NSIndexPath  *sourceIndexPath = nil; ///< Initial index path, where gesture begins.
    
    if(indexPath.row!=self.arrayOfImagePaths.count){
        
        switch (state) {
            case UIGestureRecognizerStateBegan:
            {
                if (indexPath) {
                    sourceIndexPath = indexPath;
                    MultipleImagesCell *cell = (MultipleImagesCell *)[_addImagesCollectionView cellForItemAtIndexPath:sourceIndexPath];
                    // Take a snapshot of the selected row using helper method.
                    snapshot = [self customSnapshotFromView:cell];
                    
                    // Add the snapshot as subview, centered at cell's center...
                    __block CGPoint center = cell.center;
                    snapshot.center = center;
                    snapshot.alpha = 0.0;
                    [_addImagesCollectionView addSubview:snapshot];
                    [UIView animateWithDuration:0.25 animations:^{
                        
                        // Offset for gesture location.
                        center.x = location.x;
                        snapshot.center = center;
                        snapshot.transform = CGAffineTransformMakeScale(1.05, 1.05);
                        snapshot.alpha = 0.98;
                        
                        // Fade out.
                        cell.alpha = 0.0;
                        
                    } completion:^(BOOL finished) {
                        
                        cell.hidden = YES;
                        
                    }];
                }
                break;
            }
                
            case UIGestureRecognizerStateChanged: {
                CGPoint center = snapshot.center;
                center.x = location.x;
                snapshot.center = center;
                
                // Is destination valid and is it different from source?
                if (indexPath.row == self.arrayOfImagePaths.count) {
                    
                    indexPath = sourceIndexPath;
                    [self.arrayOfImagePaths exchangeObjectAtIndex:indexPath.row withObjectAtIndex:sourceIndexPath.row];
                    [_addImagesCollectionView moveItemAtIndexPath:sourceIndexPath toIndexPath:indexPath];
                    sourceIndexPath = indexPath;
                }
                else {
                    if (indexPath && ![indexPath isEqual:sourceIndexPath]) {
                        
                        // ... update data source.
                        [self.arrayOfImagePaths exchangeObjectAtIndex:indexPath.row withObjectAtIndex:sourceIndexPath.row];
                        
                        // ... move the rows.
                        [_addImagesCollectionView moveItemAtIndexPath:sourceIndexPath toIndexPath:indexPath];
                        
                        // ... and update source so it is in sync with UI changes.
                        sourceIndexPath = indexPath;
                    }
                }
                break;
            }
            default: {
                // Clean up.
                MultipleImagesCell *cell = (MultipleImagesCell *)[_addImagesCollectionView cellForItemAtIndexPath:sourceIndexPath];
                cell.hidden = NO;
                cell.alpha = 0.0;
                [UIView animateWithDuration:0.25 animations:^{
                    
                    snapshot.center = cell.center;
                    snapshot.transform = CGAffineTransformIdentity;
                    snapshot.alpha = 0.0;
                    // Undo fade out.
                    cell.alpha = 1.0;
                    
                } completion:^(BOOL finished) {
                    sourceIndexPath = nil;
                    [snapshot removeFromSuperview];
                    snapshot = nil;
                }];
                break;
            }
        }
    }
    
}
// Add this at the end of your .m file. It returns a customized snapshot of a given view.
- (UIView *)customSnapshotFromView:(UIView *)inputView {
    
    // Make an image from the input view.
    UIGraphicsBeginImageContextWithOptions(inputView.bounds.size, NO, 0);
    [inputView.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    // Create an image view.
    UIView *snapshot = [[UIImageView alloc] initWithImage:image];
    snapshot.layer.masksToBounds = NO;
    snapshot.layer.cornerRadius = 0.0;
    snapshot.layer.shadowOffset = CGSizeMake(-5.0, 0.0);
    snapshot.layer.shadowRadius = 5.0;
    snapshot.layer.shadowOpacity = 0.4;
    
    return snapshot;
}


/*--------------------------------------*/
#pragma mark
#pragma mark - TextField Delegate
/*--------------------------------------*/


-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    activeField = textField ;
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    [textField resignFirstResponder];
    
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if([textField isEqual:_textFieldForTitle])
    {
        if ([textField isFirstResponder])
        {
            if ([[[textField textInputMode] primaryLanguage] isEqualToString:@"emoji"] || ![[textField textInputMode] primaryLanguage])
            {
                return NO;
            }
        }
        return YES;
    }
    
    if([textField isEqual:_textFieldForPrice ]){
        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_CHARACTERSFORPRICE] invertedSet];
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs]componentsJoinedByString:@""];
        if ([string isEqualToString:filtered]) {
            NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
            NSArray  *arrayOfString = [newString componentsSeparatedByString:@"."];
            NSArray *arrayOfDollor = [newString componentsSeparatedByString:@"$"];
            NSArray *arrayOfRupee = [newString componentsSeparatedByString:@"₹"];
            
            if ([arrayOfString count] > 2  ||[arrayOfDollor count] > 2  ||[arrayOfRupee count] > 2 ) {
                return NO;
            }
            else {
                //checking number of characters after dot.
                NSRange range = [newString rangeOfString:@"."];
                if ([newString containsString:@"."] && range.location == (newString.length - 4))
                    return NO;
                else
                    return YES;
            }
            
        }
        else
            return NO;
    }
    
    return  YES;
}


-(BOOL)prefersStatusBarHidden {
    return NO;
}




-(void)tapGestureshowFullImage:(id)sender {
   }
/*--------------------------------------*/
#pragma mark
#pragma mark - tapGesture.
/*--------------------------------------*/

-(void)tapToSeeFullImage :(NSInteger)index
{ UIImageView *fullImageView = [[UIImageView alloc]init];
    JTSImageInfo *imageInfo = [[JTSImageInfo alloc] init];
    
        if([self.arrayOfImagePaths[index][@"imageType"] isEqualToString:@"cloudinaryUrl"]){
            NSData * imageData = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:self.arrayOfImagePaths[index][@"imageValue"]]];
            fullImageView.image = [UIImage imageWithData: imageData];
                }
        else
        {
            NSString *filePath = flStrForObj(self.arrayOfImagePaths[index][@"imageValue"]);
            NSData *imgData = [NSData dataWithContentsOfFile:filePath];
            fullImageView.image = [[UIImage alloc]initWithData:imgData];
        
        }
    
    imageInfo.image = fullImageView.image;
    imageInfo.referenceRect = fullImageView.frame;
    imageInfo.referenceView = fullImageView.superview;
    imageInfo.referenceContentMode = fullImageView.contentMode;
    imageInfo.referenceCornerRadius = fullImageView.layer.cornerRadius;

    JTSImageViewController *imageViewer = [[JTSImageViewController alloc]
                                           initWithImageInfo:imageInfo
                                           mode:JTSImageViewControllerMode_Image
                                           backgroundStyle:JTSImageViewControllerBackgroundOption_None];
    [imageViewer showFromViewController:self transition:JTSImageViewControllerTransition_FromOriginalPosition];

}

/**
 Tap on image to Hide it.
 
 @param sender single tapping.
 */
- (IBAction)tapToHideImageViewAction:(id)sender {
    self.navigationController.navigationBarHidden = NO;
}

/*--------------------------------------*/
#pragma mark -
#pragma mark -ButtonActions
/*--------------------------------------*/

- (IBAction)addLocationButtonAction:(id)sender {
    PinAddressController *PinVC=[self.storyboard instantiateViewControllerWithIdentifier:mPinAddressStoryboardID];
    PinVC.navigationItem.title = NSLocalizedString(mNavTitleForPinAddress, mNavTitleForPinAddress);
    PinVC.lat = latInDouble;
    PinVC.longittude = longInDouble;
    PinVC.callBackForLocation=^(NSDictionary *locDict)// *location ,double selectedLattitude,double selectedLongitude)
    {
        self->postCityName = locDict[@"city"];
        self->postCountryShortName = locDict[@"countryShortName"];
//        NSString *location = locDict[@"address"];
        NSString *location = [NSString stringWithFormat: @"%@, %@", locDict[@"selectedCity"], locDict[@"selectedCountry"]];
        if(location.length == 0){
         self.labelForAddLocation.text = NSLocalizedString(addLocationTitle, addLocationTitle);
        }
        else
        {
        self.labelForAddLocation.text = location ;
        }
        self.buttonToRemoveLocOutlet.hidden = NO;
        
        self->latInDouble =  [locDict[@"lat"] doubleValue];
        self->longInDouble = [locDict[@"long"] doubleValue];
    };
    [self.navigationController pushViewController:PinVC animated:YES];
}

- (IBAction)loactionCancelButtonAction:(id)sender {
    self.labelForAddLocation.text = NSLocalizedString(addLocationTitle, addLocationTitle);
    self.buttonToRemoveLocOutlet.hidden = YES;

}

- (IBAction)tagPeopleButtonAction:(id)sender {
    [self performSegueWithIdentifier:@"TagPeopleSegue" sender:nil];
}


/*------------------------------------------------------------------------------*/
#pragma mark -
#pragma mark -  data reciving from places view controller.
/*-----------------------------------------------------------------------------*/
-(void)sendDataToA:(NSMutableArray *)array andPositions:(NSMutableArray *)positionsArray {
    taggedProductsArray = array;
    taggedProductPositions = positionsArray;
    switch (array.count) {
        case 0:{
            self.labelForTaggedProducts.hidden = YES;
            }
            break;
        case 1:{
             self.labelForTaggedProducts.hidden = NO;
            self.labelForTaggedProducts.text = array[0];
            self.labelForTaggedProducts.layer.borderColor = mBaseColor.CGColor ;
            }
            break;
            
           default:
            self.labelForTaggedProducts.layer.borderColor = [UIColor whiteColor].CGColor ;
            self.labelForTaggedProducts.hidden = NO;
            self.labelForTaggedProducts.text = [[NSString stringWithFormat:@"%lu",(unsigned long)array.count] stringByAppendingString:@" items"] ;
            break;
    }
}

/*------------------------------*/
#pragma mark- CollectionView DataSource Method
/*------------------------------*/

-(NSInteger )collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
 
    if(_arrayOfImagePaths.count<5){
            self.heightOfAddImagesCountView.constant = 12;
            return self.arrayOfImagePaths.count+1;
           }
        else{
            self.heightOfAddImagesCountView.constant = 0;
            return self.arrayOfImagePaths.count;
        }
    
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    MultipleImagesCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:mAddMultiImagesCellID forIndexPath:indexPath];
        if( indexPath.row == self.arrayOfImagePaths.count)
        {
            [cell updateCellObjectWithImage:[UIImage imageNamed:mAddImageButtonImageName]];
//            cell.imageViewContainer.tag=1000 + self.arrayOfImagePaths.count;
            return cell;
        }

        
            if([self.arrayOfImagePaths[indexPath.row][@"imageType"] isEqualToString:@"cloudinaryUrl"])
            {   UIImageView *tempImageView = [[UIImageView alloc]init];

              containerImage =  [tempImageView image];
              containerImage =  [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:self.arrayOfImagePaths[indexPath.row][@"imageValue"]]]];
            }
            else
            {
                NSString *filePath = flStrForObj(self.arrayOfImagePaths[indexPath.row][@"imageValue"]);
                NSData *imgData = [NSData dataWithContentsOfFile:filePath];
                containerImage = [[UIImage alloc] initWithData:imgData];
            }


        NSNumber *width = [NSNumber numberWithFloat:containerImage.size.width];
        NSNumber *height= [NSNumber numberWithFloat:containerImage.size.height];
        [arrayOfContainerWidths addObject:width];
        [arrayOfContainerHeights addObject:height];
        [cell updateCellWithImage:containerImage andIndex:indexPath];
        [cell.removeButton addTarget:self action:@selector(removeImage:) forControlEvents:UIControlEventTouchUpInside];
        return cell;
    
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
        CGSize cellSize;
        cellSize.width = mWidthOfAddMultiImagesCell ;
        cellSize.height = mHeightOfAddMultiImagesCell;
        return cellSize;
    
}


-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if(collectionView.tag == 100)
    {
        if(indexPath.row == self.arrayOfImagePaths.count)
        {
                CameraViewController *cameraVC  = [self.storyboard instantiateViewControllerWithIdentifier:@"CameraStoryBoardID"];
                cameraVC.cameraAgain = true;
                cameraVC.arrayOfChoosenImages = self.arrayOfImagePaths ;
                cameraVC.cameraDelegate = self;
                [self.navigationController pushViewController:cameraVC animated:NO];
        }
            
        else
        {
            [self tapToSeeFullImage:indexPath.row];
        }
        
    }
}

/*----------------------------------------------------*/
#pragma mark
#pragma mark - navigation bar buttons
/*-----------------------------------*/

- (void)createNavLeftButton {
    UIButton *navLeft = [[UIButton alloc]init];
    if(_editingPost)
    {
     navLeft = [CommonMethods createNavButtonsWithselectedState:mCloseButton normalState:mCloseButton];
    }
    else
    {
    navLeft = [CommonMethods createNavButtonsWithselectedState:mNavigationBackButtonImageName normalState:mNavigationBackButtonImageName];
    [navLeft rotateButton];
    }
    
    UIBarButtonItem *navLeftButton = [[UIBarButtonItem alloc]initWithCustomView:navLeft];
    [navLeft setFrame:CGRectMake(0,0,25,20)];
    self.navigationItem.leftBarButtonItem = navLeftButton;
    [navLeft addTarget:self action:@selector(navLeftButtonAction) forControlEvents:UIControlEventTouchUpInside];
     [self.navigationItem setLeftBarButtonItems:@[[CommonMethods getNegativeSpacer],navLeftButton]];
}

- (void)navLeftButtonAction
{
    
    if(self.editingPost)
    {
    [self dismissViewControllerAnimated:YES completion:nil];
    }
    else
    {
     if(self.callBackForCamera)
     {
         self.callBackForCamera(self.arrayOfImagePaths ) ;
     }
    [self.navigationController popViewControllerAnimated:YES];
    }
}
/*-------------------------------------*/
#pragma mark
#pragma mark - RemoveImage Selector Method.
/*------------------------------------*/

-(void)removeImage:(id) sender
{
    UIButton *btn=(UIButton *)sender;
    NSInteger tag=btn.tag%1000;
    [self.arrayOfImagePaths removeObjectAtIndex:tag];
    [self.addImagesCollectionView reloadData];
    int imageCount = (unsigned)(self.arrayOfImagePaths.count);
    self.labelForAddImagesCount.text = [NSString stringWithFormat:@"Add upto %u more photos (optional)",(unsigned)(5 - imageCount)];
    if(self.arrayOfImagePaths.count==0)
    self.labelForAddImagesCount.text = NSLocalizedString(addAtleastSingleImage, addAtleastSingleImage);
}

/*-----------------------------------------------*/
#pragma mark -
#pragma mark - textView Delegate
/*----------------------------------------------*/

-(void)textViewDidBeginEditing:(UITextView *)textView
{
    textViewKeyboard = YES;
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    [self.captionTextViewOutlet resignFirstResponder];
    textViewKeyboard = NO;
}

/*--------------------------------------*/
#pragma mark -
#pragma mark - CLLocationManagerDelegate
/*--------------------------------------*/

/**
 Get the current location of user.
 */
-(void)GetTheUserCurrentLocation { 
    if ([CLLocationManager locationServicesEnabled ]) {
        self.locationManager = [[CLLocationManager alloc] init];
        self.locationManager.delegate = self;
        // Check for iOS 8. Without this guard the code will crash with "unknown selector" on iOS 7.
        if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
            [self.locationManager requestWhenInUseAuthorization];
        }
        [self.locationManager startUpdatingLocation];
    }
    
}

- (void)errorAlert:(NSString *)message {
    UIAlertController *controller = [CommonMethods showAlertWithTitle:@"Message" message:message actionTitle:@"Ok"];
    mPresentAlertController;
    
}



/*------------------------------------------*/
#pragma mark -
#pragma mark - Social Network Actions
/*------------------------------------------*/

- (IBAction)TumblrAction:(id)sender
{}
- (IBAction)FacebookAction:(id)sender{
    
    if (self.facebookSwitch.on) {
        [Helper checkFbLoginforViewController:self ];
        self.facebookButtonOutlet.selected =YES;
    }
    else {
        self.facebookButtonOutlet.selected =NO;
    }
}
- (IBAction)twitterAction:(id)sender{
    
    if (self.twitterSwitch.on) {
        [Helper chkTwitterLogin];
        self.TwitterButtonOutlet.selected = YES;
    }
    else
    {
        self.TwitterButtonOutlet.selected = NO;
    }
}
- (IBAction)instgramAction:(id)sender {
    if (self.instgramSwitch.on ) {
        self.InstagrambuttonOutlet.selected = YES;
    }
    else {
        self.InstagrambuttonOutlet.selected = NO;
        //[Helper ins]
    }
}
-(void)fbNotification:(NSNotification *)noti {
    self.facebookButtonOutlet.selected = NO;
    self.facebookSwitch.on = NO;
}

-(void)twitterNotification:(NSNotification *)noti {
    dispatch_async(dispatch_get_main_queue(), ^{
        self.TwitterButtonOutlet.selected = NO;
        self.twitterSwitch.on = NO;
      
        [Helper showAlertWithTitle:NSLocalizedString(alertTitle, alertTitle) Message:NSLocalizedString(noTwitterAcoount, noTwitterAcoount) viewController:self];
    });
}


/*------------------------------------------*/
#pragma mark -
#pragma mark - Drop Downs for Fields.
/*------------------------------------------*/

- (IBAction)chooseCategoryButton:(id)sender {
    CategoriesListTableViewController *listVC = [self.storyboard instantiateViewControllerWithIdentifier:@"listTableView"];
    listVC.navigationItem.title = NSLocalizedString(navTitleForProductCategory, navTitleForProductCategory);
    listVC.showResultsFor = @"categories";
    listVC.previousSelection = self.labelForCategory.text;
    listVC.callBack = ^(NSMutableArray *arrayOfList)
    {
        self->arrayOfSubList = arrayOfList;
        self.labelForCategory.text = [NSString stringWithFormat:@"%@",[self->arrayOfSubList valueForKey:@"name"]];
        self.labelForCategory.text = [self.labelForCategory.text capitalizedString];
        self.labelForCategory.text = [self.labelForCategory.text stringByReplacingOccurrencesOfString:@"And" withString:@"and"];
        self->arrayOfSubcategory = [self->arrayOfSubList valueForKey:@"subcategory"];
        if(self->arrayOfSubcategory.count){
            self.labelForSubCategory.text = @"";
            self.viewForSubcategory.hidden = NO;
            self.hieghtOfSubcategoryView.constant = 50;
            self.heightOfSubcategoryDivider.constant = 0.5;
        }
        else
        {
            self.viewForSubcategory.hidden = YES;
            self.hieghtOfSubcategoryView.constant = 0;
            self.heightOfSubcategoryDivider.constant = 0;
        }
    };
    [self.navigationController pushViewController:listVC animated:YES];
}

- (IBAction)chooseSubCategoryButton:(id)sender {
    [self chooseList:arrayOfSubcategory forLabel:self.labelForSubCategory showResults:@"subcategory" navTitle:@"Product Subcategory" andKey:@"subcategoryname"];
}

- (IBAction)chooseConditionButton:(id)sender {
    [self chooseList:[mArrayOfConditions mutableCopy] forLabel:self.labelForCondition showResults:@"conditions" navTitle:@"Product Condition" andKey:@"conditions"];
}

- (IBAction)chooseCurrencyButtonAction:(id)sender {
    CurrencySelectVC *currencySelectVC = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"CurrencySelectVC"];
    [currencySelectVC setDelegate:self];
    currencySelectVC.previousSelection = self.labelForCurrency.text ;
    [self.navigationController pushViewController:currencySelectVC animated:YES];
}

#pragma mark - CurrencyDelegate Method
-(void) country:(CurrencySelectVC *)country didChangeValue:(id)value{
    [country setDelegate:nil];
    NSDictionary *countryDict = value;
    self.labelForCurrency.text=[NSString stringWithFormat:@"%@",[countryDict objectForKey:CURRENCY_CODE]];
    self.currency = [NSString stringWithFormat:@"%@",[countryDict objectForKey:CURRENCY_CODE]];
}


/**
 Method to choose from list in Table.
 
 @param requiredArray  arrayOfdata want to forward.
 @param labelFor      label for selected Field.
 @param results       results to show in TableViewController.
 */
-(void)chooseList :(NSMutableArray *)requiredArray forLabel:(UILabel *)labelFor showResults :(NSString *)results navTitle:(NSString *)navTitle andKey:(NSString *)key
{
    CategoriesListTableViewController *listVC = [self.storyboard instantiateViewControllerWithIdentifier:@"listTableView"];
    listVC.navigationItem.title = navTitle;
    listVC.previousSelection = self.labelForCondition.text;
    listVC.showResultsFor = results;
    listVC.key = key;
    listVC.dataArray = requiredArray;
    listVC.callBack = ^(NSMutableArray *arrayOfList)
    {
        NSString * text = [NSString stringWithFormat:@"%@",arrayOfList];
        labelFor.text = NSLocalizedString(text, text) ;
        labelFor.text = [labelFor.text capitalizedString];
    };
    
    [self.navigationController pushViewController:listVC animated:YES];
    
    
}


/**
 Posting details to home screen
 
 @param sender selfButton
 */
- (IBAction)postButtonAction:(id)sender {
    
    strcount = 0;
    otherCount = 0;
    strNumValue = @"";
    
    if (![self.captionTextViewOutlet.text isEqualToString:@""]) {
        strDescription = self.captionTextViewOutlet.text;
        NSCharacterSet* notDigits = [[NSCharacterSet letterCharacterSet] invertedSet];
        for (int i = 0; i< [self.captionTextViewOutlet.text length]; i++) {
            NSString *ichar  = [NSString stringWithFormat:@"%c", [self.captionTextViewOutlet.text characterAtIndex:i]];
            if ([ichar rangeOfCharacterFromSet:notDigits].location == NSNotFound) {
                NSLog(@"Called");
                if (strcount > 7 && strcount < 13) {
                    strDescription = [strDescription stringByReplacingOccurrencesOfString:strNumValue withString:@" "];
                }
                strcount = 0;
                strNumValue = @"";
            } else {
                NSLog(@"Called else");
                NSCharacterSet* noDigits = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
                if ([ichar rangeOfCharacterFromSet:noDigits].location == NSNotFound) {
                    NSLog(@"Called else if");
                    strcount += 1;
                } else {
                    NSLog(@"Called else else");
                    
                }
                strNumValue = [strNumValue stringByAppendingString:ichar];
                
            }
        }
        if (strcount > 7 && strcount < 13) {
            strDescription = [strDescription stringByReplacingOccurrencesOfString:strNumValue withString:@" "];
        }
        strcount = 0;
        strNumValue = @"";
    }
    
    strcount = 0;
    strNumValue = @"";
    
    if (![self.textFieldForTitle.text isEqualToString:@""]) {
        strTitle = self.textFieldForTitle.text;
        NSCharacterSet* notDigits = [[NSCharacterSet letterCharacterSet] invertedSet];
        for (int i = 0; i< [self.textFieldForTitle.text length]; i++) {
//            NSString *ichar  = [NSString stringWithFormat:@"%c", [self.textFieldForTitle.text characterAtIndex:i]];
            char ch = [self.textFieldForTitle.text characterAtIndex:i];
            NSString *ichar  = [NSString stringWithFormat:@"%c", ch];
            if ([ichar rangeOfCharacterFromSet:notDigits].location == NSNotFound) {
                NSLog(@"Called");
                if (strcount > 7 && strcount < 13) {
                    strTitle = [strTitle stringByReplacingOccurrencesOfString:strNumValue withString:@" "];
                }
                strcount = 0;
                strNumValue = @"";
            } else {
                NSLog(@"Called else");
                NSCharacterSet* noDigits = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
                if ([ichar rangeOfCharacterFromSet:noDigits].location == NSNotFound) {
                    NSLog(@"Called else if");
                    strcount += 1;
                } else {
                    NSLog(@"Called else else");
                    
                }
                strNumValue = [strNumValue stringByAppendingString:ichar];
            }
        }
        
        if (strcount > 7 && strcount < 13) {
            strTitle = [strTitle stringByReplacingOccurrencesOfString:strNumValue withString:@" "];
        }
        strcount = 0;
        strNumValue = @"";
    }
    
    strcount = 0;
    strNumValue = @"";
    
    if(self.negotiableSwitchOutlet.on)
        negotiable = [NSNumber numberWithInteger:1];
    else
        negotiable = [NSNumber numberWithInteger:0];
    if ([self.textFieldForTitle.text isEqualToString:@""])
        [self showAlertForMissingField:NSLocalizedString(mTitleMissingAlert, mTitleMissingAlert)];
    else if ([self.labelForCategory.text isEqualToString:@""])
        [self showAlertForMissingField:NSLocalizedString(mCategoryMissingAlert, mCategoryMissingAlert)];
    else if([self.labelForCondition.text isEqualToString:@""])
        [self showAlertForMissingField:NSLocalizedString(mConditionMissingAlert, mConditionMissingAlert)];
    else if([self.labelForCurrency.text isEqualToString:@""])
        [self showAlertForMissingField:NSLocalizedString(mCurrencyMissingAlert, mCurrencyMissingAlert)];
    else if ([self.textFieldForPrice.text isEqualToString:@""])
        [self showAlertForMissingField:NSLocalizedString(mPriceMissingAlert, mPriceMissingAlert)];
    else if([self.labelForAddLocation.text isEqualToString:NSLocalizedString(addLocationTitle, addLocationTitle)])
        [self showAlertForMissingField:NSLocalizedString(mAddLocationAlert,mAddLocationAlert) ];
    else if(_editingPost)
    {
        ProgressIndicator *HomePI = [ProgressIndicator sharedInstance];  [HomePI showPIOnView:self.view withMessage:NSLocalizedString(UpdatingIndicatorTitle, UpdatingIndicatorTitle)];
        [self uploadEditingOnPost];
    }
    else {
         ProgressIndicator *HomePI = [ProgressIndicator sharedInstance];
        [HomePI showPIOnView:self.view withMessage:NSLocalizedString(PostingIndicatorTitle, PostingIndicatorTitle)];
        
        postModel = [[PostingScreenModel alloc] initWithTitle:strTitle description:strDescription category:self.labelForCategory.text subcategory:self.labelForSubCategory.text currency:self.currency condition:self.labelForCondition.text price:self.textFieldForPrice.text negotiable:negotiable place:self.labelForAddLocation.text latitude:latInDouble longitude:longInDouble postedImagePath:self.postedImagePath postedThumbnailPath:self.postedthumbNailImagePath facebookSwitchState:self.facebookButtonOutlet.selected twitterSwitchState:self.TwitterButtonOutlet.selected instgramSwitchState:self.InstagrambuttonOutlet.selected arrayOfimagePaths:self.arrayOfImagePaths tagPoductCoordinates:taggedProductPositions taggedFriendsString:taggedProductsArray countryShortName:postCountryShortName andPostId:@"" andCity:postCityName];
        [PostingScreenModel sharedInstance].modelObj = postModel;
        postModel.startUpload = YES;
            UploadToCloudinary *cloudinaryObj = [[UploadToCloudinary alloc]initWithArrayOfImagePaths:postModel.arrayOfImagePaths];
            cloudinaryObj.delegate = self;
            [UploadToCloudinary sharedInstance].cloudObj = cloudinaryObj;
            [cloudinaryObj uploadingImageToCloudinaryWithArrayOfPaths];

        }
    
}

/**
 To show an alert.
 
 @param message message want to display.
 */
-(void)showAlertForMissingField :(NSString *)message
{
    [Helper showAlertWithTitle:NSLocalizedString(alertTitle, alertTitle) Message:message viewController:self];
}

/**
 Get details of post and update the respective fields accordingly.
 */
-(void)updateFieldsForPostedProduct
{
    self.textFieldForTitle.text = [NSString stringWithFormat:@"%@",flStrForObj([self.postedProductDetails valueForKey:@"productName"])];
    self.captionTextViewOutlet.text = [NSString stringWithFormat:@"%@",flStrForObj([self.postedProductDetails valueForKey:@"description"])];
    self.labelForCategory.text = flStrForObj([[self.postedProductDetails valueForKey:@"categoryData"][0] valueForKey:@"category"]);
   
    if(_labelForCategory.text.length < 2)
    {
       _labelForCategory.text = flStrForObj([self.postedProductDetails valueForKey:@"category"]);
    }
     self.labelForCategory.text = [self.labelForCategory.text capitalizedString];
    self.labelForCurrency.text =  [NSString stringWithFormat:@"%@",flStrForObj([self.postedProductDetails valueForKey:@"currency"])];
    self.labelForCondition.text =  [NSString stringWithFormat:@"%@",flStrForObj([self.postedProductDetails valueForKey:@"condition"])];
    self.labelForCondition.text = [self.labelForCondition.text capitalizedString];
    self.textFieldForPrice.text =  [NSString stringWithFormat:@"%@",flStrForObj([self.postedProductDetails valueForKey:@"price"])];
    
    NSString *place = [NSString stringWithFormat:@"%@",flStrForObj([self.postedProductDetails valueForKey:@"place"])];
    latInDouble = [[NSString stringWithFormat:@"%@",flStrForObj([self.postedProductDetails valueForKey:mlatitude])]floatValue];
    longInDouble = [[NSString stringWithFormat:@"%@",flStrForObj([self.postedProductDetails valueForKey:mlongitude])]floatValue];
    self.labelForAddLocation.text = place;
    postCityName = [NSString stringWithFormat:@"%@",flStrForObj([self.postedProductDetails valueForKey:@"city"])];
    postCountryShortName = [NSString stringWithFormat:@"%@",flStrForObj([self.postedProductDetails valueForKey:@"countrySname"])];
    
    if([[NSString stringWithFormat:@"%@",flStrForObj([self.postedProductDetails valueForKey:@"negotiable"])]integerValue])
        [self.negotiableSwitchOutlet setOn:YES animated:YES];
    else
        [self.negotiableSwitchOutlet setOn:NO animated:YES];
    [self addImageUrlsToUpdate];
    [self.addImagesCollectionView reloadData];
    
}

-(void)addImageUrlsToUpdate
{
    NSArray *arrayOfUrlNames = [[NSArray alloc]initWithObjects:@"mainUrl",@"imageUrl1",@"imageUrl2",@"imageUrl3",@"imageUrl4", nil];
    NSArray *arrayOfPublicId = [[NSArray alloc]initWithObjects:mcloudinaryPublicId,mcloudinaryPublicId1,mcloudinaryPublicId2,mcloudinaryPublicId3,mcloudinaryPublicId4, nil];
    NSInteger imageCount = [[NSString stringWithFormat:@"%@",flStrForObj([self.postedProductDetails valueForKey:@"imageCount"])]integerValue]; 
    
    for(int j=0;j<imageCount;j++)
    {   NSString *key = [NSString stringWithFormat:@"%@",[arrayOfUrlNames objectAtIndex:j]];
        NSString *publicIdKey = [NSString stringWithFormat:@"%@",[arrayOfPublicId objectAtIndex:j]];
        NSString *typeOfImage = @"cloudinaryUrl";
        NSString *valueForImage =[NSString stringWithFormat:@"%@",flStrForObj([self.postedProductDetails valueForKey:key])];
        NSString *publicId = [NSString stringWithFormat:@"%@",flStrForObj([self.postedProductDetails valueForKey:publicIdKey])];
        NSMutableDictionary  *dictForImageUrl = [[NSMutableDictionary alloc] init];
        [dictForImageUrl setValue:typeOfImage forKey:@"imageType"];
        [dictForImageUrl setValue:valueForImage forKey:@"imageValue"];
        [dictForImageUrl setValue:publicId forKey:@"publicId"];
        [self.arrayOfImagePaths addObject:dictForImageUrl];
    }
}

/*-----------------------------------------------------*/
#pragma mark - Handling Response.
/*----------------------------------------------------*/

/**
 Handling Response Coming from webservice.
 
 @param response dictionary with posted data.
 */
-(void)handlingResponseForEditPost:(NSDictionary *)response
{
    
}

/**
 This method will create a model object of posting details, cloudinary details and upload edited post.
 */
-(void)uploadEditingOnPost
{
    PostingScreenModel *post = [[PostingScreenModel alloc] initWithTitle:strTitle description:strDescription category:self.labelForCategory.text subcategory:self.labelForSubCategory.text currency:self.currency condition:self.labelForCondition.text price:self.textFieldForPrice.text negotiable:negotiable place:self.labelForAddLocation.text latitude:latInDouble longitude:longInDouble postedImagePath:self.postedImagePath postedThumbnailPath:self.postedthumbNailImagePath facebookSwitchState:self.facebookButtonOutlet.selected twitterSwitchState:self.TwitterButtonOutlet.selected instgramSwitchState:self.InstagrambuttonOutlet.selected arrayOfimagePaths:self.arrayOfImagePaths tagPoductCoordinates:taggedProductPositions taggedFriendsString:taggedProductsArray countryShortName:postCountryShortName andPostId:[NSString stringWithFormat:@"%@",flStrForObj([self.postedProductDetails valueForKey:@"postId"])] andCity:postCityName];
    post.editPost = YES;
    [PostingScreenModel sharedInstance].modelObj = post;
    post.modelObj = post;
    
    UploadToCloudinary *cloudinaryObj = [[UploadToCloudinary alloc]initWithArrayOfImagePaths:self.arrayOfImagePaths];
    cloudinaryObj.delegate = self;
    [UploadToCloudinary sharedInstance].cloudObj = cloudinaryObj;
    [cloudinaryObj uploadingImageToCloudinaryWithArrayOfPaths];
    
}



#pragma mark -
#pragma mark - Cloudinary Delegates
/**
 Get dictionary from coudinary Model.

 @param requestDic Dictionary.
 */
-(void)getDictionaryFromCloudinaryModelClass:(NSDictionary *)requestDic
{
    
    if(self.editingPost)
       {
        [WebServiceHandler editPost:requestDic andDelegate:self];
       }
    else
    {
       [WebServiceHandler postImageOrVideo:requestDic andDelegate:self];
       // [self postToSocialMedia:requestDic];
    }
   
}


-(void)cloudinaryError:(NSString *)errorResult andCheck:(BOOL)isError
{
    [[ProgressIndicator sharedInstance] hideProgressIndicator];
    NSString *message;
    
    message = NSLocalizedString(unableToPostYourListings, unableToPostYourListings) ;
    
    
    UIAlertController *controller  = [UIAlertController alertControllerWithTitle:NSLocalizedString(sorry, sorry) message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *action = [UIAlertAction actionWithTitle:NSLocalizedString(alertOk, alertOk) style:UIAlertActionStyleDefault handler:
                             ^(UIAlertAction *action)
                             {
                                 [WebServiceHandler getCloudinaryCredintials:@{@"":@""} andDelegate:self];
                                 
                             }];
    
    [controller addAction:action];
    [self presentViewController:controller animated:YES completion:nil];
}


/*----------------------------------------------------- */
#pragma mark - Share To SocialMedia
/*----------------------------------------------------*/

- (void) postToSocialMedia:(NSMutableArray*)postDetails
{
    if (postModel.facebookSwitchState){
        dispatch_async(dispatch_get_main_queue(), ^{
            [Helper makeFBPostWithParams:postDetails];
        });
    }
    if (postModel.instgramSwitchState) {
        
        NSString *mainUrl = postDetails[0][@"mainUrl"];
        NSURL *instagramURL = [NSURL URLWithString:@"instagram:app"];
        if ([[UIApplication sharedApplication] canOpenURL:instagramURL]) {
            [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"Instagram_Share"];
            [[NSUserDefaults standardUserDefaults]setValue:mainUrl forKey:@"postUrl"];
            [[NSUserDefaults standardUserDefaults]synchronize];
        }
        
        else                                                                              
        {
            [Helper showAlertWithTitle:NSLocalizedString(alertMessage,alertMessage) Message:NSLocalizedString(instagramIsNotInstalled,instagramIsNotInstalled) viewController:self];

        }
    }
}





/**
 This method is delegate method get call on success of web service call.
 
 @param requestType RequestType.
 @param response    response comes from web Service.
 @param error       error if any.
 */

-(void)didFinishLoadingRequest:(RequestType)requestType withResponse:(id)response error:(NSError *)error
{
    [[ProgressIndicator sharedInstance] hideProgressIndicator];
    [self.activityIndicator stopAnimating];
    self.postButtonOutlet.titleLabel.hidden = NO;
    [self.postButtonOutlet setTitle:NSLocalizedString(postButtonTitle, postButtonTitle) forState:UIControlStateNormal];
    
    if (error) {
        
        return;
    }
    
    NSMutableDictionary *responseDict = (NSMutableDictionary*)response;
    NSLog(@"%@", responseDict);
    if (requestType == RequestTypePost) {
        //success response(200 is for success code).
        switch ([responseDict[@"code"] integerValue]) {
            case 200: {
                [self postToSocialMedia:response[@"data"]];
                [[ProgressIndicator sharedInstance] hideProgressIndicator];
                [[NSNotificationCenter defaultCenter] postNotificationName:mSellingPostNotifiName object:response[@"data"]];
                [[NSNotificationCenter defaultCenter] postNotificationName:mAddNewPost object:response[@"data"]];
                SuccessFullyPostedViewController *newVC = [self.storyboard instantiateViewControllerWithIdentifier:@"PostedSuccessfullyStoryboardId"];
                newVC.postingDelegate = self ;
                newVC.isTwitterSharing = postModel.twitterSwitchState ;
                newVC.postId = flStrForObj(response[@"data"][0][@"postId"]);
                [self presentViewController:newVC animated:NO completion:nil];
            }
                break;
                //failure responses.
            case 1986:
                break;
            case 1987:
                break;
            case 1988: {
                [self errorAlert:responseDict[@"message"]];
            }
                break;
            default:
                break;
        }
    }
    
    //response for hashtagsuggestion api.
    if (requestType == RequestTypeEditPost) {
        switch ([responseDict[@"code"] integerValue]) {
            case 200: {
                response[@"data"][0][@"city"] = postCityName;
                response[@"data"][0][@"countrySname"] = postCountryShortName;
                [[NSNotificationCenter defaultCenter] postNotificationName:mUpdatePostDataNotification object:responseDict];
                ProgressIndicator *pi = [ProgressIndicator sharedInstance];
                [pi hideProgressIndicator];
                if(self.popDelegate)
                {
                    [self.popDelegate popToRootViewController:YES];
                }
                [self dismissViewControllerAnimated:YES completion:nil];
               
            }
                break;
                
        }
    }
        
        if (requestType == RequestTypeCloudinaryCredintials ) {
            
            switch ([responseDict[@"code"] integerValue]) {
                case 200: {
                    // success response.
                    [[NSUserDefaults standardUserDefaults] setObject:responseDict forKey:cloudinartyDetails];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                }
                    break;
                    
                default:
                    break;
            }
        }
    
}

#pragma mark -
#pragma mark - CameraScreen Delegate

-(void)getMaintainedDataOfImages:(NSMutableArray *)arrayOfmaintainedImages
{
    self.arrayOfImagePaths = arrayOfmaintainedImages;
    [self.addImagesCollectionView reloadData];
}

#pragma mark -
#pragma mark - Library Delegate

-(void)getMaintainedDataOfImagesFromLibrary:(NSNotification *)noti
{
    self.arrayOfImagePaths = noti.object;
    [self.addImagesCollectionView reloadData];
}


#pragma mark
#pragma mark - PostedSuccessfully Delegate

-(void)postingListedSuccessfully
{
    [self dismissViewControllerAnimated:NO completion:nil];
    if(self.cameraFromTabBar){
        [self.tabBarController setSelectedIndex:0];
        [self.navigationController popToRootViewControllerAnimated:NO];
    }
    else
    {
        [self dismissViewControllerAnimated:NO completion:Nil];
    }
    
    
}
@end
