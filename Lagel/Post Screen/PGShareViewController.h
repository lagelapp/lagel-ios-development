//
//  ShareViewController.h

//
//  Created by Rahul Sharma on 3/16/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import "SCRecorder.h"

@protocol updateCallBack <NSObject>

-(void)popToRootViewController :(BOOL)pop ;
@end;



/**
 Callback to maintain the images from Camera.

 @param arrayOfCameraImages array of captured images paths.
 */
typedef void (^CallBackForCameraScreen)(NSArray *arrayOfCameraImages);

@interface PGShareViewController : UIViewController<UIScrollViewDelegate,UITextViewDelegate,MKMapViewDelegate, CLLocationManagerDelegate,UITableViewDelegate,UICollectionViewDelegate,UICollectionViewDataSource,SCPlayerDelegate> {
    @private
}


@property BOOL editingPost, cameraFromTabBar;
@property (strong,nonatomic)NSMutableArray *postedProductDetails, *arrayOfImagePaths;

@property (weak,nonatomic)id<updateCallBack>popDelegate;

@property (copy,nonatomic) CallBackForCameraScreen callBackForCamera ;
  //constraints outlets

@property (strong, nonatomic) IBOutlet UIView *contentView;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *textViewTopConstraintOutlet;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *textViewHeightOUtletConstraint;
@property (strong, nonatomic) IBOutlet UISwitch *negotiableSwitchOutlet;

//Array to store added images
@property(nonatomic)NSMutableArray *addImages;

  //imageView outlets

  //label outlets
@property (strong, nonatomic) IBOutlet UILabel *labelForAddLocation;

@property (strong, nonatomic) IBOutlet UILabel *labelForCategory;
@property (strong, nonatomic) IBOutlet UILabel *labelForSubCategory;
@property (strong, nonatomic) IBOutlet UILabel *labelForCondition;
@property (strong, nonatomic) IBOutlet UILabel *labelForCurrency;
@property (strong,nonatomic) NSString *currency;

@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;


  //button outlets

@property (weak, nonatomic) IBOutlet UIButton *loactionCancelButtonAction;
@property (weak, nonatomic) IBOutlet UIButton *addLocationButtonOutlet;

  //textView Outlets
@property (weak, nonatomic) IBOutlet UITextView *captionTextViewOutlet;

   //location

@property(nonatomic,strong)CLLocation *currentLocation;
@property (nonatomic,strong) CLLocationManager *locationManager;

  //view outlets
@property (weak, nonatomic) IBOutlet UIView *textViewSuperViewOutlet;

//button actions
- (IBAction)addLocationButtonAction:(id)sender;
- (IBAction)loactionCancelButtonAction:(id)sender;

- (IBAction)FacebookAction:(id)sender;
- (IBAction)twitterAction:(id)sender;
- (IBAction)instgramAction:(id)sender;
- (IBAction)chooseCategoryButton:(id)sender;
- (IBAction)chooseSubCategoryButton:(id)sender;
- (IBAction)chooseConditionButton:(id)sender;
- (IBAction)tapToHideImageViewAction:(id)sender;

//Buttons Outlet
@property (strong, nonatomic) IBOutlet UISwitch *twitterSwitch;
@property (strong, nonatomic) IBOutlet UISwitch *facebookSwitch;
@property (weak, nonatomic) IBOutlet UISwitch *instgramSwitch;
@property (weak, nonatomic) IBOutlet UIButton *InstagrambuttonOutlet;
@property (weak, nonatomic) IBOutlet UIButton *TwitterButtonOutlet;
@property (weak, nonatomic) IBOutlet UIButton *facebookButtonOutlet;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *hieghtOfSubcategoryView;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *heightOfSubcategoryDivider;

@property NSString *postedImagePath;
@property NSString *postedthumbNailImagePath;

 @property(nonatomic,strong)NSArray *searchResults;
 @property(nonatomic,strong)NSArray *firstResut;


@property NSString *pathOfVideo;
@property SCRecordSession *recordsession;
@property (nonatomic,strong) SCPlayer *player;
@property bool sharingVideo;
@property NSString *imageForVideoThumabnailpath;
@property UIImage *videoimg;
@property (weak, nonatomic) IBOutlet UICollectionView *addImagesCollectionView;
@property (weak, nonatomic) IBOutlet UITextField *textFieldForTitle;
@property (strong, nonatomic) IBOutlet UIButton *postButtonOutlet;
- (IBAction)postButtonAction:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *textFieldForPrice;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;

@property (strong, nonatomic) IBOutlet UILabel *labelForAddImagesCount;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *heightOfAddImagesCountView;
@property (strong, nonatomic) IBOutlet UIView *viewForSubcategory;
@property (strong, nonatomic) IBOutlet UIButton *buttonToRemoveLocOutlet;
@property (strong, nonatomic) IBOutlet UILabel *labelForTaggedProducts;
- (IBAction)chooseCurrencyButtonAction:(id)sender;

@property (strong, nonatomic) IBOutlet UITableView *tableViewForHashTags;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *heightOfSocialMedia;

@property (strong, nonatomic) IBOutlet UIView *socialMediaView;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *contentViewBottomConstraint;


@end
