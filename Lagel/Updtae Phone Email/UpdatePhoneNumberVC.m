//
//  UpdatePhoneNumberVC.m

//
//  Created by Rahul Sharma on 7/22/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import "UpdatePhoneNumberVC.h"
#import "TinderGenericUtility.h"
#import "WebServiceConstants.h"
#import "WebServiceHandler.h"
#import "CountryListViewController.h"
#import "FontDetailsClass.h"
#import "Helper.h"
#import "OtpScreenViewController.h"
#import "PhoneNumberViewController.h"
#import "AppDelegate.h"

@interface UpdatePhoneNumberVC ( )<WebServiceHandlerDelegate>
{
    UIButton *nextButton;
    UIActivityIndicatorView *av;
    NSString *countryCode, *countryName;
}
@end

@implementation UpdatePhoneNumberVC
-(void)viewDidLoad {
    [super viewDidLoad];
    [self createNavRightButton];
    [self createNavLeftButton];
    self.navigationItem.title = Localized(mNavTitleForUpdateNumber);
    self.navigationController.navigationBar.tintColor = [UIColor blackColor];
    
    countryCode = @"+1";
    countryName = @"US";
    
    [self resetCountryCodeButton];
    
    [self.phoneNumberTextField becomeFirstResponder];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    self.navigationController.navigationBar.hidden = NO;
}

- (void)viewDidAppear:(BOOL)animated {
    self.navigationController.navigationBar.hidden = NO;
    
    // Update country and phone code
    
    AppDelegate *delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    if (delegate.userPhoneCode != nil && ![delegate.userPhoneCode  isEqual: @""]) {
        countryCode = delegate.userPhoneCode;
        delegate.userPhoneCode = @"";
        countryName = delegate.counryCode;
        delegate.counryCode = @"";
        
        [self resetCountryCodeButton];
    }
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
    [self.view endEditing:YES];
}

- (void)resetCountryCodeButton {
    NSString *codeString = [NSString stringWithFormat:@"%@ %@", countryName,countryCode];
    [_countryNameLabel setTitle:codeString forState:UIControlStateNormal];
}


/*-------------------------------------------*/
#pragma mark
#pragma mark - navigation bar buttons
/*-------------------------------------------*/

- (void)createNavLeftButton {
    
    if(self.verify)
    {
        UIButton *navLeft = [CommonMethods createNavButtonsWithselectedState:mCloseButton normalState:mCloseButton];
        UIBarButtonItem *navLeftButton = [[UIBarButtonItem alloc]initWithCustomView:navLeft];
        [navLeft setFrame:CGRectMake(0,0,25,40)];
        self.navigationItem.leftBarButtonItem = navLeftButton;
        [navLeft addTarget:self action:@selector(backButtonClicked) forControlEvents:UIControlEventTouchUpInside];
        [self.navigationItem setLeftBarButtonItems:@[[CommonMethods getNegativeSpacer],navLeftButton]];

    }
    else
    {
    UIButton *navCancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
     [navCancelButton rotateButton];
    [navCancelButton setImage:[UIImage imageNamed:mNavigationBackButtonImageName]
                     forState:UIControlStateNormal];
    [navCancelButton setImage:[UIImage imageNamed:mNavigationBackButtonImageName]
                     forState:UIControlStateSelected];
    [navCancelButton addTarget:self
                        action:@selector(backButtonClicked)
              forControlEvents:UIControlEventTouchUpInside];
    [navCancelButton setFrame:CGRectMake(0,0,25,40)];
    // Create a container bar button
    UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc] initWithCustomView:navCancelButton];
    // UIBarButtonItem *homeButton = [[UIBarButtonItem alloc] initWithCustomView:segmentView];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -10;// it was -6 in iOS 6
    [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];
    }
}

//method for creating navigation bar right button.
- (void)createNavRightButton {
   nextButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [nextButton setTitle:Localized(nextButtonTitle)
                   forState:UIControlStateNormal];
      [nextButton setTitleColor:mBaseColor forState:UIControlStateNormal];
    nextButton.titleLabel.font = [UIFont fontWithName:RobotoRegular size:17];
    [nextButton setFrame:CGRectMake(0,0,40,40)];
    [nextButton addTarget:self action:@selector(nextButtonClicked)
            forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc] initWithCustomView:nextButton];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    [self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];
}

- (void)backButtonClicked {
    if(self.verify)
    {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    else {
    [self.navigationController popViewControllerAnimated:YES];
    }
}

-(void)nextButtonClicked {
    if (countryName.length == 0) {
        [Helper showAlertWithTitle:nil Message:Localized(phoneNumberIsInvalid) viewController:self];
        return;
    }
    
    _phoneNumberWithCountryCode  = [countryCode stringByAppendingString:self.phoneNumberTextField.text ];
    
    if ([self validatePhone:_phoneNumberWithCountryCode]) {
        nextButton.hidden = YES;
        [self createActivityViewInNavbar];
        NSDictionary *requestDict = @{
                                      mauthToken :flStrForObj([Helper userToken]),
                                      mphoneNumber :flStrForObj(_phoneNumberWithCountryCode)
                                      };
        [WebServiceHandler RequestTypePhoneNumberCheckEditProfile:requestDict andDelegate:self];
    }
    else {
        
        [Helper showAlertWithTitle:nil Message:Localized(phoneNumberIsInvalid) viewController:self];
    }
}

#pragma mark
#pragma mark - phone number validation

- (BOOL)validatePhone:(NSString *)enteredphoneNumber {
    NSString *phoneNumber = enteredphoneNumber;
    NSString *phoneRegex = @"^((\\+)|(00))[0-9]{6,14}$";
    NSPredicate *test = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
    BOOL matches = [test evaluateWithObject:phoneNumber];
    return matches;
}

//method for creating activityview in  navigation bar right.
- (void)createActivityViewInNavbar {
    av = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [av setFrame:CGRectMake(0,0,50,30)];
    [self.view addSubview:av];
    av.tag  = 1;
    [av startAnimating];
    UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc] initWithCustomView:av];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    [self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];
}

/*-------------------------------------------------------*/
#pragma mark -
#pragma mark - WebServiceDelegate
/*-------------------------------------------------------*/

- (void) didFinishLoadingRequest:(RequestType)requestType withResponse:(id)response error:(NSError*)error {
    
    [av stopAnimating];
    [self createNavRightButton];
    
    if (error) {
        [Helper showAlertWithTitle:Localized(alertError) Message:[error localizedDescription] viewController:self];
        return;
    }
    //storing the response from server to dictonary.
    NSDictionary *responseDict = (NSDictionary*)response;
    //checking the request type and handling respective response code.
    if (requestType ==  RequestTypePhoneNumberCheckEditProfile ) {
        switch ([responseDict[@"code"] integerValue]) {
            case 200: {
                
                OtpScreenViewController *otpVC = [self.storyboard instantiateViewControllerWithIdentifier:mOtpStoryboardID];
                otpVC.numb = self.phoneNumberWithCountryCode;
                otpVC.updateNumber = YES;
                [self.navigationController pushViewController:otpVC animated:YES];
                
            }
                break;
                //failure response.
            case 23465: {
                [self errAlert:responseDict[@"message"]];
            }
                break;
            case 23456: {
                [self errAlert:responseDict[@"message"]];
            }
                break;
            case 1973: {
                [self errAlert:responseDict[@"message"]];
            }
                break;
            case 1974: {
                [self errAlert:responseDict[@"message"]];
            }
                break;
            default:
                break;
        }
    }
}

- (void)errAlert:(NSString *)message {
    //creating alert for error message.
    [Helper showAlertWithTitle:Localized(alertMessage) Message:message viewController:self];
}

- (IBAction)countrySelectionButton:(id)sender {
    PhoneNumberViewController *cv = [self.storyboard instantiateViewControllerWithIdentifier:@"phoneNumberVC"];
    [self.navigationController pushViewController:cv animated:YES];
}

@end
