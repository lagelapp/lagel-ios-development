//
//  ChatSafeView.m

//  Created by Imma Web Pvt Ltd on 07/07/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "ChatSafeView.h"

@implementation ChatSafeView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(id)init
{
    self = [[[NSBundle mainBundle] loadNibNamed:@"ChatSafeView" owner:self options:nil] firstObject];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                          action:@selector(hidePOPup)];
   
    tap.delegate = self;
    [self addGestureRecognizer:tap];
    return self;
}

- (IBAction)okButtonAction:(id)sender {
    [self hidePOPup];
    [_delegate hideAlert];
}

-(void)hidePOPup{
    
    self.contentView.alpha = 1;
    
    [UIView animateWithDuration:0.3
                     animations:^{
                         self.contentView.alpha = 1;
                     }
                     completion:^(BOOL finished) {
                         [self removeFromSuperview];
                     }];
    
}

-(void)onWindow:(UIWindow *)onWindow
{
   
    self.contentView = onWindow;
    self.frame = onWindow.frame;
    [onWindow addSubview:self];
    [UIView animateWithDuration:0.8
                     animations:^{
//                         self.contentView.alpha = 0.4;
                     }
                     completion:^(BOOL finished)
     {
     }];
}


@end
