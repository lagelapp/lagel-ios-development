//
//  LikeCommentTableViewCell.h
//  InstaVideoPlayerExample
//
//  Created by Rahul Sharma on 13/09/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductDetails.h"

@interface LikeCommentTableViewCell : UITableViewCell <UICollectionViewDelegate,UICollectionViewDataSource>
@property (weak, nonatomic) IBOutlet UIButton *shareButtonOutlet;
@property (weak, nonatomic) IBOutlet UIButton *likeButtonOutlet;
@property (weak, nonatomic) IBOutlet UIButton *commentButtonOutlet;
@property (weak, nonatomic) IBOutlet UIButton *moreButtonOutlet;
@property (strong, nonatomic) IBOutlet UICollectionView *likersCollectionViewOutlet;
@property (strong, nonatomic) IBOutlet UILabel *labelForPrice;
@property (strong, nonatomic) IBOutlet UILabel *labelForCurrency;
@property (strong, nonatomic) IBOutlet UILabel *labelForTitle;
@property (strong, nonatomic) NSMutableArray  *arrayOfProfilePics;
@property (strong,nonatomic) UINavigationController *navController;
@property (strong,nonatomic) NSString *collectionViewCellID;
@property (strong, nonatomic) IBOutlet UIButton *viewCountButton;
@property (strong,nonatomic) NSIndexPath *index;
@property (strong, nonatomic)ProductDetails *product ;
/**
 Methods to set properties of outlets.
 */
-(void)setPropertiesOfOutletscheckFlag:(BOOL)flag;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicatorOutlet;


/**
 Set Values.

 @param price    price.
 @param currency currency.
 @param title    title.
 */
-(void)setPrice :(NSString *)price setCurrency :(NSString *)currency andSetTitle:(NSString *)title;

@end
