//
//  LibraryPhotosViewController.h

//
//  Created by Rahul Sharma on 18/04/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol LibraryScreenDelegate <NSObject>

-(void)getMaintainedDataOfImagesFromLibrary :(NSMutableArray *)arrayOfmaintainedLibImages;

@end


@interface LibraryPhotosViewController : UIViewController
@property (strong, nonatomic) IBOutlet UILabel *lableForAlbumName;
- (IBAction)tapToSeeAllAlbumButton:(id)sender;

@property (strong, nonatomic) IBOutlet UIView *veiwForNavigationTitle;

@property BOOL  addAgain, cameraFromTab ;
@property (strong, nonatomic) IBOutlet UICollectionView *collectionViewForPickedPhotos;

@property (strong, nonatomic) IBOutlet UICollectionView *collectionViewForLibPhotos;

@property (strong, nonatomic) IBOutlet UIView *albumView;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *bottomConstraintforAlbumView;

@property (strong, nonatomic) IBOutlet UITableView *albumTableView;

@property (strong, nonatomic) NSMutableArray *arrayOfCameraImages;

@property (strong,nonatomic)id<LibraryScreenDelegate> libDelegate;

- (IBAction)doneButtonAction:(id)sender;

@property (strong, nonatomic) IBOutlet UIButton *navigationLeftButtonOutlet;

@property (strong, nonatomic) IBOutlet UIView *enableLibraryAccessPermissionView;
@end
