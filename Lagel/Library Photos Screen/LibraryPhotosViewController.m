//
//  LibraryPhotosViewController.m

//
//  Created by Rahul Sharma on 18/04/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "LibraryPhotosViewController.h"
#import "MultipleImagesCell.h"
#import "LibPhotosCollectionViewCell.h"
#import "Photos/Photos.h"
#import "AlbumsTableViewCell.h"
#import "CameraViewController.h"
#import "PGShareViewController.h"


#define mCollectionViewCellID     @"collectionViewCellForPickedLib"
#define mCameraIconName           @"camera_icon"
@interface LibraryPhotosViewController () <UICollectionViewDataSource,UICollectionViewDelegate>
{
    NSMutableArray *arrayOfLibImages;
    
}
@property (strong) NSArray *collectionsFetchResults;
@property (strong) NSArray *collectionsFetchResultsAssets;
@property (strong) NSArray *collectionsFetchResultsTitles;
@property (strong) PHCachingImageManager *imageManager;
@property (strong) PHFetchResult *assetsFetchResults;
@end

@implementation LibraryPhotosViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    arrayOfLibImages = [NSMutableArray new];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [self setNavigationBar];
    [self checkLibraryPermissionsStatus];
    
    if(_arrayOfCameraImages!=nil)
    {
        arrayOfLibImages = self.arrayOfCameraImages ;
        [self.collectionViewForPickedPhotos reloadData];
        [self.collectionViewForLibPhotos reloadData];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*-------------------------------------*/
#pragma mark
#pragma mark - Navigation Bar
/*-------------------------------------*/


/**
 Set navigation bar with custom view in nav title and buttons.
 */
-(void)setNavigationBar
{
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.shadowImage = nil;
    self.navigationItem.titleView = self.veiwForNavigationTitle;
    
    [self.navigationLeftButtonOutlet addTarget:self action:@selector(cancelBarButtonAction) forControlEvents:UIControlEventTouchUpInside];
     UIBarButtonItem *navLeft = [[UIBarButtonItem alloc]initWithCustomView:self.navigationLeftButtonOutlet];
    self.navigationItem.leftBarButtonItem = navLeft;
    
    UIButton *navRight = [CommonMethods createNavButtonsWithselectedState:mCameraIconName normalState:mCameraIconName];
    UIBarButtonItem *navRightButton = [[UIBarButtonItem alloc]initWithCustomView:navRight];
    [navRight setFrame:CGRectMake(0,0,25,40)];
    self.navigationItem.rightBarButtonItem = navRightButton;
    [navRight addTarget:self action:@selector(backToCamera:) forControlEvents:UIControlEventTouchUpInside];
//    [self.navigationItem setRightBarButtonItems:@[[CommonMethods getNegativeSpacer],navRightButton]];
    
}

/**
 Back to camera screen with left flip.
 
 @param sender nav right button.
 */
-(void)backToCamera:(id)sender
{
    
    [self.libDelegate getMaintainedDataOfImagesFromLibrary:arrayOfLibImages];
    [UIView transitionWithView:self.navigationController.view
                      duration:0.50
                       options:UIViewAnimationOptionTransitionFlipFromLeft
                    animations:^{
                        [self.navigationController popViewControllerAnimated:YES];
                    }
                    completion:nil];
}

-(void)cancelBarButtonAction
{
    if(self.cameraFromTab){
        [self.tabBarController setSelectedIndex:0];
        [self.navigationController popToRootViewControllerAnimated:YES];
        
    }
    else
    {
        [self dismissViewControllerAnimated:YES completion:Nil];
    }

}

/*-------------------------------------*/
#pragma mark
#pragma mark -custom photo picker
/*-------------------------------------*/

- (void)loadPhotos {
    
    PHAuthorizationStatus status = [PHPhotoLibrary authorizationStatus];
    if (status ==  PHAuthorizationStatusAuthorized) {
        [self fetchPhotosFromLibrary];
    }
    if (status ==PHAuthorizationStatusNotDetermined) {
        // Access has not been determined.
        [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
            
            if (status == PHAuthorizationStatusAuthorized) {
                // Access has been granted.
                [self fetchPhotosFromLibrary];
            }
        }];
    }
    if (status == PHAuthorizationStatusRestricted) {
        // Access has not been determined.
        [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
            
            if (status == PHAuthorizationStatusAuthorized) {
                // Access has been granted.
                dispatch_async(dispatch_get_main_queue(), ^{
                    // This block will be executed asynchronously on the main thread.
                    [self fetchPhotosFromLibrary];
                });
            }
            else {
                // Access has been denied.
                dispatch_async(dispatch_get_main_queue(), ^{
                    // This block will be executed asynchronously on the main thread.
                    //  [self createCustomViewWhenGalleryPermissionDenied];
                    //self.navTitleButton.enabled = NO;
                });
            }
        }];
    }
}



-(void)fetchPhotosFromLibrary {
    //All album: Sorted by descending creation date.
    NSMutableArray *allFetchResultArray = [[NSMutableArray alloc] init];
    NSMutableArray *allFetchResultLabel = [[NSMutableArray alloc] init];
    PHFetchOptions *options = [[PHFetchOptions alloc] init];
    options.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"creationDate" ascending:NO]];
    PHFetchResult *assetsFetchResult;
    assetsFetchResult = [PHAsset  fetchAssetsWithOptions:options];
    [allFetchResultArray addObject:assetsFetchResult];
    [allFetchResultLabel addObject:@"All photos"];
    
    self.assetsFetchResults = assetsFetchResult;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        // This block will be executed asynchronously on the main thread.
        [self.collectionViewForLibPhotos reloadData];
    });
}



/*-------------------------------------*/
#pragma mark
#pragma mark -Collectionview Datasource
/*-------------------------------------*/




-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (collectionView == self.collectionViewForLibPhotos) {
        return self.assetsFetchResults.count;
    }
    else {
        
        if(arrayOfLibImages.count>4)
            return 5;
        return arrayOfLibImages.count + 1;
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath;
{
    
    if (collectionView == self.collectionViewForLibPhotos) {
        LibPhotosCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"LibPhotosCollectionCell" forIndexPath:indexPath];
        PHAsset *asset = self.assetsFetchResults[indexPath.item];

        NSInteger retinaScale = [UIScreen mainScreen].scale;
        CGSize retinaSquare = CGSizeMake(self.view.frame.size.width/3*retinaScale, self.view.frame.size.width/3*retinaScale);
        
        PHImageRequestOptions *options = [[PHImageRequestOptions alloc] init];
        options.networkAccessAllowed = YES ;
        options.resizeMode = PHImageRequestOptionsResizeModeExact;
        
        CGFloat cropSideLength = MIN(asset.pixelWidth, asset.pixelHeight);
        CGRect square = CGRectMake(0, 0, cropSideLength, cropSideLength);
        CGRect cropRect = CGRectApplyAffineTransform(square,
                                                     CGAffineTransformMakeScale(1.0 / asset.pixelWidth,
                                                                                1.0 / asset.pixelHeight));
        
        options.normalizedCropRect = cropRect;
        
        
        
        
        
        //NSLog(@"Image manager: Requesting FILL image for iPhone");
        [self.imageManager requestImageForAsset:asset
                                     targetSize:retinaSquare
                                    contentMode:PHImageContentModeAspectFill
                                        options:options
                                  resultHandler:^(UIImage *result, NSDictionary *info) {
                                      
                                      // Only update the thumbnail if the cell tag hasn't changed. Otherwise, the cell has been re-used.
                                      [cell.imageViewForLib setImage:result];
                                    
                                  }];
        
        
        
        NSString *selectedImageIdentifiers =[[arrayOfLibImages valueForKey:@"uniqueIdenifierForAsset"] componentsJoinedByString:@",,"];
        if(![selectedImageIdentifiers containsString:asset.localIdentifier])
        {
            cell.imageViewForLib.layer.borderWidth = 0;
            cell.imageViewForLib.layer.borderColor =[UIColor clearColor].CGColor;
            cell.check_mark_image.hidden = YES;
        }
        else {
            cell.imageViewForLib.layer.borderWidth = 2;
            cell.imageViewForLib.layer.borderColor = mBaseColor2.CGColor;
            cell.check_mark_image.hidden = NO;
        }
        return cell;
    }
    else {
        MultipleImagesCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"PickedPhotoCollectionCell" forIndexPath:indexPath];
        
        if( indexPath.row == arrayOfLibImages.count)
        {
            cell.imageViewContainer.image = [UIImage imageNamed:@"cover_image1"];
            if(arrayOfLibImages.count>0)
            {
                cell.labelForCover.hidden = YES;
            }
            else
            {
                cell.labelForCover.hidden = NO;
            }
            [cell layoutIfNeeded];
            cell.imageViewContainer.layer.cornerRadius = 5;
            cell.imageViewContainer.clipsToBounds = YES;
            cell.removeButton.hidden = YES ;
            return cell;
        }
        if([arrayOfLibImages[indexPath.row][@"imageType"] isEqualToString:@"cloudinaryUrl"])
        {
            [cell.imageViewContainer sd_setImageWithURL:arrayOfLibImages[indexPath.row][@"imageValue"]];
        }
        else{
        
        NSString *filePath = flStrForObj(arrayOfLibImages[indexPath.row][@"imageValue"]);
        NSData *imgData = [NSData dataWithContentsOfFile:filePath];
          cell.imageViewContainer.image  = [[UIImage alloc] initWithData:imgData];
            cell.labelForCover.hidden = YES;
            cell.removeButton.hidden = NO ;
            cell.removeButton.tag = indexPath.row;
            [cell.removeButton addTarget:self action:@selector(removeImage:) forControlEvents:UIControlEventTouchUpInside];
          }
        return cell;
    }
}

/*-------------------------------------*/
#pragma mark
#pragma mark -Collectionview Delegate
/*-------------------------------------*/
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    
    if(collectionView == self.collectionViewForLibPhotos)
        return  CGSizeMake(self.view.frame.size.width/3 - 2 ,self.view.frame.size.width/3 - 2);
    else{
        CGSize cellSize;
        cellSize.width = 65;
        cellSize.height = 70;
        return cellSize;
    }
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    NSLog(@"arrayofImage = %@",arrayOfLibImages);
    if(collectionView == self.collectionViewForLibPhotos)
    {
        LibPhotosCollectionViewCell *cell = (LibPhotosCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
            
            if(!cell.check_mark_image.hidden) {
                cell.imageViewForLib.layer.borderWidth = 0;
                cell.imageViewForLib.layer.borderColor =[UIColor clearColor].CGColor;
                cell.check_mark_image.hidden = YES;
                for(NSDictionary *dict in arrayOfLibImages) {
                    if(dict[@"indexPath"] == indexPath) {
                        [arrayOfLibImages removeObject:dict];
                        break;
                    }
                }
                
                [self.collectionViewForPickedPhotos reloadData];
                
            }else{
                
                if (arrayOfLibImages.count  == 5) {
                    [Helper showAlertWithTitle:nil Message:NSLocalizedString(addUptoMaximumFiveImages, addUptoMaximumFiveImages) viewController:self];
                    return;
                }
                cell.imageViewForLib.layer.borderWidth = 2;
                cell.imageViewForLib.layer.borderColor = mBaseColor2.CGColor;
                cell.check_mark_image.hidden = NO;
                NSString *typeOfImage = @"storingImagePath";
                NSMutableDictionary  *dictForImagePath = [[NSMutableDictionary alloc] init];
                [dictForImagePath setObject:indexPath forKey:@"indexPath"];
                [dictForImagePath setValue:typeOfImage forKey:@"imageType"];
                
                PHAsset *selectedImage = (PHAsset *)self.assetsFetchResults[indexPath.row];
                [dictForImagePath setValue:flStrForObj(selectedImage.localIdentifier) forKey:@"uniqueIdenifierForAsset"];
                PHCachingImageManager *newimageManager = [[PHCachingImageManager alloc] init];
                
                
                PHImageRequestOptions *option = [PHImageRequestOptions new];
                option.synchronous = YES;
                option.networkAccessAllowed = YES ;
            
                [newimageManager requestImageForAsset:selectedImage
                                           targetSize:CGSizeMake(selectedImage.pixelWidth,selectedImage.pixelHeight)
                                          contentMode:PHImageContentModeAspectFill
                                              options:option
                                        resultHandler:^(UIImage *result, NSDictionary *info) {
                                             NSString *imageName = [NSString stringWithFormat:@"%@%@.jpg",@"Image",[self getCurrentTime]];
//                                            NSData *imgData1 = UIImageJPEGRepresentation(result , 0.2f);
                                            NSData *imgData1 = [self compressImage:result];
                                            NSLog(@"Size of Image(bytes):%ld",(unsigned long)[imgData1 length]);
                                            NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                                            NSString* documentsDirectory = [paths objectAtIndex:0];
                                            NSString* imagePath = [documentsDirectory stringByAppendingPathComponent:imageName];
                                            [imgData1 writeToFile:imagePath atomically:NO];
                                            
                                            [dictForImagePath setValue:imagePath forKey:@"imageValue"];
                                            
                                            if(![self->arrayOfLibImages containsObject:dictForImagePath]) {
                                                [self->arrayOfLibImages addObject:dictForImagePath];
                                                [self.collectionViewForPickedPhotos reloadData];
                                            }
                                           
                                            
//                                             [collectionView setUserInteractionEnabled:YES];
                                        }];
            }
    }
    else {
        
        if (arrayOfLibImages.count >0) {
            if (indexPath.row != arrayOfLibImages.count) {
            }
            
        }
    }
}

-(NSData *)compressImage:(UIImage *)image{
    
    NSData *imgData = UIImageJPEGRepresentation(image, 1); //1 it represents the quality of the image.
    NSLog(@"Size of Image(bytes):%ld",(unsigned long)[imgData length]);
    
    float actualHeight = image.size.height;
    float actualWidth = image.size.width;
    float maxHeight = 600.0;
    float maxWidth = 800.0;
    float imgRatio = actualWidth/actualHeight;
    float maxRatio = maxWidth/maxHeight;
    float compressionQuality = 0.2;//50 percent compression
    
    if (actualHeight > maxHeight || actualWidth > maxWidth){
        if(imgRatio < maxRatio){
            //adjust width according to maxHeight
            imgRatio = maxHeight / actualHeight;
            actualWidth = imgRatio * actualWidth;
            actualHeight = maxHeight;
        }
        else if(imgRatio > maxRatio){
            //adjust height according to maxWidth
            imgRatio = maxWidth / actualWidth;
            actualHeight = imgRatio * actualHeight;
            actualWidth = maxWidth;
        }
        else{
            actualHeight = maxHeight;
            actualWidth = maxWidth;
        }
    }
    
    CGRect rect = CGRectMake(0.0, 0.0, actualWidth, actualHeight);
    UIGraphicsBeginImageContext(rect.size);
    [image drawInRect:rect];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    NSData *imageData = UIImageJPEGRepresentation(img, compressionQuality);
    UIGraphicsEndImageContext();
    
    NSLog(@"Size of Image(bytes):%ld",(unsigned long)[imageData length]);
    
    return imageData;
}

-(NSString*)getCurrentTime {
    NSDate *currentDateTime = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    // Set the dateFormatter format
    //[dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    // or this format to show day of the week Sat,11-12-2011 23:27:09
    [dateFormatter setDateFormat:@"EEEMMddyyyyHHmmss"];
    // Get the date time in NSString
    NSString *dateInStringFormated = [dateFormatter stringFromDate:currentDateTime];
    //  //NSLog(@"%@", dateInStringFormated);
    return dateInStringFormated;
    // Release the dateFormatter
    //[dateFormatter release];
}

-(void)updateFetchResults
{
    //What I do here is fetch both the albums list and the assets of each album.
    //This way I have acces to the number of items in each album, I can load the 3
    //thumbnails directly and I can pass the fetched result to the gridViewController.
    
    self.collectionsFetchResultsAssets=nil;
    self.collectionsFetchResultsTitles=nil;
    
    //Fetch PHAssetCollections:
    PHFetchResult *topLevelUserCollections = [self.collectionsFetchResults objectAtIndex:0];
    PHFetchResult *smartAlbums = [self.collectionsFetchResults objectAtIndex:1];
    
    //All album: Sorted by descending creation date.
    NSMutableArray *allFetchResultArray = [[NSMutableArray alloc] init];
    NSMutableArray *allFetchResultLabel = [[NSMutableArray alloc] init];
    {
        PHFetchOptions *options = [[PHFetchOptions alloc] init];
        options.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"creationDate" ascending:NO]];
        PHFetchResult *assetsFetchResult;
        assetsFetchResult = [PHAsset fetchAssetsWithMediaType:PHAssetMediaTypeImage options:options];
        [allFetchResultArray addObject:assetsFetchResult];
        [allFetchResultLabel addObject:@"All photos"];
    }
    
    //User albums:
    NSMutableArray *userFetchResultArray = [[NSMutableArray alloc] init];
    NSMutableArray *userFetchResultLabel = [[NSMutableArray alloc] init];
    for(PHCollection *collection in topLevelUserCollections)
    {
        if ([collection isKindOfClass:[PHAssetCollection class]])
        {
            PHFetchOptions *options = [[PHFetchOptions alloc] init];
            PHAssetCollection *assetCollection = (PHAssetCollection *)collection;
            
            //Albums collections are allways PHAssetCollectionType=1 & PHAssetCollectionSubtype=2
            options.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"creationDate" ascending:NO]];
            options.predicate = [NSPredicate predicateWithFormat:@"mediaType = %d",PHAssetMediaTypeImage];
            PHFetchResult *assetsFetchResult = [PHAsset fetchAssetsInAssetCollection:assetCollection options:options];
            [userFetchResultArray addObject:assetsFetchResult];
            [userFetchResultLabel addObject:collection.localizedTitle];
        }
    }
    
    
    
    //Smart albums: Sorted by descending creation date.
    NSMutableArray *smartFetchResultArray = [[NSMutableArray alloc] init];
    NSMutableArray *smartFetchResultLabel = [[NSMutableArray alloc] init];
    for(PHCollection *collection in smartAlbums)
    {
        if ([collection isKindOfClass:[PHAssetCollection class]])
        {
            PHAssetCollection *assetCollection = (PHAssetCollection *)collection;
            
            //Smart collections are PHAssetCollectionType=2;
            
            PHFetchOptions *options = [[PHFetchOptions alloc] init];
            options.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"creationDate" ascending:NO]];
            options.predicate = [NSPredicate predicateWithFormat:@"mediaType = %d",PHAssetMediaTypeImage];
            PHFetchResult *assetsFetchResult = [PHAsset fetchAssetsInAssetCollection:assetCollection options:options];
            if(assetsFetchResult.count>0)
            {
                [smartFetchResultArray addObject:assetsFetchResult];
                [smartFetchResultLabel addObject:collection.localizedTitle];
            }
        }
    }
    
    self.collectionsFetchResultsAssets= @[allFetchResultArray,userFetchResultArray,smartFetchResultArray];
    self.collectionsFetchResultsTitles= @[allFetchResultLabel,userFetchResultLabel,smartFetchResultLabel];
    
    [self.albumTableView reloadData];
}

- (NSString *)tableCellSubtitle:(PHFetchResult*)assetsFetchResult
{
    
    return [NSString stringWithFormat:@"%ld", (long)[assetsFetchResult count]];
}



/*-------------------------------------*/
#pragma mark
#pragma mark -TableView Datasource
/*-------------------------------------*/

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.collectionsFetchResultsAssets.count;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    PHFetchResult *fetchResult = self.collectionsFetchResultsAssets[section];
    return fetchResult.count;
}

/*-------------------------------------*/
#pragma mark
#pragma mark -TableView Delegates
/*-------------------------------------*/

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AlbumsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"albumsTableCell"];
    
    
    // Increment the cell's tag
    NSInteger currentTag = cell.tag + 1;
    cell.tag = currentTag;
    
    // Set the label
    
    cell.labelForAlbumName.text = (self.collectionsFetchResultsTitles[indexPath.section])[indexPath.row];
    
    
    // Retrieve the pre-fetched assets for this album:
    PHFetchResult *assetsFetchResult = (self.collectionsFetchResultsAssets[indexPath.section])[indexPath.row];
    
    // Display the number of assets
    cell.labelForPhotosCount.text = [self tableCellSubtitle:assetsFetchResult];
    
    
    if ([assetsFetchResult count] > 0) {
        CGFloat scale = [UIScreen mainScreen].scale;
        //Compute the thumbnail pixel size:
        CGSize tableCellThumbnailSize1 = CGSizeMake(40*scale,40*scale);
        PHAsset *asset = assetsFetchResult[0];
        //        [cell setVideoLayout:(asset.mediaType==PHAssetMediaTypeVideo)];
        [self.imageManager requestImageForAsset:asset
                                     targetSize:tableCellThumbnailSize1
                                    contentMode:PHImageContentModeAspectFill
                                        options:nil
                                  resultHandler:^(UIImage *result, NSDictionary *info) {
                                      if (cell.tag == currentTag) {
                                          cell.coverImageViewForAlbums.image = result;
                                      }
                                  }];
        
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.lableForAlbumName.text = (self.collectionsFetchResultsTitles[indexPath.section])[indexPath.row] ;
    self.assetsFetchResults = [[_collectionsFetchResultsAssets objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    [self.collectionViewForLibPhotos reloadData];
    self.albumView.hidden = YES;
}

- (IBAction)doneButtonAction:(id)sender {
    
    if(_addAgain)
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:mLibraryNotification object:arrayOfLibImages];
        PGShareViewController *View = [self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count-3];
        [self.navigationController popToViewController:View animated:YES];
    }
    else
    {
        if (arrayOfLibImages.count >0) {
            PGShareViewController *postScreen = [self.storyboard instantiateViewControllerWithIdentifier:mPostScreenStoryboardID];
            postScreen.arrayOfImagePaths = arrayOfLibImages;
            postScreen.cameraFromTabBar = self.cameraFromTab ;
            [self.navigationController pushViewController:postScreen animated:YES];
        }
        else {
           [Helper showAlertWithTitle:nil Message:NSLocalizedString(needAtleastOnePhoto, needAtleastOnePhoto) viewController:self];
        }
    }
}

#pragma mark-
#pragma mark - Tap to see All Albums

- (IBAction)tapToSeeAllAlbumButton:(id)sender {
    self.albumView.hidden = NO;
    // Fetch PHAssetCollections:
    PHFetchResult *topLevelUserCollections = [PHCollectionList fetchTopLevelUserCollectionsWithOptions:nil];
    PHFetchResult *smartAlbums = [PHAssetCollection fetchAssetCollectionsWithType:PHAssetCollectionTypeSmartAlbum subtype:PHAssetCollectionSubtypeAlbumRegular options:nil];
    self.collectionsFetchResults = @[topLevelUserCollections, smartAlbums];
    [self updateFetchResults];
}

/*-------------------------------------*/
#pragma mark
#pragma mark - RemoveImage Selector Method.
/*------------------------------------*/

-(void)removeImage:(id) sender
{
    UIButton *btn=(UIButton *)sender;
    NSInteger tag=btn.tag%1000;
    [arrayOfLibImages removeObjectAtIndex:tag];
    [self.collectionViewForPickedPhotos reloadData];
    [self.collectionViewForLibPhotos reloadData];
}

/**
 Check permission to check the access for Photos library.
 handle the permission view and load photos on the basis of status.
 */
-(void)checkLibraryPermissionsStatus {
    
    PHAuthorizationStatus status = [PHPhotoLibrary authorizationStatus];
    if (status == PHAuthorizationStatusAuthorized) {
        self.enableLibraryAccessPermissionView.hidden = YES ;
        self.imageManager  = [[PHCachingImageManager alloc]init];
        [self loadPhotos];
        // Access has been granted.
    }
    
    else if (status == PHAuthorizationStatusDenied) {
        self.enableLibraryAccessPermissionView.hidden = NO ;
    }
    
    else if (status == PHAuthorizationStatusNotDetermined) {
        
        // Access has not been determined.
        [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
            
            if (status == PHAuthorizationStatusAuthorized) {
                // Access has been granted.
                self.enableLibraryAccessPermissionView.hidden = YES ;
                self.imageManager  = [[PHCachingImageManager alloc]init];
                [self loadPhotos];
            }
            
            else {
                // Access has been denied.
                self.enableLibraryAccessPermissionView.hidden = NO ;
            }
        }];
    }
    
    else if (status == PHAuthorizationStatusRestricted) {
        // Restricted access - normally won't happen.
        self.enableLibraryAccessPermissionView.hidden = NO ;
    }
    
}



/**
 Enable Photos lib access Button Action.
 Redirect user to settings.
 
 @param sender enableLibrary access button.
 */
- (IBAction)enableLibraryAccessButtonAction:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
}

@end
