//
//  CellForLocationCheck.m

//
//  Created by Rahul Sharma on 13/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "CellForLocationCheck.h"
#import <GoogleMaps/GoogleMaps.h>
@implementation CellForLocationCheck

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)displayLocationName:(ProductDetails *)product
{
    NSString * city = [product.city capitalizedString];
    NSString *countryName = [[NSLocale systemLocale] displayNameForKey:NSLocaleCountryCode value:product.countrySname];
    self.labelForLoc.text = [NSString stringWithFormat:@"%@, %@",city , countryName];
}

//- (CLLocationCoordinate2D) locationWithBearing:(float)bearing distance:(float)distanceMeters fromLocation:(CLLocationCoordinate2D)origin {
//    CLLocationCoordinate2D target;
//    const double distRadians = distanceMeters / (6372797.6); // earth radius in meters
//
//    float lat1 = origin.latitude * M_PI / 180;
//    float lon1 = origin.longitude * M_PI / 180;
//
//    float lat2 = asin( sin(lat1) * cos(distRadians) + cos(lat1) * sin(distRadians) * cos(bearing));
//    float lon2 = lon1 + atan2( sin(bearing) * sin(distRadians) * cos(lat1),
//                              cos(distRadians) - sin(lat1) * sin(lat2) );
//
//    target.latitude = lat2 * 180 / M_PI;
//    target.longitude = lon2 * 180 / M_PI; // no need to normalize a heading in degrees to be within -179.999999° to 180.00000°
//
//    return target;
//}

-(void)setUpMapView:(double )latitude andLongitude:(double )longi andUserID:(NSString *)userID {
    
    
//    int distRadians = 5000;
//
//
//    NSString *lat1 = [NSString stringWithFormat:@"%f", latitude];
//    NSString *lon1 = [NSString stringWithFormat:@"%f", longi];
//    NSArray *lat1arr = [lat1 componentsSeparatedByString:@"."];
//    NSArray *lon1arr = [lon1 componentsSeparatedByString:@"."];
//
//    NSNumber *latnum = lat1arr.lastObject;
//    NSNumber *lonnum = lon1arr.lastObject;
//
//    int finallat = [latnum intValue] + distRadians;
//    int finallon = [lonnum intValue] + distRadians;
//
//    double latnew = [[NSString stringWithFormat:@"%@.%d",[lat1arr firstObject],finallat] doubleValue];
//    double lonnew = [[NSString stringWithFormat:@"%@.%d",[lon1arr firstObject],finallon] doubleValue];
//
//    NSString *uID = [[NSUserDefaults standardUserDefaults] valueForKey:@"userId"];
//    if ([userID isEqualToString: uID]) {
//        latnew = latitude;
//        lonnew = longi;
//    }
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:latitude                                                                 longitude:longi zoom:11];
    [self.viewForMap clear];
    [self.viewForMap animateToCameraPosition:camera];
    self.viewForMap.myLocationEnabled = NO;
    self.viewForMap.mapType = kGMSTypeNormal;
    self.viewForMap.settings.myLocationButton = NO;
    self.viewForMap.settings.zoomGestures = YES;
    self.viewForMap.settings.tiltGestures = NO;
    self.viewForMap.settings.rotateGestures = NO;
    self.viewForMap.userInteractionEnabled=NO;
    CLLocationCoordinate2D position = CLLocationCoordinate2DMake(latitude , longi);
    
    GMSCircle *circ = [GMSCircle circleWithPosition:position
                                             radius:4023.36];
    circ.fillColor = [mBaseColor colorWithAlphaComponent:0.4];
    circ.strokeColor = [UIColor clearColor];
    circ.strokeWidth = 1;
    circ.map = self.viewForMap;
    
//    GMSCircle *circ1 = [GMSCircle circleWithPosition:position
//                                             radius:2682.24];
//    circ1.fillColor = [[UIColor colorWithRed:58/255.0f green:181/255.0f blue:236/255.0f alpha:1.0f] colorWithAlphaComponent:0.4];
//    circ1.strokeColor = [UIColor clearColor];
//    circ1.strokeWidth = 1;
//    circ1.map = self.viewForMap;
//
//    GMSCircle *circ2 = [GMSCircle circleWithPosition:position
//                                              radius:1341.12];
//    circ2.fillColor = [[UIColor colorWithRed:58/255.0f green:181/255.0f blue:236/255.0f alpha:1.0f] colorWithAlphaComponent:0.7];
//    circ2.strokeColor = [UIColor clearColor];
//    circ2.strokeWidth = 1;
//    circ2.map = self.viewForMap;
//
//    GMSCircle *circ3 = [GMSCircle circleWithPosition:position
//                                              radius:670.56];
//    circ3.fillColor = [[UIColor colorWithRed:58/255.0f green:181/255.0f blue:236/255.0f alpha:1.0f] colorWithAlphaComponent:1.0];
//    circ3.strokeColor = [UIColor clearColor];
//    circ3.strokeWidth = 1;
//    circ3.map = self.viewForMap;
    
//    GMSMarker *marker = [[GMSMarker alloc] init];
//    marker.icon = [UIImage imageNamed:@"itemMapIcon"];
//    marker.position = CLLocationCoordinate2DMake(latnew, lonnew);
//    marker.map = self.viewForMap;
  
}

@end
