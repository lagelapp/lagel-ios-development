//
//  CellForProdSummry.h

//
//  Created by Rahul Sharma on 13/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductDetails.h"
@interface CellForProdSummry : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *productName;
@property (weak, nonatomic) IBOutlet UILabel *productType;
@property (strong, nonatomic) IBOutlet UILabel *labelForTimeStamp;

-(void)updateFieldsForProductWithDataArray :(ProductDetails *)product;

@end
