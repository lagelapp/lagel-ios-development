//
//  CellForProdSummry.m

//
//  Created by Rahul Sharma on 13/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "CellForProdSummry.h"


@implementation CellForProdSummry

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


/**
 Update Fields in cell with data.

 @param product product details.
 */
-(void)updateFieldsForProductWithDataArray :(ProductDetails *)product
{
    NSString *strTitle = @"";
    int strcount = 0;
    NSString *strNumValue = @"";
    
    if (![product.productName isEqualToString:@""]) {
        strTitle = product.productName;
        NSCharacterSet* notDigits = [[NSCharacterSet letterCharacterSet] invertedSet];
        for (int i = 0; i< [product.productName length]; i++) {
            //            NSString *ichar  = [NSString stringWithFormat:@"%c", [self.textFieldForTitle.text characterAtIndex:i]];
            char ch = [product.productName characterAtIndex:i];
            NSString *ichar  = [NSString stringWithFormat:@"%c", ch];
            if ([ichar rangeOfCharacterFromSet:notDigits].location == NSNotFound) {
                NSLog(@"Called");
                if (strcount > 7 && strcount < 13) {
                    strTitle = [strTitle stringByReplacingOccurrencesOfString:strNumValue withString:@" "];
                }
                strcount = 0;
                strNumValue = @"";
            } else {
                NSLog(@"Called else");
                NSCharacterSet* noDigits = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
                if ([ichar rangeOfCharacterFromSet:noDigits].location == NSNotFound) {
                    NSLog(@"Called else if");
                    strcount += 1;
                } else {
                    NSLog(@"Called else else");
                    
                }
                strNumValue = [strNumValue stringByAppendingString:ichar];
            }
        }
        
        if (strcount > 7 && strcount < 13) {
            strTitle = [strTitle stringByReplacingOccurrencesOfString:strNumValue withString:@" "];
        }
        strcount = 0;
        strNumValue = @"";
    }
    
    self.productName.text = strTitle;
    self.productName.text = [self.productName.text capitalizedString];
    self.productType.text=  product.category ;
    self.productType.text = [self.productType.text capitalizedString];
    self.productType.text = [self.productType.text stringByReplacingOccurrencesOfString:@"And" withString:@"and"];
    NSString *timeStamp = [Helper convertEpochToNormalTimeInshort:product.postedOn];
    self.labelForTimeStamp.text = timeStamp;
}

@end
