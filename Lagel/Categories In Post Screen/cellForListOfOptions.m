//
//  cellForListOfOptions.m

//
//  Created by Rahul Sharma on 14/02/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "cellForListOfOptions.h"

@implementation cellForListOfOptions

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

-(void)updateCell :(NSArray *)dataArray withKey:(NSString *)key IndexPath :(NSIndexPath *)indexpath andPreviousSelection:(NSString *)previousSelection
{
    if([key isEqualToString:@"subcategoryname"]){
        self.labelForList.text = dataArray[indexpath.row][@"subcategoryname"];
        [self.iconImage sd_setImageWithURL:[NSURL URLWithString:flStrForObj(dataArray[indexpath.row][@"subcategoryImage"])] placeholderImage:[UIImage imageNamed:@"defaultpp.png"]];
    }
    else if ([key isEqualToString:@"conditions"]){
        self.labelForList.text = NSLocalizedString(dataArray[indexpath.row],dataArray[indexpath.row]);
    }
    else{
    self.labelForList.text = dataArray[indexpath.row][@"name"];
    [self.iconImage sd_setImageWithURL:[NSURL URLWithString:flStrForObj(dataArray[indexpath.row][@"activeimage"])] placeholderImage:[UIImage imageNamed:@"defaultpp.png"]];
    }
    
    self.labelForList.text = [self.labelForList.text capitalizedString];
    self.labelForList.text = [self.labelForList.text stringByReplacingOccurrencesOfString:@"And" withString:@"and"];
    
    if([previousSelection isEqualToString:self.labelForList.text])
        self.imageViewForSelection.hidden = NO;
    else
        self.imageViewForSelection.hidden = YES;
}

@end
