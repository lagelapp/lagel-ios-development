//
//  CategoriesListTableViewController.h

//
//  Created by Rahul Sharma on 14/02/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LanguageManager.h"
@interface CategoriesListTableViewController : UITableViewController

typedef void(^getDataOfSub)(NSMutableArray *arrayOfList);
@property (strong, nonatomic) IBOutlet UITableView *tableViewForList;
@property (strong,nonatomic) NSMutableArray *dataArray;
@property (strong,nonatomic) NSString *showResultsFor,*key;
@property (nonatomic,weak) NSString *previousSelection;
@property (nonatomic)getDataOfSub callBack;
@property (nonatomic) NSInteger currentIndex;


@end
