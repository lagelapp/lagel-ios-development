//
//  cellForListOfOptions.h

//
//  Created by Rahul Sharma on 14/02/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface cellForListOfOptions : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *iconImage;

@property (strong, nonatomic) IBOutlet UILabel *labelForList;
@property (strong, nonatomic) IBOutlet UIImageView *imageViewForSelection;
-(void)updateCell :(NSArray *)dataArray withKey :(NSString *)key IndexPath :(NSIndexPath *)indexpath andPreviousSelection :(NSString *)previousSelection;
@end
