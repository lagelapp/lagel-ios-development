//
//  PinAddressController.h

//
//  Created by Rahul Sharma on 23/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>
@interface PinAddressController : UIViewController <GMSMapViewDelegate,CLLocationManagerDelegate >
typedef void (^Location)( NSDictionary *locationDict);// NSString *location, double lattitude, double longitude );

@property (nonatomic,strong)NSString *selectedLocation ,*city,*countryShortCode;
@property(nonatomic)BOOL isProductLocation , isFromFilters;
@property(nonatomic)double lat,longittude;
@property (nonatomic,strong) CLLocationManager *locationManager;
@property (strong, nonatomic) IBOutlet UIButton *confirmButnOutlet;
- (IBAction)confirmBtnAction:(UIButton *)sender;
@property (strong, nonatomic) IBOutlet GMSMapView *viewForMap;
@property (strong, nonatomic) IBOutlet UILabel *yourLocLabel;
- (IBAction)allowServiceButton:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *viewYourLoc;
@property (strong, nonatomic) IBOutlet UIImageView *imageForPin;
@property (nonatomic,copy)Location callBackForLocation;

- (IBAction)searchButtonAction:(id)sender;
- (IBAction)RemoveButtonAction:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *buttonRemoveLocationAction;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *hieghtOfPlacePinView;
@property (strong, nonatomic) IBOutlet UIView *viewForPlacePin;
@property (strong,nonatomic) NSMutableArray *waypoints_;
@property (strong,nonatomic) NSMutableArray *waypointStrings_;
@end
