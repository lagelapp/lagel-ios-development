//
//  PinAddressController.m

//
//  Created by Rahul Sharma on 23/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "PinAddressController.h"
#import <GoogleMaps/GoogleMaps.h>
#import "SearchLocationController.h"
#import "CommonMethods.h"
#import "DirectionService.h"

@interface PinAddressController ()
{
    CLLocationCoordinate2D coor;
    CLLocation *currentLoc;
    double updatedLatitude,updatedLongitude;
    GMSCoordinateBounds *bounds;
    GMSMarker *productLocationMarker,*currentLocationMarker;
    BOOL isAddressPickedManually;
    NSString *cityName;
    NSString *countryName;
}

@end

@implementation PinAddressController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationController.navigationBar.hidden = NO;
    
    self.waypoints_ = [NSMutableArray new];
    self.waypointStrings_ = [NSMutableArray new];
    
    [self askforpermissiontoenablelocation];
    
    isAddressPickedManually = NO;
    [self createNavLeftButton];
    if(_isProductLocation){
        [self setupMapViewForProductLocation];
     }
    else{
        updatedLatitude = self.lat;
        updatedLongitude = self.longittude;
        self.viewForMap.delegate = self;
        [self setupMapView];
        
      }
    [self getCurrentLocation];
    
    _confirmButnOutlet.layer.cornerRadius=22;
    _confirmButnOutlet.clipsToBounds=YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    self.navigationController.navigationBar.hidden = NO;
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.shadowImage = nil;
}

-(void)viewWillDisappear:(BOOL)animated

{
    [super viewWillDisappear:YES];
}

#pragma mark- Navigation Buttons

- (void)createNavLeftButton {
    UIButton *navLeft = [CommonMethods createNavButtonsWithselectedState:mNavigationBackButtonImageName normalState:mNavigationBackButtonImageName];
    [navLeft rotateButton];
    UIBarButtonItem *navLeftButton = [[UIBarButtonItem alloc]initWithCustomView:navLeft];
    [navLeft setFrame:CGRectMake(0,0,40,40)];
    self.navigationItem.leftBarButtonItem = navLeftButton;
    [navLeft addTarget:self action:@selector(navLeftButtonAction) forControlEvents:UIControlEventTouchUpInside];
    [self.navigationItem setLeftBarButtonItems:@[[CommonMethods getNegativeSpacer],navLeftButton]];

}
- (void)navLeftButtonAction
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark- Map setup
- (void)setupMapView
{
    self.imageForPin.hidden = NO;
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:updatedLatitude longitude:updatedLongitude zoom:16];
    [self.viewForMap animateToCameraPosition:camera];
    self.viewForMap.myLocationEnabled = YES;
    self.viewForMap.mapType = kGMSTypeNormal;
    self.viewForMap.settings.myLocationButton = YES;
    self.viewForMap.settings.zoomGestures = YES;
    self.viewForMap.settings.tiltGestures = NO;
    self.viewForMap.settings.rotateGestures = NO;
    self.viewForMap.userInteractionEnabled=YES;
   [self.viewForMap addSubview:_viewYourLoc];
   [self.viewForMap addSubview:_imageForPin];

}

#pragma mark- Map setup
- (void)setupMapViewForProductLocation
{
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:self.lat longitude:self.longittude zoom:12];
    [self.viewForMap animateToCameraPosition:camera];
    self.viewForMap.myLocationEnabled = YES;
    self.viewForMap.mapType = kGMSTypeNormal;
    self.viewForMap.settings.myLocationButton = YES;
    self.viewForMap.settings.zoomGestures = YES;
    self.viewForMap.settings.tiltGestures = NO;
    self.viewForMap.settings.rotateGestures = NO;
    self.viewForMap.userInteractionEnabled=YES;
    CLLocationCoordinate2D position = CLLocationCoordinate2DMake(self.lat , self.longittude);
    productLocationMarker = [[GMSMarker alloc]init];
    productLocationMarker = [GMSMarker markerWithPosition:position];
    self.viewYourLoc.hidden = YES;
    self.hieghtOfPlacePinView.constant = 0;
    self.viewForPlacePin.hidden = YES;
    GMSCircle *circ = [GMSCircle circleWithPosition:position
                                            radius:4023.36];
    circ.fillColor = [mBaseColor colorWithAlphaComponent:0.6];
    circ.strokeColor = [UIColor clearColor];
    circ.strokeWidth = 1;
    circ.map = self.viewForMap;
    self.imageForPin.hidden = YES;

}

//********************************
#pragma mark- Mapview Delegate
//********************************
-(void)mapView:(GMSMapView *)mapView idleAtCameraPosition:(GMSCameraPosition *)position
{
    
    if (!isAddressPickedManually) {
    CGPoint point1 = self.viewForMap.center;
    coor = [self.viewForMap.projection coordinateForPoint:point1];
    updatedLatitude = coor.latitude;
    updatedLongitude = coor.longitude;
    CLLocation *location = [[CLLocation alloc]initWithLatitude:updatedLatitude longitude:updatedLongitude];
    [self getAddress:location];
    }
else
{
    isAddressPickedManually = NO;
    CLLocation *location = [[CLLocation alloc]initWithLatitude:updatedLatitude longitude:updatedLongitude];
    [self getAddress:location];
}
}
#pragma mark - Get Current Location Methods -

- (void)getAddress:(CLLocation *)coordinate
{
    
    CLGeocoder *geocoderN = [[CLGeocoder alloc] init];
    
    [geocoderN reverseGeocodeLocation:coordinate
                    completionHandler:^(NSArray *placemarks, NSError *error)
     {
         if (!error)
         {
             CLPlacemark *placemark1 = [placemarks objectAtIndex:0];
             self.city = [placemark1 locality];
             self.countryShortCode = [placemark1 ISOcountryCode];

             if (![[placemark1 locality] isEqualToString: @""]) {
                 self->cityName = [placemark1 locality];
             } else {
                 self->cityName = [placemark1 subAdministrativeArea];
             }
             if(self->cityName == nil){
                  self->cityName = [placemark1 subLocality];
             }
             if(self->cityName == nil){
                 self->cityName = [placemark1 administrativeArea];
             }
             self.city = self->cityName;
             self->countryName = [placemark1 country];
             
//             self.yourLocLabel.text = [[placemark1.addressDictionary valueForKey:@"FormattedAddressLines"] componentsJoinedByString:@", "];
             self.yourLocLabel.text = [NSString stringWithFormat:@"%@, %@", self->cityName, self->countryName];
         }
     }];
}

#pragma mark- Get User current Location

-(void)GetTheUserCurrentLocation {
    if ([CLLocationManager locationServicesEnabled ]) {
        self.locationManager = [[CLLocationManager alloc] init];
        _locationManager.delegate = self;
        // Check for iOS 8. Without this guard the code will crash with "unknown selector" on iOS 7.
        if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
            [self.locationManager requestWhenInUseAuthorization];
        }
        [self.locationManager startUpdatingLocation];
    }
}


- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
   // NSLog(@"%@", [locations lastObject]);
    
    
    
    CLLocation *tempLocation = [locations lastObject];;
    if (tempLocation != nil)
    {
        NSString *longitude = [NSString stringWithFormat:@"%.8f", tempLocation.coordinate.longitude];
        NSString *lattitude = [NSString stringWithFormat:@"%.8f", tempLocation.coordinate.latitude];
        
        [[NSUserDefaults standardUserDefaults]setObject:longitude forKey:@"longitude"];
        [[NSUserDefaults standardUserDefaults]setObject:lattitude forKey:@"lattitude"];
        [[NSUserDefaults standardUserDefaults]synchronize];
    }
    
    [self.locationManager stopUpdatingLocation];
    //    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    currentLoc = [locations lastObject];
    //NSLog(@"%f %f",currentLoc.coordinate.latitude,currentLoc.coordinate.longitude);
    
    if(_isProductLocation || _isFromFilters){
        currentLocationMarker = [[GMSMarker alloc]init];
        currentLocationMarker = [GMSMarker markerWithPosition:currentLoc.coordinate];
        [_waypoints_ addObject:currentLocationMarker];
        
    }
    
    updatedLatitude = currentLoc.coordinate.latitude;
    updatedLongitude = currentLoc.coordinate.longitude;
    [self setupMapView];
    [self getAddress:currentLoc];
}

- (IBAction)confirmBtnAction:(UIButton *)sender {
    
    NSDictionary * dict = [NSDictionary new];
    dict = @{@"address": _yourLocLabel.text,
             @"lat" :[NSString stringWithFormat:@"%lf",updatedLatitude],
             @"long":[NSString stringWithFormat:@"%lf",updatedLongitude],
             @"city" :self.city,
             @"selectedCity" :cityName,
             @"selectedCountry" :countryName,
             @"countryShortName" : self.countryShortCode
             };
    self.callBackForLocation(dict);
    [self.navigationController popViewControllerAnimated:YES];
}


- (IBAction)allowServiceButton:(id)sender {
      [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
}
- (IBAction)searchButtonAction:(id)sender {
    SearchLocationController *SVC =[self.storyboard instantiateViewControllerWithIdentifier:@"PredictLocations"];
    SVC.currLat=currentLoc.coordinate.latitude;
    SVC.currLongi=currentLoc.coordinate.longitude;
    SVC.callBackForLocation=^(NSDictionary *locDict)
    {
        self->isAddressPickedManually = YES;
        self->updatedLatitude = [locDict[@"lat"] doubleValue];
        self->updatedLongitude = [locDict[@"long"] doubleValue];
       // self.yourLocLabel.text = locDict[@"address"];
        self.city = locDict[@"city"];
        self->cityName = locDict[@"city"];
        self.countryShortCode = locDict[@"countryShortName"];
        self->countryName = locDict[@"countryFullName"];
        self.yourLocLabel.text = [NSString stringWithFormat:@"%@, %@", self->cityName, self->countryName];
        GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:self->updatedLatitude longitude:self->updatedLongitude zoom:16];
        [self.viewForMap setCamera:camera];
    };
    
    UINavigationController *navigation = [[UINavigationController alloc] initWithRootViewController:SVC];
    [self presentViewController:navigation animated:YES completion:nil];

}


/**
 This action method will remove the location and keep lat long nil.

 @param sender self.
 */
- (IBAction)RemoveButtonAction:(id)sender {
    
    self.yourLocLabel.text = @"";
    self.lat = 0;
    self.longittude = 0;
}


-(void)fitBoundsAndDrawPolyline
{
    
    // Fit marker into visible area
    bounds = [[GMSCoordinateBounds alloc] init];
    bounds = [[GMSCoordinateBounds alloc] initWithCoordinate:productLocationMarker.position
                                                  coordinate:currentLocationMarker.position];
    //set uiedgeinset to show the marker from corners with given edge constraints
    GMSCameraUpdate *update = [GMSCameraUpdate fitBounds:bounds
                                          withEdgeInsets:UIEdgeInsetsMake(60, 30,40, 30)];
    [self.viewForMap animateWithCameraUpdate:update];
    
    //for polyline uncomment the following code
    if([self.waypoints_ count] > 1)
    {
        NSString *sensor = @"false";
        NSArray *parameters = [NSArray arrayWithObjects:sensor, self.waypointStrings_,
                               nil];
        NSArray *keys = [NSArray arrayWithObjects:@"sensor", @"waypoints", nil];
        NSDictionary *query = [NSDictionary dictionaryWithObjects:parameters
                                                          forKeys:keys];
        DirectionService *mds=[[DirectionService alloc] init];
        SEL selector = @selector(addDirections:);
        [mds setDirectionsQuery:query withSelector:selector withDelegate:self];
         
         
    }
}

-(void)addDirections:(NSDictionary *)json
{
    if ([json[@"routes"] count]>0) {
        
        NSDictionary *routes = [json objectForKey:@"routes"][0];
        NSDictionary *route = [routes objectForKey:@"overview_polyline"];
        NSString *overview_route = [route objectForKey:@"points"];
        GMSPath *path = [GMSPath pathFromEncodedPath:overview_route];
        GMSPolyline *polyline = [GMSPolyline polylineWithPath:path];
        [polyline setStrokeWidth:3.0];
        [polyline setStrokeColor:[UIColor redColor]];
        polyline.map = self.viewForMap;
    }
}

#pragma mark - 
#pragma mark - Enable Location Services

-(void) askforpermissiontoenablelocation
{
        if ([CLLocationManager locationServicesEnabled]) {
            if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied) {
                [self enableLocationServices];
            }
            else {
                [self getCurrentLocation];
            }
        }
        else {
            [self enableLocationServiceFromPrivacy];
        }

}

-(void)getCurrentLocation
{
    if(self.lat ==0 || self.longittude == 0)
    {
        [self GetTheUserCurrentLocation];
    }
}

-(void)enableLocationServices {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Location Services Disabled!" message:@"Please enable Location Based Services for better results! We promise to keep location private" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *settingsButton = [UIAlertAction actionWithTitle:@"Settings" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
    }];
    
    UIAlertAction *cancelButton = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    
    [alertController addAction:settingsButton];
    [alertController addAction:cancelButton];
    [self presentViewController:alertController animated:YES completion:nil];
}

-(void)enableLocationServiceFromPrivacy
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Location Services Disabled!" message:@"Please Go to Privacy setting and enable location Services." preferredStyle:UIAlertControllerStyleAlert];
//    UIAlertAction *settingsButton = [UIAlertAction actionWithTitle:@"Settings" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
//        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
//    }];
    
    UIAlertAction *okButton = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:nil];
    
//    [alertController addAction:settingsButton];
    [alertController addAction:okButton];
    [self presentViewController:alertController animated:YES completion:nil];

  
  //  [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"prefs:root=LOCATION_SERVICES"]];
   
}

@end
