//
//  StringsConstatntHeader.h
//  
//
//  Created by Rahul Sharma on 31/08/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//
//
//  StringConstant.h
//  Lagel
//
//  Created by Rahul Sharma on 19/09/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#ifndef StringConstant_h
#define StringConstant_h

#define Localized(string)       NSLocalizedString(string,string)

#define APP_NAME                         @"Lagel"
static NSString *const alertOk           = @"Ok" ;
static NSString *const alertYes          = @"Yes" ;
static NSString *const alertNo           = @"No" ;
static NSString *const alertCancel       = @"Cancel" ;
static NSString *const alertMessage      = @"Message" ;
static NSString *const alertTitle        = @"Alert" ;
static NSString *const alertError        = @"Error" ;
static NSString *const doneButtonTitle   = @"Done" ;
static NSString *const nextButtonTitle   = @"Next" ;
static NSString *const followButtonTitle    = @"Follow" ;
static NSString *const followingButtonTitle = @"Following" ;
static NSString *const mCommonServerErrorMessage = @"Request Failed" ;

static NSString *const LoadingIndicatorTitle   = @"Loading.." ;
static NSString *const DeletingIndicatorTitle   = @"Deleting..";
static NSString *const SyncContactsIndicatorTitle = @"Syncing Contacts .." ;
static NSString *const updateDetailsIndicatorTitle = @"Updating Details..";
static NSString *const LoggingOutIndicatorTitle = @"Logging out..." ;
static NSString *const PostingIndicatorTitle   = @"Posting.." ;
static NSString *const UpdatingIndicatorTitle   = @"Updating.." ;
//*------------------------------------------------*/
#pragma mark -
#pragma mark - Landing Page
//*------------------------------------------------*/
#define mFacebookLoginErrorMessage             @"Facebook login failed.Please try again."
#define mLoginFailedMessage                    @"Login failed.Please try again."
#define mGoogleLoginErrorMessage               @"Google login failed.Please try again."



//*------------------------------------------------*/
#pragma mark -
#pragma mark - Login SignUp

//*------------------------------------------------*/
static NSString *const phoneNumberIsInvalid       =  @"PhoneNumber is invalid";
static NSString *const emailIsInvalid             =  @"Email is invaild";
static NSString *const usernameIsInvalid          =  @"username is invalid";
static NSString *const LoginButtonTitle           =  @"Log In" ;
static NSString *const signupButtonTitle          =  @"Create" ;
static NSString *const incorrectPasswordMessage   = @"The Password you entered doesn't appear to belong to this account.Please check your password and try again." ;
static NSString *const countryCodeMissing         = @"Country code is missing.";
static NSString *const userNotFound               = @"The username you entered doesn't appear to belong to an account.Please check your username and try again." ;
static NSString *const importProfilePicFromFacebook    = @"Import from Facebook" ;
static NSString *const takePhotoFromCamera        = @"Take Photo" ;
static NSString *const chooseFromLibrary          = @"Choose From Library" ;
static NSString *const removeCurrentProfilePic        = @"Remove Current Photo" ;
static NSString *const cameraPermissionNavTitle = @"Photo" ;
static NSString *const smsMessageBody = @"To verify your ownership, use OTP _code_. Do not share it with anyone.";
#define muserNotFoundAlertTitle                  @"Incorrect Username"
#define mincorrectPasswordAlertTitle            @"Incorrect Password"

#define mUsernameRegisteredAlert                @"This username is already registered"
#define mEmailAlreadyRegistered                 @"This email address is already registered"
#define mNumberAlreadyRegistered                @"This Phone number is already registered"
#define mCameraPermissionTitle                  @"Camera"
#define mPhotosLibraryPermissionTitle           @"PhotosLibrary"
#define mgalleryPermissionActionTitle           @"Enable Library Access"
#define mcameraPermissionAlertTitle             @"Take Photos With Lagel"
#define mcameraPermissionAlertMessage           @"Allow access to your camera to start taking photos with Lagel."
#define mcameraPermissionActionTitle            @"Enable Camera Access"
#define mgalleryPermissionAlertTitle            @"Please Allow Access to your photos"
#define mgalleryPermissionAlertMessage          @"This allows Lagel to share photos from your library and save photos to your camera roll."
#define mAgreeWithTermsAndPrivacy               @"Mark Terms and Privacy Policy"

#define mVerifyCodeScreenDescription            @"Let us Know this mobile number belogns to you. Enter the Code in the SMS sent to "

//*------------------------------------------------*/
#pragma mark -
#pragma mark - Home Screen -
//*------------------------------------------------*/
static NSString *const noPostsNearYouText                          = @"No post yet near you.Be the first to post." ;
static NSString *const noPostsForAppliedFilter                     = @"Oops No results for this near you.Try looking something else please" ;
static NSString *const noInstagramAccountAlertMessage              = @"Unable to share on Instagram.You don't have Instagram installed.";
static NSString *const sharedVia = @"Shared via" ;

//*------------------------------------------------*/
#pragma mark -
#pragma mark - Location and Notification Permission -
//*------------------------------------------------*/

static NSString *const allowLocationTitle                          = @"ALLOW LOCATION" ;
static NSString *const allowLocationMessage                        = @"Do you want to see products near you ?" ;
static NSString *const allowLocationButtonTitle                    = @"Yes, Use my Location" ;
static NSString *const allowNotificationTitle                          = @"ALLOW NOTIFICATION" ;
static NSString *const allowNotificationMessage                    = @"Do you want to be notified when another user like, follow, review or make any offer on your Ads ?" ;
static NSString *const allowNotificationButtonTitle                = @"Yes, Notify me" ;

static NSString *const allowLocationAlertTitle                     = @"Location Service Disabled" ;
static NSString *const allowLocationAlertMessage                   = @"To re-enable, please go to Settings and turn on Location Service to find products near you." ;
static NSString *const allowLocationSettingsActionTitle            = @"Go to settings" ;


//*------------------------------------------------*/
#pragma mark-
#pragma mark - Product Filter Screen -
//*------------------------------------------------*/


static NSString *const navTitleForFilterScreen = @"Filters" ;

static NSString *const navTitleForChangeLocation = @"Change Location" ;

static NSString *const minPriceAlertMessage   = @"The minimum Price cannot be higher than the maximum Price";


static NSString *const allowLocAlertActionTitle    = @"Go to settings" ;

// Section Header Name in Filter Product Screen
static NSString *const filterLocationSectionTitle  = @"LOCATION" ;
static NSString *const filterLocation  = @"LOCATION" ;
static NSString *const filterDistanceSectionTitle   = @"DISTANCE" ;
static NSString *const filterCategorySectionTitle   = @"CATEGORIES";
static NSString *const filterSortBySectionTitle     = @"SORT BY" ;
static NSString *const filterPostedWithInSectionTitle = @"POSTED WITHIN";
static NSString *const filterPriceSectionTitle         = @"PRICE" ;

// label title in Cell
static NSString *const sortingByNewestFirst = @"Newest First" ;
static NSString *const sortingByClosestFirst = @"Closest First" ;
static NSString *const sortingByPriceLowToHigh = @"Price: low to high";
static NSString *const sortingByPriceHighToLow = @"Price: high to low" ;

// label title in PostedWithIn cell

static NSString *const postedWithLast24h = @"The last 24h" ;
static NSString *const postedWithLast7d  = @"The last 7 days" ;
static NSString *const postedWithLast30d = @"The last 30 days" ;
static NSString *const postedWithAllProducts = @"All products" ;
//

static NSString *const filterCurrencyTitle  = @"Currency" ;
static NSString *const filterPriceFrom  = @"From  :" ;
static NSString *const filterPriceTo  = @"To  :" ;
static NSString *const distanceFilterNotSet = @"Not set" ;
static NSString *const distanceFilterMax = @"Max" ;

//*------------------------------------------------*/
#pragma mark-
#pragma mark - Product Details Screen -
//*------------------------------------------------*/

static NSString *const productRemovedAlertMessage  = @"This Product has been removed by the user" ;
static NSString *const productSoldOut              = @"Sold Out";
static NSString *const MakeofferButtonTitle        = @"Make Offer" ;
static NSString *const MarkAsSoldButtonTitle       = @"Mark As Sold" ;
static NSString *const productNotNegotiable        = @"Not Negotiable" ;
static NSString *const productNegotiable           = @"Negotiable" ;
static NSString *const unfollowAlert               = @"Unfollow" ;
static NSString *const actionSheetCopyUrl          = @"Copy Share URL";
static NSString *const actionSheetShareOnFacebook  = @"Share to Facebook";
static NSString *const productConditionTitle       = @"Condition" ;
static NSString *const productDescriptionTitle     = @"Description" ;

//*------------------------------------------------*/
#pragma mark
#pragma mark - All Likes  Screen Constants
/*-------------------------------------------------*/
static NSString *const navTitleForLikes                                     = @"Likes" ;

static NSString *const emptyLikesListLabelText                            = @"No Likes Yet! Be the first to like this product" ;


//*------------------------------------------------*/
#pragma mark
#pragma mark - Followers  Screen Constants
/*-------------------------------------------------*/
static NSString *const navTitleForFollowers                                     = @"Followers" ;
static NSString *const emptyFollowersListLabelText                            = @"No Followers Yet!" ;


//*------------------------------------------------*/
#pragma mark
#pragma mark - Followings Screen Constants
/*-------------------------------------------------*/
static NSString *const navTitleForFollowing                                     = @"Following" ;
static NSString *const emptyFollowingsListLabelText                            = @"No one Following Yet!" ;


//*------------------------------------------------*/
#pragma mark
#pragma mark - Comments  Screen Constants
/*-------------------------------------------------*/

static NSString *const navTitleForComments                   = @"Reviews" ;
static NSString *const loadMoreCommentsText     =  @"Load More comments" ;



//*------------------------------------------------*/
#pragma mark-
#pragma mark - Profile Screen -
//*------------------------------------------------*/
static NSString *const sellProductAgain                    = @"Sell it Again" ;
static NSString *const actionsheetTitleForCall             = @"Call" ;
static NSString *const actionsheetEmailOptionTitle          = @"Email" ;
static NSString *const emailSubject                         = @"Contact Regarding Business " ;
static NSString *const facebookAccountAlreadyLinked         = @"This Facebook account is already linked with other account.";
static NSString *const googleAccountAlreadyLinked         = @"This Google+ account is already linked with other account." ;
static NSString *const alertMessageToOpenSafari           = @"Open Safari?" ;
static NSString *const invalidUrl                         = @"It is not a valid url" ;
static NSString *const emptySellingPosts                  = @"Snap and Post in 30 seconds to sell stuff" ;
static NSString *const emptyPostsText                     = @"No Posts Yet !" ;
static NSString *const emptyFavouritesText1               = @"Not Found any Favourite Yet !" ;
static NSString *const emptyFavouritesText2               = @"Posts you love & mark as favourite will appear here.";
static NSString *const facebookVerificationFailed         = @"Facebook verification failed. Please try again." ;


//*------------------------------------------------*/
#pragma mark
#pragma mark - DiscoverPeople  Screen Constants
/*-------------------------------------------------*/

static NSString *const navTitleForDiscoverPeople   =  @"Discover People" ;
static NSString *const titleForFriends             = @" Friends" ;
static NSString *const titleForFriend              = @" Friend" ;
static NSString *const titleForContacts            = @" Contacts" ;
static NSString *const titleForContact             = @" Contact" ;
static NSString *const toFollowYourFriends         = @"to follow your friends" ;
static NSString *const noPostsTitle                = @"No Posts" ;
static NSString *const connectToContacts           = @"Connect to Contacts" ;
static NSString *const connectedContacts           = @"Connected Contacts" ;
static NSString *const connectToFacebook           = @"Connect to Facebook" ;
static NSString *const connectedFacebook           = @"Connected Facebook" ;
static NSString *const noneOfFriendUsingApp        = @"None of Your Friends Are Using" ;


//*------------------------------------------------*/
#pragma mark
#pragma mark - Connect to Facebook/Contacts  Screen Constants
/*-------------------------------------------------*/

#define mFacebookTitle                          @"Facebook"
#define mContactsTitle                          @"Contacts"

static NSString *const disconnectAlertActionTitle      = @"Disconnect" ;

static NSString *const alertToRemoveAllContacts  = @"Syncing your contacts helps you follow friends.We'll remove contact info from our system when you disconnect.";

static NSString *const allowAccessToContactsMessage         = @"This app requires access to your contacts to function properly. Please visit to the \"Privacy\" section in the iPhone Settings app." ;

static NSString *const errorFetchingContactList          = @"Error Fetching In Contact list" ;
static NSString *const noSuggestionsAvailable          = @"No Suggestions Available" ;
static NSString *const helpPeopleToFindMessage          = @"To help people connect to Lagel,your contacts are periodically synced and securely stored on our servers.You choose which contacts to follow.Disconnect at any time to remove them." ;

static NSString *const findPeopleToFollow       = @"Find people to follow " ;
static NSString *const allowAccess       = @"Allow Access" ;


//*------------------------------------------------*/
#pragma mark
#pragma mark - Edit Profile Screen Constants
/*-------------------------------------------------*/

static NSString * const navTitleForEditProfile = @"Edit Profile" ;
static NSString * const profileChangesFailed =  @"Profile changes failed" ;
static NSString *const unsavedChangesMessage = @"You have unsaved changes.Are you sure you want to cancel?";
static NSString *const unsavedChanges = @"Unsaved Changes" ;
static NSString *const emptyUserName =  @"Please Choose a User Name";
static NSString *const UsernameTextLimitsWarningMessage   = @"Select username less than 30 characters";
static NSString *const NameTextLimitsWarningMessage  = @"Select fullname less than 30 characters";
static NSString *const invalidWebUrl   = @"enter valid website url";
static NSString *const NotSpecified  =  @"Not Specified" ;
static NSString *const Male  =  @"Male" ;
static NSString *const female  =  @"Female" ;
static NSString *const bio =   @"Bio" ;
static NSString *const bioTextLimitsWarningMessage                              = @"Bio must be 150 or less characters" ;
static NSString *const waitImageIsUploading                        = @"wait need to upload image to cloudinary" ;
static NSString *const sorry                        = @"Sorry" ;
static NSString *const uploadErrorMessage         = @"Unable to upload your Profile pic .Please Try again" ;

//*------------------------------------------------*/
#pragma mark
#pragma mark - Edit Product Screen Constants
/*-------------------------------------------------*/


static NSString *const soldSomewhereElseAlertMessage = @"Sold" ;
static NSString *const deleteThisPostActionSheetTitle  = @"Delete this Post";
static NSString *const editThisPostActionSheetTitle  = @"Edit this Post" ;
static NSString *const priceOfferedStatement  = @"Price Offered";


//*------------------------------------------------*/
#pragma mark
#pragma mark - Settings Screen Constants
/*-------------------------------------------------*/

static NSString * const navTitleForSettingsScreen = @"Settings" ;
static NSString *const findContacts = @"Find Contacts" ;
static NSString *const findFacebookFriends = @"Find Facebook Friends";
static NSString *const myPayments = @"My Payments" ;
static NSString *const reportProblem = @"Report a Problem" ;
static NSString *const LogOutTitle = @"Log Out" ;
static NSString *const  logoutAlertMessage     = @"Are you sure want to logout" ;
static NSString *const  problemOn    = @"Problem on" ;


//*------------------------------------------------*/
#pragma mark
#pragma mark - Camera Screen
/*-------------------------------------------------*/
static NSString *const needAtleastOnePhoto   = @"Need atleast one photo to post" ;


//*------------------------------------------------*/
#pragma mark
#pragma mark - Library Photos Screen
/*-------------------------------------------------*/
static NSString *const addUptoMaximumFiveImages   = @"Please Add upto maximum 5 images" ;


//*------------------------------------------------*/
#pragma mark
#pragma mark - Post Screen
/*-------------------------------------------------*/
static NSString * const navTitleForPostProduct  = @"Post Product" ;
static NSString * const navTitleForEditPost     = @"Edit Post" ;
static NSString * const addAtleastSingleImage     =@"Add atleast single image (mandatory)";

#define mTitleMissingAlert                       @"Please give a Title to the product"
#define mCategoryMissingAlert                    @"Please choose a category"
#define mConditionMissingAlert                   @"Please choose a condition"
#define mPriceMissingAlert                       @"Please set a price of the product"
#define mCurrencyMissingAlert                    @"select any currency"
#define mAddImageButtonImageName                 @"productPosting_addImage"
#define mAddLocationAlert                        @"Add your location its mandatory"

static NSString *const noTwitterAcoount  = @"There is no Twitter account configured" ;
static NSString *const addLocationTitle  = @"Add Location" ;
static NSString *const unableToPostYourListings  = @"unable to post your listings.Please Try again";
static NSString *const instagramIsNotInstalled  =  @"Unable to share on Instagram.You don't have Instagram installed." ;
static NSString *const postButtonTitle   = @"Post" ;
static NSString *const updateButtonTitle = @"Update" ;

//*------------------------------------------------*/
#pragma mark
#pragma mark - Product Category Screen Constants
/*-------------------------------------------------*/

static NSString * const navTitleForProductCategory = @"Product Category" ;

//*------------------------------------------------*/
#pragma mark
#pragma mark - Activity Screen Constants
/*-------------------------------------------------*/

static NSString * const navTitleForActivity = @"Activity" ;
static NSString * const review = @"review";
static NSString * const likedYourPostStatement = @"liked your post.";
static NSString * const startedFollowingYouStatement = @"started following you." ;
static NSString * const leftReviewOnYourPostStatement = @"left a review on your post." ;
static NSString * const sendOfferStatement = @"send a offer on your product" ;
static NSString * const from = @"from";
static NSString * const rateSellerStatement = @"Please rate your experience with the seller by clicking here.";
static NSString * const youJustBoughtStatement = @"You just bought" ;
static NSString * const rateButtonTitle = @"Rate User" ;
static NSString * const mentionedInReviewStatement  = @"Mentioned in review" ;
static NSString * const likedStatement  = @"liked" ;
static NSString * const startedFollowingStatement = @"started following";
static NSString * const leftReviewStatement = @"left a review on" ;
static NSString * const noSelfActivityLabelText = @"Welcome to Lagel! Make money by selling what you don't need and find great deals nearby.";
static NSString * const startSellingButtonTitle =  @"Start Selling";
static NSString * const noFollowingActivityLabelText = @"When someone you follow like or review on a post, you'll see it here." ;
static NSString * const findPeopleButtonTitle =  @"Find people to follow" ;


//*------------------------------------------------*/
#pragma mark
#pragma mark - Search Screen Constants
/*-------------------------------------------------*/
static NSString * const navTitleForSearch = @"Search" ;
static NSString * const searchPosts = @"Search Posts" ;
static NSString * const searchPeople = @"Search People" ;


//*------------------------------------------------*/
#pragma mark
#pragma mark - Report Constants
/*-------------------------------------------------*/

static NSString * const navTitleForReportUser = @"Report User" ;
static NSString * const navTitleForReportItem = @"Report Item" ;
static NSString * const userHasBeenReprted = @"The user has been reported.Thanks";
static NSString * const userIsAlreadyReported = @"The user is already reported.Thanks" ;
static NSString * const productIsReported = @"The product has been reported.Thanks";
static NSString * const productIsAlreadyReported = @"The product is already reported.Thanks";
static NSString * const whyToReportUser = @"Why do you want to report this user?" ;
static NSString * const whyToReportProduct = @"Why do you want to report this item?" ;

static NSString * const report1 = @"It's prohibited on lagel" ;
static NSString * const report2 = @"It's offensive to me" ;
static NSString * const report3 = @"It's not a real post" ;
static NSString * const report4 = @"It's a duplicate post" ;
static NSString * const report5 = @"It's in the wrong category" ;
static NSString * const report6 = @"It may be wrong" ;
static NSString * const report7 = @"It may be stolen" ;
static NSString * const report8 = @"Other" ;



static NSString * const userReport1 = @"Missed our meeting" ;
static NSString * const userReport2 = @"Not respond to messages" ;
static NSString * const userReport3 = @"Sold me something broken" ;
static NSString * const userReport4 = @"Inappropriate" ;
static NSString * const userReport5 = @"Possible Scamme" ;
static NSString * const userReport6 = @"Their items may be stolen" ;
static NSString * const userReport7 = @"Incident at MeetUp" ;
static NSString * const userReport8 = @"Other" ;

//*------------------------------------------------*/
#pragma mark
#pragma mark - Terms Privacy Constants
/*-------------------------------------------------*/

static NSString * const navTitleForTerms = @"Terms & Conditions" ;
static NSString * const navTitleForPrivacy = @"Privacy Policy" ;


//*------------------------------------------------*/
#pragma mark
#pragma mark - Insights Constants
/*-------------------------------------------------*/
static NSString * const navTitleForInsights = @"Insights" ;
static NSString * const clicks = @"Clicks" ;

//*------------------------------------------------*/
#pragma mark
#pragma mark - Pin Address Screen Constants
/*-------------------------------------------------*/

#define mNavTitleForProductLocation                @"Product Location"
#define mNavTitleForPinAddress                     @"Pin the Address"

//*------------------------------------------------*/
#pragma mark
#pragma mark - Update Phone Number
/*-------------------------------------------------*/

#define mNavTitleForUpdateNumber                @"Phone Number"
#define mNavTitleForUpdateEmail                 @"Email address"

//*------------------------------------------------*/
#pragma mark
#pragma mark - Conditions Array
/*-------------------------------------------------*/

#define mArrayOfConditions                      @[@"Used",@"Refurbished",@"Open Box",@"Collectible",@"New",@"Other(see description)"]

#endif /* StringConstant_h */
