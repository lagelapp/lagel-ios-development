//
//  VerifyCodeViewController.m
//  Lagel
//
//  Created by admin on 2018/10/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import "VerifyCodeViewController.h"

@interface VerifyCodeViewController () <WebServiceHandlerDelegate,UITextFieldDelegate>

@end

@implementation VerifyCodeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    NSString *description = Localized(mVerifyCodeScreenDescription);
    self.descriptionLabel.text = [description stringByAppendingString:self.userNumber];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)errorAlert:(NSString *)message {
    //[self dismissKeyboard];
    //creating alert for error message
    
    [UIView animateWithDuration:0.2 animations:^{
        [self.verifyCodeTextField resignFirstResponder];
        [self.passwordTextField resignFirstResponder];
        [self.confirmPaswordTextField resignFirstResponder];
        
        CGRect frameOfView = self.view.frame;
        frameOfView.origin.y = 0;
        frameOfView.origin.x=0;
        self.view.frame = frameOfView;
    }  completion:^(BOOL finished) {
        UIAlertController *controller = [CommonMethods showAlertWithTitle:@"Message" message:message actionTitle:@"Ok"];
        mPresentAlertController;
    }];
}

#pragma mark - IBAction

- (IBAction)tapBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)tapSend:(id)sender {
    if (_verifyCodeTextField.text.length > 0) {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        NSDictionary *requestDict = @{@"phoneNumber": self.userNumber,
                                      @"password": _passwordTextField.text,
                                      @"repeatPassword": _confirmPaswordTextField.text,
                                      @"otp": _verifyCodeTextField.text
                                      };
        [WebServiceHandler checkVerificationCode:requestDict andDelegate:self];
    } else {
        [self errorAlert:@"Please check the input fields"];
    }
}

- (IBAction)tapChangePassword:(id)sender {
    if (_passwordTextField.text.length > 0 && [_passwordTextField.text isEqualToString:_confirmPaswordTextField.text]) {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        NSDictionary *requestDict = @{@"phoneNumber": self.userNumber,
                                      @"password": _passwordTextField.text,
                                      @"repeatPassword": _confirmPaswordTextField.text,
                                      @"otp": _verifyCodeTextField.text
                                      };
        [WebServiceHandler changePassword:requestDict andDelegate:self];
    } else {
        [self errorAlert:@"Please check the input fields"];
    }
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - WebServiceDelegate

- (void) didFinishLoadingRequest:(RequestType)requestType withResponse:(id)response error:(NSError*)error {
    //handling response.
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    if (error) {
        [self errorAlert:[error localizedDescription]];
        return;
    }
    //storing the response from server to dictonary(responseDict).
    NSDictionary *responseDict = (NSDictionary*)response;
    if (requestType == RequestTypemakeChangePassword) {
        switch ([responseDict[@"code"] integerValue]) {
            case 200: {
                
                UIAlertController *controller = [UIAlertController alertControllerWithTitle:nil message:responseDict[@"message"] preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *action = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                    [self.navigationController popToRootViewControllerAnimated:YES];
                }];
                [controller addAction:action];
                [self presentViewController:controller animated:YES completion:nil];
                
            }
                break;
                // all these responses are error messages.
            case 204: {
                [self errorAlert:responseDict[@"message"]];
            }
                break;
            default:
                [self errorAlert:responseDict[@"message"]];
                break;
        }
    } else {
        switch ([responseDict[@"code"] integerValue]) {
            case 200:
                [self.passwordView setHidden:NO];
                break;
                // all these responses are error messages.
            case 204: {
                [self errorAlert:responseDict[@"message"]];
            }
                break;
            default:
                [self errorAlert:responseDict[@"message"]];
                break;
        }
    }
}


@end
