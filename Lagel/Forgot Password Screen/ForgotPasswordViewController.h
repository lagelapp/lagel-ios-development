
//
//  Created by Rahul_Sharma on 04/11/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ForgotPasswordViewController : UIViewController

@property (weak, nonatomic) IBOutlet AlignmentOfLabel *descriptinLabel;

@property (weak, nonatomic) IBOutlet UIView *emailView;
@property (weak, nonatomic) IBOutlet UIView *phoneView;
@property (weak, nonatomic) IBOutlet UIView *bttomView;
@property (weak, nonatomic) IBOutlet UIButton *phoneButton;
@property (weak, nonatomic) IBOutlet UIButton *emailButton;
@property (weak, nonatomic) IBOutlet UIButton *countryCodeButton;

@property (weak, nonatomic) IBOutlet UIButton *sendLoginLinkButtonOutlet;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *phoneTextField;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

//- (IBAction)tapToDismissKeyboard:(id)sender;
//- (IBAction)emailTextfieldValueIsChanged:(id)sender;

@end
