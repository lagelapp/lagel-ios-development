//
//  ForgotPasswordViewController.m

//
//  Created by Rahul_Sharma on 04/11/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import "ForgotPasswordViewController.h"
#import "FontDetailsClass.h"
#import "PhoneNumberViewController.h"
#import "AppDelegate.h"
#import "Lagel-Swift.h"
#import "VerifyCodeViewController.h"

@interface ForgotPasswordViewController ()<WebServiceHandlerDelegate,UITextFieldDelegate> {
    UIView *bottomAnimationView;
    BOOL phoneFlag;
    NSString *phoneCode;
    NSString *countryCode;
}

@end

@implementation ForgotPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationController.navigationBar.hidden = NO ;
    self.navigationItem.title = NSLocalizedString(@"Forgot Password", nil) ;
    UIButton *navLeft = [CommonMethods createNavButtonsWithselectedState:mNavigationBackButtonImageName normalState:mNavigationBackButtonImageName];
    [navLeft rotateButton];
    UIBarButtonItem *navLeftButton = [[UIBarButtonItem alloc]initWithCustomView:navLeft];
    [navLeft setFrame:CGRectMake(0,0,40,40)];
    self.navigationItem.leftBarButtonItem = navLeftButton;
    [navLeft addTarget:self action:@selector(navLeftButton) forControlEvents:UIControlEventTouchUpInside];
    [self.navigationItem setLeftBarButtonItems:@[[CommonMethods getNegativeSpacer],navLeftButton]];
    
    phoneCode = @"+1";
    countryCode = @"US";
    phoneFlag = YES;
    
    [self resetCountryCodeButton];
    
    self.descriptinLabel.text = Localized(@"Enter your phone number associated with your account.");
    [self.descriptinLabel setTextAlignment:NSTextAlignmentCenter];
    
    bottomAnimationView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.bttomView.frame.size.width/2, 1.0)];
    bottomAnimationView.backgroundColor = [UIColor blackColor];
    [self.bttomView addSubview:bottomAnimationView];
    
    _emailView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    _emailView.layer.borderWidth = 1;
    _phoneView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    _phoneView.layer.borderWidth = 1;
    
    [self changeSendButtonStatus];
}

-(void)viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:YES];
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(veiwMoveAccKeybaord:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(veiwMoveToOriginal) name:UIKeyboardWillHideNotification object:nil];
}

- (void)viewDidAppear:(BOOL)animated {
    if (phoneFlag){
        [bottomAnimationView setFrame:CGRectMake(0, 0, self.bttomView.frame.size.width/2, 1.0)];
        self.phoneView.hidden = NO;
        self.emailView.hidden = YES;
        
    }else{
        [bottomAnimationView setFrame:CGRectMake(self.bttomView.frame.size.width/2, 0, self.bttomView.frame.size.width/2, 1.0)];
        self.phoneView.hidden = YES;
        self.emailView.hidden = NO;
    }
    
    // Update country and phone code
    
    AppDelegate *delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];

    if (delegate.userPhoneCode != nil && ![delegate.userPhoneCode  isEqual: @""]) {
        phoneCode = delegate.userPhoneCode;
        delegate.userPhoneCode = @"";
        countryCode = delegate.counryCode;
        delegate.counryCode = @"";
        
        [self resetCountryCodeButton];
    }
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:YES];
    [self.view endEditing:YES];
    self.navigationController.navigationBar.hidden = YES ;
    [[NSNotificationCenter defaultCenter] removeObserver:UIKeyboardWillShowNotification];
    [[NSNotificationCenter defaultCenter] removeObserver:UIKeyboardWillHideNotification ];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL)prefersStatusBarHidden {
    return NO;
}

- (NSString *)getFullPhoneNumber {
    NSString *text = [NSString stringWithFormat:@"%@%@", phoneCode, self.phoneTextField.text];
    return text;
}

- (void)resetCountryCodeButton {
    NSString *codeString = [NSString stringWithFormat:@"%@ %@", countryCode,phoneCode];
    [_countryCodeButton setTitle:codeString forState:UIControlStateNormal];
}

- (void)showVerificaitonCodeViewController {
    VerifyCodeViewController *verifyCodeVC = [self.storyboard instantiateViewControllerWithIdentifier:@"verifyCodeVC"];
    verifyCodeVC.userNumber = phoneFlag ? [self getFullPhoneNumber] : self.emailTextField.text;
    verifyCodeVC.phoneFlag = phoneFlag;
    [self.navigationController pushViewController:verifyCodeVC animated:YES];
}

- (BOOL)emailValidationCheck:(NSString *)emailToValidate
{
    NSString *regexForEmailAddress = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailValidation = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",regexForEmailAddress];
    return [emailValidation evaluateWithObject:emailToValidate];
}

- (void)errrAlert:(NSString *)message {
    //[self dismissKeyboard];
    //creating alert for error message
    
    [UIView animateWithDuration:0.2 animations:^{
        [self.emailTextField resignFirstResponder];
        CGRect frameOfView = self.view.frame;
        frameOfView.origin.y = 0;
        frameOfView.origin.x=0;
        self.view.frame = frameOfView;
    }  completion:^(BOOL finished) {
        UIAlertController *controller = [CommonMethods showAlertWithTitle:@"Message" message:message actionTitle:@"Ok"];
        mPresentAlertController;
    }];
}

- (void)changeSendButtonStatus {
    /*
    if (phoneFlag) {
        if (_emailTextField.text.length < 2 ){
            self.sendLoginLinkButtonOutlet.enabled = NO;
            _sendLoginLinkButtonOutlet.backgroundColor = mLoginButtonDisableBackgroundColor;
        }
        else {
            self.sendLoginLinkButtonOutlet.enabled = YES;
            self.sendLoginLinkButtonOutlet.backgroundColor = mLoginButtonEnableBackgroundColor;
        }
    } else {
        if (_emailTextField.text.length < 2 ){
            self.sendLoginLinkButtonOutlet.enabled = NO;
            _sendLoginLinkButtonOutlet.backgroundColor = mLoginButtonDisableBackgroundColor;
        }
        else {
            self.sendLoginLinkButtonOutlet.enabled = YES;
            self.sendLoginLinkButtonOutlet.backgroundColor = mLoginButtonEnableBackgroundColor;
        }
    }
     */
}

#pragma mark
#pragma mark - phone number validation

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self tapSendVerificationCode:self];
    return YES;
}

#pragma mark - IBAction

- (IBAction)tapSendVerificationCode:(id)sender {
    NSDictionary *requestDict = nil;
    
    if (phoneFlag) {
        if (phoneCode.length == 0) {
            [self showAlert:Localized(@"Warning") message:Localized(phoneNumberIsInvalid)];
            return;
        }
        
        if (![self.phoneTextField hasText]) {
            [self.phoneTextField becomeFirstResponder];
            [self showAlert:Localized(@"Warning") message:Localized(phoneNumberIsInvalid)];
            return;
        }
        
        requestDict = @{@"type" : @"1",
                        mphoneNumber : [self getFullPhoneNumber]
                        };
    } else {
        if (![self.emailTextField hasText]) {
            [self.emailTextField becomeFirstResponder];
            [self showAlert:Localized(@"Warning") message:Localized(@"Please input the Email")];
            return;
        }
        
        if (![self emailValidationCheck:[_emailTextField text]]) {
            [self errrAlert:@"Please enter a valid email"];
            return;
        }
        
        requestDict = @{@"type" :@"0",
                        mEmail    : _emailTextField.text
                        };
        
    }
    
    if (requestDict) {
        [self.activityIndicator startAnimating];
        [WebServiceHandler resetPassword:requestDict andDelegate:self];
    }
    
}

- (IBAction)tapPhone:(id)sender {
    phoneFlag = YES;
    [UIView animateWithDuration:0.5 animations:^{
        [self->bottomAnimationView setFrame:CGRectMake(0, 0, self.bttomView.frame.size.width/2, 1.0)];
        [self.phoneButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [self.emailButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    } completion:^(BOOL finished) {
        NSLog(@"%@", NSStringFromCGRect(self->bottomAnimationView.frame));
    }];
    self.phoneView.hidden = NO;
    self.emailView.hidden = YES;
    self.descriptinLabel.text = Localized(@"Enter your phone number associated with your account.");
    [self changeSendButtonStatus];
}

- (IBAction)tapEmail:(id)sender {
    phoneFlag = NO;
    [UIView animateWithDuration:0.5 animations:^{
        [self->bottomAnimationView setFrame:CGRectMake(self.bttomView.frame.size.width/2, 0, self.bttomView.frame.size.width/2, 1.0)];
        [self.phoneButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        [self.emailButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    } completion:^(BOOL finished) {
        NSLog(@"%@", NSStringFromCGRect(self->bottomAnimationView.frame));
    }];
    self.phoneView.hidden = YES;
    self.emailView.hidden = NO;
    self.descriptinLabel.text = Localized(@"Enter your e-mail address associated with your account.");
    [self changeSendButtonStatus];
}

- (IBAction)tapCountryCode:(id)sender {
    PhoneNumberViewController *passwordVC = [self.storyboard instantiateViewControllerWithIdentifier:@"phoneNumberVC"];
    [self.navigationController pushViewController:passwordVC animated:YES];
}

- (IBAction)tapEnterCode:(id)sender {
    [self showVerificaitonCodeViewController];
}

- (IBAction)tapBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark
#pragma mark - phone number validation

- (BOOL)validatePhone:(NSString *)enteredphoneNumber {
    NSString *phoneNumber = enteredphoneNumber;
    NSString *phoneRegex = @"[2356789][0-9]{6}([0-9]{3})?";
    NSPredicate *test = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
    BOOL matches = [test evaluateWithObject:phoneNumber];
    return matches;
}


#pragma mark - Move View

/**
 This method will move view by evaluating keyboard height.
 
 @param notification post by Notificationcentre.
 */
-(void)veiwMoveAccKeybaord :(NSNotification *)notification {
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    //Given size may not account for screen rotation
    NSInteger HeightOfkeyboard = MIN(keyboardSize.height,keyboardSize.width);
    float maxY = CGRectGetMaxY(_sendLoginLinkButtonOutlet.frame) + HeightOfkeyboard +3;
    float reminder = CGRectGetHeight(self.view.frame) - maxY;
    if (reminder < 0) {
        [UIView animateWithDuration:0.4 animations: ^ {
            CGRect frameOfView = self.view.frame;
            frameOfView.origin.y = reminder;
            self.view.frame = frameOfView;}];
    }
}

/**
 This method will move view back to original frame.
 */
-(void)veiwMoveToOriginal
{
    [UIView animateWithDuration:0.4 animations: ^ {
        CGRect frameOfView = self.view.frame;
        frameOfView.origin.y = 0;
        self.view.frame = frameOfView;}];
}

#pragma mark
#pragma mark - WebServiceDelegate

- (void) didFinishLoadingRequest:(RequestType)requestType withResponse:(id)response error:(NSError*)error {
    //handling response.
    
    [self.activityIndicator stopAnimating];
    if (error) {
        [self errrAlert:[error localizedDescription]];
        return;
    }
    //storing the response from server to dictonary(responseDict).
    NSDictionary *responseDict = (NSDictionary*)response;
    if (requestType == RequestTypemakeresetPassword) {
        switch ([responseDict[@"code"] integerValue]) {
            case 200: {
                
                UIAlertController *controller = [UIAlertController alertControllerWithTitle:nil message:responseDict[@"message"] preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *action = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                    if (self->phoneFlag) {
                        [self showVerificaitonCodeViewController];
                    } else {
                        [self.navigationController popViewControllerAnimated:YES];
                    }
                }];
                [controller addAction:action];
                [self presentViewController:controller animated:YES completion:nil];
              
            }
                break;
            // all these responses are error messages.
            case 204: {
                [self errrAlert:responseDict[@"message"]];
            }
                break;
            default:
                [self errrAlert:responseDict[@"message"]];
                break;
        }
    }
}

- (void) navLeftButton{
    [self.navigationController popViewControllerAnimated:YES];
 }

- (IBAction)tapToDismissKeyboard:(id)sender {
    [self.view endEditing:YES];
}

- (IBAction)emailTextfieldValueIsChanged:(id)sender {
    [self changeSendButtonStatus];
}

#pragma mark - Twilio

- (NSString *)getRandomPINString:(NSInteger)length
{
    NSMutableString *returnString = [NSMutableString stringWithCapacity:length];
    
    NSString *numbers = @"0123456789";
    
    // First number cannot be 0
    [returnString appendFormat:@"%C", [numbers characterAtIndex:(arc4random() % ([numbers length]-1))+1]];
    
    for (int i = 1; i < length; i++)
    {
        [returnString appendFormat:@"%C", [numbers characterAtIndex:arc4random() % [numbers length]]];
    }
    
    return returnString;
}

- (NSString *)generateSMSMessage {
    NSString *result = [NSString stringWithFormat:@"%@ from Lagel", [self getRandomPINString:4]];
    return result;
}

- (void)sendSMSToNumber:(NSString *)number message:(NSString *)message {
    NSString *kTwilioSID = @"AC16fad06566037a047ef1b10d14f651be";
    NSString *kTwilioSecret = @"94ac57c83b3739b8a26536ac32ab72f6";
    NSString *fromNumber = @"+1 786 837 5949";
    
    NSCharacterSet *set = [NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789"];
    
    NSString *toNumber = [number stringByAddingPercentEncodingWithAllowedCharacters:set];
    
    NSString *reqeustURLString = [NSString stringWithFormat:@"https://%@:%@@api.twilio.com/2010-04-01/Accounts/%@/SMS/Messages", kTwilioSID, kTwilioSecret, kTwilioSID];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:reqeustURLString]];
    [request setHTTPMethod:@"POST"];
    NSString *httpBody = [NSString stringWithFormat:@"From=%@&To=%@&Body=%@", fromNumber, toNumber, message];
    [request setHTTPBody:[httpBody dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES]];
    
    [[[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (data != NULL) {
            NSString *responseDetails = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            NSLog(@"%@", responseDetails);
        }
    }] resume] ;
}

@end
