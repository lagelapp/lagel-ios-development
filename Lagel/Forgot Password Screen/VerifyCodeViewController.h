//
//  VerifyCodeViewController.h
//  Lagel
//
//  Created by admin on 2018/10/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VerifyCodeViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet UITextField *verifyCodeTextField;

@property (weak, nonatomic) IBOutlet UIView *passwordView;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UITextField *confirmPaswordTextField;

@property (strong, nonatomic) NSString *userNumber;
@property (assign, nonatomic) BOOL phoneFlag;


@end
