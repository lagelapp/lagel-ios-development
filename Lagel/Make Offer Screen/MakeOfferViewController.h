//
//  MakeOfferViewController.h

//
//  Created by Rahul Sharma on 10/03/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CouchbaseLite/CouchbaseLite.h>
#import "CBObjects.h"
#import "CouchbaseEvents.h"
#import "Database.h"
#import "MBProgressHUD.h"
#import "ContacDataBase.h"
#import "ProductDetails.h"


@class MakeOfferViewController;
@protocol MakeOfferDelegate <NSObject>

@optional
-(void)sendOfferMessage:(NSDictionary*)makeOfferDetail onDocument:(CBLDocument *)document groupId:(NSString *)groupId ;

@end



@interface MakeOfferViewController : UIViewController < UIImagePickerControllerDelegate,CouchBaseEventsDelegate>

@property (weak, nonatomic) id<MakeOfferDelegate> delegate;
@property (nonatomic)BOOL fromSearchScreen ;
@property (strong, nonatomic) IBOutlet UIImageView *productImage;
@property (strong, nonatomic) IBOutlet UILabel *productName;
@property (strong, nonatomic) IBOutlet UILabel *productArea;
@property (strong, nonatomic) IBOutlet UILabel *ProductDistance;
@property (strong, nonatomic) IBOutlet UILabel *actualPrice;
@property (strong, nonatomic) IBOutlet UITextField *OfferpriceTextField;
@property (strong, nonatomic) IBOutlet UIButton *makeOfferOutlet;
- (IBAction)makeOfferButtonAction:(id)sender;
@property (strong, nonatomic) ProductDetails *product;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *viewbuttomConstrain;
@property (strong, nonatomic) NSMutableArray *totalRows;
@property (strong ,nonatomic)NSMutableArray *totalDocumentID;
@property (strong, nonatomic) CBLQueryEnumerator *result;
@property(assign, nonatomic) NSInteger docCount;
@property ( nonatomic) int chatAvaliable;
@property (strong, nonatomic) NSMutableArray *totallGroupDetails;
@property (strong, nonatomic) NSMutableArray *totallGroupVisible;
- (IBAction)backToProductAction:(id)sender;
@property (strong, nonatomic) NSString *comingFromChat;
-(void)createObjectForMakeOfferwithData:(ProductDetails *)dataDict withPrice:(UInt64)price andType:(NSString *)type withAPICall:(BOOL)isAPICall;
@end

