//

//
//  Created by Rahul Sharma on 28/12/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MultipleImagesCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIButton *removeButton;
@property (strong, nonatomic) IBOutlet UIImageView *imageViewContainer;
@property (weak, nonatomic) IBOutlet UILabel *labelForCover;

-(void)updateCellObjectWithImage:(UIImage *)Image;
-(void)updateCellWithImage:(UIImage *)img andIndex:(NSIndexPath *)indexPath;
@end
