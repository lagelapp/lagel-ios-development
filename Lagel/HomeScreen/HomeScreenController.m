
//  HomeScreenController.h
//  Created by Rahul Sharma on 4/12/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_RETINA ([[UIScreen mainScreen] scale] >= 2.0)

#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define SCREEN_MAX_LENGTH (MAX(SCREEN_WIDTH, SCREEN_HEIGHT))
#define SCREEN_MIN_LENGTH (MIN(SCREEN_WIDTH, SCREEN_HEIGHT))

#define IS_IPHONE_4_OR_LESS (IS_IPHONE && SCREEN_MAX_LENGTH < 568.0)
#define IS_IPHONE_5 (IS_IPHONE && SCREEN_MAX_LENGTH == 568.0)
#define IS_IPHONE_6 (IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
#define IS_IPHONE_6P (IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)
#define IS_IPHONE_X  (IS_IPHONE && SCREEN_MAX_LENGTH == 812.0)
#define IS_IPHONE_XS_MAX  (IS_IPHONE && SCREEN_MAX_LENGTH == 896.0)
#define IS_IPAD_AIR  (IS_IPHONE && SCREEN_MAX_LENGTH == 896.0)
#define IS_IPAD_PRO (IS_IPHONE && SCREEN_MAX_LENGTH == 1366.0)



#import "HomeScreenController.h"
#import "ListingCollectionViewCell.h"
#import "SVPullToRefresh.h"
#import "ProductDetailsViewController.h"
#import "CellForFilteredItem.h"
#import "FilterViewController.h"
#import "RFQuiltLayout.h"
#import "ActivityViewController.h"
#import "CameraViewController.h"
#import "SearchPostsViewController.h"
#import "ZoomInteractiveTransition.h"
#import "ZoomTransitionProtocol.h"
#import "StartBrowsingViewController.h"
#import "AskPermissionViewController.h"
#import "ProductDetails.h"
#import "Lagel-Swift.h"
#import "AppDelegate.h"
#import "SDWebImagePrefetcher.h"
#import "MakeOfferViewController.h"
#define FiltersViewHeight 60


@import Firebase;

UIButton *btnClear;
NSMutableArray *arrProductOriginal;
bool isFirstTimeLoaded;
NSMutableDictionary *filterdict;
bool isEditClicked = false;
@interface HomeScreenController ()<WebServiceHandlerDelegate,RFQuiltLayoutDelegate,GetCurrentLocationDelegate,ZoomTransitionProtocol , SDWebImageManagerDelegate , AskPermissionDelegate, CLLocationManagerDelegate ,ProductDetailsDelegate,UICollectionViewDataSourcePrefetching >

@end

@implementation HomeScreenController

/*--------------------------------------*/
#pragma mark
#pragma mark - ViewController LifCycle
/*--------------------------------------*/

- (void)viewDidLoad {
    [super viewDidLoad];
   // [[Crashlytics sharedInstance] crash];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    self.tblHeaderView.frame = CGRectMake(0, 0, self.view.frame.size.width, 40);
    [self.txtSearchBar setPlaceholder:Localized(@"Search")];
    dispatch_async(dispatch_get_main_queue(),^{
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Chat" bundle: nil];
        BuyerSellerChatViewController *lvc = (BuyerSellerChatViewController *)[storyboard instantiateViewControllerWithIdentifier:@"chatTabbarController"];
        [lvc.view setAlpha:0];
        isFirstTimeLoaded = YES;
        [self displayContentController:lvc];
    });
    [self.postTableViewHeight setConstant:0];
    [self.postTableView setHidden:YES];
     [self.navigationController.interactivePopGestureRecognizer setDelegate:nil];
    
     self.transition = [[ZoomInteractiveTransition alloc] initWithNavigationController:self.navigationController];
    [CommonMethods setNegativeSpacingforNavigationItem:self.navigationItem andExtraBarItem:self.searchButtonOutlet];
    minPrice = @""; maxPrice = @""; postedWithin = @""; sortBy = @"";
    dist = 0;
    self.paging = 1;
    self.cellDisplayedIndex = -1;
    self.leadingConstraint = 40;
    self.FilterViewTopConstraint.constant= -FiltersViewHeight;
    currencyCode = @"All";
    currencySymbol = @"";
    arrayOfcategory = [[NSMutableArray alloc]init];
    explorePostsresponseData = [[NSMutableArray alloc] init];
    arrayOfCategoryImagesUrls = [[NSMutableArray alloc]init];
    arrProductOriginal = [[NSMutableArray alloc] init];
    productsArray = [[NSMutableArray alloc]init];
    filterdict = [[NSMutableDictionary alloc]init];
     temp = [[NSMutableArray alloc] init];
    [self addingActivityIndicatorToCollectionViewBackGround];
    [self RFQuiltLayoutIntialization];
    [self addingRefreshControl];
    [self notficationObservers];
    [self getCategoriesListFromServer];
    [self checkPermissionForAllowLocation];
    
    if([[Helper userToken] isEqualToString:mGuestToken]){
        NSDictionary *param = [CommonMethods updateDeviceDetailsForAdmin];
        [WebServiceHandler logGuestUserDevice:param andDelegate:self];
    }
    
    self.collectionViewOutlet.prefetchDataSource = self ;
    self.collectionViewOutlet.prefetchingEnabled = YES ;
    
    
    

    UIImageView *img = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"search_ic"]];
    img.contentMode = UIViewContentModeScaleAspectFit;
    _txtSearchBar.leftView = img;
    _txtSearchBar.leftViewMode = UITextFieldViewModeUnlessEditing;


    btnClear = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnClear addTarget:self
                action:@selector(btnClearClicked:)
      forControlEvents:UIControlEventTouchUpInside];
    [btnClear setImage:[UIImage imageNamed:@"navigationBackButton.png"] forState:UIControlStateNormal];
    btnClear.frame = CGRectMake((_txtSearchBar.frame.size.width - 25), 5.0, 20.0, 20);
    _txtSearchBar.rightView = btnClear;
    _txtSearchBar.rightViewMode = UITextFieldViewModeWhileEditing;
    _txtSearchBar.layer.borderColor = [UIColor colorWithRed:235.0/255.0 green:235.0/255.0 blue:235.0/255.0 alpha:1.0].CGColor;
    _txtSearchBar.layer.borderWidth = 1.5;
    _txtSearchBar.layer.cornerRadius = 2;
    _txtSearchBar.attributedPlaceholder = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"    %@",Localized(@"Search for anything")] attributes:@{NSForegroundColorAttributeName:[UIColor colorWithRed:95.0/255.0 green:95.0/255.0 blue:95.0/255.0 alpha:1.0]}];
    [self.navigationController.navigationBar setValue:@(YES) forKeyPath:@"hidesShadow"];
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:247.0/255.0 green:246.0/255.0 blue:242.0/255.0 alpha:1.0]];
 }



-(IBAction)btnBackClicked:(id)sender
{
    [self.postTableView setHidden:true];
    [self.txtSearchBar endEditing:true];
    [self.txtSearchBar setText:@""];
  //  [self reloadCollectionView];
    [self refreshData:self];
}
-(IBAction)btnClearClicked:(id)sender
{
    _txtSearchBar.text = @"";
    UIButton *btnTmp = (UIButton *)self.txtSearchBar.rightView;
    
    [btnTmp setImage:[UIImage imageNamed:@"navigationBackButton.png"] forState:UIControlStateNormal];
}


-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    
    [self.postTableViewHeight setConstant: 0];
    [self.postTableView setHidden:YES];
    self.isScreenDidAppear = NO;
    self.sellStuffButtonView.hidden = NO;
    self.sellStuffButtonView.alpha = 0 ;
    self.bottomConstraintOfSellButton.constant = 60;
    NSDictionary *checkAdsCampaign  = [[NSUserDefaults standardUserDefaults] objectForKey:mAdsCampaignKey];
    if(checkAdsCampaign)
    {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:mAdsCampaignKey];
        [[NSNotificationCenter defaultCenter] postNotificationName:mTriggerCampaign object:checkAdsCampaign];
    }
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"openActivity"]) {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"openActivity"];
        [self.tabBarController setSelectedIndex:0];
        ActivityViewController *activityVC  = [self.storyboard instantiateViewControllerWithIdentifier:@"ActivityControllerId"];
        UINavigationController *nav  = [[UINavigationController alloc]initWithRootViewController:activityVC];
        [self.navigationController presentViewController:nav animated:YES completion:nil];
        [[NSUserDefaults standardUserDefaults]synchronize];
    }
    [self getnotificationCount];
    [self showWelcomeMessageForBrowsing];
    
    SDWebImagePrefetcher *prefetcher = [SDWebImagePrefetcher sharedImagePrefetcher];
    [prefetcher prefetchURLs:arrayOfCategoryImagesUrls progress:nil completed:^(NSUInteger completedNo, NSUInteger skippedNo) {
    }];
    
    if([[NSUserDefaults standardUserDefaults] valueForKey:@"recent_login"])
    {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"recent_login"];
        [self requesForProductListings];
    }
    
    // Update User Location
    if(![[Helper userToken] isEqualToString:mGuestToken]) // Only for user.
    {
    if([[GetCurrentLocation sharedInstance] currentCity] && [[GetCurrentLocation sharedInstance] countryShortCode] && [[GetCurrentLocation sharedInstance] location])
    {
        double lattitudeToUpdate , longitudeToUpdate;
        
        lattitudeToUpdate =  [[NSString stringWithFormat:@"%.4lf", [[GetCurrentLocation sharedInstance]lastLatLong].latitude]doubleValue];
        
        longitudeToUpdate = [[NSString stringWithFormat:@"%.4lf", [[GetCurrentLocation sharedInstance]lastLatLong].longitude]doubleValue];
        
        if(lattitudeToUpdate != [[GetCurrentLocation sharedInstance] existingLat] && longitudeToUpdate != [[GetCurrentLocation sharedInstance] existingLong])
        {
        [self updateLocationForUser];
        }
    }
    }
    
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:YES];
    AppDelegate *delegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    [delegate setStatusBarBackgroundColor:[UIColor clearColor]];
    [self.view endEditing:YES];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    
    
    
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.shadowImage = nil;
    
    if([[NSUserDefaults standardUserDefaults] valueForKey:@"Instagram_Share"])
    {
        [self ShareOnInstagram:[[NSUserDefaults standardUserDefaults]valueForKey:@"postUrl"]];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"postUrl"];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"Instagram_Share"];
    }
    [UIView transitionWithView:self.sellStuffButtonView
                      duration:0.3
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                        self.sellStuffButtonView.alpha = 1.0 ;
                    }
                    completion:NULL];
    
    self.isScreenDidAppear = YES ;
    
}


/**
 Clear cache memory on warning.
 */
-(void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    SDImageCache *imageCache = [SDImageCache sharedImageCache];
    [imageCache clearMemory];
    [imageCache clearDiskOnCompletion:NULL];
}

/**
 Deallocate memory again.
 */
-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:mDeletePostNotifiName];
    [[NSNotificationCenter defaultCenter] removeObserver:mSellingAgainNotification];
    [[NSNotificationCenter defaultCenter] removeObserver:mUpdatePostDataNotification];
    [[NSNotificationCenter defaultCenter] removeObserver:@"openActivityScreen"];
    [[NSNotificationCenter defaultCenter] removeObserver:mSellingPostNotifiName];
    [[NSNotificationCenter defaultCenter] removeObserver:mShowAdsCampaign];
}

- (void) displayContentController: (BuyerSellerChatViewController *) content;
{
    //add as childViewController
    [self addChildViewController:content];
    [content didMoveToParentViewController:self];
    [content.view setFrame:CGRectMake(0, 0, 0, 0)];
    [self.view addSubview:content.view];
    [content removeFromParentViewController];
}

-(void)openActivityScreen{
    [self openActivityScreenForpush];
    
}

-(void)keyboardWillShow:(NSNotification *)noti {
//        let frame = (notification.userInfo![UIKeyboardFrameEndUserInfoKey] as! NSValue).CGRectValue()
    NSDictionary* keyboardInfo = [noti userInfo];
    NSValue* keyboardFrameBegin = [keyboardInfo valueForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardFrameBeginRect = [keyboardFrameBegin CGRectValue];
    keyBoardHeight = keyboardFrameBeginRect.size.height;
    keyBoardHeight -= 50;
}

#pragma mark -
#pragma mark - Notification Observer

-(void)notficationObservers
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deletePostFromNotification:) name:mDeletePostNotifiName object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(sellingPostAgain:) name:mSellingAgainNotification object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(UpdatePostOnEditing:) name:mUpdatePostDataNotification object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(refreshData:) name:mUpdatePromotedPost object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(addNewPost:)name:mSellingPostNotifiName object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(openActivityScreen)name:@"openActivityScreen" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(postNotificationToAppdelegate:)name:mShowAdsCampaign object:nil];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"Instagram_Share"];
}



#pragma mark
#pragma mark - Location Permission & Delegate -

-(void)checkPermissionForAllowLocation
{
    BOOL locationServiceEnable ;
    if ([CLLocationManager locationServicesEnabled]) {
        if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusNotDetermined) {
            locationServiceEnable = YES;
            AskPermissionViewController *askVC = [self.storyboard instantiateViewControllerWithIdentifier:@"AskPermissionStoryboardId"];
            askVC.permissionDelegate = self;
            askVC.locationPermission = YES ;
            askVC.locationEnable = locationServiceEnable ;
            [self.navigationController presentViewController:askVC animated:NO completion:nil];
        }
        else if([CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied)
        {
            locationServiceEnable = NO;
            AskPermissionViewController *askVC = [self.storyboard instantiateViewControllerWithIdentifier:@"AskPermissionStoryboardId"];
            askVC.permissionDelegate = self;
            askVC.locationPermission = YES ;
            askVC.locationEnable = locationServiceEnable ;
            [self.navigationController presentViewController:askVC animated:NO completion:nil];
        }
        else {
            [self allowPermission:YES];
        }
    }
    else {
        AskPermissionViewController *askVC = [self.storyboard instantiateViewControllerWithIdentifier:@"AskPermissionStoryboardId"];
        askVC.permissionDelegate = self;
        askVC.locationPermission = YES ;
        askVC.LocationPrivacy = YES ;
        [self.navigationController presentViewController:askVC animated:NO completion:nil];
    }
}


- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {

    if (status == kCLAuthorizationStatusAuthorizedWhenInUse)
    {
        [self allowPermission:YES];
        
    }
    else
    {
        [self allowPermission:NO];
    }
}

- (void)updatedLocation:(double)latitude and:(double)longitude
{
    self.latForFilter = latitude;
    self.longiForFilter = longitude;
    self.currentLat = latitude ;
    self.currentLong = longitude ;
    if(self.flagForLocation)
    {
        self.flagForLocation  = NO;
        [self requesForProductListings];
    }
}

- (void)updatedAddress:(NSString *)currentAddress
{
    [self updateLocationForUser];

}

#pragma mark -
#pragma mark - RFQuiltLayout Intialization -

/**
 This method will create an object for RFQuiltLayout.
 */
-(void)RFQuiltLayoutIntialization
{
    RFQuiltLayout* layout = [[RFQuiltLayout alloc]init];
    layout.direction = UICollectionViewScrollDirectionVertical;
    if(self.view.frame.size.width == 375)
    {
      layout.blockPixels = CGSizeMake( 24.7,31);
    }
    
    else if(self.view.frame.size.width == 414)
    {
        layout.blockPixels = CGSizeMake( 68,31);
    }
    else
    {
       layout.blockPixels = CGSizeMake(52.5, 31);
    }
    
    _collectionViewOutlet.collectionViewLayout = layout;
    layout.delegate=self;
    
}



#pragma mark
#pragma mark - Product Filter -

/**
 This is Delegate method of Filter VC.
 
 */
-(void)getFilteredItems:(NSMutableDictionary *)dicOfFilters miniPrice:(NSString *)min maxPrice:(NSString *)max curncyCode:(NSString *)currCode curncySymbol:(NSString *)currSymbol anddistance:(int)distnce
{
    minPrice = min;
    maxPrice = max;
    dist = distnce;
    currencyCode = currCode;
    currencySymbol = currSymbol;
    filterdict = dicOfFilters;
    arrayOfcategory = dicOfFilters[@"category"];
    [temp removeAllObjects];
    for (int i = 0 ;i <arrayOfcategory.count;i++) {
        NSString *typeOfFilter = @"category";
        NSString *valueForTheFilter = flStrForObj(arrayOfcategory[i]);
        NSMutableDictionary  *cbmc = [[NSMutableDictionary alloc] init];
        [cbmc setValue:typeOfFilter forKey:@"typeOfFilter"];
        [cbmc setValue:valueForTheFilter forKey:@"value"];
        [temp addObject:cbmc];
    }
    
    if ([dicOfFilters valueForKey:@"sortBy"]) {
        NSMutableDictionary  *cbmc = [[NSMutableDictionary alloc] init];
        [cbmc setValue:@"sortBy" forKey:@"typeOfFilter"];
        [cbmc setValue:dicOfFilters[@"sortBy"] forKey:@"value"];
        [temp addObject:cbmc];
        
        NSString *tempString = [dicOfFilters valueForKey:@"sortBy"];
        [self handleSortByString:tempString];
        
        
    }
    
    if ([dicOfFilters valueForKey:@"postedWithin"]) {
        NSMutableDictionary  *cbmc = [[NSMutableDictionary alloc] init];
        [cbmc setValue:@"postedWithin" forKey:@"typeOfFilter"];
        [cbmc setValue:dicOfFilters[@"postedWithin"] forKey:@"value"];
        [temp addObject:cbmc];
         NSString *tempString = [dicOfFilters valueForKey:@"postedWithin"];
        [self handleSortByString:tempString];

    }
    
    if ([dicOfFilters valueForKey:@"price"]) {
        NSMutableDictionary  *cbmc = [[NSMutableDictionary alloc] init];
        [cbmc setValue:@"price" forKey:@"typeOfFilter"];
        [cbmc setValue:dicOfFilters[@"price"] forKey:@"value"];
        [temp addObject:cbmc];
    }
    
    if ([dicOfFilters valueForKey:@"distance"]) {
        NSMutableDictionary  *cbmc = [[NSMutableDictionary alloc] init];
        [cbmc setValue:@"distance" forKey:@"typeOfFilter"];
        [cbmc setValue:dicOfFilters[@"distance"] forKey:@"value"];
        [temp addObject:cbmc];
    }
      self.leadingConstraint = [[dicOfFilters valueForKey:@"leadingConstraint"]integerValue];
    
    if([dicOfFilters valueForKey:@"locationName"]){
        NSMutableDictionary  *cbmc = [[NSMutableDictionary alloc] init];
        [cbmc setValue:@"locationName" forKey:@"typeOfFilter"];
        [cbmc setValue:dicOfFilters[@"locationName"] forKey:@"value"];
        [temp addObject:cbmc];
        self.locationName = [dicOfFilters valueForKey:@"locationName"];
        self.latForFilter = [[dicOfFilters valueForKey:@"lat"]doubleValue];
        self.longiForFilter = [[dicOfFilters valueForKey:@"long"]doubleValue];
    }
    
    [explorePostsresponseData removeAllObjects];
    [self reloadCollectionView] ;
    [avForCollectionView startAnimating];

    if(temp.count>0){
        self.FilterViewTopConstraint.constant=0;
        self.currentIndex = 0;
        self.paging = 1;
        self.cellDisplayedIndex = -1;
        [self applyFiltersOnProductsWithIndex:self.currentIndex];
    }
    else{
        [self requesForProductListings];
        self.FilterViewTopConstraint.constant = -FiltersViewHeight;
//        currencyCode = @"All";
//        currencySymbol = @"";
    }
    [_collectionviewFilterItem reloadData];
    

}


-(void)handleSortByString:(NSString *)checkString
{
    
    if([checkString containsString: NSLocalizedString(sortingByNewestFirst,sortingByNewestFirst ) ])
    {
        sortBy = @"postedOnDesc";
    }
    
    if([checkString containsString:NSLocalizedString(sortingByClosestFirst, sortingByClosestFirst) ])
    {
        sortBy  = @"distanceAsc";
    }
    
    if([checkString containsString:NSLocalizedString(sortingByPriceLowToHigh, sortingByPriceLowToHigh) ])
    {
        sortBy = @"priceAsc" ;
    }
    
    if([checkString containsString:NSLocalizedString(sortingByPriceHighToLow, sortingByPriceHighToLow) ])
    {
        sortBy = @"priceDsc" ;
    }
    
    if([checkString containsString:NSLocalizedString(postedWithLast24h, postedWithLast24h) ])
    {
        postedWithin = @"1" ;
    }
    if([checkString containsString:NSLocalizedString(postedWithLast7d, postedWithLast7d) ])
    {
        postedWithin = @"7" ;
    }
    
    if([checkString containsString:NSLocalizedString(postedWithLast30d,postedWithLast30d) ])
    {
        postedWithin = @"30" ;
    }
    
    
}


-(void)handleRemoveFilters:(NSString *)checkString
{
    if([checkString containsString:NSLocalizedString(sortingByNewestFirst,sortingByNewestFirst )])
    {
        sortBy = @"";
    }
    
    if([checkString containsString:NSLocalizedString(sortingByClosestFirst, sortingByClosestFirst)])
    {
        sortBy  = @"";
    }
    
    if([checkString containsString:NSLocalizedString(sortingByPriceLowToHigh, sortingByPriceLowToHigh)])
    {
        sortBy = @"" ;
    }
    
    if([checkString containsString:NSLocalizedString(sortingByPriceHighToLow, sortingByPriceHighToLow)])
    {
        sortBy = @"" ;
    }
    
    if([checkString containsString:NSLocalizedString(postedWithLast24h, postedWithLast24h)])
    {
        postedWithin = @"" ;
    }
    if([checkString containsString:NSLocalizedString(postedWithLast7d, postedWithLast7d)])
    {
        postedWithin = @"" ;
    }
    
    if([checkString containsString:NSLocalizedString(postedWithLast30d,postedWithLast30d)])
    {
        postedWithin = @"" ;
    }
    
    
}






#pragma mark -
#pragma mark - Apply Filters
/**
 Apply search on Products by applying selected Filters.
 */
-(void)applyFiltersOnProductsWithIndex :(NSInteger)checkIndex
{
    
    if(self.locationName.length < 1)
    {
        self.locationName = getLocation.currentCity;
        self.latForFilter = self.currentLat;
        self.longiForFilter = self.currentLong;
    }

//    if (dist == 0) {
//        dist = 8.00;
//    }
    
    categoryInStringFormat = [arrayOfcategory componentsJoinedByString:@","]; /** string with all categories**/
    
    NSDictionary *requestDic = @{mauthToken :flStrForObj([Helper userToken]),
                                 mlimit :@"20",
                                 moffset : flStrForObj([NSNumber numberWithInteger:checkIndex *20]),
                                 mSearchCategory  : flStrForObj(categoryInStringFormat),
                                 mSortby :   sortBy,
                                 mCurrency:  [currencyCode isEqualToString:@"All"] ? @"" : currencyCode,
                                 mMinPrice  : minPrice,
                                 mMaxPrice  : maxPrice,
                                 mPostedWithIn : postedWithin,
                                 mDistance  : [NSString stringWithFormat:@"%f",dist == 0?48:dist],
                                 mlocation  : flStrForObj(self.locationName),
                                 mlatitude  : [NSString stringWithFormat:@"%lf",self.latForFilter],
                                 mlongitude : [NSString stringWithFormat:@"%lf",self.longiForFilter],
                                 };
    
    [WebServiceHandler searchProductsByFilters:requestDic andDelegate:self];
}

#pragma mark -
#pragma mark - Remove Filters
/**
 Remove filter For Product.
 
 @param sender delete Filter button.
 */
- (IBAction)removeProductFilterButtonAcion:(id)sender {
    UIButton *btn=(UIButton *)sender;
    NSInteger tag= [btn tag]%100;
    NSString *key = temp[tag][@"typeOfFilter"];
    
    if([temp[tag][@"typeOfFilter"] isEqualToString:@"category"])
        [arrayOfcategory removeObjectAtIndex:tag];
    
    if([temp[tag][@"typeOfFilter"] isEqualToString:@"distance"])
    {
        dist = 0;
        self.leadingConstraint = 40;
    }
    
    if([temp[tag][@"typeOfFilter"] isEqualToString:@"price"])
    {
        minPrice = @"";
        maxPrice = @"";
        
    }
    if([temp[tag][@"typeOfFilter"] isEqualToString:@"sortBy"] || [temp[tag][@"typeOfFilter"] isEqualToString:@"postedWithin"])
    {
        [self handleRemoveFilters:temp[tag][@"value"]];
    }
    
    if([temp[tag][@"typeOfFilter"] isEqualToString:@"locationName"])
    {
    self.locationName = @"";
    self.latForFilter = 0;
    self.longiForFilter = 0;
    }


    
    if([filterdict.allKeys containsObject:key]) {
        [filterdict removeObjectForKey:key];
    }
    [temp removeObjectAtIndex:tag];
    
    [explorePostsresponseData removeAllObjects];
     [self reloadCollectionView] ;
    [avForCollectionView startAnimating];

    
    if (temp.count ==0) {
        [UIView animateWithDuration:0.4 animations:^{
            self.FilterViewTopConstraint.constant = -FiltersViewHeight;
            [self.view layoutIfNeeded];
        }];
        self.currentIndex = 0;
        self.paging = 1;
        self.cellDisplayedIndex = -1;
        [self requestForExplorePosts:self.currentIndex];
        currencyCode = @"All";
        currencySymbol = @"";
    }
    else{
        self.currentIndex = 0;
        self.paging = 1;
        self.cellDisplayedIndex = -1;
        [self applyFiltersOnProductsWithIndex:self.currentIndex];
    }
        [self.collectionviewFilterItem reloadData];
 
   }



#pragma mrk -
#pragma mark - Pull To refresh -

-(void)addingRefreshControl {
    refreshControl = [[UIRefreshControl alloc]init];
    [self.collectionViewOutlet addSubview:refreshControl];
    [refreshControl addTarget:self action:@selector(refreshData:) forControlEvents:UIControlEventValueChanged];
}
-(void)refreshData:(id)sender {
    [self.postTableViewHeight setConstant:0];
    [self.postTableView setHidden: YES];
//    self.txtSearchBar.text = @"";
    [self getnotificationCount];
    self.currentIndex = 0;
    self.paging = 1;
    self.cellDisplayedIndex = -1;
    if(temp.count==0){
       [self allowPermission:YES];
    }
    else{
        [self applyFiltersOnProductsWithIndex:self.currentIndex];
    }
}
- (void)stopAnimation {
    __weak HomeScreenController *weakSelf = self;
    int64_t delayInSeconds = 2.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [weakSelf.collectionViewOutlet.pullToRefreshView stopAnimating];
        [weakSelf.collectionViewOutlet.infiniteScrollingView stopAnimating];
    });
}

#pragma mark -
#pragma mark - tableview delegates -

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
//    if (self.isClickedOnTextField) {
//        return 1;
//    } else {
        if (recentSearchHistory.count > 0) {
            return 2;
        } else {
            return 1;
        }
 //   }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//    if (self.isClickedOnTextField) {
//        if (recentSearchHistory != (id)[NSNull null]) {
//            return recentSearchHistory.count;
//        } else {
//            return 0;
//        }
//    } else {
        if (section == 0) {
            if (_txtSearchBar.text.length>0)
            {
                return 0;
            }
            return topResponseData.count;
        } else {
            return recentSearchHistory.count;
        }
//    }
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
//    if (self.isClickedOnTextField) {
//        return @"Suggetions";
//    } else {
        if (section == 0) {
//            if(_txtSearchBar.text.length>0)
//                return @"Suggetions";
//            return @"Recent";

            return @"";
        } else {
            if (recentSearchHistory.count == 0) {
                return @"Recent";
            } else {
                return @"Suggetions";
            }
        }
//    }
    
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
//    if (self.isClickedOnTextField) {
//        UIView *view  = [UIView new];
//        [view setBackgroundColor:[UIColor clearColor]];
//        [view addSubview: self.tblHeaderView];
//        return view;
//    } else {
        if (section == 0) {
            UIView *view  = [UIView new];
            [view setBackgroundColor:[UIColor clearColor]];
            [view addSubview: self.tblHeaderView];
            if (_txtSearchBar.text.length>0)
            {
                _lblHeaderView.text = @"Suggetions";
                _btnEditHeader.hidden = true;
            }
            else
            {
                _lblHeaderView.text = @"Recent";
                _btnEditHeader.hidden = false;
            }
            
            return view;
        } else {
            UIView *view  = [UIView new];
            [view setBackgroundColor:[UIColor clearColor]];
            [view addSubview: self.tblHeaderView];
            if (_txtSearchBar.text.length>0)
            {
                _lblHeaderView.text = @"Suggetions";
            }
            else
            {
                _lblHeaderView.text = @"Recent";
            }
            
            return view;
        }
 //   }
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
//    if (self.isClickedOnTextField) {
//        return 40;
//    } else {
        if (section == 0) {
            if (_txtSearchBar.text.length > 0)
                return 0;
            else
                return 40;
        } else {
            return 40;
        }
  //  }
}

-(void)btnDeleteSearchClicked:(UIButton*)sender
{
    if (sender.tag == 0)
        [topResponseData removeObjectAtIndex:0];
    else
        [topResponseData removeObjectAtIndex:sender.tag];

    [[NSUserDefaults standardUserDefaults] setValue:topResponseData forKey:@"RecentSearch"];
    isEditClicked = true;
    [_postTableView reloadData];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    TopTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:mTopTableViewCellID  forIndexPath:indexPath];
    
    
    [cell.btnDeleteSearch addTarget:self action:@selector(btnDeleteSearchClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    if (_txtSearchBar.text.length==0 && isEditClicked == true)
    {
        cell.btnDeleteSearch.hidden = false;
        cell.btnDeleteSearch.enabled = true;
    }
    else
    {
        cell.btnDeleteSearch.hidden = true;
        cell.btnDeleteSearch.enabled = false;

    }
    
//    if (self.isClickedOnTextField) {
//        if (recentSearchHistory.count != 0) {
//            NSString *strHashTag = [recentSearchHistory objectAtIndex: indexPath.row];
//         //   strHashTag = [strHashTag stringByReplacingOccurrencesOfString:@"hashTag : " withString:@""];
//         //   [cell setSuggestionsName:flStrForObj(strHashTag)];
//            [cell setSuggestionsName:strHashTag];
//        }
//    } else {
        if (indexPath.section == 0) {
            if (indexPath.row == (topResponseData.count - 1)) {
                [cell.bottomline setHidden:YES];
            }
            cell.btnDeleteSearch.tag = (NSInteger)indexPath.row;
             [cell.btnDeleteSearch addTarget:self action:@selector(btnDeleteSearchClicked:) forControlEvents:UIControlEventTouchUpInside];
            [cell setSuggestionsName:flStrForObj(topResponseData[indexPath.row])];
        } else {
            if (recentSearchHistory.count != 0) {
                NSString *strHashTag = [recentSearchHistory objectAtIndex: indexPath.row];
               // strHashTag = [strHashTag stringByReplacingOccurrencesOfString:@"hashTag : " withString:@""];
              //  [cell setSuggestionsName:flStrForObj(strHashTag)];
                [cell setSuggestionsName:strHashTag];
            }
        }
//    }
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
  
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self.postTableViewHeight setConstant: 0];
    [self.postTableView setHidden: YES];
    [self.txtSearchBar resignFirstResponder];
    self.currentIndex = 0;
    self.paging = 1;
    self.cellDisplayedIndex = -1;
   
    NSMutableArray *tmpRecentArr = [[NSMutableArray alloc] init];
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"RecentSearch"] != nil)
    {
        tmpRecentArr = [[[NSUserDefaults standardUserDefaults] valueForKey:@"RecentSearch"] mutableCopy];
    }
    if (_txtSearchBar.text.length > 0)
        [tmpRecentArr addObject:[recentSearchHistory objectAtIndex: indexPath.row]];
    
    
    
    [[NSUserDefaults standardUserDefaults] setValue:tmpRecentArr forKey:@"RecentSearch"];
    NSString *keyword = @"";
    if (self.isClickedOnTextField) {
        if (recentSearchHistory.count != 0) {
            keyword = [recentSearchHistory objectAtIndex: indexPath.row];
        //    keyword = [keyword stringByReplacingOccurrencesOfString:@"hashTag : " withString:@""];
        }
    } else {
        if (indexPath.section == 0) {
            keyword = topResponseData[indexPath.row];
            dispatch_async(dispatch_get_main_queue(),^{
                [self addToRecentSearchwithKeyword:keyword];
            });
        } else {
            if (recentSearchHistory.count != 0) {
                keyword = [recentSearchHistory objectAtIndex: indexPath.row];
           //     keyword = [keyword stringByReplacingOccurrencesOfString:@"hashTag : " withString:@""];
            }
        }
    }
    
//    self.txtSearchBar.text = keyword;
    
    NSMutableArray *arrTmp = [[NSUserDefaults standardUserDefaults] objectForKey:@"recentSearch"];
    [arrTmp addObject:keyword];
    [[NSUserDefaults standardUserDefaults] setObject:arrTmp forKey:@"recentSearch"];
     [recentSearchHistory removeAllObjects];
    _txtSearchBar.text = @"";
   [self requestForPosts: keyword];
    
    _popUpHeader.hidden = true;

//    [self searchForKeyWord:flStrForObj(topResponseData[indexPath.row][@"productName"])];
}

#pragma mark -
#pragma mark - collectionview delegates -

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{

    if(collectionView==_collectionviewFilterItem)
    {
        return temp.count;
        
    }
    return  explorePostsresponseData.count;
    
}

-(void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row +5 == explorePostsresponseData.count && self.paging!= self.currentIndex && self.cellDisplayedIndex != indexPath.row) {
        self.cellDisplayedIndex = indexPath.row ;
        if(temp.count)
        {
        [self applyFiltersOnProductsWithIndex:self.currentIndex];
        }
        else
        {
        [self requestForExplorePosts:self.currentIndex];
        }
    }
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    if(collectionView==_collectionviewFilterItem)
    {
        CellForFilteredItem *cell=[collectionView dequeueReusableCellWithReuseIdentifier:mProductFiltersCellId forIndexPath:indexPath];
       [ cell.buttonToDisplayFilter setTitle: flStrForObj(temp[indexPath.row][@"value"]) forState:UIControlStateNormal];
        cell.deleteButton.tag = 100 + indexPath.row;
        return cell;
    }
    ListingCollectionViewCell  *collectionViewCell = [collectionView dequeueReusableCellWithReuseIdentifier:mSearchCollectioncellIdentifier forIndexPath:indexPath];
    if (collectionViewCell == nil) {
        collectionViewCell.postedImageOutlet = nil ;
    }
    else {
        dispatch_async(dispatch_get_main_queue(), ^{
            [collectionViewCell setImageForCell:self->explorePostsresponseData forIndexPath:indexPath];
            collectionViewCell.btnChat.layer.cornerRadius = collectionViewCell.btnChat.frame.size.height/2;
            collectionViewCell.btnChat.layer.borderColor = [UIColor colorWithRed:0.0/255.0 green:167.0/255.0 blue:157.0/255.0 alpha:1.0].CGColor;
            collectionViewCell.btnChat.layer.borderWidth = 1;
            collectionViewCell.btnChat.tag = indexPath.row;
            [collectionViewCell.btnChat addTarget:self action:@selector(btnOpenChat:) forControlEvents:UIControlEventTouchUpInside];
            collectionViewCell.productNameLabel.text = flStrForObj(self->explorePostsresponseData[indexPath.item][@"productName"]);
            collectionViewCell.productPriceLabel.text = [NSString stringWithFormat:@"%@ %@",flStrForObj(self->explorePostsresponseData[indexPath.item][@"currency"]),flStrForObj(self->explorePostsresponseData[indexPath.item][@"price"])] ;
            
            NSString *userName = [Helper userName];
            if([ self->explorePostsresponseData[indexPath.item][@"postedByUserName"] isEqualToString:userName] ||
               [ flStrForObj(self->explorePostsresponseData[indexPath.item][@"membername"]) isEqualToString:userName])
            {
                collectionViewCell.btnChat.hidden = YES;
            }
            else
            {
                collectionViewCell.btnChat.hidden = NO;
            }
        });
    }
    return collectionViewCell;
}
-(void)btnOpenChat:(UIButton*)sender{
    if ([[Helper userToken] isEqualToString:@"guestUser"]) {
        UINavigationController *navigationController = [CommonMethods presentLoginScreenController];
        [self presentViewController:navigationController animated:YES completion:nil];
    } else {
        [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"isFromHomeScreen"];
        MakeOfferViewController *controller = [[MakeOfferViewController alloc] init];
        [self.navigationController dismissViewControllerAnimated:true completion:nil];
        ProductDetails *product  =  [[ProductDetails alloc]initWithDictionary:explorePostsresponseData[sender.tag]];
        [controller createObjectForMakeOfferwithData:product withPrice:0 andType:@"0" withAPICall:NO];
    }
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CellForFilteredItem *cell=(CellForFilteredItem*)[collectionView cellForItemAtIndexPath:indexPath];
    UILabel *lbl=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width,cell.buttonToDisplayFilter.frame.size.height)];
    lbl.font=cell.buttonToDisplayFilter.titleLabel.font;
    if(temp.count)
    {
    lbl.text=flStrForObj(temp[indexPath.row][@"value"]);
    }
    CGFloat labelWidth = [CommonMethods measureWidthLabel:lbl];
    CGFloat cellWidth= labelWidth + 50;
    if(cellWidth >200){
        cellWidth = 200;
        return CGSizeMake(cellWidth,50 );
    }
    return CGSizeMake(cellWidth,50 );
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    [self.txtSearchBar resignFirstResponder];
//    [self.txtSearchBar setText:@""];
    [self.postTableViewHeight setConstant: 0];
    [recentSearchHistory removeAllObjects];
    [self.postTableView setHidden: YES];
    ListingCollectionViewCell *cell = (ListingCollectionViewCell *)[self.collectionViewOutlet cellForItemAtIndexPath:indexPath];
    if([collectionView isEqual:self.collectionViewOutlet]) {
        ProductDetailsViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:mInstaTableVcStoryBoardId];
        newView.postedByUserID = flStrForObj(explorePostsresponseData[indexPath.row][@"postedByUserNodeId"]);
        newView.indexPath = indexPath ;
        newView.productDelegate = self ;
        newView.hidesBottomBarWhenPushed = YES;
        newView.postId = flStrForObj(explorePostsresponseData[indexPath.row][@"postId"]);
        newView.dataFromHomeScreen = YES;
        newView.currentCity = getLocation.currentCity;
        newView.countryShortName = getLocation.countryShortCode ;
        newView.currentLattitude = [NSString stringWithFormat:@"%lf",self.currentLat];
        newView.currentLongitude = [NSString stringWithFormat:@"%lf",self.currentLong];
        newView.movetoRowNumber = indexPath.item;
        newView.imageFromHome = cell.postedImageOutlet.image;
        newView.product = [[ProductDetails alloc]initWithDictionary:explorePostsresponseData[indexPath.row]];
        [self.navigationController pushViewController:newView animated:YES];
    }
}
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(5,5,5,5);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section;
{
    return 0;
}

#pragma mark -
#pragma mark – RFQuiltLayoutDelegate -

-(CGSize) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout blockSizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    NSInteger widthTemp = (collectionView.frame.size.width-15)/3;
    NSInteger newHeight = ([flStrForObj(explorePostsresponseData[indexPath.row][@"containerHeight"]) integerValue] * widthTemp)/[flStrForObj(explorePostsresponseData[indexPath.row][@"containerWidth"]) integerValue];
    newHeight += 90;
    newHeight = newHeight/28;
    if IS_IPHONE_5{
        widthTemp = widthTemp/35;
    }else if (IS_IPHONE_6 || IS_IPHONE_X){
        widthTemp = widthTemp/22;
    }else if (IS_IPHONE_6P || IS_IPHONE_XS_MAX){
        widthTemp = widthTemp/45;
    }else{
        NSInteger width,width2;
        NSInteger height,height2;
        width = 2;
        height2 = [flStrForObj(explorePostsresponseData[indexPath.row][@"containerHeight"]) integerValue]/30;
        width2 =  [flStrForObj(explorePostsresponseData[indexPath.row][@"containerWidth"]) integerValue]/55;
        height2 += 15;
        height = (width * height2)/width2;
        if (height > 11)
        {
            height = 9 ;
        }
        return CGSizeMake(width,height);
    }
    return CGSizeMake(widthTemp,newHeight);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetsForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return UIEdgeInsetsMake (5,5,0,0);
}


#pragma mark -
#pragma mark - Request For Listings -

-(void)requesForProductListings
{
    self.currentIndex = 0;
    self.paging = 1;
    self.cellDisplayedIndex = -1;
    [self requestForExplorePosts:0];
}

-(void)requestForExplorePosts:(NSInteger )receivedindex {
    
    NSString *currentLattitudeParam,*currentLongitudeParam ;
    currentLattitudeParam = [NSString stringWithFormat:@"%lf",self.currentLat];
    currentLongitudeParam = [NSString stringWithFormat:@"%lf",self.currentLong];
    if(self.currentLat == 0)
    {
        currentLattitudeParam = @"";
        currentLongitudeParam = @"";
    }
    NSString *token = [[NSUserDefaults standardUserDefaults] objectForKey:mdeviceToken];
    if (token.length>0) {
        if ([[Helper userToken] isEqualToString:mGuestToken]) {
            NSDictionary *requestDict = @{
                                          mauthToken :flStrForObj([Helper userToken]),
                                          moffset:flStrForObj([NSNumber numberWithInteger:receivedindex * mPagingValue]),
                                          mlimit:flStrForObj([NSNumber numberWithInteger:mPagingValue]),
                                          mPushTokenKey : token,
                                          mlatitude : currentLattitudeParam,
                                          mlongitude : currentLongitudeParam
                                          };
            [WebServiceHandler getExplorePostsForGuest:requestDict andDelegate:self];
        }
        else{
            NSDictionary *requestDict = @{
                                          mauthToken :flStrForObj([Helper userToken]),
                                          moffset:flStrForObj([NSNumber numberWithInteger:receivedindex* mPagingValue]),
                                          mlimit:flStrForObj([NSNumber numberWithInteger: mPagingValue]),
                                          mlatitude : currentLattitudeParam,
                                          mlongitude: currentLongitudeParam,
                                          mPushTokenKey : token
                                          };
            [WebServiceHandler getExplorePosts:requestDict andDelegate:self];
        }

    } else{
    
    if ([[Helper userToken] isEqualToString:mGuestToken]) {
        NSDictionary *requestDict = @{
                                      mauthToken :flStrForObj([Helper userToken]),
                                      moffset:flStrForObj([NSNumber numberWithInteger:receivedindex * mPagingValue]),
                                      mlimit:flStrForObj([NSNumber numberWithInteger:mPagingValue]),
                                      mlatitude : currentLattitudeParam,
                                      mlongitude : currentLongitudeParam
                                      };
        [WebServiceHandler getExplorePostsForGuest:requestDict andDelegate:self];
    }
    else{
        NSDictionary *requestDict = @{
                                      mauthToken :flStrForObj([Helper userToken]),
                                      moffset:flStrForObj([NSNumber numberWithInteger:receivedindex* mPagingValue]),
                                      mlimit:flStrForObj([NSNumber numberWithInteger: mPagingValue]),
                                      mlatitude : currentLattitudeParam,
                                      mlongitude: currentLongitudeParam
                                      };
        [WebServiceHandler getExplorePosts:requestDict andDelegate:self];
    }
    }
}

-(void)requestForPosts:(NSString *)searchText {
    
  //  ProgressIndicator *HomePI = [ProgressIndicator sharedInstance];  [HomePI showPIOnView:self.view withMessage:@"Loading..."];
    
    [explorePostsresponseData removeAllObjects];
    for (int i=0;i<arrProductOriginal.count;i++)
    {
        
        NSArray *tmp = [arrProductOriginal objectAtIndex:i];
        if ([[tmp valueForKey:@"productName"] isEqualToString:searchText ] || [[tmp valueForKey:@"category"] isEqualToString:searchText])
        {
            [explorePostsresponseData addObject:tmp];
            
        }
    }
     [self reloadCollectionView] ;
    
    //explorePostsresponseData
  //  [WebServiceHandler getSearchForPosts:requestDict andDelegate:self];
}

-(void)requestForPostsAutoFill:(NSString *)searchText {
    
 //   [[NSUserDefaults standardUserDefaults] setObject:response[@"data"] forKey:mKeyForSavingCategoryList];
    NSArray *arrCatagory = [[NSUserDefaults standardUserDefaults] objectForKey:mKeyForSavingCategoryList];
    
        [recentSearchHistory removeAllObjects];
    
    recentSearchHistory = [NSMutableArray new];
    
    NSString *str;
    for (int i=0;i<arrCatagory.count;i++)
    {
        
        str = [[arrCatagory objectAtIndex:i] valueForKey:@"name"];
        if ([str containsString:searchText])
        {
            [recentSearchHistory addObject:str];
        }
    }
    
    for (int i=0;i<arrProductOriginal.count;i++)
    {
        //productName
        str = [[arrProductOriginal objectAtIndex:i] valueForKey:@"productName"];
        if ([str containsString:searchText])
        {
            [recentSearchHistory addObject:str];
        }
    }
    
    [_postTableView reloadData];
    
  //  recentSearchHistory
//    [WebServiceHandler getKeyWordsSuggestionPostSearch:requestDict andDelegate:self];
}

-(void)requestForGetRecentSearch  {
    if (![[Helper userToken] isEqualToString:@"guestUser"]) {
        NSDictionary *requestDict = @{
                                      mauthToken :flStrForObj([Helper userToken]),
                                      mUserName :flStrForObj([Helper userName]),
                                      };
        [WebServiceHandler getUserSearchHistory:requestDict andDelegate:self];
    }
}

-(void)addToRecentSearchwithKeyword:(NSString *)keyword {
    if (![[Helper userToken] isEqualToString:@"guestUser"]) {
        NSDictionary *requestDict = @{
                                      maddToSearchKey :flStrForObj(keyword),
                                      maddToSearchType :flStrForObj(@"1"),
                                      mUserName :flStrForObj([Helper userName]),
                                      mauthToken :flStrForObj([Helper userToken]),
                                      };
        [WebServiceHandler addTosearchHistory:requestDict andDelegate:self];
    }
}


#pragma mark -
#pragma mark - WebServiceDelegate -



-(void)internetIsNotAvailable:(RequestType)requsetType {
    [avForCollectionView stopAnimating];
    [refreshControl endRefreshing];
    if(!explorePostsresponseData.count)
    {
        self.collectionViewOutlet.backgroundView = [Helper showMessageForNoInternet:YES forView:self.view];
    }
    
}

- (void) didFinishLoadingRequest:(RequestType)requestType withResponse:(id)response error:(NSError*)error {
    
    [[ProgressIndicator sharedInstance] hideProgressIndicator];
    [Helper showMessageForNoInternet:NO forView:self.view];
    
    if (error) {
        NSLog(@"requestType is :%lu",(unsigned long)requestType);
        [Helper showAlertWithTitle:NSLocalizedString(alertError, alertError) Message:NSLocalizedString(mCommonServerErrorMessage , mCommonServerErrorMessage) viewController:self];
        [avForCollectionView stopAnimating];
        [refreshControl endRefreshing];
        return;
    }
    else
    {
        [avForCollectionView stopAnimating];
        [refreshControl endRefreshing];
    }
    
    NSDictionary *responseDict = (NSDictionary*)response;
    
    if (requestType == RequestTypePostSearchKeywords ) {
        switch ([responseDict[@"code"] integerValue]) {
            case 200: {
                self.isClickedOnTextField  = NO;
                [self handlingPostsResponseData:responseDict];
            }
                break;
            case 19031:
            case 19032: {
                [self errorAlert:responseDict[@"message"]];
            }
        }
    }
    
    if (requestType == RequestTypeGetSearchForPosts ) {
        switch ([responseDict[@"code"] integerValue]) {
            case 200: {
                [self handlingResponseOfExplorePosts:response];
            }
                break;
            case 204 :
            {
                [explorePostsresponseData removeAllObjects];
                [self reloadCollectionView] ;
                if(!explorePostsresponseData.count)
                {
                    [self showingMessageForCollectionViewBackgroundForType:1];
                }
            }
                break;
        }
    }
    
    if (requestType == RequestTypegetUserSearchHistory) {
        switch ([responseDict[@"code"] integerValue]) {
            case 200: {
                NSLog(@"searched history is :%@",responseDict);
                recentSearchHistory = [[NSMutableArray alloc] init];
                if ([[responseDict[@"data"] valueForKey: @"searchHistory"] objectAtIndex:0] != nil) {
                    recentSearchHistory = [[responseDict[@"data"] valueForKey: @"searchHistory"] objectAtIndex:0];
                    NSLog(@"searched history array is :%@",recentSearchHistory);
                }
                [self.postTableView reloadData];
            }
                break;
        }
    }
    
    if (requestType == RequestTypeAddToSearchHistory) {
        switch ([responseDict[@"code"] integerValue]) {
            case 200: {
              NSLog(@"added key to searched history is :%@",responseDict);
            }
                break;
        }
    }
    
    switch (requestType) {
        case RequestTypeGetExploreposts:
        {
            [avForCollectionView stopAnimating];
            switch ([responseDict[@"code"] integerValue]) {
                case 200: {
                    
                    [self handlingResponseOfExplorePosts:response];
                }
                    break;
                case 204: {
                    if(!explorePostsresponseData.count){
                        [self showingMessageForCollectionViewBackgroundForType:0];
                    }else{
                        if (self.currentIndex == 0){
                            [explorePostsresponseData removeAllObjects];
                            [arrProductOriginal removeAllObjects];
                            [self reloadCollectionView] ;
                            [self showingMessageForCollectionViewBackgroundForType:0];
                        }
                    }
                }
                    break;
            }
        }
            break;
        case RequestTypeunseenNotificationCount:
        {
            [avForCollectionView stopAnimating];
            switch ([responseDict[@"code"] integerValue]) {
                case 200: {
                    NSString *unseenCount = flStrForObj(response[@"data"]);
                    [[NSUserDefaults standardUserDefaults] setInteger:[unseenCount integerValue] forKey:@"actbadge"];
                    if ([unseenCount isEqualToString:@"0"]) {
                        self.labelNotificationsCount.hidden = YES;
                        NSInteger badge = [[NSUserDefaults standardUserDefaults] integerForKey:@"badge"];
                        badge = badge - [self.labelNotificationsCount.text integerValue];
                        [[NSUserDefaults standardUserDefaults] setInteger:badge forKey:@"badge"];
                        [[UIApplication sharedApplication] setApplicationIconBadgeNumber:badge];
                    }
                    else
                    {
                        self.labelNotificationsCount.hidden = NO;
                        NSInteger badge = [[NSUserDefaults standardUserDefaults] integerForKey:@"badge"];
                        badge = badge - [self.labelNotificationsCount.text integerValue];
                        self.labelNotificationsCount.text = unseenCount ;
                        badge = badge + [unseenCount integerValue];
                        [[NSUserDefaults standardUserDefaults] setInteger:badge forKey:@"badge"];
                        [[UIApplication sharedApplication] setApplicationIconBadgeNumber:badge];
                    }
                }
                    break;
                    
            }
        }
            break ;
        
        case RequestTypeSearchProductsByFilters:
        {
            [[ProgressIndicator sharedInstance] hideProgressIndicator];
            switch ([responseDict[@"code"] integerValue]) {
                case 200: {
                    [self handlingResponseOfExplorePosts:response];
                }
                    break;
                case 204:{
                    if(self.cellDisplayedIndex == -1)
                    {
                        [explorePostsresponseData removeAllObjects];
                         [self reloadCollectionView] ;
                        [self showingMessageForCollectionViewBackgroundForType:1];
                    }
                }
                    break;
            }
            
        }
            break ;
            
        case RequestTypeGetCategories:
        {
            switch ([responseDict[@"code"] integerValue]) {
                case 200: {
                    NSDictionary *categoryData = [[NSDictionary alloc]init];
                    categoryData = response[@"data"];
                    arrayOfCategoryImagesUrls = [[NSMutableArray alloc]init];
                    
                    for(NSDictionary *dic in categoryData)
                    {
                        [arrayOfCategoryImagesUrls addObject:dic[@"activeimage"]];
                        [arrayOfCategoryImagesUrls addObject:dic[@"deactiveimage"]];
                    }
                    [[NSUserDefaults standardUserDefaults] setObject:response[@"data"] forKey:mKeyForSavingCategoryList];
                }
                    break;
                default:
                    break;
            }
        }
            break ;
        default:
            break;
    }
}



#pragma mark -
#pragma mark - Handling Response -

-(void)handlingPostsResponseData:(NSDictionary *)postsData {
    if (postsData) {
        topResponseData =[[NSMutableArray alloc] init];
        recentSearchHistory = [[NSMutableArray alloc] init];
        topResponseData = postsData[@"data"];
        if (![[Helper userToken] isEqualToString:@"guestUser"]) {
            recentSearchHistory = postsData[@"searchHistoryData"];
        }
        
        [self.postTableView reloadData];
    }
    else {
        topResponseData = nil;
        [self.postTableView reloadData];
    }
}

- (void)errorAlert:(NSString *)message {
    //alert for failure response.
    UIAlertController *controller = [CommonMethods showAlertWithTitle:@"Message" message:message actionTitle:@"Ok"];
    [self presentViewController:controller animated:YES completion:nil];
}

-(void)
handlingResponseOfExplorePosts:(NSMutableDictionary *)receivedData {
    self.collectionViewOutlet.backgroundView = nil;
    [refreshControl endRefreshing];
    if(self.currentIndex == 0) {
        [explorePostsresponseData removeAllObjects];
        [arrProductOriginal removeAllObjects];
        [explorePostsresponseData  addObjectsFromArray:receivedData[@"data"]];
        [arrProductOriginal addObjectsFromArray:explorePostsresponseData];
    }
    else {
        [explorePostsresponseData  addObjectsFromArray:receivedData[@"data"]];
        [arrProductOriginal addObjectsFromArray:explorePostsresponseData];
    }
    self.currentIndex ++;
    self.paging ++;
    [self stopAnimation];
     [self reloadCollectionView] ;
}


-(void)addingActivityIndicatorToCollectionViewBackGround
{
    avForCollectionView = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    avForCollectionView.frame =CGRectMake(self.view.frame.size.width/2 -12.5, self.view.frame.size.height/2 - 100, 25,25);
    avForCollectionView.tag  = 1;
    [self.collectionViewOutlet addSubview:avForCollectionView];
    [avForCollectionView startAnimating];
}


#pragma mark -
#pragma mark - Button Actions -


- (IBAction)sellStuffButton:(id)sender{
    if([[Helper userToken]isEqualToString:mGuestToken])
    {
        UINavigationController *navigationController = [CommonMethods presentLoginScreenController];
        [self presentViewController:navigationController animated:YES completion:nil];
        
    }
    else{
        
        [self openCameraScreen];
    }
}

- (IBAction)searchButtonAction:(id)sender {
    
    
    SearchPostsViewController *searchVC = [self.storyboard instantiateViewControllerWithIdentifier:mSearchPostsID];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:searchVC];
    [self.navigationController presentViewController:navigationController  animated:YES completion:nil];
}

- (IBAction)notificationButtonACtion:(id)sender {
    if([[Helper userToken] isEqualToString:@"guestUser"]){
        UINavigationController *nav = [CommonMethods presentLoginScreenController];
        [self.navigationController presentViewController:nav animated:YES completion:nil];
    }
    else{
        ActivityViewController *activityVC  = [self.storyboard instantiateViewControllerWithIdentifier:@"ActivityControllerId"];
        activityVC.callBackForStartSelling = ^(BOOL isStartSelling) {
            if(isStartSelling)
            {
                [self openCameraScreen];
            }
        };
        UINavigationController *nav  = [[UINavigationController alloc]initWithRootViewController:activityVC];
        [self.navigationController presentViewController:nav animated:YES completion:nil];
    }
}

-(void)openActivityScreenForpush{
    if([[Helper userToken] isEqualToString:@"guestUser"]){
        
    }
    else{
        ActivityViewController *activityVC  = [self.storyboard instantiateViewControllerWithIdentifier:@"ActivityControllerId"];
        activityVC.showOwnActivity = YES ;
        UINavigationController *nav  = [[UINavigationController alloc]initWithRootViewController:activityVC];
        [self.navigationController presentViewController:nav animated:YES completion:nil];
    }
}

- (IBAction)filterButtonAction:(id)sender {
    SDWebImagePrefetcher *prefetcher = [SDWebImagePrefetcher sharedImagePrefetcher];
    [prefetcher prefetchURLs:arrayOfCategoryImagesUrls progress:nil completed:^(NSUInteger completedNo, NSUInteger skippedNo) {
    }];
    FilterViewController *Fvc = [self.storyboard instantiateViewControllerWithIdentifier:@"FilterItemsStoryboardID"];
    Fvc.hidesBottomBarWhenPushed = YES;
    Fvc.delegate=self;
    Fvc.checkArrayOfFilters = YES;
    Fvc.minPrce = minPrice;
    Fvc.maxPrce = maxPrice;
    Fvc.distnce = dist;
    Fvc.arrayOfSelectedFilters = temp;
    Fvc.lattitude = self.latForFilter;
    Fvc.longitude = self.longiForFilter;
    Fvc.currencyCode = currencyCode ;
    Fvc.currencySymbol = currencySymbol;
    Fvc.leadingConstraint = self.leadingConstraint ;
    UINavigationController *navigation = [[UINavigationController alloc] initWithRootViewController:Fvc];
    [self.navigationController presentViewController:navigation animated:YES completion:nil];
}

- (IBAction)btnBrowseAction:(id)sender {
    [self.txtSearchBar resignFirstResponder];
    [self.postTableViewHeight setConstant: 0];
    [self.postTableView setHidden:YES];
    if (![self.txtSearchBar.text isEqualToString:@""]) {
        self.currentIndex = 0;
        self.paging = 1;
        self.cellDisplayedIndex = -1;
        [self requestForPosts:self.txtSearchBar.text];
        dispatch_async(dispatch_get_main_queue(),^{
            [self addToRecentSearchwithKeyword: self.txtSearchBar.text];
        });
    }
}

- (IBAction)btnInviteAction:(id)sender {
    [self share];
}

-(void) share {
    NSString* shareText = @"Lagel - Achetez et vendez by Stopoint inc ";
    NSURL *website = [NSURL URLWithString:@"https://itunes.apple.com/in/app/lagel-achetez-et-vendez/id1344853218?mt=8"];
    NSArray *shareArray = @[shareText, website];
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:shareArray applicationActivities:nil];
    
    [activityVC setValue:@"Someone shared an eddress with you" forKey:@"subject"];
    
    NSArray *excludeActivities = @[UIActivityTypeAirDrop,
                                   UIActivityTypePrint,
                                   UIActivityTypeAssignToContact,
                                   UIActivityTypeSaveToCameraRoll,
                                   UIActivityTypeAddToReadingList,
                                   UIActivityTypePostToFlickr,
                                   UIActivityTypePostToVimeo];
    
    activityVC.excludedActivityTypes = excludeActivities;
    
    [self.navigationController presentViewController:activityVC animated:YES completion:nil];
}

#pragma mark -
#pragma mark - UITextField Delegates -

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self.txtSearchBar resignFirstResponder];
    NSMutableDictionary *dictTmp = [[NSMutableDictionary alloc]init];
    [recentSearchHistory removeAllObjects];

    NSMutableArray *tmpRecentArr = [[NSMutableArray alloc] init];
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"RecentSearch"] != nil)
    {
        tmpRecentArr = [[[NSUserDefaults standardUserDefaults] valueForKey:@"RecentSearch"] mutableCopy];
    }
    
    [tmpRecentArr addObject:_txtSearchBar.text];
    [[NSUserDefaults standardUserDefaults] setValue:tmpRecentArr forKey:@"RecentSearch"];
    
    
    [dictTmp setValue:_txtSearchBar.text forKey:@"value"];
    NSMutableArray *arrTmp = [[NSMutableArray alloc]init];
    if ([filterdict valueForKey:@"category" ] !=nil)
    {
        arrTmp = [filterdict valueForKey:@"category" ];
    }
    [arrTmp addObject:_txtSearchBar.text];
    [filterdict setValue: arrTmp forKey:@"category"];


 //   [temp removeAllObjects];
    
    _popUpHeader.hidden = true;
    [self getFilteredItems:filterdict miniPrice:minPrice maxPrice:maxPrice curncyCode:currencyCode curncySymbol:currencySymbol anddistance:dist];


    
        self.currentIndex = 0;
        self.paging = 1;
        self.cellDisplayedIndex = -1;


    [self.postTableViewHeight setConstant: 0];

    [self.postTableView setHidden:YES];

    _txtSearchBar.text=@"";
    return true;
}

#pragma mark -
#pragma mark - UITextField Action -

-(void)textFieldDidEndEditing:(UITextField *)textField{
    UIImageView *img = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"search_ic"]];
    img.contentMode = UIViewContentModeScaleAspectFit;
    _txtSearchBar.leftView = img;
    _txtSearchBar.leftViewMode = UITextFieldViewModeUnlessEditing;
}

- (IBAction)textDidChange:(id)sender {
    NSLog(@"DidChange Called");
    
    
    if (_txtSearchBar.text.length > 0)
    {
        self.btnEditHeader.hidden = true;
        self.popUpHeader.hidden = true;
    }
    else
    {
        self.btnEditHeader.hidden = false;
        self.popUpHeader.hidden = true;
    }
    NSString *encodedString =  [self.txtSearchBar.text stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
    [self.postTableView setHidden:NO];
    [self.postTableViewHeight setConstant: (self.view.frame.size.height - self.postTableView.frame.origin.y) - keyBoardHeight];
    if (![self.txtSearchBar.text isEqualToString:@""]) {
        [self requestForPostsAutoFill:encodedString];
        
        UIButton *btnTmp = (UIButton *)self.txtSearchBar.rightView;
        [btnTmp setImage:[UIImage imageNamed:@"close_button"] forState:UIControlStateNormal];

//        for (int index=0;index<topResponseData.count;index++) {
//            [topResponseData removeObjectAtIndex:index];
//        }
        
    }
    else {
        [recentSearchHistory removeAllObjects];
        
        
        
        if ([[NSUserDefaults standardUserDefaults] valueForKey:@"RecentSearch"] != nil)
        {
            topResponseData = [[[NSUserDefaults standardUserDefaults] valueForKey:@"RecentSearch"] mutableCopy];
        }
        
        [self.postTableView reloadData];
        UIButton *btnTmp = (UIButton *)self.txtSearchBar.rightView;

        [btnTmp setImage:[UIImage imageNamed:@"navigationBackButton.png"] forState:UIControlStateNormal];
    }

}

- (IBAction)textFieldTouchInsideAction:(id)sender {
    NSLog(@"TouchupInside Called");
    
    UIButton *btnBack = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnBack addTarget:self
                action:@selector(btnBackClicked:)
      forControlEvents:UIControlEventTouchUpInside];
    [btnBack setImage:[UIImage imageNamed:@"navigationBackButton.png"] forState:UIControlStateNormal];
    btnBack.frame = CGRectMake((_txtSearchBar.frame.size.width - 25), 5.0, 25.0, _txtSearchBar.frame.size.height);
    _txtSearchBar.leftView = btnBack;
    _txtSearchBar.leftViewMode = UITextFieldViewModeWhileEditing;

    
    isEditClicked = false;
    if (recentSearchHistory.class != [NSNull class])
    {
    [recentSearchHistory removeAllObjects];
    }
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"RecentSearch"] != nil)
    {
        topResponseData = [[[NSUserDefaults standardUserDefaults] valueForKey:@"RecentSearch"] mutableCopy];
    }
    
    [self.postTableView reloadData];
        [self.postTableView reloadData];
    [self.postTableView setHidden:NO];
    [self.postTableViewHeight setConstant: (self.view.frame.size.height - self.postTableView.frame.origin.y) - keyBoardHeight];
    if (![[Helper userToken] isEqualToString:@"guestUser"]) {
       self.isClickedOnTextField = YES;
    }
  //  [self requestForGetRecentSearch];
}


#pragma mark -
#pragma mark - No Posts View

-(void)showingMessageForCollectionViewBackgroundForType :(NSInteger )type{
    
    if(type == 0)
    {
        self.emptyFilterTextLabel.text = NSLocalizedString(noPostsNearYouText, noPostsNearYouText) ;
    }
    else if(type == 1)
    {
        self.emptyFilterTextLabel.text = NSLocalizedString(noPostsForAppliedFilter, noPostsForAppliedFilter) ;
    }
    UIView *backGroundViewForNoPost = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.collectionViewOutlet.frame.size.width,self.collectionViewOutlet.frame.size.height)];
    self.emtyFiltersView.frame = backGroundViewForNoPost.frame ;
    [backGroundViewForNoPost addSubview:self.emtyFiltersView];
    self.collectionViewOutlet.backgroundView = backGroundViewForNoPost;
}

#pragma mark -
#pragma mark - ZoomTransitionProtocol

-(UIView *)viewForZoomTransition:(BOOL)isSource
{
    NSIndexPath *selectedIndexPath = [[self.collectionViewOutlet indexPathsForSelectedItems] firstObject];
    ListingCollectionViewCell *cell = (ListingCollectionViewCell *)[self.collectionViewOutlet cellForItemAtIndexPath:selectedIndexPath];
    return cell.postedImageOutlet;
}

#pragma mark -
#pragma mark - Welcome Screen

-(void)showWelcomeMessageForBrowsing
{
    if([[NSUserDefaults standardUserDefaults] boolForKey:mSignupFirstTime]){
        
        [[NSUserDefaults standardUserDefaults]setBool:NO forKey:mSignupFirstTime];
        [[NSUserDefaults standardUserDefaults]synchronize];
    self.tabBarController.tabBar.userInteractionEnabled = NO;
    StartBrowsingViewController *browsingVC = [self.storyboard instantiateViewControllerWithIdentifier:@"StartBrowsingStoryboardId"];
    self.modalPresentationStyle = UIModalPresentationCurrentContext;
    browsingVC.callbackToActivateTab = ^(BOOL activate){
        self.tabBarController.tabBar.userInteractionEnabled = activate;
    };
    [self presentViewController:browsingVC animated:YES completion:nil];
    }
    else {
        self.tabBarController.tabBar.userInteractionEnabled = YES;

    }

}

#pragma mark -
#pragma mark - Tab Bar animation -

/* pass a param to describe the state change, an animated flag and a completion block matching UIView animations completion*/
- (void)setTabBarVisible:(BOOL)visible animated:(BOOL)animated completion:(void (^)(BOOL))completion {
      if ([self tabBarIsVisible] == visible) return (completion)? completion(YES) : nil;
    // bail if the current state matches the desired state

    // get a frame calculation ready
    CGRect frame = self.tabBarController.tabBar.frame;
    CGFloat height = frame.size.height;
    CGFloat offsetY = (visible)? -height : height;
    
    // zero duration means no animation
    CGFloat duration = (animated)? 0.3 : 0.0;
    
    [UIView animateWithDuration:duration animations:^{
        self.tabBarController.tabBar.frame = CGRectOffset(frame, 0, offsetY);
    } completion:completion];
}

//Getter to know the current state
- (BOOL)tabBarIsVisible {

    return self.tabBarController.tabBar.frame.origin.y < CGRectGetMaxY(self.view.frame);
}

#pragma mark -
#pragma mark - ScrollView Delegate -

-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    scrollView = self.collectionViewOutlet;
    _currentOffset = self.collectionViewOutlet.contentOffset.y;
    lastScrollContentOffset = scrollView.contentOffset ;
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (self.isScreenDidAppear)
    {
    dispatch_async(dispatch_get_main_queue(),^{
    if (self->lastScrollContentOffset.y > scrollView.contentOffset.y) {
      // scroll up
         self.bottomConstraintOfSellButton.constant = 60;
        [self setTabBarVisible:YES animated:YES completion:nil];
        [[self navigationController] setNavigationBarHidden:NO animated:YES];
        AppDelegate *delegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
        [delegate setStatusBarBackgroundColor:[UIColor clearColor]];
    }
    
    else if (self->lastScrollContentOffset.y < scrollView.contentOffset.y) {
       // sroll down
        self.bottomConstraintOfSellButton.constant = 15;
        [self setTabBarVisible:NO animated:YES completion:nil];
        [[self navigationController] setNavigationBarHidden:YES animated:YES];
        AppDelegate *delegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
        [delegate setStatusBarBackgroundColor:[UIColor whiteColor]];
    }
    });
    }
 
}


#pragma mark -
#pragma mark - Notification Methods -

-(void)sellingPostAgain:(NSNotification *)noti
{
    
    
}

#pragma mark -
#pragma mark - Product Edited Notification -

-(void)UpdatePostOnEditing:(NSNotification *)noti
{
    [self requesForProductListings];
}

#pragma mark -
#pragma mark - Product Removed Notification -

-(void)deletePostFromNotification:(NSNotification *)noti {
    NSString *updatepostId = flStrForObj(noti.object[@"data"][@"postId"]);
    for (int i=0; i <explorePostsresponseData.count;i++) {
        
        if ([flStrForObj(explorePostsresponseData[i][@"postId"]) isEqualToString:updatepostId])
        {
            [explorePostsresponseData removeObjectAtIndex:i];
            [self reloadCollectionView] ;
            break;
        }
    }
}


#pragma mark -
#pragma mark - Product Added Notification -

-(void)addNewPost :(NSNotification *)noti
{
    NSMutableArray *newPost = (NSMutableArray *)noti.object[0];
    [explorePostsresponseData insertObject:newPost atIndex:0];
    
    self.collectionViewOutlet.backgroundView = nil ;
     [self reloadCollectionView] ;
    
}

#pragma mark -
#pragma mark - Ads Campaign Notification


/**
 Notify the method in App delegate to open the ads campaign
 
 @param noti details of campaign in NSdictionary format.
 */
-(void)postNotificationToAppdelegate:(NSNotification *)noti
{
    
    [[NSNotificationCenter defaultCenter] postNotificationName:mTriggerCampaign object:noti.object];
}

#pragma mark -
#pragma mark - Check Permission For Location -


/**
 This method is invoked after checking the permission for location services.
 On the basis of location services further server is requested for Listings.
 @param value Bool Value.
 */
-(void)allowPermission:(BOOL)value
{
    if(value)
    {
        //To get the current location
        getLocation = [GetCurrentLocation sharedInstance];
        [getLocation getLocation];
        getLocation.delegate = self;
        self.flagForLocation = YES;
    }
    else
    {
        [self requesForProductListings];
    }
    
}

#pragma mark-
#pragma mark - ProductDetails Delegate -


/**
 If product is removed.This delegate method get invoked.

 @param indexpath indexpath row.
 */
-(void)productIsRemovedForIndex:(NSIndexPath *)indexpath
{
    [explorePostsresponseData removeObjectAtIndex:indexpath.row];
     [self reloadCollectionView] ;
}

#pragma mark-
#pragma mark -  Share On Instagram -


/**
 Once Product is successfully posted.This method get invoked to ask the permission to share on instagram.
 @param postUrl url to share.
 */

-(void)ShareOnInstagram:(NSString *)postUrl
{
    [Helper shareOnInstagram:postUrl viewController:self] ;
    
}

#pragma mark -
#pragma mark - Pending Notifications -

-(void)getnotificationCount
{
    if(![[Helper userToken] isEqualToString:mGuestToken]) {
        NSDictionary *requestDict = @{
                                      @"token":[Helper userToken]
                                      };
        [WebServiceHandler getUsernNotificationCount:requestDict andDelegate:self];
    }
}


#pragma mark -
#pragma mark - Get Categories -

/**
 Prefectch The Category List From  Server for Product Filters.
 */
-(void)getCategoriesListFromServer
{
NSDictionary *requestDict = @{
                              mLimit : @"50",
                              moffset :@"0",
                              mlanguage : [LanguageManager currentLanguageCode]
                              };
[WebServiceHandler getCategories:requestDict andDelegate:self];
}

#pragma mark -
#pragma mark - reload CollectionView -


/**
 Reload collectionView without Animation.
 */
-(void)reloadCollectionView{
    [UIView animateWithDuration:0 animations:^{
            [self.collectionViewOutlet reloadData];
    }];
}


#pragma mark -
#pragma mark - Camera Screen -

-(void)openCameraScreen
{
    CameraViewController *cameraVC=[self.storyboard instantiateViewControllerWithIdentifier:@"CameraStoryBoardID"];
    cameraVC.sellProduct = TRUE;
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:cameraVC];
    [self presentViewController:nav animated:NO completion:nil];
}


#pragma mark -
#pragma mark - Update Location -

/**
 Update User Location.
 */
-(void)updateLocationForUser
{
    [[GetCurrentLocation sharedInstance] setLat:[[GetCurrentLocation sharedInstance] lastLatLong].latitude andLong:[[GetCurrentLocation sharedInstance] lastLatLong].longitude];
    
    if(![[Helper userToken] isEqualToString:mGuestToken]) // Only for user.
    {
        NSString *updatedlattitude = [NSString stringWithFormat:@"%lf", [[GetCurrentLocation sharedInstance] lastLatLong].latitude];
        NSString *updatedLongitude = [NSString stringWithFormat:@"%lf", [[GetCurrentLocation sharedInstance] lastLatLong].longitude];
    NSDictionary *requestDict = @{
                                  mauthToken :flStrForObj([Helper userToken]),
                                  mlatitude:flStrForObj(updatedlattitude) ,
                                  mlongitude: flStrForObj(updatedLongitude),
                                  mCity :flStrForObj([[GetCurrentLocation sharedInstance] currentCity]) ,
                                  mlocation:flStrForObj([[GetCurrentLocation sharedInstance] location]) ,
                                  mCountryShortName:flStrForObj([[GetCurrentLocation sharedInstance] currentCity])
                                  };
    [WebServiceHandler updateUserLocation:requestDict andDelegate:self];
    }

}

- (IBAction)btnEditHeaderClicked:(id)sender
{
    _popUpHeader.hidden = false;
    _popUpHeader.userInteractionEnabled = true;
    isEditClicked = true;
    [_postTableView reloadData];
}
- (IBAction)btnCancelHeaderClicked:(id)sender
{
    _popUpHeader.hidden = true;
    _popUpHeader.userInteractionEnabled = false;
    isEditClicked = false;
    [_postTableView reloadData];
}
- (IBAction)btnDeleteAllHeaderClicked:(id)sender
{
     [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"RecentSearch"];
    topResponseData = nil;
    _popUpHeader.hidden = true;
    isEditClicked = false;
    [_postTableView reloadData];
}






@end
