//
//  HomeScreenController

//
//  Created by Rahul Sharma on 4/12/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FilterViewController.h"
#import "PostingScreenModel.h"
#import <CoreLocation/CLLocationManager.h>
#import "TopTableViewCell.h"
#import "LanguageManager.h"



@interface HomeScreenController : UIViewController<UICollectionViewDataSource,UICollectionViewDelegate, UITableViewDelegate, UITableViewDataSource,UIScrollViewDelegate,filterDelegate, UITextFieldDelegate>
{
        PostingScreenModel *post;
        NSString *categoryInStringFormat ;
        UIRefreshControl *refreshControl;
        NSMutableArray  *explorePostsresponseData , *arrayOfCategoryImagesUrls, *arrayOfcategory,*temp ,*productsArray;
        UIActivityIndicatorView  *avForCollectionView;
        UICollectionView *filteredCV;
        NSString  *minPrice,*maxPrice, *currencyCode, *currencySymbol, *sortBy,*postedWithin;;
        float  dist;
        UILabel *errorMessageLabelOutlet;
        CGPoint lastScrollContentOffset;
        GetCurrentLocation *getLocation;
        NSMutableArray *topResponseData,*peopleData,*recentSearchHistory;
        int keyBoardHeight;

}

#pragma mark -
#pragma mark - Non IB Properties-

@property (nonatomic, strong) CLLocationManager * locationManager;
@property (nonatomic)BOOL isFilter, flagForLocation, noPostsAreAvailable ,isScreenDidAppear, isClickedOnTextField;
@property (nonatomic)double latForFilter,longiForFilter ,currentLat, currentLong;
@property (nonatomic,strong) NSString *locationName;
@property int currentIndex,paging;
@property NSInteger cellDisplayedIndex , leadingConstraint;
@property NSString *postedImagePath, *postedthumbNailImagePath;
@property (nonatomic, strong) ZoomInteractiveTransition * transition;
@property(assign, nonatomic) CGFloat currentOffset;

#pragma mark -
#pragma mark - UITextField Outlets-

@property (strong, nonatomic) IBOutlet UITextField *txtSearchBar;
- (IBAction)textDidChange:(id)sender;
- (IBAction)textFieldTouchInsideAction:(id)sender;

#pragma mark -
#pragma mark - UIView Outlets-
@property (strong, nonatomic) IBOutlet UIView *emtyFiltersView;
@property (strong, nonatomic) IBOutlet UIView *tblHeaderView;


#pragma mark -
#pragma mark - Label Outlets-
@property (strong, nonatomic) IBOutlet UILabel *labelNotificationsCount;
@property (strong, nonatomic) IBOutlet UILabel *emptyFilterTextLabel;

#pragma mark -
#pragma mark - Button Outlets-

@property (weak, nonatomic) IBOutlet UIView *sellStuffButtonView;
@property (strong, nonatomic) IBOutlet UIButton *filterButtonOutlet;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *rightBarButtonItem;

@property (strong, nonatomic) IBOutlet UIBarButtonItem *searchButtonOutlet;
@property (strong, nonatomic) IBOutlet UIButton *btnBrowse;
@property (strong, nonatomic) IBOutlet UIButton *btnInvite;

#pragma mark -
#pragma mark - CollectionView Outlets

@property (weak, nonatomic) IBOutlet UICollectionView *collectionviewFilterItem;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionViewOutlet;

#pragma mark -
#pragma mark - UITableView Outlets
@property (strong, nonatomic) IBOutlet UITableView *postTableView;

#pragma mark -
#pragma mark - Button Actions-
- (IBAction)searchButtonAction:(id)sender;
- (IBAction)notificationButtonACtion:(id)sender;
- (IBAction)filterButtonAction:(id)sender;
- (IBAction)sellStuffButton:(id)sender;
- (IBAction)removeProductFilterButtonAcion:(id)sender;
- (IBAction)btnBrowseAction:(id)sender;
- (IBAction)btnInviteAction:(id)sender;


#pragma mark -
#pragma mark - NSConstraints Outlets

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *FilterViewTopConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *bottomConstraintOfSellButton;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *postTableViewHeight;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *searchTopConst;

@property (weak, nonatomic) IBOutlet UILabel *lblHeaderView;
@property (weak, nonatomic) IBOutlet UIButton *btnEditHeader;

@property (weak, nonatomic) IBOutlet UIView *popUpHeader;
@property (weak, nonatomic) IBOutlet UIButton *btnCancelHeader;
@property (weak, nonatomic) IBOutlet UIButton *btnDeleteAllHeader;

















@end
