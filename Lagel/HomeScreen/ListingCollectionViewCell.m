//
//  ListingCollectionViewCell.m

//
//  Created by Rahul Sharma on 4/12/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import "ListingCollectionViewCell.h"
#import "TinderGenericUtility.h"
#import "UIImageView+WebCache.h"

@implementation ListingCollectionViewCell

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
-(void)awakeFromNib
{
    [super awakeFromNib];
    self.layer.borderWidth = 0.5f;
    self.contentView.backgroundColor =[UIColor clearColor];
    self.postedImageOutlet.contentMode = UIViewContentModeScaleAspectFill;
    self.imageForShowVideoOrNot.hidden = YES;
    
    self.layer.masksToBounds = NO ;
    self.clipsToBounds = true;
    self.layer.shadowColor = mDividerColor.CGColor;
    self.layer.shadowOffset = CGSizeZero ;
    self.layer.shadowOpacity = 1;
    self.layer.shadowRadius = 2.0 ;
    self.contentView.backgroundColor =[UIColor whiteColor];
    self.layer.borderColor = mDividerColor.CGColor ;
    [self layoutIfNeeded];
}

-(void)prepareForReuse
{
    [super prepareForReuse];
    self.postedImageOutlet.image = nil;
    self.postedImageOutlet.clipsToBounds = NO ;
    
}


-(void)setImageForCell:(NSArray *)dataArray forIndexPath :(NSIndexPath *)indexPath
{
    if (dataArray.count) {
        NSString *isPromotedValue = flStrForObj(dataArray[indexPath.row][@"isPromoted"]);
        BOOL isPromoted = [isPromotedValue boolValue];
        
        if(isPromoted)
        {
            self.featuredView.hidden = NO ;
        }
        else
        {
            self.featuredView.hidden = YES ;
        }
    
        SDWebImageManager *manager = [SDWebImageManager sharedManager];
        
        [self.postedImageOutlet setImageWithURL:nil usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        
        NSString *thumbimgUrl = flStrForObj(dataArray[indexPath.item][@"mainUrl"]);
        NSString *imageUrl = [thumbimgUrl stringByReplacingOccurrencesOfString:@"upload/" withString:@"upload/c_fit,h_500,q_35,w_500/"];
        
        NSURL *imageURL = [NSURL URLWithString:imageUrl];
        
        [manager diskImageExistsForURL:[NSURL URLWithString:imageUrl] completion:^(BOOL isInCache) {
            if (isInCache) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.postedImageOutlet setImage: [[SDImageCache sharedImageCache] imageFromDiskCacheForKey:imageUrl]];
                });
            } else {
                [self.postedImageOutlet setImageWithURL:imageURL placeholderImage:[UIImage imageNamed:@""] options:0 completed:^(UIImage *image,NSError *error, SDImageCacheType cacheType,NSURL *imageURL){
                    dispatch_async(dispatch_get_main_queue(),^{
                        [UIView transitionWithView:self.postedImageOutlet duration:0.2 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
                            [self.postedImageOutlet setImage:image];
                        }completion:NULL];
                    });
                } usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray] ;
            }
        }];
    }
}

@end
