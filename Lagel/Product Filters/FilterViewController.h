//
//  FilterViewController.h

//
//  Created by Rahul Sharma on 31/12/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

/*Ajay Thakur*/

@protocol filterDelegate <NSObject>

-(void)getFilteredItems:(NSMutableDictionary *)dicOfFilters miniPrice:(NSString *)min maxPrice:(NSString *)max curncyCode:(NSString *)currCode curncySymbol :(NSString *)currSymbol anddistance:(int)distance;

@end
@interface FilterViewController : UIViewController <UITableViewDelegate,UITableViewDataSource ,UIGestureRecognizerDelegate , UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UIView *viewForTableView;

@property (strong, nonatomic) IBOutlet UIButton *navCancelButtonOutlet;
@property (strong, nonatomic) IBOutlet UIButton *navResetButtonOutlet;


@property(weak,nonatomic) NSString *sortBy, *currencyCode,*currencySymbol;
@property(weak,nonatomic) NSString *postedWithin;
@property (weak,nonatomic) NSMutableArray *arrayOfSelectedFilters;
@property (nonatomic,strong) NSString *selectedLocation;
@property (nonatomic) double lattitude,longitude;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UIButton *applyButtonOutlet;


- (IBAction)applyFiltersButtonAction:(id)sender;

@property(nonatomic)int distnce;
@property (nonatomic)NSInteger leadingConstraint ;
@property(nonatomic)NSString *minPrce,*maxPrce;
@property BOOL checkArrayOfFilters;

@property(nonatomic,weak)id<filterDelegate>delegate;
@end
