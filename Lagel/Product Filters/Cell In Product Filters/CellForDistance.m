//
//  CellForDistance.m

//  Created by Rahul Sharma on 12/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "CellForDistance.h"

@implementation CellForDistance

//- (instancetype)initWithCoder:(NSCoder *)aDecoder {
//    if (self = [super initWithCoder:aDecoder]) {
//        self.sliderForDistance.minimumValue = 0;
//        self.sliderForDistance.maximumValue = 80;
//    }
//    return self;
//}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    

    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)updateThumblabel {
    CGRect trackRect = [self.sliderForDistance trackRectForBounds:self.sliderForDistance.bounds];
    CGRect thumbRect = [self.sliderForDistance thumbRectForBounds:self.sliderForDistance.bounds
                                                        trackRect:trackRect
                                                            value:self.sliderForDistance.value];
    
    self.labelLeadingConstraint.constant = thumbRect.origin.x + self.sliderForDistance.frame.origin.x - 10;
    self.leadingConstraint = self.labelLeadingConstraint.constant ;
}

- (IBAction)sliderValueChangedAction:(id)sender {
    float step = 8;
//    if (self.sliderForDistance.value < 32) {
//        step = 8;
//    }
//    else if (self.sliderForDistance.value < 48) {
//        step = 16;
//    }
//    else if (self.sliderForDistance.value < 80) {
//        step = 32;
//    }
    
    
    
    float tempValue = fabsf(fmodf(self.sliderForDistance.value, step)); //need to import <math.h>
    float newValue;
    //if the remainder is greater than or equal to the half of the interval then return the higher interval
    //otherwise, return the lower interval
    if(tempValue >= (step / 2.0))
        newValue =  self.sliderForDistance.value - tempValue + step;
    else
        newValue =  self.sliderForDistance.value - tempValue;
    self.sliderForDistance.value = newValue;
    

//    int val  = [[NSString stringWithFormat:@"%f",self.sliderForDistance.value]intValue];
//    CGRect trackRect = [self.sliderForDistance trackRectForBounds:self.sliderForDistance.bounds];
//    CGRect thumbRect = [self.sliderForDistance thumbRectForBounds:self.sliderForDistance.bounds
//                                             trackRect:trackRect
//                                                 value:self.sliderForDistance.value];
//
//    self.labelLeadingConstraint.constant = thumbRect.origin.x + self.sliderForDistance.frame.origin.x - 10;
//    self.leadingConstraint = self.labelLeadingConstraint.constant ;
    [self setDistance:self.sliderForDistance.value];
}

-(void)setValuesForSlider:(int)distanceValue andLeadingConstraint :(NSInteger)leadingConstraint
{
    self.sliderForDistance.value = distanceValue;
    self.labelLeadingConstraint.constant = leadingConstraint;
    [self setDistance:distanceValue];

}

-(void)setDistance:(int)distanceValue
{
    if(distanceValue == 0)
    {
        if(self.callBackForDistance)
        {
            self.callBackForDistance(0 ,self.labelLeadingConstraint.constant);
        }
        self.label.text = NSLocalizedString(distanceFilterNotSet,distanceFilterNotSet ) ;
    }
    else if(distanceValue <=48 )
    {
        if(self.callBackForDistance)
        {
            self.callBackForDistance(distanceValue , self.leadingConstraint);
        }
        self.label.text = [[NSString stringWithFormat:@"%d ",distanceValue]stringByAppendingString:@"Km"];
    }
    
    else
    {
        if(self.callBackForDistance)
        {
            self.callBackForDistance(3000 , self.leadingConstraint);
        }
        self.label.text = NSLocalizedString(distanceFilterMax,distanceFilterMax);
    }
    [self updateThumblabel];
}

@end
