//
//  RestKitWebServiceHandler.m
//  RedBag
//
//  Created by Nabeel Gulzar on 12/28/15.
//  Copyright (c) 2015 3Embed. All rights reserved.
//

#import "RestKitWebServiceHandler.h"
#import "ProgressIndicator.h"
#import "Helper.h"
#import "AFNetworking.h"
@interface RestKitWebServiceHandler ()
@property(nonatomic,strong) NSString *lastMethod;
@end
@implementation RestKitWebServiceHandler

static RestKitWebServiceHandler *webservice;

+ (id)sharedInstance {
    if (!webservice) {
        webservice  = [[self alloc] init];
    }
    
    return webservice;
}




-(void)composeRequestWithMethod:(NSString*)method paramas:(NSDictionary*)params onComplition:(void (^)(BOOL succeeded, NSDictionary  *response))completionBlock
{
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments|NSJSONReadingMutableContainers];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json",@"text/json", nil];
    
    
    [manager POST:method parameters:params progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        //here is place for code executed in success case
        
        completionBlock(YES,responseObject);
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        
        [[ProgressIndicator sharedInstance]hideProgressIndicator];
        completionBlock(NO,nil);
        
    }];
}


-(void)composeRequestWithMethodGET:(NSString*)method paramas:(NSDictionary*)params onComplition:(void (^)(BOOL succeeded, NSDictionary  *response))completionBlock
{
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments|NSJSONReadingMutableContainers];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json",@"text/json", nil];
    
    [manager GET:method parameters:params progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        //here is place for code executed in success case
        
        completionBlock(YES,responseObject);
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        
        [[ProgressIndicator sharedInstance]hideProgressIndicator];
        completionBlock(NO,nil);
        
    }];
}

-(NSString*)getBaseString:(NSString*)method
{
    // NSLog(@"%@%@",BASE_URL_RESTKIT,method);
    return [NSString stringWithFormat:@"%@%@", BASE_URL_RESTKIT, method];
}
@end
