//
//  EditProductViewController.m

//
//  Created by Rahul Sharma on 01/03/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "EditProductViewController.h"
#import "ScreenConstants.h"
#import "PGShareViewController.h"
#import "PostingScreenModel.h"
#import "WebServiceHandler.h"
#import "BuyerSellerTableViewCell.h"
#import "UIImageView+AFNetworking.h"
#import "SelectBuyerSellerViewController.h"
#import "JTSImageViewController.h"
#import "JTSImageInfo.h"
#import "InsightsViewController.h"
#import "InAppViewController.h"

@interface EditProductViewController ()<UITableViewDataSource,UITableViewDelegate,WebServiceHandlerDelegate,updateCallBack>
{
    PostingScreenModel *post;
    NSInteger imgNumber;
    NSMutableArray *makeOfferUsers, *basicInsights, *timeInsights, *locationInsights;
   
}
@end

@implementation EditProductViewController

/*-----------------------------------------------------*/
#pragma mark - ViewController LifeCycle.
/*----------------------------------------------------*/

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.titleView = self.navTitleImageView;
    [self updateFeildsWithData];
     self.markAsSoldView.layer.borderColor = mBaseColor.CGColor ;
    self.updateView.layer.borderColor = mBaseColor.CGColor ;
    
    [self createNavLeftButton];
    [self getPostDetails];
    if(_showInsights)
    {
     [self getInsightsOfProduct];
    }
    
    if(_isPromoted)
    {
     self.promoteThisItem.hidden = YES ;
    }
    else
    {
    self.promoteThisItem.hidden = NO ;
    }
    
    basicInsights = [NSMutableArray new];
    timeInsights = [NSMutableArray new];
    locationInsights = [NSMutableArray new];
     makeOfferUsers = [NSMutableArray new];
  }

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.shadowImage = nil;
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self.view endEditing:YES];
}




-(void)getPostDetails
{
    if(self.isProductScreen)
    {
        [self.imageViewItemImage sd_setImageWithURL:[NSURL URLWithString:self.product.thumbnailImageUrl] placeholderImage:[UIImage imageNamed:@"defaultpp.png"]];
        self.labelProductName.text = self.product.productName ;
        self.labelCategory.text = self.product.category ;
        NSDictionary *dictWeb = @{mauthToken: [Helper userToken],
                                  mpostid: self.product.postId,
                                  mposttype: @"0",
                                  mLimit: @"20",
                                  moffset:@"0",
                                  };
        [WebServiceHandler acceptedOffers:dictWeb andDelegate:self];
    }
    else
    {
    
    
    NSDictionary *dictWeb = @{mauthToken: [Helper userToken],
                              mpostid: [NSString stringWithFormat:@"%@",[self.memberPostDetails valueForKey:@"postId"]],
                              mposttype: @"0",
                              mLimit: @"50",
                              moffset:@"0",
                           };
    [WebServiceHandler acceptedOffers:dictWeb andDelegate:self];
    }
    ProgressIndicator *loginPI = [ProgressIndicator sharedInstance];
    [loginPI showPIOnView:self.view withMessage:NSLocalizedString(LoadingIndicatorTitle, LoadingIndicatorTitle)];
}




/**
 Update Fields acording to posted data.
 */
-(void)updateFeildsWithData
{
    
    
    
    NSString *imageUrl = [NSString stringWithFormat:@"%@",[self.memberPostDetails valueForKey:@"mainUrl"]];
    
    self.productUrl  = flStrForObj(imageUrl);
    self.postId = [NSString stringWithFormat:@"%@",[self.memberPostDetails valueForKey:@"postId"]];
    self.price = [NSString stringWithFormat:@"%@",[self.memberPostDetails valueForKey:@"price"]];
    self.currency = [NSString stringWithFormat:@"%@",[self.memberPostDetails valueForKey:@"currency"]];
    self.productName = [NSString stringWithFormat:@"%@",[self.memberPostDetails valueForKey:@"productName"]];
    
    [self.imageViewItemImage sd_setImageWithURL:[NSURL URLWithString:imageUrl] placeholderImage:[UIImage imageNamed:@"defaultpp.png"]];
    self.labelProductName.text = self.productName;
    NSString *name = [NSString stringWithFormat:@"%@",[self.memberPostDetails valueForKey:@"category"]];
    if(name==nil || name==(id)[NSNull null] || [name isEqualToString:@"(null)"] || [name isEqualToString:@"defaultUrl"] )
    {
        self.labelCategory.text = @"";
    }
    else
    {
        self.labelCategory.text = [NSString stringWithFormat:@"%@",[self.memberPostDetails valueForKey:@"category"]];
    }
}


-(void)createNavRightButton
{
    [self.insightsButtonOutlet addTarget:self action:@selector(insightsButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *navLeft = [[UIBarButtonItem alloc]initWithCustomView:self.insightsButtonOutlet];
    self.navigationItem.rightBarButtonItem = navLeft;
}
-(void)createNavLeftButton
{
    UIButton *navLeft = [CommonMethods createNavButtonsWithselectedState:mNavigationBackButtonImageName normalState:mNavigationBackButtonImageName];
    [navLeft rotateButton];
    UIBarButtonItem *navLeftButton = [[UIBarButtonItem alloc]initWithCustomView:navLeft];
    [navLeft setFrame:CGRectMake(0,0,20,20)];
    self.navigationItem.leftBarButtonItem = navLeftButton;
    [navLeft addTarget:self action:@selector(navLeftButtonAction) forControlEvents:UIControlEventTouchUpInside];
    [self.navigationItem setLeftBarButtonItems:@[[CommonMethods getNegativeSpacer],navLeftButton]];

}

-(void)insightsButtonAction:(id)sender
{
    InsightsViewController *insightsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"insightsStoryboard"];
    insightsVC.postId = [NSString stringWithFormat:@"%@",flStrForObj([self.memberPostDetails valueForKey:@"postId"])];
    insightsVC.basicInsight = basicInsights;
    insightsVC.timeInsight = timeInsights;
    insightsVC.locationInsight = locationInsights ;
    [self.navigationController pushViewController:insightsVC animated:YES];
}

-(void)navLeftButtonAction
{
    [self.navigationController popViewControllerAnimated:YES];
}
/*-----------------------------------------------------*/
#pragma mark - Button Actions.
/*----------------------------------------------------*/

/**
 Mark button action method.This method willmark product as selling or sold.
 
 @param sender Mark button.
 */
- (IBAction)buttonMarkAsAction:(id)sender {
    if(makeOfferUsers.count != 0 )
    {
    [self performSegueWithIdentifier:@"toSellectionListView" sender:nil];
    }
    else{
        
        UIAlertController *alertController  = [UIAlertController alertControllerWithTitle:nil message:NSLocalizedString(soldSomewhereElseAlertMessage, soldSomewhereElseAlertMessage) preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *yes = [UIAlertAction actionWithTitle:NSLocalizedString(alertYes, alertYes) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        
            NSString *postId;
            if(self.isProductScreen)
            {
                postId = self.product.postId ;
            }
            else
            {
                postId = flStrForObj([self.memberPostDetails valueForKey:@"postId"]);
            }
            
            
            NSDictionary *requestDic = @{ mpostid : postId,
                                          mtype : @"0",
                                          mauthToken : [Helper userToken]
                                          };
            
            [ WebServiceHandler soldSomeWhere:requestDic andDelegate:self];
        
        }];
        UIAlertAction *no = [UIAlertAction actionWithTitle:NSLocalizedString(alertNo, alertNo) style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:yes];
        [alertController addAction:no];
        [self presentViewController:alertController animated:YES completion:nil];
    }
    
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"toSellectionListView"]) {
        SelectBuyerSellerViewController *vc = [segue destinationViewController];
        vc.responseDict = makeOfferUsers;
    }
}

/**
 Edit Button action will prsent posting screen for editing.

 @param sender edit button.
 */
- (IBAction)buttonEditAction:(id)sender {
    UIAlertController *controller = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(alertCancel, alertCancel) style:UIAlertActionStyleCancel handler:nil];
    UIAlertAction *deleteAction = [UIAlertAction actionWithTitle:NSLocalizedString(deleteThisPostActionSheetTitle, deleteThisPostActionSheetTitle) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
     
        ProgressIndicator *loginPI = [ProgressIndicator sharedInstance];
        [loginPI showPIOnView:self.view withMessage:NSLocalizedString(DeletingIndicatorTitle, DeletingIndicatorTitle)];
        NSString *postId;
        if(self.isProductScreen)
        {
            postId = self.product.postId ;
        }
        else
        {
            postId = flStrForObj([self.memberPostDetails valueForKey:@"postId"]);
        }
        
        NSDictionary *requestDict = @{mpostid : postId,
                                      mauthToken : [Helper userToken]};
        [WebServiceHandler deletePost:requestDict andDelegate:self];
    }];
    UIAlertAction *editAction = [UIAlertAction actionWithTitle:NSLocalizedString(editThisPostActionSheetTitle, editThisPostActionSheetTitle) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){ PGShareViewController *shareVC = [self.storyboard instantiateViewControllerWithIdentifier:mPostScreenStoryboardID];
        shareVC.editingPost = YES;
        shareVC.postedProductDetails = self.memberPostDetails ;
        shareVC.popDelegate  = self ;
        UINavigationController *navBar =[[UINavigationController alloc]initWithRootViewController:shareVC];
        [self.navigationController presentViewController:navBar animated:YES completion:nil];;
    }];
    
    [controller addAction:cancelAction];
    [controller addAction:deleteAction];
    if(!self.isProductScreen)
    {
    [controller addAction:editAction];
    }
    mPresentAlertController;
   }

/*-----------------------------------------------------*/
#pragma mark - Webservice Delegate.
/*----------------------------------------------------*/


-(void)internetIsNotAvailable:(RequestType)requsetType
{
    
}

/**
 WebService delegate

 @param requestType RequestType.
 @param response    response by Web Service.
 @param error       error.
 */
-(void)didFinishLoadingRequest:(RequestType)requestType withResponse:(id)response error:(NSError *)error
{
     [[ProgressIndicator sharedInstance] hideProgressIndicator];
    if (error) {
        [Helper showAlertWithTitle:NSLocalizedString(alertError, alertError) Message:NSLocalizedString(mCommonServerErrorMessage, mCommonServerErrorMessage) viewController:self];
    }
    
    NSDictionary *responseDict = (NSDictionary*)response;
    
    switch (requestType) {
        case RequestTypeacceptedOffers:
        {
            switch ([responseDict[@"code"] integerValue]) {
                case 200:
                    makeOfferUsers = responseDict[@"data"];
                    [self.tableView reloadData];
                    break;
                case 204 :
                    [self showingMessageForCollectionViewBackground];
                    break;
                default:
                    break;
            }
        }
            break;
            
        case RequestTypeGetPostsByUsers:
        {
            switch ([responseDict[@"code"] integerValue]) {
                case 200: {
                    //                self.currentIndex = 0;
                    
                    self.memberPostDetails =responseDict[@"data"][0];
                    
                    [self updateFeildsWithData];
                    [self handlingResponseOfExplorePosts:response];
                    [[ProgressIndicator sharedInstance] hideProgressIndicator];
                }
                    break;
                    
                default:
                    break;
            }
            
            
        }
            break;
            
            case RequestTypeDeletePost:
        {
            switch ([response[@"code"] integerValue])
            {
                case 200:
                    [[NSNotificationCenter defaultCenter] postNotificationName:mDeletePostNotifiName object:[NSDictionary dictionaryWithObject:response forKey:@"data"]];
                    [self.navigationController popViewControllerAnimated:YES];
                    break;
                default:
                    break;
            }
        }
            break;
            
        case RequestTypesoldElseWhere:
        {
            
            switch ([response[@"code"] integerValue])
            {
                case 200:
                    [[NSNotificationCenter defaultCenter]postNotificationName:mSellingToSoldNotification object:[NSDictionary dictionaryWithObject:response[@"data"] forKey:@"data"]];
                    [[NSNotificationCenter defaultCenter]postNotificationName:mDeletePostNotifiName object:[NSDictionary dictionaryWithObject:response[@"data"] forKey:@"data"]];
                    [self.navigationController popViewControllerAnimated:YES];
                    break;
                default:
                    break;
            }
            
        }
        
            break;
        case RequestToGetInsights :
        {
            
            switch ([response[@"code"] integerValue])
            {
                case 200:
                {
                [self createNavRightButton];
                [self extractInsights:responseDict[@"data"]];
                    
                }
                    break;
                default:
                    break;
            }
            
        }
            break ;
        default:
            break;
    }
    
}


-(void)handlingResponseOfExplorePosts :(NSDictionary *) response
{
  
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return makeOfferUsers.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    BuyerSellerTableViewCell *cell;
    cell = [tableView dequeueReusableCellWithIdentifier:@"ListCell" forIndexPath:indexPath];
    NSDictionary * data = [makeOfferUsers objectAtIndex:indexPath.row];
    cell.nameLabel.text = [NSString stringWithFormat:@"%@",data[@"buyername"]];
    NSString *offeredPrice = data[@"offerPrice"];
    if (offeredPrice != nil && ![offeredPrice isEqualToString:@""]) {
        cell.addressLabel.text = [[NSString stringWithFormat:NSLocalizedString(priceOfferedStatement, priceOfferedStatement),@" %@",data[@"currency"]]stringByAppendingString:[NSString stringWithFormat:@" %@",data[@"offerPrice"]]];
    }
    else {
        cell.addressLabel.text = @"";
    }
    
    NSString *imageURL1 =[NSString stringWithFormat:@"%@",data[@"buyerProfilePicUrl"]];
        NSURL *imageUrl =[NSURL URLWithString:imageURL1];
        [cell.memberPic sd_setImageWithURL:imageUrl placeholderImage:[UIImage imageNamed:@"defaultpp"]];
//    NSString *timeStamp = [Helper convertEpochToNormalTimeInshort:data[@"offerCreatedOn"]];
//    NSString *time = [Helper convertEpochToNormalTime:data[@"offerCreatedOn"]];
    
//    if (timeStamp != nil && ![timeStamp isEqualToString:@""]) {
//        cell.timeStampLabel.text = timeStamp;
//    }
//    else {
        cell.timeStampLabel.text = @"";
//    }
//    cell.timeStampLabel.text = timeStamp;

    return cell;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

-(void)showingMessageForCollectionViewBackground{
    
    self.backViewForNoOffers.frame = CGRectMake(0, 0, self.tableView.bounds.size.width, self.tableView.bounds.size.height);
    self.tableView.backgroundView = self.backViewForNoOffers;
}

#pragma mark-
#pragma mark- Pop To root viewcontroller

-(void)popToRootViewController:(BOOL)pop
{
    if(pop)
    {
        [self.navigationController popToRootViewControllerAnimated:NO];
    }
}




-(void)getInsightsOfProduct
{
    
    NSDictionary *requestDict = @{mauthToken : [Helper userToken],
                                  mpostid :[NSString stringWithFormat:@"%@",flStrForObj([self.memberPostDetails valueForKey:@"postId"])],
                                  mDurationType : @"week"
                                  };
    
    [WebServiceHandler getInsightsOfProduct:requestDict andDelegate:self];
    
    
}
                   
-(void)extractInsights:(NSMutableArray *)response
{
    [basicInsights addObjectsFromArray:response[0][@"basicInsight"][@"data"]];
    
    [timeInsights addObjectsFromArray:response[1][@"timeInsight"][@"data"][@"count"]];
    
    [locationInsights addObjectsFromArray:response[2][@"locationInsight"][@"data"]];
    
    
}


#pragma mark-
#pragma mark - Tap to see full image

- (IBAction)showFullProductImageButtonAction:(id)sender {
    JTSImageInfo *imageInfo = [[JTSImageInfo alloc] init];
    imageInfo.image = self.imageViewItemImage.image;
    imageInfo.referenceRect = self.imageViewItemImage.frame;
    imageInfo.referenceView = self.imageViewItemImage.superview;
    imageInfo.referenceContentMode = self.imageViewItemImage.contentMode;
    imageInfo.referenceCornerRadius = self.imageViewItemImage.layer.cornerRadius;
    
    JTSImageViewController *imageViewer = [[JTSImageViewController alloc]
                                           initWithImageInfo:imageInfo
                                           mode:JTSImageViewControllerMode_Image
                                           backgroundStyle:JTSImageViewControllerBackgroundOption_None];
    [imageViewer showFromViewController:self transition:JTSImageViewControllerTransition_FromOriginalPosition];

}
- (IBAction)promotoButtonAction:(id)sender {
    
    if(_isProductScreen)
    {
        InAppViewController *promotionVC = [self.storyboard instantiateViewControllerWithIdentifier:@"PromotionStoryboard"];
        promotionVC.imageUrl = self.product.thumbnailImageUrl  ;
        promotionVC.currency = self.product.currency ;
        promotionVC.price = self.product.price ;
        promotionVC.productName = self.product.productName ;
        promotionVC.postId = self.product.postId ;
        [self.navigationController presentViewController:promotionVC animated:YES completion:nil];
    }
    else
    {
        InAppViewController *promotionVC = [self.storyboard instantiateViewControllerWithIdentifier:@"PromotionStoryboard"];
        promotionVC.imageUrl = self.productUrl ;
        promotionVC.currency = self.currency ;
        promotionVC.price = self.price ;
        promotionVC.productName = self.productName ;
        promotionVC.postId = self.postId ;
        [self.navigationController presentViewController:promotionVC animated:YES completion:nil];
    }
    
   
}
@end
