//
//  BuyerSellerChatViewController.swift
//  
//
//  Created by Rahul Sharma on 18/09/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import Firebase

class BuyerSellerChatViewController: UIViewController {
    
    struct Constants {
        static let openChatSegue = "openChatSegue"
    }
    
    @IBOutlet weak var buyingLabelOutlet: UILabel!
    
    @IBOutlet weak var buyingBadgeCountLabel: UILabel!
    
    @IBOutlet weak var sellingLabelOutlet: UILabel!
    
    @IBOutlet weak var sellingBadgeCount: UILabel!
    @IBOutlet weak var labelCountForNotifications: UILabel!
    @IBOutlet weak var searchBarOutlet: UISearchBar!
    @IBOutlet weak var buyngButtonOutlet: UIButton!
    @IBOutlet weak var buyingViewOutlet: UIView!
    @IBOutlet weak var buyingUnselectedViewOutlet: UIView!
    @IBOutlet weak var sellingUnselectedViewOutlet: UIView!
    @IBOutlet weak var sellingButtonOutlet: UIButton!
    @IBOutlet weak var sellingViewOutlet: UIView!
    var productDetailsObject : ProductDetails!
    var chatListViewModel: ChatListViewModel!
    var controller:BuyerSellerChatPageViewController!
    let mqttChatManager = MQTTChatManager.sharedInstance
    
    var temp:Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tabBarController?.tabBar.items?[AppConstants.chatTabIndex].title = NSLocalizedString("Chat", comment: "Chat")

        labelCountForNotifications.layer.borderColor = UIColor.white.cgColor
        
        controller = self.childViewControllers.first as? BuyerSellerChatPageViewController
        self.setBuyingButtonSelected()
        controller.indexCangedDelegate = self
        guard let mqttId = Helper.getMQTTID() else { return }
        if mqttId != mMqttId {
            mqttChatManager.subscribeGetChatTopic(withUserID: mqttId)
        }
        let name = NSNotification.Name(rawValue: "ChatUpdatedNotification")
        NotificationCenter.default.addObserver(self, selector: #selector(BuyerSellerChatViewController.updateChatList), name: name, object: nil)
        let countNotifName = NSNotification.Name(rawValue: "updateChatCount")
        NotificationCenter.default.addObserver(self, selector: #selector(updateChatListCount), name: countNotifName, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(BuyerSellerChatViewController.receivedChat(notification:)), name: NSNotification.Name(rawValue: "OfferInitiated"), object: nil)
        API().getChats(withPageNo:"0")
    
    }
    
    func receivedChat(notification: NSNotification) {
        let userInfo = notification.userInfo as! [String: Any]
        guard let chatObj = userInfo["chatObj"] as? [String : Any] else { return }
        guard let receiverID = chatObj["receiverID"] as? String, let secretID = chatObj["secretID"] as? String else { return }
        
        if let product = userInfo["productObj"] as? ProductDetails
        {
        productDetailsObject = product
        }
        
        let individualCVMObj = IndividualChatViewModel(couchbase: Couchbase())
        let toDocID = individualCVMObj.fetchIndividualChatDoc(withRecieverID: receiverID, andSecretID: secretID)
        if toDocID == nil {
//            guard let productObj = userInfo["productObj"] as? ProductDetails else { return }
            self.performSegue(withIdentifier: Constants.openChatSegue, sender: productDetailsObject)
        } else {
            let chatsDocVMObj = ChatsDocumentViewModel(couchbase: Couchbase())
            if let chat = chatsDocVMObj.getChatObject(forChatDocID: toDocID, chatUserID:receiverID, productID: secretID) {
                self.performSegue(withIdentifier: Constants.openChatSegue, sender: chat)
            }
        }
        self.updateChatListCount()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == Constants.openChatSegue{
            if let chatController = segue.destination as? ChatViewController {
                if let chatObj = sender as? Chat {
                    let chatVMObject = ChatViewModel(withChatData: chatObj)
                    chatController.productObj = self.productDetailsObject
                    chatController.chatViewModelObj = chatVMObject
                } else if let productObj = sender as? ProductDetails {
                    chatController.productObj = productObj
                }
            }
        }
    }
    
    func updateChatList() {
        let couchbaseObj = Couchbase.sharedInstance
        let chatsDocVMObject = ChatsDocumentViewModel(couchbase: couchbaseObj)
        guard let chats = chatsDocVMObject.getChatsFromCouchbase() else { return }
        let sortedChats = chats.sorted {
            guard let uniqueID1 = $0.lastMessageDate, let uniqueID2 = $1.lastMessageDate else { return false }
            return uniqueID1 > uniqueID2
        }
        self.chatListViewModel = ChatListViewModel(withChatObjects: sortedChats)
        self.title = self.chatListViewModel.userName ?? ""
        controller.chatlistModelObject = self.chatListViewModel
        self.chatListViewModel.setupNotificationSettings()
        self.chatListViewModel.authCheckForNotification()
        controller.reloadCurrentView()
        self.updateChatListCount()
        self.setBadgeCount()
    }
    
    
    @objc public func updateChatListCount() {
        self.tabBarController?.tabBar.items?[AppConstants.chatTabIndex].title = NSLocalizedString("Chat", comment: "Chat")
        let count = self.chatListViewModel.unreadChatsCounts()
        let actbadge: Int = UserDefaults.standard.integer(forKey: "actbadge")
        var badge: Int = UserDefaults.standard.integer(forKey: "badge")
        badge = actbadge + Int(count)
        UserDefaults.standard.set(badge, forKey: "badge")
        UIApplication.shared.applicationIconBadgeNumber = badge
        
        if count == 0 {
            self.tabBarController?.tabBar.items?[AppConstants.chatTabIndex].badgeValue = nil
        } else {
            self.tabBarController?.tabBar.items?[AppConstants.chatTabIndex].badgeValue = "\(count)"
        }
    }
    
    func setBadgeCount()
    {
        if let buyingBadgeCount = self.chatListViewModel.unreadBuyerChats
        {
        if (buyingBadgeCount > 0)
        {
            self.buyingBadgeCountLabel.isHidden = false
        }
        else
        {
           self.buyingBadgeCountLabel.isHidden = true
        }
        }
        
        if let sellingBadgeCount = self.chatListViewModel.unreadSellerChats
        {
            if (sellingBadgeCount > 0)
            {
                self.sellingBadgeCount.isHidden = false
            }
            else
            {
               self.sellingBadgeCount.isHidden = true
            }
        }
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let notificationSharedObject = NotificationCount.sharedInstance()
        if notificationSharedObject?.notificationsCount == "0" {
         self.labelCountForNotifications.isHidden = true;
        }
        else
        {
            self.labelCountForNotifications.isHidden = false;
            self.labelCountForNotifications.text = notificationSharedObject?.notificationsCount ;
        }
 
        self.updateChatList()
        self.tabBarController?.tabBar.items?[AppConstants.chatTabIndex].title = NSLocalizedString("Chat", comment: "Chat")
        self.tabBarController?.tabBar.isHidden = false
    
     }
    
    @IBAction func refreshButtonAction(_ sender: Any) {
        API().getChats(withPageNo:"0")
        if (!MQTT.sharedInstance.isConnected) {
            MQTT.sharedInstance.createConnection()
        }
        let pIndicator = ProgressIndicator.sharedInstance()
        pIndicator?.showPI(on: self.view, withMessage: "Loading...")
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            pIndicator?.hide()
        }
    }
    
    @IBAction func buyingButtonAction(_ sender: Any) {
        self.updateChatList()
        self.setBuyingButtonSelected()
    }
    
    @IBAction func sellingButtonActoin(_ sender: Any) {
        self.updateChatList()
        self.setSellingButtonSelected()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.tabBarController?.tabBar.items?[AppConstants.chatTabIndex].title = NSLocalizedString("Chat", comment: "Chat")
    }
    
    func setBuyingButtonSelected() {
        
        buyngButtonOutlet.isSelected = true
        sellingButtonOutlet.isSelected = false
        controller.openController(withIndex: 0)
        
        
        self.buyingViewOutlet.backgroundColor = AppConstants.chatColor.chatBaseColor
        self.buyingLabelOutlet.textColor = AppConstants.chatColor.chatBaseColor
        self.sellingLabelOutlet.textColor = AppConstants.chatColor.chatLightBaseColor
        self.sellingViewOutlet.backgroundColor = UIColor.clear
        self.buyingUnselectedViewOutlet.isHidden = true
        self.sellingUnselectedViewOutlet.isHidden = false
       
    }
    
    func setSellingButtonSelected() {
        buyngButtonOutlet.isSelected = false
        sellingButtonOutlet.isSelected = true
        controller.openController(withIndex: 1)
        self.buyingViewOutlet.backgroundColor = UIColor.clear
        self.sellingViewOutlet.backgroundColor = AppConstants.chatColor.chatBaseColor
        self.sellingLabelOutlet.textColor = AppConstants.chatColor.chatBaseColor
        self.buyingLabelOutlet.textColor = AppConstants.chatColor.chatLightBaseColor
        self.sellingUnselectedViewOutlet.isHidden = true
        self.buyingUnselectedViewOutlet.isHidden = false
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    @IBAction func notificationButtonAction(_ sender: Any) {
        
        let notificationSharedObject = NotificationCount.sharedInstance()
        notificationSharedObject?.notificationsCount = "0"
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let newVC :ActivityViewController = storyboard.instantiateViewController(withIdentifier:"ActivityControllerId") as! ActivityViewController
        let nav  = UINavigationController(rootViewController: newVC)
        self.navigationController?.present(nav, animated: true, completion: nil)
        
    }
    
}

extension BuyerSellerChatViewController : IndexCangedDelegate {
    func currentIndexGotChanged(index: Int) {
        switch index{
        case 0:
            self.setBuyingButtonSelected()
            
        default:
            self.setSellingButtonSelected()
        }
    }
}

extension BuyerSellerChatViewController : UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let newString = (searchBar.text! as NSString).replacingCharacters(in: range, with: text)
        getSearchData(withSearchText: newString)
        return true
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
    }
    
    func getSearchData(withSearchText searchString: String) {
        let index = self.controller.currentIndex
        let chats = self.chatListViewModel.searchInChatModels(withString: searchString, withSelectedIndex: index)
        let chatListVMObj = ChatListViewModel(withChatObjects: chats)
        self.controller.reloadController(withIndex: index, andChatVMObj: chatListVMObj)
    }
}


