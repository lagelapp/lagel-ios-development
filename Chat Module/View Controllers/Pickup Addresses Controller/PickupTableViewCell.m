//
//  PickupTableViewCell.m
//
//  Created by Rahul Sharma on 3/16/16.
//  Copyright © 2016 3embed. All rights reserved.
//

#import "PickupTableViewCell.h"

@implementation PickupTableViewCell

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
