//
//  StaySafeViewController.swift
//  
//
//  Created by Sachin Nautiyal on 20/11/2017.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit

class StaySafeViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func okButtonAction(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
}
