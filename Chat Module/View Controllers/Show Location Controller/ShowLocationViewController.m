//
//  ShowLocationViewController.m
//
//  Created by Rahul Sharma on 3/11/16.
//  Copyright © 2016 3embed. All rights reserved.
//

#import "ShowLocationViewController.h"
#import "JGActionSheet.h"

@interface ShowLocationViewController ()<MKMapViewDelegate,CLLocationManagerDelegate>{
    
    double latitude;
    double logitude;
    CLLocationCoordinate2D coordinate;
    NSString *address;
    NSString *subAddress;
    CLLocationManager *locationManager;
    
    
}

@end

@implementation ShowLocationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    locationManager = [[CLLocationManager alloc] init];
    [locationManager setDelegate:self];
    
    //Set some parameters for the location object.
    [locationManager setDistanceFilter:kCLDistanceFilterNone];
    [locationManager setDesiredAccuracy:kCLLocationAccuracyBest];
    
    if([CLLocationManager locationServicesEnabled] &&
       [CLLocationManager authorizationStatus] != kCLAuthorizationStatusDenied)
    {
        _mapView.delegate = self;
        _mapView.showsUserLocation =YES;
        [locationManager  requestWhenInUseAuthorization];
        
    }else{
        
        [Helper showAlertWithTitle:NSLocalizedString(allowLocationAlertTitle, allowLocationAlertTitle) Message:NSLocalizedString(allowLocationAlertMessage, allowLocationAlertMessage) viewController:self];
    }
    

    
    _Segment.selectedSegmentIndex = 0;
    [_Segment addTarget:self
                         action:@selector(action:)
               forControlEvents:UIControlEventValueChanged];
    _mapView.delegate = self;
    _mapView.showsUserLocation =YES;
   // _mapView.showsCompass = YES;
    
    _mapView.mapType = MKMapTypeStandard;
    
    //NSLog(@"store location =%@",_storeLocationStr);
    NSArray *Arr = [_storeLocationStr componentsSeparatedByString:@"@@"];

    NSArray *subArr = [[Arr objectAtIndex:0] componentsSeparatedByString:@","];
  //  NSLog(@"subArr =%@",subArr);
    
    NSString *latitudeStr = [NSString stringWithFormat:@"%@",[subArr objectAtIndex:0]];
    latitudeStr = [latitudeStr substringFromIndex:1];
    NSString *logitudeStr = [NSString stringWithFormat:@"%@",[subArr objectAtIndex:1]];
    logitudeStr = [logitudeStr substringToIndex:[logitudeStr length] - 1];
    
    latitude = [latitudeStr doubleValue];
    logitude = [logitudeStr doubleValue];
    
    address = [NSString stringWithFormat:@"%@",[Arr objectAtIndex:1]];
    subAddress = [NSString stringWithFormat:@"%@",[Arr objectAtIndex:2]];
    
    coordinate.latitude = latitude;
    coordinate.longitude = logitude;
    [self.navigationItem setTitle:subAddress];

    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation{
    
    
        MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(coordinate,800,800);
        [self.mapView setRegion:[self.mapView regionThatFits:region] animated:YES];
    
        MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
        point.coordinate = coordinate;
        point.title = address;
        point.subtitle = subAddress;
        [self.mapView addAnnotation:point];
    
}

- (void)showAnnotations:(NSArray<id<MKAnnotation>> *)annotations animated:(BOOL)animated {
   
   // NSLog(@"shoew");
}


- (void)action:(id)sender{
    
    if (_Segment.selectedSegmentIndex == 0) {

        _mapView.mapType =  MKMapTypeStandard;
    }
    else if (_Segment.selectedSegmentIndex == 1){
        
        _mapView.mapType = MKMapTypeHybrid;
        
    }
    else if (_Segment.selectedSegmentIndex ==2){
        
        _mapView.mapType = MKMapTypeSatellite;
        
    }
    
    
}
- (IBAction)shareBtncliked:(id)sender {
    JGActionSheetSection *section1 = [JGActionSheetSection sectionWithTitle:nil message:nil buttonTitles:@[@"Open in Maps", @"Open in Google Maps"] buttonStyle:JGActionSheetButtonStyleGreen];
    JGActionSheetSection *cancelSection = [JGActionSheetSection sectionWithTitle:nil message:nil buttonTitles:@[@"Cancel"] buttonStyle:JGActionSheetButtonStyleCancel];
    
    NSArray *sections = @[section1, cancelSection];
    
    JGActionSheet *sheet = [JGActionSheet actionSheetWithSections:sections];
    
    [sheet setButtonPressedBlock:^(JGActionSheet *sheet, NSIndexPath *indexPath) {
        
        if (indexPath.section ==0) {
            
            if (indexPath.row ==0) {
                
                NSString *latlog = [NSString stringWithFormat:@"%f,%f",self->coordinate.latitude,self->coordinate.longitude];
                NSString *url = [NSString stringWithFormat:@"http://maps.apple.com/?q=%@",latlog];
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
                
            }else if (indexPath.row ==1){
                
                NSString *latlog = [NSString stringWithFormat:@"%f,%f",self->coordinate.latitude,self->coordinate.longitude];
                if ([[UIApplication sharedApplication] canOpenURL:
                     [NSURL URLWithString:@"comgooglemaps://"]])
                {
                    NSString *url = [NSString stringWithFormat:@"comgooglemaps://?q=%@&center=%@",latlog,latlog];
                    
                    [[UIApplication sharedApplication] openURL:
                     [NSURL URLWithString:url]];
                    
                } else {
                    
                      [Helper showAlertWithTitle:@"Oops" Message:@"Google Maps App not found" viewController:self];
                }

            }
        }
        
        [sheet dismissAnimated:YES];
    }];
    
    [sheet showInView:[[UIApplication sharedApplication] keyWindow] animated:YES];
}

- (IBAction)backAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

@end
