//
//  CounterOfferViewController.swift
//  
//
//  Created by Rahul Sharma on 05/10/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import Kingfisher
import CoreLocation

class CounterOfferViewController: UIViewController {
    
    @IBOutlet weak var labelForProdNameNonNego: UILabel!
    @IBOutlet weak var labelForNonNegotiablePrice: UILabel!
    @IBOutlet weak var nonNegotiableOfferView: UIView!
    @IBOutlet weak var productImageOutlet: UIImageView!
    @IBOutlet weak var productNameOutlet: UILabel!

    @IBOutlet weak var newPriceTextFieldOutlet: UITextField!
    @IBOutlet weak var currencyLabelOutlet: UILabel!
    @IBOutlet weak var bottomConstraintOutlet: NSLayoutConstraint!
    
    var messageData : Message?
    var chatViewModelObj : ChatViewModel?
    var offerData : [String : Any]?
    var userName : String?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.createNavLeftButton()
        
        if let isNegotiable = chatViewModelObj?.isNegotiable {
            if(isNegotiable) {
                self.nonNegotiableOfferView.isHidden = true
                self.newPriceTextFieldOutlet.isHidden = false
                self.productNameOutlet.isHidden = false
                self.currencyLabelOutlet.isHidden = false
            }
            else {
                self.nonNegotiableOfferView.isHidden = false
                self.newPriceTextFieldOutlet.isHidden = true
                self.productNameOutlet.isHidden = true
                self.currencyLabelOutlet.isHidden = true
            }
        }
        
        if let productName = chatViewModelObj?.productName, let productImage = chatViewModelObj?.productImage, let price = messageData?.messagePayload, let currency = chatViewModelObj?.currency {
            self.currencyLabelOutlet.text = currency
            self.productNameOutlet.text = "Enter your counter offer for " +  productName
            self.labelForProdNameNonNego.text = productName
            self.productImageOutlet.kf.setImage(with: productImage)
            self.newPriceTextFieldOutlet.text = price
            self.labelForNonNegotiablePrice.text = currency + price
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(CounterOfferViewController.keyboardWillShow(withNotification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(CounterOfferViewController.keyboardWillHide(withNotification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    /**
     Create Navigation Bar Left Button method.
     */
     func createNavLeftButton() {
        self.navigationController?.navigationBar.isTranslucent = false;
        self.navigationController?.navigationBar.shadowImage = nil;
        let navButton = UIButton(frame: CGRect(x: 0, y: 0, width: 25, height: 40))
        navButton.rotateButton()
        let image = UIImage(named: "navigationBackButton")
        navButton.setImage(image, for: .normal)
        navButton.addTarget(self, action: #selector(backButtonClicked), for: .touchUpInside)
        
        let button1 = UIBarButtonItem.init(customView: navButton)
        self.navigationItem.leftBarButtonItem  = button1
    }
    
    func backButtonClicked() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func keyboardWillShow(withNotification notification: NSNotification) {
        let keyboardSize = (notification.userInfo![UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue.size
        self.bottomConstraintOutlet.constant = keyboardSize.height + CGFloat(20)
    }

    func keyboardWillHide(withNotification notification : NSNotification) {
        self.bottomConstraintOutlet.constant = 20
    }

    func getDistance(fromLat lat:Float, long:Float) {
        
//        let locationObj:GetCurrentLocation = GetCurrentLocation.sharedInstance()
//        let latLongs = locationObj.lastLatLong
//        let location1 = CLLocation(latitude: CLLocationDegrees(lat), longitude: CLLocationDegrees(long))
//        let location2 = CLLocation(latitude: latLongs.latitude, longitude: latLongs.longitude)

//        let distanceInMeters = location1.distance(from: location2) // result is in meters
//        let str = String(format: "%.2f", distanceInMeters/1000)
//        self.productDistanceOutlet.text = "\(str) km away"
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func counterOfferButtonAction(_ sender: Any) {
        if let newPrice = self.newPriceTextFieldOutlet.text, let msgData = messageData, let chatVMObj = chatViewModelObj {
            if newPrice.count>0 {
                msgData.messangerName = chatVMObj.chat.productName
                messageData?.messagePayload = self.newPriceTextFieldOutlet.text
                self.chatViewModelObj?.counterOffered(withMessageObj: msgData, andUserName: self.userName)
                self.navigationController?.popViewController(animated: true)
            } else {
                self.showAlert("Alert!", message: "There is no value for your new offer.")
            }
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
}
