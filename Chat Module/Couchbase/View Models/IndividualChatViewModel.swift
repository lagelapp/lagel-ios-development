//
//  IndividualChatViewModel.Swift
//  MQTT Chat Module
//
//  Created by Rahul Sharma on 01/09/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit


/// For individual chats document related methods.
/// ### Individual Chats Doument Structure: ###
///````
///1. receiverUidArray -> This will store all receiving users IDs in an Array.
///2. receiverDocIdArray -> This will store all receiving users Chat Document IDs in an Array.
///3. secretIdArray -> This will store all secret IDs in an Array.
///````

class IndividualChatViewModel: NSObject {
    
    
    /// static instance of the couchbase
    public let couchbase: Couchbase
    
    
    /// static instance of the MQTTChatManager
    let mqttChatManager = MQTTChatManager.sharedInstance
    
    /// Initializing with the couchbase Object
    init(couchbase: Couchbase) {
        self.couchbase = couchbase
    }
    
    /// For getting chat doc on respected to reciever ID and secret ID.
    /// - Returns: this will return the chat doc ID respected to reciever ID and secret ID.
    func fetchIndividualChatDoc(withRecieverID recieverIDG:String, andSecretID secretIDG : String) -> String? {
        let indexDocVMObject = IndexDocumentViewModel(couchbase: couchbase)
        guard let userID = self.getUserID() else { return nil }
        guard let indexID = indexDocVMObject.getIndexValue(withUserSignedIn: true) else { return  nil }
        guard let indexData = couchbase.getData(fromDocID: indexID) else { return  nil }
        guard let userIDArray = indexData["userIDArray"] as? [String] else { return  nil }
        if userIDArray.contains(userID) {
            guard let userDocArray = indexData["userDocIDArray"] as? [String] else { return nil }
            if let index = userIDArray.index(of: userID) {
                let userDocID = userDocArray[index]
                guard let userDocData = couchbase.getData(fromDocID: userDocID) else { return nil  }
                if let chatDocID = userDocData["chatDocument"] as? String {
                    guard let chatData = couchbase.getData(fromDocID: chatDocID) else { return nil  }
                    guard let recieverUIDArray = chatData["receiverUidArray"] as? [String] else { return nil  }
                    guard let scretIDArray = chatData["secretIdArray"] as? [String] else { return  nil }
                    guard let receiverChatDocIdArray = chatData["receiverDocIdArray"] as? [String] else { return nil  }
                    if !scretIDArray.isEmpty { //something is wrong here need to check logic
                        for index in 0 ..< recieverUIDArray.count {
                            let reciverIDL = recieverUIDArray[index]
                            if (recieverIDG == reciverIDL ) {
                                if (scretIDArray.count > index){
                                    if scretIDArray[index] == secretIDG {
                                        return receiverChatDocIdArray[index]
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return nil
    }
    
    /// For getting the user ID from User Default
    ///
    /// - Returns: optional value of user id in string
    fileprivate func getUserID() -> String? {
        guard let mqttID = Helper.getMQTTID() else { return nil }
        if mqttID == "mqttId" {
            return nil
        }
        return mqttID
    }
    
    
    /// Used for updating/creating individual docID with chatDociD, receiver ID and secret iD.
    /// - Returns: individual docID
    fileprivate func insertIndividualDocID(withDocID chatDocID : String,recieverID :String, andSecretID secretID : String) -> Bool {
        let indexDocVMObject = IndexDocumentViewModel(couchbase: couchbase)
        guard let userID = self.getUserID() else { return false }
        guard let indexID = indexDocVMObject.getIndexValue(withUserSignedIn: true) else { return  false }
        guard let indexData = couchbase.getData(fromDocID: indexID) else { return  false }
        guard let userIDArray = indexData["userIDArray"] as? [String] else { return  false }
        if userIDArray.contains(userID) {
            guard let userDocArray = indexData["userDocIDArray"] as? [String] else { return false }
            if let index = userIDArray.index(of: userID) {
                let userDocID = userDocArray[index]
                guard let userDocData = couchbase.getData(fromDocID: userDocID) else { return false  }
                if let individualChatDocID = userDocData["chatDocument"] as? String {
                    self.updateIndividualChatsDocument(withrecieverDocID: chatDocID, secretID: secretID, andRecieverUID: recieverID, intoChatsDocumentID: individualChatDocID)
                    return true
                }
            }
        }
        return false
    }
    
    /// This method is used for creating a document to maintain all the chats document
    /// - Returns: Object of CBLDocument / Document
    func createIndividualChatsDocument(withRecieverUIDArray recieverIDArray: [String], recieverDocIDArray : [String], secretIDArray : [String]) -> String? {
        
        let params = ["receiverUidArray":recieverIDArray,
                      "receiverDocIdArray":recieverDocIDArray,
                      "secretIdArray":secretIDArray] as [String:Any]
        let docID = couchbase.createDocument(withProperties: params)
        return docID
    }
    
    
    /// for deleting the document data by passing the chat doc ID.
    ///
    /// - Parameter chatDocId: chat doc ID you want to remove the data.
    func deleteDocIDData(fromChatDocID chatDocId : String?) {
        let indexDocVMObject = IndexDocumentViewModel(couchbase: couchbase)
        guard let userID = self.getUserID(), let chatDocId = chatDocId else { return  }
        guard let indexID = indexDocVMObject.getIndexValue(withUserSignedIn: true) else { return }
        guard let indexData = couchbase.getData(fromDocID: indexID) else { return }
        guard let userIDArray = indexData["userIDArray"] as? [String] else { return }
        if userIDArray.contains(userID) {
            guard let userDocArray = indexData["userDocIDArray"] as? [String] else { return }
            if let index = userIDArray.index(of: userID) {
                let userDocID = userDocArray[index]
                guard let userDocData = couchbase.getData(fromDocID: userDocID) else { return  }
                if let chatDocID = userDocData["chatDocument"] as? String {
                    guard let chatData = couchbase.getData(fromDocID: chatDocID) else { return  }
                    guard let receiverChatDocIdArray = chatData["receiverDocIdArray"] as? [String] else { return }
                    guard let scretIDArray = chatData["secretIdArray"] as? [String] else { return }
                    guard let recieverUIDArray = chatData["receiverUidArray"] as? [String] else { return }
                    if receiverChatDocIdArray.contains(chatDocId) {
                        if let index:Int = receiverChatDocIdArray.index(of: chatDocId) {
                            var chatDocIDarray = receiverChatDocIdArray
                            chatDocIDarray.remove(at: index)
                            var secretIDarray = scretIDArray
                            secretIDarray.remove(at: index)
                            
                            var recieverUIDarray = recieverUIDArray
                            recieverUIDarray.remove(at: index)
                            
                            var chatDocData = chatData
                            chatDocData["receiverDocIdArray"] = chatDocIDarray
                            chatDocData["secretIdArray"] = secretIDarray
                            chatDocData["receiverUidArray"] = recieverUIDarray
                            couchbase.updateData(data: chatDocData, toDocID: chatDocID)
                            couchbase.deleteDocument(withDocID: chatDocId)
                        }
                    }
                }
            }
        }
    }
    
    
    /// for getting chat docID with recieverID, secretID, contact object and message data
    ///
    /// - Parameters:
    ///   - recieverID: recieverID
    ///   - secretID: secretID
    ///   - chatObj: chatObj
    ///   - message: message
    /// - Returns: chatDoc ID
    func getChatDocID(withRecieverID recieverID:String, andSecretID secretID : String, withContactObj chatObj: Chat?, messageData message : [String : Any]?,withProductDetails productDetailsObj : ProductDetails?) -> String? {
        let chatsDocVMObject = ChatsDocumentViewModel(couchbase: couchbase)
        guard let userID = Helper.getMQTTID() else { return nil }
        if userID != "mqttId" { // If I gpt the chat doc id on depends upon the reciever and secret id.
            if let docID = fetchIndividualChatDoc(withRecieverID: recieverID, andSecretID: secretID) {
                return docID
            } else { // If there is data available but no chat doc created
                if let chatObj = chatObj {
                    guard let docID = chatsDocVMObject.createChatDoc(withRecieverName: chatObj.name,secretID: secretID, message : message, recieverImage: chatObj.image, selfDocID: "", selfUID: userID, withProductDetails: productDetailsObj) else { return nil }
                    
                    let isDataWrittenToDoc = self.insertIndividualDocID(withDocID: docID, recieverID: recieverID, andSecretID: secretID)
                    if isDataWrittenToDoc {
                        return docID
                    }else {
                        return nil
                    }
                } else { // Used for if there is no chat available.
                    if let msgObj = message {
                        guard let docID = chatsDocVMObject.createChatDoc(withRecieverName: "",secretID : secretID, message: msgObj, recieverImage: "", selfDocID: "", selfUID: userID, withProductDetails: productDetailsObj) else { return nil }
                        
                        let isDataWrittenToDoc = self.insertIndividualDocID(withDocID: docID, recieverID: recieverID, andSecretID: secretID)
                        if isDataWrittenToDoc {
                            return docID
                        }else {
                            return nil
                        }
                    } else {
                        guard let docID = chatsDocVMObject.createChatDoc(withRecieverName: "",secretID : secretID, message: nil, recieverImage: "", selfDocID: "", selfUID: userID, withProductDetails: productDetailsObj) else { return nil }
                        
                        let isDataWrittenToDoc = self.insertIndividualDocID(withDocID: docID, recieverID: recieverID, andSecretID: secretID)
                        if isDataWrittenToDoc {
                            return docID
                        }else {
                            return nil
                        }
                    }
                }
            }
        }
        return nil
    }
    
    
    /// Used for updating chats document with receiver ID, secret ID and chats docID
    func updateIndividualChatsDocument(withrecieverDocID recieverDocID : String, secretID :String, andRecieverUID recieverUID: String, intoChatsDocumentID chatsDocID : String ) {
        guard let documentData = couchbase.getData(fromDocID: chatsDocID) else {
            print("failed to get documet from provided DocID")
            return
        }
        var dic = documentData
        
        var docIDArray = dic["receiverDocIdArray"] as! [String]
        if docIDArray.contains("")
        {
            if let docIDindex = docIDArray.index(of: ""){
                docIDArray.remove(at: docIDindex)
            }
        }
        docIDArray.append(recieverDocID)
        dic["receiverDocIdArray"] = docIDArray
        
        var uidArray = dic["receiverUidArray"] as! [String]
        if uidArray.contains("")
        {
            if let docIDindex = uidArray.index(of: ""){
                uidArray.remove(at: docIDindex)
            }
        }
        uidArray.append(recieverUID)
        dic["receiverUidArray"] = uidArray
        
        var secretIdArray = dic["secretIdArray"] as! [String]
        if secretIdArray.contains("")
        {
            if let docIDindex = secretIdArray.index(of: ""){
                secretIdArray.remove(at: docIDindex)
            }
        }
        secretIdArray.append(secretID)
        dic["secretIdArray"] = secretIdArray
        
        couchbase.updateData(data: dic, toDocID: chatsDocID)
    }
    
    /// Used for getting the chat doc on the basis of the message object.
    /// - Returns: chat Document ID
    func getChatsDocID(fromMessage messageObj : [String:Any]) -> String? {
        
        var productID = ""
        var recieverID = ""
        var message = messageObj
        if let secretID = messageObj["secretId"] as? String {
            productID = secretID
        } else if let secretID = messageObj["productID"] as? String {
            productID = secretID
        }
        guard let fromID = messageObj["from"] as?  String else { return nil }
        guard let userID = self.getUserID() else { return nil }
        if fromID == userID {
            guard let toID = messageObj["to"] as?  String else { return nil }
            recieverID = toID
            message["to"] = toID
            message["from"] = fromID
        } else {
            recieverID = fromID
        }
        if recieverID == userID {
            return nil
        }
        
        guard let chatDocID = self.getChatDocID(withRecieverID: recieverID, andSecretID: productID, withContactObj: nil, messageData : message,withProductDetails: nil) else { return nil }
        return chatDocID
    }
    
    
    /// Used for getting the individual chatDocId
    ///
    /// - Returns: chat Document ID
    func getIndividualChatDocIDFromCouchbase() -> String? {
        let indexDocObject = IndexDocumentViewModel(couchbase: couchbase)
        guard let userID = self.getUserID() else { return nil }
        guard let indexID = indexDocObject.getIndexValue(withUserSignedIn: true) else { return nil }
        guard let indexData = couchbase.getData(fromDocID: indexID) else { return nil  }
        guard let userIDArray = indexData[AppConstants.indexDocumentConstants.userIDArray] as? [String] else { return nil  }
        if userIDArray.contains(userID) {
            guard let userDocArray = indexData[AppConstants.indexDocumentConstants.userDocIDArray] as? [String] else { return nil  }
            if let index = userIDArray.index(of: userID) {
                let userDocID = userDocArray[index]
                guard let userDocData = couchbase.getData(fromDocID: userDocID) else { return nil  }
                if let chatDocID = userDocData[AppConstants.indexDocumentConstants.chatDocument] as? String {
                    return chatDocID
                }
            }
        }
        return nil
    }
    
    /// For creating the individual chat doc with message object, product ID and receiver ID.
    /// - Returns: chat Document ID
    func createIndividualChatDoc(withMsgObj messageObj: [String : Any], productId : String, receiverID : String) -> String? { // create Chat id and also update data to individual chat.
        let chatsDocVMObject = ChatsDocumentViewModel(couchbase: couchbase)
        print(messageObj)
        guard let chatDocID = self.getChatDocID(withRecieverID: receiverID, andSecretID: productId, withContactObj: nil, messageData: messageObj, withProductDetails: nil) else {
            print("Unable to create chat Doc Please debug here")
            return nil
        }
        guard let chatData = couchbase.getData(fromDocID: chatDocID) else { return nil }
        chatsDocVMObject.updateChatData(withData: chatData, msgObject: messageObj, inDocID: chatDocID, updateChatMsgs: true)
        return chatDocID
    }
    
    
    /// updating individual chat document with message obejct, docid and is upadting chats also if it is chat coming initially as a boolean value
    ///
    /// - Parameters:
    ///   - msgObject: message Object.
    ///   - docID: documentID
    ///   - isUpdatingChats: is Updating Chats bool value
    ///   - isComingInitially: is Coming Initially boolean value
    func updateIndividualChatDoc(withMsgObj msgObject : Any,toDocID docID : String?, isUpdatingChats : Bool, isComingInitially : Bool) {
        let chatsDocVMObject = ChatsDocumentViewModel(couchbase: couchbase)
        guard let msgData = msgObject as? [String : Any] else { return }
        var secretID = ""
        if let docID = docID {
            guard let chatData = couchbase.getData(fromDocID: docID) else { return }
            if isUpdatingChats {
                chatsDocVMObject.updateChatDataFromChats(withData: chatData, msgObject : msgObject, inDocID  : docID, isComingInitially: isComingInitially)
            }
            else {
                chatsDocVMObject.updateChatData(withData: chatData, msgObject : msgObject, inDocID  : docID, updateChatMsgs: true)
            }
        } else {
            guard let chatDocID = self.getChatsDocID(fromMessage: msgObject as! [String : Any]) else { return }
            guard let chatDta = couchbase.getData(fromDocID: chatDocID) else { return }
            guard let individualChatsDocID = self.getIndividualChatDocIDFromCouchbase() else { return }
            guard let individualchatsData = couchbase.getData(fromDocID: individualChatsDocID) else { return }
            guard let recieverID = msgData["from"] as? String else { return }
            if let productID = msgData["secretId"] as? String {
                secretID = productID
            } else if let productID = msgData["productID"] as? String {
                secretID = productID
            }
            guard let reciverUIDArray = individualchatsData["receiverUidArray"] as? [String] else { return }
            guard let reciverDOCIDArray = individualchatsData["receiverDocIdArray"] as? [String] else { return }
            if reciverUIDArray.count>0 {
                if reciverDOCIDArray.contains(chatDocID) {
                    if isUpdatingChats { //for coming from chats
                        chatsDocVMObject.updateChatDataFromChats(withData: chatDta, msgObject : msgObject, inDocID  : chatDocID, isComingInitially: isComingInitially)
                    }
                    else { // for coming from messages
                        chatsDocVMObject.updateChatData(withData: chatDta, msgObject : msgObject, inDocID  : chatDocID, updateChatMsgs: true)
                    }
                }else {
                    //have to create a chat id and have to update in chats doc
                    guard let chatDocID = self.createIndividualChatDoc(withMsgObj: msgData, productId: secretID, receiverID: recieverID) else { return }
                    self.updateIndividualChatsDocument(withrecieverDocID: chatDocID, secretID: secretID, andRecieverUID: recieverID, intoChatsDocumentID: individualChatsDocID)
                }
            } else {
                //have to create a chat id and have to update in chats doc
                guard let chatDocID = self.createIndividualChatDoc(withMsgObj: msgData, productId: secretID, receiverID: recieverID) else { return }
                self.updateIndividualChatsDocument(withrecieverDocID: chatDocID, secretID: secretID, andRecieverUID: recieverID, intoChatsDocumentID: individualChatsDocID)
            }
        }
        let name = NSNotification.Name(rawValue: "ChatUpdatedNotification")
        NotificationCenter.default.post(name: name, object: self, userInfo: nil)
    }
    
    
    /// For deleting all the individual chats by using individual chat doc id.
    ///
    /// - Parameter individualDocID: individualDocID
    func deleteAllChats(fromIndividualID individualDocID: String) {
        
    }
}

