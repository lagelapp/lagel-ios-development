//
//  Couchbase.Swift
//  MQTT Chat Module
//
//  Created by Rahul Sharma on 21/07/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit

/// Used for getting the Couch DB Object
class Couchbase: NSObject {
    
    
    struct Constants {
        static let DBCreationError = "Database creation or opening failed"
        static let DocumentCreationError = "Failed to create the document"
    }
    
    /// singleton object of Couchbase DB with DB Name "MQTTChatDB"
    static let sharedInstance = Couchbase()
    let manager = CBLManager.sharedInstance()
    
    /// Get the current database object.
    var database : CBLDatabase! = nil
    
    override init() {
        do {
            self.database = try CBLManager.sharedInstance().databaseNamed(AppConstants.CouchbaseConstant.dbName)
        }
        catch let dbCreationError{
            print(Constants.DocumentCreationError,dbCreationError)
        }
    }
    
    /// Used for creating document with given properties and it returns the document ID
    ///
    /// - Parameter properties: document properties
    /// - Returns: document ID in optional format.
    func createDocument(withProperties properties: [String : Any]) -> String? {
        guard let document = database?.createDocument() else { return nil }
        do {
            try document.putProperties(properties)
            print(document.properties ?? "")
            return document.documentID
        }
        catch let couchBaseError{
            print(Constants.DBCreationError,couchBaseError)
            return document.documentID
        }
    }
    
    /// Update data into particaular document which can be accessed by the DocID passed along with data.
    ///
    /// - Parameters:
    ///   - data: Data to update into existing document.
    ///   - docID: current documents DocID.
    func updateData(data : [String : Any], toDocID docID: String) {
        guard let document = self.getDocumentObject(fromDocID: docID) else {
            print("failed to get documet from provided DocID")
            return
        }
        do{
            try document.putProperties(data)
        }
        catch let error{
            print("Data not saved \(error)")
        }
        print(document.properties ?? "")
    }
    
    /// It will delete the document by using the docID passed along with.
    ///
    /// - Parameter docID: current docID of the couchbase Document.
    func deleteDocument(withDocID docID : String) {
        guard let document = self.getDocumentObject(fromDocID: docID) else { return }
        do{
            try document.delete()
            print("deleted successfully")
            return
        }
        catch let error{
            print("unable to delete \(error)")
        }
    }
    
    /// To get the data object by using the document.
    ///
    /// - Parameter documentID: Current document ID
    /// - Returns: document
    fileprivate func getDocumentObject(fromDocID documentID : String) -> CBLDocument? {
        guard let document = database?.document(withID: documentID) else { return nil}
        return document
    }
    
    func getData(fromDocID docID : String) -> [String:Any]? {
        if docID != "" {
            let document = self.getDocumentObject(fromDocID: docID)!
            let messageArray = document.properties
            return messageArray
        } else {
            return nil
        }
    }
}
