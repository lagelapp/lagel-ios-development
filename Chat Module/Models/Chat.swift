//
//  Chat.swift
//  MQTT Chat Module
//
//  Created by Rahul Sharma on 22/07/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit


/// This class is used as the chat module
class Chat : NSObject {
    
    
    /// message array with all the message of chat
    var messageArray : [String]?
    
    /// if the new messages are there
    var hasNewMessage : Bool?
    
    /// New message string
    var newMessage : String?
    
    /// time of the new message
    var newMessageTime : String?
    
    /// New message date.
    var newMessageDateInString : String?
    
    /// Count of the new messages
    var newMessageCount : String?
    
    /// Last message sent date
    var lastMessageDate :String?
    
    /// Receiver Users ID array
    var recieverUIDArray : [String]?
    
    /// Receiver Doc ID array
    var recieverDocIDArray :  [String]?
    
    /// name of the sender
    var name : String?
    
    /// Image of the user
    var image : String?
    
    /// Secret Id of the Chat.
    var secretID : String?
    
    /// user ID of the chat
    var userID : String?
    
    /// Document ID for the Chat
    var docID : String?
    
    /// for invitation
    var wasInvited : Bool?
    
    /// Destruction time for the message.
    var destructionTime : Float?
    
    /// message date in date format.
    var msgDate : Date?
    
    /// if the secret invite visible.
    var isSecretInviteVisible : Bool?
    
    //newly added
    
    /// chat ID for the chat
    var chatID : String?
    
    /// initiated by bool flag, if it is initiated by the seller then it will return it true else false
    var initiated : Bool?
    
    /// Offer type in string
    var offerType : String?
    
    /// Product ID in string
    var productId : String?
    
    /// Product image in string
    var productImage : String?
    
    /// User image
    var userImage : String?
    
    /// Sender ID for the chat
    var senderId : String?
    
    /// recipient ID
    var recipientId : String?
    
    /// Product Name
    var productName : String?
    
    //product related
    var membername :String?
    
    /// Latitude of the product
    var latitude: Float?
    
    /// longitude of the product
    var longitude: Float?
    
    /// main url for the product image
    var mainUrl : String?
    
    /// Currency for the product price
    var currency: String?
    
    /// products Place
    var place : String?
    
    /// Profile pic url
    var profilePicUrl : String?
    
    /// Is product negotiable
    var negotiable : Bool?
    
    /// Price of the product.
    var price : Int?
    
    /// If accepted then the price will come here.
    var acceptedPrice : String?
    
    /// if product is sold then it will be set by here.
    var isProductSold : Bool
    
    /// Used for initializing the chat modal object with all the data passed with it.
    ///
    /// - Parameters:
    ///   - messageArray: array of messages
    ///   - hasNewMessage: Boolean flag for new messages.
    ///   - newMessage: newMessage in string format
    ///   - newMessageTime: newMessageTime in string
    ///   - newMessageDateInString: new Message Date In String
    ///   - newMessageCount: new Message Count in string
    ///   - lastMessageDate: last Message Date in string
    ///   - recieverUIDArray: reciever Users ID Array
    ///   - recieverDocIDArray: reciever Document ID Array
    ///   - name: name in string
    ///   - image: image in string
    ///   - secretID: secretID in string
    ///   - userID: userID in string
    ///   - docID: docId in string
    ///   - wasInvited: was invited bool value
    ///   - destructionTime: destruction time in string
    ///   - isSecretInviteVisible: is Secret Invite Visible boolean value
    ///   - chatID: current chatID string
    ///   - initiated: initiated in bool
    ///   - initiated: chat initiated by
    ///   - productId: productId/SecretID of the chat.
    ///   - productImage: productImage string
    ///   - userImage: userImage string
    ///   - senderId: senderId string
    ///   - recipientId: recipientId string
    ///   - productName: productName string
    ///   - membername: membername string
    ///   - currency: currency string
    ///   - latitude: latitude string
    ///   - longitude: longitude string
    ///   - mainUrl: mainUrl string
    ///   - place: place of the product
    ///   - profilePicUrl: profilePicUrl of the user.
    ///   - negotiable: if the product is negotiable
    ///   - price: product price
    ///   - acceptedPrice: products acceptedPrice
    ///   - bitcoinPrice : bitcoin price
    ///   - isProductSold : For product is sold or not.
    
    init(messageArray : [String]?,hasNewMessage : Bool?, newMessage : String?, newMessageTime : String?, newMessageDateInString : String?,newMessageCount : String?, lastMessageDate : String?,recieverUIDArray : [String]?,recieverDocIDArray : [String]?, name : String?, image : String?, secretID : String?, userID : String?,docID : String?,wasInvited : Bool?,destructionTime : Float?,isSecretInviteVisible : Bool?,chatID : String?, initiated : Bool?, offerType : String?, productId : String?, productImage : String?, userImage : String?, senderId : String?, recipientId : String?, productName : String?, membername :String?, currency: String?, latitude: Float?, longitude: Float?, mainUrl : String?, place : String?, profilePicUrl : String?, negotiable : Bool?, price : Int?, acceptedPrice : String?, isProductSold : Bool) {
        self.messageArray = messageArray
        self.hasNewMessage = hasNewMessage
        self.newMessage = newMessage
        self.newMessageTime = newMessageTime
        self.newMessageDateInString = newMessageDateInString
        self.newMessageCount = newMessageCount
        self.lastMessageDate = lastMessageDate
        self.recieverUIDArray = recieverUIDArray
        self.recieverDocIDArray = recieverDocIDArray
        self.name = name
        self.image = image
        self.secretID = secretID
        self.userID = userID
        self.docID = docID
        self.wasInvited = wasInvited
        self.destructionTime = destructionTime
        self.isSecretInviteVisible = isSecretInviteVisible
        self.chatID = chatID
        self.initiated = initiated
        self.offerType = offerType
        self.productId = productId
        self.productImage = productImage
        self.userImage = userImage
        self.senderId = senderId
        self.recipientId = recipientId
        self.productName = productName
        self.membername = membername
        self.latitude = latitude
        self.longitude = longitude
        self.mainUrl = mainUrl
        self.currency = currency
        self.place = place
        self.profilePicUrl = profilePicUrl
        self.negotiable = negotiable
        self.price = price
        self.acceptedPrice = acceptedPrice
        self.isProductSold = isProductSold
    }
}

