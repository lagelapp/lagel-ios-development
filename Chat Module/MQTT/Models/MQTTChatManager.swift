//
//  MQTTChatManager.swift
//  MQTT Chat Module
//
//  Created by Rahul Sharma on 30/08/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import MQTTClient
import UserNotifications

class MQTTChatManager: NSObject {
    
    struct Constants {
        fileprivate static let timeInterval:TimeInterval = 1
        
        fileprivate static let payloadKey = "payload"
        fileprivate static let nameKey = "name"
        fileprivate static let messageDataKey = "messageData"
        fileprivate static let channelNameKey = "channelName"
        fileprivate static let messageIDKey = "messageID"
        
        fileprivate static let unsentMessagesDocumentKey = "unsentMessagesDocument"
        fileprivate static let unsentMessagesArrayKey = "unsentMessagesArray"
        
        fileprivate static let mqttLocalNotification = "MQTTLocalNotification"
    }
    
    let mqttModel = MQTT.sharedInstance
    let couchbaseObj = Couchbase.sharedInstance
    
    static let sharedInstance = MQTTChatManager()
    
    /// Used only for sending the acknowladgment to the other user about the message delivery.
    ///
    /// - Parameters:
    ///   - channel: Acknowledgement + user ID whom you wanted tosend the acknowladgment
    ///   - message: Current message with acknowladgment format (check Docs).
    ///   - delivering: Delivering with current QOS. // i.e. 2
    func sendAcknowledgment(toChannel channel:String?, withMessage message :[String:Any], withQOS delivering : MQTTQosLevel ) {
        guard let channel = channel, !channel.isEmpty
            else { return }
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: message, options: .prettyPrinted)
            mqttModel.publishData(wthData: jsonData, onTopic: channel, retain: false, withDelivering: delivering)
        }
        catch let error{
            print(error.localizedDescription)
        }
    }
    
    /// Used only for sending the message to the other user about the message delivery.
    ///
    /// - Parameters:
    ///   - channel: Message + user ID whom you wanted tosend the message
    ///   - message: Current message with message format (check Docs).
    ///   - delivering: Delivering with current QOS. //i.e. 1
    func sendMessage(toChannel channel:String?, withMessage message :[String:Any], withQOS delivering : MQTTQosLevel ) {
        
        guard let channel = channel, !channel.isEmpty else { return }
        let msgChannel = "\(AppConstants.MQTT.messagesTopicName)\(channel)"
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: message, options: .prettyPrinted)
            mqttModel.publishData(wthData: jsonData, onTopic: msgChannel, retain: false, withDelivering: delivering)
            self.sendMessageThroughPushNotification(withMessageData: message, toTopic: channel)
        }
        catch  {
            print(error.localizedDescription)
        }
    }
    
    func sendMessageThroughPushNotification(withMessageData msgData: [String:Any], toTopic topic : String) {
        guard let type = msgData["type"] as? String else { return }
        guard let messageTypeObj:MessageTypes = MessageTypes(rawValue: Int(type)!) else { return }
        guard let msgText = (msgData["payload"] as? String)?.fromBase64() else { return }
        guard let userName = Helper.userName() else { return }
        
        guard let receiverID = Helper.getMQTTID(), let secretID = msgData["secretId"] as? String else { return }
        
        let data:[String : String] = ["receiverID":receiverID,
                                      "secretID":secretID]
        
        switch messageTypeObj {
        case .text:
            API().sendNotification(withText: msgText, andTitle: Helper.userName(), toTopic: topic, andData: data)
        case .location:
            API().sendNotification(withText: " ", andTitle: "\(userName) has shared the location with you.", toTopic: topic, andData: data)
        case .image:
            API().sendNotification(withText: " ", andTitle: "\(userName) has sent you an image.", toTopic: topic, andData: data)
        case .payment:
            API().sendNotification(withText: " ", andTitle: "\(userName) has shared the payment link with you.", toTopic: topic, andData: data)
        default:
            API().sendNotification(withText: "\(userName)", andTitle: "has sent you a message.", toTopic: topic, andData: data)
        }
    }
    
    func publishUnsentMessage(withData jsonData : Data, in channel : String, withMsgID msgID: UInt16, delivering: MQTTQosLevel, withunsentDocID unsentDocID :String) {
        mqttModel.publishData(wthData: jsonData, onTopic: channel, retain: false, withDelivering: delivering)
    }
    
    func sendUnsentMessages() {
        let usersDocVMObject = UsersDocumentViewModel(couchbase: couchbaseObj)
        guard let userDocID = usersDocVMObject.getCurrentUserDocID() else { return }
        guard let userDocData = couchbaseObj.getData(fromDocID: userDocID) else { return }
        guard let unsentDocID = userDocData[Constants.unsentMessagesDocumentKey]  as? String else { return }
        guard let unsentMessagesDocumentData = couchbaseObj.getData(fromDocID: unsentDocID) else { return }
        guard let unsentMessagesArray = unsentMessagesDocumentData[Constants.unsentMessagesArrayKey] as? [Any] else {return }
        self.sendUnsentMsgsAgainAfterConnection(withMsgArray: unsentMessagesArray, withUnsentMsgsDocID: unsentDocID)
    }
    
    
    func sendUnsentMsgsAgainAfterConnection(withMsgArray msgArray : [Any], withUnsentMsgsDocID unsentMsgDocID : String) {
        for msg in msgArray {
            guard let message = msg as? [String : Any] else { return }
            guard let jsonData = message[Constants.messageDataKey] as? String, let channelName = message[Constants.channelNameKey] as? String,  let msgID = message[Constants.messageIDKey] as? UInt16 else { return }
            if let data = Data(base64Encoded: jsonData) {
                self.publishUnsentMessage(withData: data, in: channelName, withMsgID: msgID, delivering: .atLeastOnce, withunsentDocID: unsentMsgDocID)
            }
        }
    }
    
    func getNewAcknowladgment(withData data: [String: Any],in topic: String) {
        if topic.range(of:AppConstants.MQTT.acknowledgementTopicName) != nil {
            guard let deliveryStatus = data["status"] as? String else { return }
            let chatdocVMObject = ChatsDocumentViewModel(couchbase: couchbaseObj)
            if deliveryStatus == "2" {
                chatdocVMObject.updateDocumentForMessageDelivered(withMessageData: data)
            } else if deliveryStatus == "3" {
                chatdocVMObject.updateDocumentForMessageRead(withMessageData: data)
            }
        }
    }
    
    func gotOnlineStatus(withData data: [String:Any], withTopic topic : String) {
        let name = NSNotification.Name(rawValue: "LastSeen")
        NotificationCenter.default.post(name: name, object: self, userInfo: ["message": data, "topic": topic])
    }
    
    func gotTypingStatus(withData data: [String : Any], withTopic topic : String) {
        if let selfID = self.getUserID() {
            if topic.range(of:selfID) != nil {
                let name = NSNotification.Name(rawValue: "Typing")
                NotificationCenter.default.post(name: name, object: self, userInfo: ["message": data, "topic": topic])
            }
        }
    }
    
    func getNewMessage(withData data: [String: Any],in topic: String) {
        guard let toID = data["to"] as? String else { return }
        let chatdocVMObject = ChatsDocumentViewModel(couchbase: couchbaseObj)
        if topic.range(of:AppConstants.MQTT.messagesTopicName) != nil {
            guard let selfID = self.getUserID() else { return }
            if selfID == toID { // This message ment for the current user, save it as a received message.
                chatdocVMObject.updateDocumentForMessageReceived(withMessageData: data, atTopic: topic, isComingInitially: false)
            }
        }
    }
    
    func gotInitiatedChat(withData data : [String : Any], inTopic topic : String) {
        let chatdocVMObject = ChatsDocumentViewModel(couchbase: couchbaseObj)
        if topic.range(of:AppConstants.MQTT.messageOffer) != nil {
            chatdocVMObject.updateDocumentForInitiatedChatReceived(withChatData: data, atTopic: topic, isComingInitially: true)
        }
    }
    
    func gotMessages(withData data : [String : Any], inTopic topic : String) {
        let chatdocVMObject = ChatsDocumentViewModel(couchbase: couchbaseObj)
        if topic.range(of:AppConstants.MQTT.getMessages) != nil {
            if let chatID = data["chatId"] as? String, let secretID = data["secretId"] as? String, let receiverID = data["opponentUid"] as? String,let messages = data["messages"] as? [[String : Any]] {
                if( secretID.count>0 && receiverID.count>0 ) {
                    guard let selfID = self.getUserID() else { return }
                    var oponentID = receiverID
                    if receiverID == selfID {
                        guard let opntID = self.getOponentID(withMessages: messages) else { return }
                        oponentID = opntID
                    }
                    chatdocVMObject.updateMessages(withMessage: messages, byChatID: chatID, receiverID: oponentID, secretId: secretID)
                    let name = NSNotification.Name(rawValue: "PullToRefresh")
                    NotificationCenter.default.post(name: name, object: self, userInfo: ["chatId": chatID, "secretId": secretID])
                }
            }
        }
    }
    
    fileprivate func getOponentID(withMessages messaegs: [[String:Any]]) -> String? {
        if let message = messaegs.first {
            guard let receiverId = message["receiverId"] as? String, let senderID = message["senderId"] as? String else { return nil }
            guard let selfID = self.getUserID() else { return nil }
            if receiverId == selfID {
                return senderID
            } else {
                return receiverId
            }
        }
        return nil
    }
    
    func gotChats(withData data : [String:Any], inTopic topic : String) {
        let chatdocVMObject = ChatsDocumentViewModel(couchbase: couchbaseObj)
        if let selfID = self.getUserID() {
            if topic.range(of:selfID) != nil {
                chatdocVMObject.updateDocumentForChatRecieved(withChatData: data, atTopic: topic, isComingInitially: true)
            }
        }
    }
    
    fileprivate func getUserID() -> String? {
        guard let mqttID = Helper.getMQTTID() else { return nil }
        if mqttID == "mqttId" {
            return nil
        }
        return mqttID
    }
    
    func sendOnlineStatus(withOfflineStatus status : Bool) { //true for offline and false for online
        guard let selfID = self.getUserID() else { return }
        let lastSeenTopic = AppConstants.MQTT.onlineStatus+selfID
        var params = [String : Any]()
        guard let timeStamp = DateExtension().sendTimeStamp(fromDate:Date()) else { return }
        if !status { // for online
            params = ["status":1,
                      "userId":selfID,
                      "lastSeenEnabled":true]
        }
        else { // for offline
            params = ["status":0,
                      "userId":selfID,
                      "timestamp":timeStamp,
                      "lastSeenEnabled":true]
        }
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: params, options: .prettyPrinted)
            mqttModel.publishData(wthData: jsonData, onTopic: lastSeenTopic, retain: true, withDelivering: .atMostOnce)
        }
        catch  {
            print(error.localizedDescription)
        }
    }
    
    func subscribeTypingChannel(withUserID userID : String) {
        let typingTopic = AppConstants.MQTT.typing+userID
        mqttModel.subscribeTopic(withTopicName: typingTopic, withDelivering: .atMostOnce)
    }
    
    func unsubscribeTypingChannel(withUserID userID : String) {
        let typingTopic = AppConstants.MQTT.typing+userID
        mqttModel.unsubscribeTopic(topic: typingTopic)
    }
    
    func subscibeToLastSeenChannel(withUserID userID : String) {
        let lastSeenTopic = AppConstants.MQTT.onlineStatus+userID
        mqttModel.subscribeTopic(withTopicName: lastSeenTopic, withDelivering: .atMostOnce)
    }
    
    func unsubscibeToLastSeenChannel(withUserID userID : String) {
        guard let subscribeDict = mqttModel.manager.subscriptions else { return }
        let keys = subscribeDict.keys
        for key in keys {
            if key.hasPrefix("\(AppConstants.MQTT.onlineStatus)") {
                mqttModel.unsubscribeTopic(topic: key)
            }
        }
    }
    
    func subscribeGetChatTopic(withUserID userID : String) {
        let getChatsTopic = AppConstants.MQTT.getChats+userID
        mqttModel.subscribeTopic(withTopicName: getChatsTopic, withDelivering: .atLeastOnce)
    }
    
    func unsubscribeAllTopics() {
        if((mqttModel.manager) != nil)
        {
        mqttModel.manager.subscriptions = nil
        mqttModel.manager.connectToLast()
        }
    }
    
    func subscribeToGetMessageTopic(withUserID userID : String) {
        let getMessagesTopic = AppConstants.MQTT.getMessages+userID
        mqttModel.subscribeTopic(withTopicName: getMessagesTopic, withDelivering: .atLeastOnce)
    }
    
    func sendTyping(toUser userID : String, andProductID secretID: String) {
        let typingTopic = AppConstants.MQTT.typing+userID
        guard let selfID = self.getUserID() else { return }
        
        let params = ["from" : selfID,
                      "to": userID,
                      "secretId" : secretID]
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: params, options: .prettyPrinted)
            mqttModel.publishData(wthData: jsonData, onTopic: typingTopic, retain: false, withDelivering: .atMostOnce)
        }
        catch  {
            print(error.localizedDescription)
        }
    }
}

struct Platform {
    static var isSimulator: Bool {
        return TARGET_OS_SIMULATOR != 0
    }
}
