//
//  ArrayExtension.swift
//  
//
//  Created by Rahul Sharma on 04/10/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation

extension Array
{
    func containsObject(object: Any) -> Bool
    {
        if let anObject = object as? [String : Any]
        {
            for obj in self
            {
                if let anObj = obj as? [String : Any]
                {
                    if (anObj["timestamp"] as? String) == (anObject["timestamp"] as? String) { return true }
                }
            }
        }
        return false
    }
}
