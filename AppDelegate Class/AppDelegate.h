//
//  AppDelegate.h

//
//  Created by Rahul Sharma on 09/02/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import <GoogleSignIn/GoogleSignIn.h>
#import <UIKit/UIKit.h>
#import <FBSDKLoginKit/FBSDKLoginManagerLoginResult.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginButton.h>
#import <UserNotifications/UserNotifications.h>


@interface AppDelegate : UIResponder <UIApplicationDelegate,UNUserNotificationCenterDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (nonatomic, strong) UIView *internetConnectionErrorView;
@property(nonatomic,assign)Float32 lat;
@property(nonatomic,assign)Float32 log;
@property (strong, nonatomic) NSString *userPhoneCode;
@property (strong, nonatomic) NSString *counryCode;

- (void)setStatusBarBackgroundColor:(UIColor *)color;
- (void)setUserPhoneCode:(NSString *)code countrycode:(NSString *)countryCode;





@end

